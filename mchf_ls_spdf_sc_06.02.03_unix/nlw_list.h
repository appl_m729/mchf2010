#ifndef _NLW_LIST_H_
#define _NLW_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class nlw_Conf;


class nlw_DataBlock : private Uncopyable {
public:

    nlw_Conf      *one_nlw;
    nlw_DataBlock *next;

    nlw_DataBlock();
    ~nlw_DataBlock();
};


class nlw_Conf_List : private Uncopyable {
public:

    nlw_Conf_List();
    ~nlw_Conf_List();
 
    const nlw_Conf* crnt_pnlw() const;
    void to_head();
    bool next(); 
    int  size() const;
    void add(nlw_Conf * const One_nlw);
    void clear();

private:

    nlw_DataBlock * const _phead;
    nlw_DataBlock *_crnt_item;
    nlw_DataBlock *_ptail;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif // _NLW_LIST_H_

// end of file

