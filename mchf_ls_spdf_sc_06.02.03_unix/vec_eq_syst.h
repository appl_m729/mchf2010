#ifndef _VECTOR_EQUATIONS_SYSTEM_H_
#define _VECTOR_EQUATIONS_SYSTEM_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Eta_rbTree;
class ksiHash_List;
class etaHash_List;
class Pnl_IndexMap;
class qBas_Conf;
class Radial_Pnl_Data;
class HamiltonianMtrx;
class Pnl_Buffer;
class VecEq_Conf;
class eta_Index;
class dqIter_Conf;
class mDIIS_Engine;


class VecEq_Syst : private Uncopyable {

public:

    VecEq_Syst(const Eta_rbTree * const etaTree,
               const ksiHash_List * const arr_ksihl,
               const etaHash_List * const arr_etahl,
               const Pnl_IndexMap * const pnlIndex,
               const qBas_Conf * const basConf,
               const dqIter_Conf * const dqIterCnf);
    ~VecEq_Syst();

    void recalc(const Radial_Pnl_Data * const radialPnl,
                const HamiltonianMtrx * const hamMtrx);

    double b(const int a, const int i) const;
    double lam(const int a, const int b) const;

private:

    const Eta_rbTree   * const _etaTree;
    const ksiHash_List * const _hash_ksi;
    const etaHash_List * const _hash_eta;
    const Pnl_IndexMap * const _pnlInd;
    const qBas_Conf    * const _qBasCnf;
    const dqIter_Conf  * const _dqIterCnf;
    const VecEq_Conf   * const _veqCnf;
    eta_Index          * const _etaInd;
    mDIIS_Engine       * const _mDiisEngn;
    double             *_A, *_x, *_lam, *_x_old, *_dx;
    double             *_tmpA, *_tmpV, *_tmpB;
    double             *_q, *_dq, *_dq_old, *_dq_err, *_sca;

    void   _Solve_VecEqs(const Radial_Pnl_Data * const radialPnl);

    void   _set_A_in_zero();
    void   _setup_first_appr_x();
    void   _zero_every_q();
    void   _calc_every_q();
    void   _calc_every_dq();
    void   _print_norm_every_dq() const;
    bool   _every_dq_is_small();
    bool   _any_dq_is_freeze();
    void   _iterate_every_x_by_dq(const Radial_Pnl_Data * const radialPnl);
    double f(const double x) const;
    void   _normalize_every_x();
    void   _save_every_x_in_x_old();
    void   _calc_every_dx();
    void   _calc_lams();
    double _lambda(const int a, const int b) const;
    double _xAx(const int ax, const int aA, const int bx) const;
    void   _print_lams() const;
    void   _print_orthogonality() const;

    double _sca_pr(const double *y1, const double *y2, const int sz) const;
    double _norm(const double * const x, const int sz) const;
    int    _max_ind(const double * const x, const int sz) const;
    void   _normalize(double * x, const int sz);

    void   _scatter_diag_ksi(const int a,
                             const Radial_Pnl_Data * const radialP,
                             const HamiltonianMtrx * const hamMtrx);

    void   _scatter_diag_eta(const int a,
                             const Radial_Pnl_Data * const radialP,
                             const HamiltonianMtrx * const hamMtrx);

    void   _scatter_off_diag_ksi(const int a, const int b,
                                 const Radial_Pnl_Data * const radialP,
                                 const HamiltonianMtrx * const hamMtrx);

    void   _scatter_off_diag_eta(const int a, const int b,
                                 const Radial_Pnl_Data * const radialP,
                                 const HamiltonianMtrx * const hamMtrx);
};

////////////////////////////////////////////////////////////////////////////

#endif // _VECTOR_EQUATIONS_SYSTEM_H_

// end of file

