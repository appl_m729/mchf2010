#include <iostream>
#include <cstdlib>
#include "eta_hash.h"
#include "k_ab_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

etaHash_DataBlock::etaHash_DataBlock():
next(0), l1(-1), l2(-1), diag_factor(0),
kabInd(0), ccvLst(0) {
}

etaHash_DataBlock::~etaHash_DataBlock() {

    ccvLst = 0;
    kabInd = 0;
    delete next; next = 0;
}

////////////////////////////////////////////////////////////////////////////

etaHash_List::etaHash_List(): _phead(new etaHash_DataBlock),
_crnt_item(_phead), _size(0) {
}

etaHash_List::~etaHash_List() {

    _crnt_item = 0;
    delete _phead;
}

void etaHash_List::clear() {

    if (_phead->next) {
        delete _phead->next; _phead->next = 0;
    }
    _crnt_item = _phead;
    _size = 0;
}

void etaHash_List::add(const ccv_List * ext_ccvLst, const int ext_l1,
                       const int ext_l2, const k_ab_Index& ext_kabInd) {

    _crnt_item = _phead;
    while (_crnt_item->next) {

        _crnt_item = _crnt_item->next;

        if ( (ext_l1 == _crnt_item->l1) && (ext_l2 == _crnt_item->l2) &&
             (ext_kabInd == *(_crnt_item->kabInd)) ) {

            if (ext_ccvLst != _crnt_item->ccvLst) {

                cout << "Error in etaHash_List::add." << endl;
                cout << "Incorrect identical adding.." << endl;
                exit(0);
            }

            _crnt_item->diag_factor++;

            if (4 < _crnt_item->diag_factor) {
                cout << "Error in etaHash_List::add; diag_factor > 4" << endl;
                exit(0);
            }

            return;
        }
    }

    _crnt_item->next = new etaHash_DataBlock;
    _crnt_item = _crnt_item->next;

    _crnt_item->l1 = ext_l1;
    _crnt_item->l2 = ext_l2;
    _crnt_item->diag_factor = 1;
    _crnt_item->ccvLst = ext_ccvLst;
    _crnt_item->kabInd = new k_ab_Index(ext_kabInd);

    _size++;
}

void etaHash_List::to_head() const {
    _crnt_item = _phead;
}

bool etaHash_List::next() const {

    if (_crnt_item->next) {
        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int etaHash_List::diag_factor() const {
    return _crnt_item->diag_factor;
}

const ccv_List& etaHash_List::ccvLst() const {
    return *(_crnt_item->ccvLst);
}

int etaHash_List::l1() const {
    return _crnt_item->l1;
}

int etaHash_List::l2() const {
    return _crnt_item->l2;
}

const k_ab_Index& etaHash_List::kabInd() const {
    return *(_crnt_item->kabInd);
}

////////////////////////////////////////////////////////////////////////////

// end of file

