#include <iostream>
#include "qua3_index.h"
#include "qua3_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

qua3_DataBlock::qua3_DataBlock():
count(0), index(0), next(0) {
//    cout << "  qua3_DataBlock constructor." << endl;
}

qua3_DataBlock::~qua3_DataBlock() {

//    cout << "  qua3_DataBlock destructor." << endl;
    delete index; index = 0;
    delete next;  next  = 0;
}

////////////////////////////////////////////////////////////////////////////

qua3_List::qua3_List():
_phead(new qua3_DataBlock), 
_crnt_item(_phead), _size(0), _count(0) {
//    cout << "qua3_List constructor." << endl;
}

qua3_List::~qua3_List() {
//    cout << "qua3_List destructor." << endl;
    _crnt_item = 0;
    delete _phead;
}

const qua3_Index* qua3_List::crnt_index() const {
    return _crnt_item->index;
}

int qua3_List::crnt_count() const {
    return _crnt_item->count;
}

void qua3_List::to_head() {
    _crnt_item = _phead;
}

bool qua3_List::next() {

    if (_crnt_item->next) {
        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

void qua3_List::add(const qua3_Index& qua3Ind) {

    _crnt_item = _phead;
    while (_crnt_item->next) {

        _crnt_item = _crnt_item->next;

        if (qua3Ind == (*_crnt_item->index)) {

            _count++;
            _crnt_item->count++;
            return;
        }
    }

    _crnt_item->next = new qua3_DataBlock;
    _crnt_item = _crnt_item->next;
    _crnt_item->index = new qua3_Index(qua3Ind);
    _crnt_item->count++;

    _count++;
    _size++;
}

void qua3_List::clear() {

    if (_phead->next) {
        delete _phead->next; _phead->next = 0;
    }
    _crnt_item = _phead;

    _size  = 0;
    _count = 0;
}

////////////////////////////////////////////////////////////////////////////

// end of file

