#include <iostream>
#include <cmath>
#include "det_state.h"
#include "dets_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Det_DataBlock::Det_DataBlock():
one_Det(0), next(0) {
//    cout << "Det_DataList constructor..." << endl;
}

Det_DataBlock::~Det_DataBlock() {

    delete one_Det; one_Det = 0;
    delete next;    next    = 0;
//    cout << "Det_DataList destructor..." << endl;
}

////////////////////////////////////////////////////////////////////////////

DetsList::DetsList():
_phead(new Det_DataBlock), _crnt_item_1(_phead),
_crnt_item_2(0), _ptail(_phead), _size(0) {
//    cout << "DetsList constructor" << endl;
}

DetsList::~DetsList() {

    _crnt_item_1 = 0;
    _crnt_item_2 = 0;
    _ptail       = 0;

    delete _phead;
//    cout << "DetsList destructor" << endl;
}

const Det_State* DetsList::crnt_pDet_1() const {
    return _crnt_item_1->one_Det;
}

const Det_State* DetsList::crnt_pDet_2() const {
    return _crnt_item_2->one_Det;
}

void DetsList::to_head_1() const {
    _crnt_item_1 = _phead;
}

void DetsList::to_head_2_sg() const {
    _crnt_item_2 = _crnt_item_1;
}

bool DetsList::next_1() const {

    if (_crnt_item_1->next) {
        _crnt_item_1 = _crnt_item_1->next;
        return true;
    }
    return false;
}

bool DetsList::next_2() const {

    if (_crnt_item_2->next) {
        _crnt_item_2 = _crnt_item_2->next;
        return true;
    }
    return false;
}

int DetsList::size() const {
    return _size;
}

bool DetsList::is_empty() const {

    if (0 == _size) return true;
    return false;
}

const Det_State& DetsList::firstDet() const {
    return *(_phead->next->one_Det);
}

void DetsList::add(const Det_State& extDet) {

    _ptail->next = new Det_DataBlock;
    _ptail = _ptail->next;
    _ptail->one_Det = new Det_State(extDet);

    _size++;
}

void DetsList::clear() {

    if (_phead->next) {
        delete _phead->next; _phead->next = 0;
    }

    _crnt_item_1 = _phead;
    _crnt_item_2 = 0;
    _ptail       = _phead;

    _size = 0;
}

bool DetsList::find(int &d_pos, const Det_State& extDet) const {

    const Det_DataBlock * t_item = _phead;

    d_pos = 0;
    while (t_item->next) {

        t_item = t_item->next;        
        if ( extDet == (*(t_item->one_Det)) ) return true;
        ++d_pos;
    }

    d_pos = -1;
    return false;
}

////////////////////////////////////////////////////////////////////////////

// end of file


