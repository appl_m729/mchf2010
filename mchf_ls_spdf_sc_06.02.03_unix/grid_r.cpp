#include <iostream>
#include <cmath>
#include <cstdlib>
#include "grid_r.h"
#include "read.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

grid_r::grid_r(const char file[]):
_N(0), _t32(0), _data_r(0), _data_h(0), _r_max(0) {

    if (ReadInt(file,"grid",0,_N)) {

        cout << "Error reading N grid" << endl;
        exit(0);
    }

    if (ReadReal(file,"grid",1,_r_max)) {

        cout << "Error reading r_max grid" << endl;
        exit(0);
    }

    if (ReadReal(file,"grid",2,_t32)) {

        cout << "no t32" << endl;
        exit(0);
    }

    if ( (_N < 0) || (_r_max < 0.0) || (_t32 < 0.0)) {

        cout << "Error in grid_1d:" << endl;
        print();
        exit(0);
    }

    _setup();
}

grid_r::~grid_r() {

    delete[] _data_r; _data_r=0;
    delete[] _data_h; _data_h=0;
}

double grid_r::r(const int i) const {
    return _data_r[2*i];
}

double grid_r::r(const int i,const int key) const {
    return _data_r[2*i+key];
}

double grid_r::r2(const int i2) const {
    return _data_r[i2];
}

double grid_r::h(const int i) const {
    return _data_h[i];
}

int grid_r::N() const {
    return _N;
}

double grid_r::r_max() const {
    return _r_max;
}

void grid_r::print() const {

    cout << "grid_r:    N = " << _N << "  r_max = " << _r_max;
    cout << "  t32 = " << _t32 << endl;
}

void grid_r::_setup() {

    _data_r = new double[2*_N + 1];
    _data_h = new double[_N];

    {for (int i = 0; i <= _N; ++i) _data_r[2*i] = _r_max*exp((i-_N)/_t32);}

    for (int i = 0; i < _N; ++i) {

        _data_h[i] = (_data_r[2*(i+1)] - _data_r[2*i])/2.0;
        _data_r[2*i+1] = _data_r[2*i] + _data_h[i];
    }
    _data_r[2*_N] = _r_max;
}

////////////////////////////////////////////////////////////////////////////

// end of file
