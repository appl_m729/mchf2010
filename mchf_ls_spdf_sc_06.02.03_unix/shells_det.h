#ifndef _SHELLS_DET_H_
#define _SHELLS_DET_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Det_State;
class nlmlms_State;
class Atom_Conf;
class q4x_Convertor;


class Shells_Det : private Uncopyable {

public:

    Shells_Det(const Atom_Conf * const aCnf);
    ~Shells_Det();

    void reset(const Det_State& ext_det);

    void dislodge(const int k);

    int length() const {return _length;}
    const nlmlms_State& elst(const int i) const;

private:

    int _core_len, _length;
    int _k_disl;
    nlmlms_State *_elst;

    void _setup_elst(const Atom_Conf * const aCnf);
};

////////////////////////////////////////////////////////////////////////////

#endif  // _SHELLS_DET_H_

// end of file

