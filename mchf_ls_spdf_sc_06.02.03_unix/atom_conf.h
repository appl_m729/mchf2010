#ifndef _ATOM_CONF_H_
#define _ATOM_CONF_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Atom_Conf : private Uncopyable {

public:

    Atom_Conf(const char file[]);
    ~Atom_Conf();

    double Z() const {return _Z;}

    int core_lmax() const {return _core_lmax;}
    int core_Nsubsh(const int l) const;         // l = 2 Nsubsh=4 => 32, 42, 52, 62
    int core_n(const int l, const int i) const; // (n=2,l=1) 3,1 4,1 3,2 ..
    int core_w(const int l, const int i) const;

    int core_Nsubsh() const {return _core_Nsubsh_tot;}
    int core_n(const int i) const;
    int core_l(const int i) const;
    int core_w(const int i) const;
    int core_Nel() const {return _core_Nel;}

    int walk_Nsubsh() const {return _walk_Nsubsh;}
    int walk_n(const int i) const;
    int walk_l(const int i) const;
    int walk_Nel() const {return _walk_Nel;}

    int hill_Nsubsh() const {return _hill_Nsubsh;}
    int hill_n(const int i) const;
    int hill_l(const int i) const;
    int hill_w(const int i) const;
    int hill_Nel() const {return _hill_Nel;}
    int hill_find(const int n, const int l) const; // return -1 if false

    int Nel_tot() const {return _walk_Nel + _hill_Nel + _core_Nel;}
    int Nel_val() const {return _walk_Nel + _hill_Nel;}

    void print() const;

    int    L () const {return _L; }
    int    ML() const {return _ML;}
    double S () const {return _S; }
    double MS() const {return _MS;}
    int    P () const {return _P; }

private:

    double _Z;

    int  _core_lmax, _core_maxNsubsh, *_core_Nsubsh;
    int *_core_n, *_core_w, _core_Nel;

    int  _core_Nsubsh_tot;
    int *_core_n_tot, *_core_l_tot, *_core_w_tot;

    int  _walk_Nel, _walk_Nsubsh;
    int *_walk_n, *_walk_l;

    int  _hill_Nel, _hill_Nsubsh;
    int *_hill_n, *_hill_l, *_hill_w;

    int  _L, _ML, _P;
    double  _S, _MS;


    void _read_core(const char file[]);
    void _setup_core_tot();
    void _read_walk(const char file[]);
    void _read_hill(const char file[]);
    void _check_overlaps_cwh();
    void _read_LMLSMLP(const char file[]);

    void _check_walk();
    void _check_hill();
    void _core_tot_swap(const int i, const int j);
};

////////////////////////////////////////////////////////////////////////////

#endif  // _ATOM_CONF_H_

// end of file

