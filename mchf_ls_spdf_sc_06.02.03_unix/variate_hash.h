#ifndef _VARIATE_HASH_H_
#define _VARIATE_HASH_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Pnl_IndexMap;
class One_List;
class Two_List;
class ksiHash_List;
class etaHash_List;
class Eta_rbTree;


class Variate_Hash : private Uncopyable {

public:

    Variate_Hash(const Pnl_IndexMap * const pnlInd,
                 const One_List * const oneLst,
                 const Two_List * const twoLst);

    ~Variate_Hash();

    void generate_ksi(ksiHash_List * const arr_khl) const;  // one
    void generate_eta(etaHash_List * const arr_ehl,         // two
                      Eta_rbTree * const etaTree) const;

private:

    const Pnl_IndexMap * const _PnlInd;
    const One_List * const _oneLst;
    const Two_List * const _twoLst;
};

////////////////////////////////////////////////////////////////////////////

#endif // _VARIATE_HASH_H_

// end of file


