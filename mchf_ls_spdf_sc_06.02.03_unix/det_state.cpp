#include <iostream>
#include <cmath>
#include <cstdlib>
#include "det_state.h"
#include "nlmlms_state.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

int Det_State::_Nel = 0;

void Det_State::set_Nel(const int Nel) {

    if (_Nel == 0 && Nel > 0) {

        _Nel = Nel;

    } else if (_Nel == Nel && Nel > 0) {

        cout << "Warning reSet_Nel in Det_State... _Nel=" << Nel << " =Nel;" << endl;

    } else {

        cout << "Error set_Nel in Det_State... _Nel=" << _Nel << " Nel=" << Nel << endl;
        exit(0);
    }
}

int Det_State::Nel() {
    return _Nel;
}

Det_State::Det_State():
_elst(0), _Parity(0), _sum_L(0), _ML(0), _MS(0), _sign(1) {

    if (_Nel > 0) {

        _elst = new nlmlms_State[_Nel];

    } else {

        _elst = 0;
        cout << "Error! Constructor of Det_State class: " << endl;
        cout << "_Nel was not properly setup... Initialization failed" << endl;

        exit(0);
    }
}

Det_State::Det_State(const Det_State& rhs):
_elst(0), _Parity(0), _sum_L(0), _ML(0), _MS(0), _sign(1) {

    if (_Nel > 0) {

        _Parity = rhs._Parity;
        _sum_L  = rhs._sum_L;
        _ML     = rhs._ML;
        _MS     = rhs._MS;
        _sign   = rhs._sign;

        _elst = new nlmlms_State[_Nel];
        for (int k = 0; k < _Nel; ++k) {
            _elst[k] = rhs._elst[k];
        }

    } else {

        _elst = 0;
        cout << "Error! Copy constructor of Det_State class: " << endl;
        cout << "_Nel was not properly setup... Incorrect Clone operation"<<endl;

        exit(0);
    }
}

Det_State::~Det_State() {
    delete[]  _elst;  _elst = 0;
}


void Det_State::set_elst_le(const nlmlms_State& elst, const int k) {

    if (0 <= k && k < _Nel) {

        _MS    += elst.ms() - _elst[k].ms();
        _ML    += elst.ml() - _elst[k].ml();
        _sum_L += elst.l()  - _elst[k].l();
        _Parity = (int) fmod(double(_sum_L),2.0);
        _elst[k].clone(elst);

        if ( !_check_Pauli_le(k) ) {

            cout << "Error in Det_State::set_elst_le; k=" << k;
            cout << " _check_Pauli_le(k) false!.." << endl;
            exit(0);
        }
    } else {

        cout << "Error in set_elst_le; k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

void Det_State::set_elst(const nlmlms_State& elst, const int k) {

    if (0 <= k && k < _Nel) {

        _MS += elst.ms() - _elst[k].ms();
        _ML += elst.ml() - _elst[k].ml();
        _sum_L += elst.l() - _elst[k].l();
        _Parity = (int) fmod(double(_sum_L),2.0);
        _elst[k].clone(elst);

        if ( !_check_Pauli(k) ) {

            cout << "Error in Det_State::set_elst; k=" << k;
            cout << " _check_Pauli(k) false!.." << endl;

            exit(0);
        }

    } else {

        cout << "Error in set_elst; k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

void Det_State::set_nlmlms(const int n, const int l, const int ml, 
                                     const double ms, const int k) {
    if (0 <= k && k < _Nel) {

        _MS += ms - _elst[k].ms();
        _ML += ml - _elst[k].ml();
        _sum_L += l - _elst[k].l();
        _Parity = (int) fmod(double(_sum_L),2.0);
        _elst[k].set_nlmlms(n,l,ml,ms);

        if ( !_check_Pauli(k) ) {

            cout << "Error in Det_State::set_nlmlms; k=" << k;
            cout << " _check_Pauli(k) false!.." << endl;
            exit(0);
        }

    } else {

        cout << "Error in Det_State::set_nlmlms; k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

void Det_State::set_n(const int n, const int k) {

    if (0 <= k && k < _Nel) {

        _elst[k].set_n(n);

    } else {

        cout << "Error in set_n; k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

void Det_State::set_l(const int l, const int k) {

    if (0 <= k && k < _Nel) {

        _sum_L += l - _elst[k].l();
        _Parity = (int) fmod(double(_sum_L),2.0);
        _elst[k].set_l(l);

    } else {

        cout << "Error in Det_State::set_l; k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

void Det_State::set_ml(const int ml, const int k) {

    if (0 <= k && k < _Nel) {

        if (ml >= -_elst[k].l() && ml <= _elst[k].l()) {

            _ML  += ml - _elst[k].ml();
            _elst[k].set_ml(ml);

        } else {

            cout << "Error in Det_State::set_ml; ml=" << ml << " _elst[k].l=" << _elst[k].l() << endl;
            exit(0);
        }

    } else {

        cout << "Error in Det_State::set_ml; k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

void Det_State::set_ms(const double ms, const int k) {

    if (0 <= k && k < _Nel) {

        _MS += ms - _elst[k].ms();
        _elst[k].set_ms(ms);

    } else {

        cout << "Error in Det_State::set_ms; k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

void Det_State::clone(const Det_State& DetSt) {

    for (int k = 0; k < _Nel; ++k) _elst[k] = DetSt.elst(k);

    _ML     = DetSt._ML;
    _MS     = DetSt._MS;
    _sum_L  = DetSt._sum_L;
    _Parity = DetSt._Parity;
    _sign   = DetSt._sign;
}

Det_State& Det_State::operator= (const Det_State& rhs) {

    if (this == &rhs) return *this;

    for(int k = 0; k < _Nel; ++k) _elst[k] = rhs._elst[k];
    _ML     = rhs._ML;
    _MS     = rhs._MS;
    _sum_L  = rhs._sum_L;
    _Parity = rhs._Parity;
    _sign   = rhs._sign;

    return *this;
}

void Det_State::recalc() {
    _recalc_PLMLMS();
}

void Det_State::sort() {

    for (int i = 0; i < _Nel-1; ++i) {
    for (int j=i+1; j < _Nel;   ++j) {

        if (_elst[i] > _elst[j]) {

            nlmlms_State t_el(_elst[i]);
            _elst[i] = _elst[j];
            _elst[j] = t_el;
            _sign *= -1;
        }
    }}
}

void Det_State::print() const {

    for (int k = 0; k < _Nel; ++k) {

        cout << "n=" << _elst[k].n() << " l=" << _elst[k].l() << " ml=";
        cout << _elst[k].ml() << " ms=" << _elst[k].ms() << endl;
    }
    cout << "sum_L=" << _sum_L << " ML=" << _ML << " MS=" << _MS;
    cout << " P=" << _Parity << " sign=" << _sign << endl;
    cout << endl;
}

int Det_State::Parity() const {
    return _Parity;
}

int Det_State::sum_L() const {
    return _sum_L;
}

int Det_State::ML() const {
    return _ML;
}

double Det_State::MS() const {
    return _MS;
}

int Det_State::sign() const {
    return _sign;
}

void Det_State::set_plus_sign() {
    _sign = 1;
}

void Det_State::nlmlms(int &n, int &l, int &ml, double &ms, const int k) const {

    if (0 <= k && k < _Nel) {

        n  = _elst[k].n();
        l  = _elst[k].l();
        ml = _elst[k].ml();
        ms = _elst[k].ms();

    } else {

        cout << "Error in Det_State::nlmlms(); k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

void Det_State::nl(int &n, int &l, const int k) const {

    if (0 <= k && k < _Nel) {

        n = _elst[k].n();
        l = _elst[k].l();

    } else {

        cout << "Error in Det_State::nl(); k=" << k << " _Nel=" << _Nel << endl;
        exit(0);
    }
}

int Det_State::n(const int k) const {

    if (0 <= k && k < _Nel) {
        return _elst[k].n();
    }

    cout << "Error in Det_State::n(); k=" << k << " _Nel=" << _Nel << endl;
    exit(0);

    return -1;
}

int Det_State::l(const int k) const {

    if (0 <= k && k < _Nel) {
        return _elst[k].l();
    }

    cout << "Error in Det_State::l(); k=" << k << " _Nel=" << _Nel << endl;
    exit(0);

    return -1;
}

int Det_State::ml(const int k) const {

    if (0 <= k && k < _Nel) {
        return _elst[k].ml();
    }

    cout << "Error in Det_State::ml(); k=" << k << " _Nel=" << _Nel << endl;
    exit(0);

    return -1;
}

double Det_State::ms(const int k) const {

    if (0 <= k && k < _Nel) {
        return _elst[k].ms();
    }

    cout << "Error in Det_State::ms(); k=" << k << " _Nel=" << _Nel << endl;
    exit(0);

    return -1;
}

const nlmlms_State& Det_State::elst(const int k) const {

    if (0 <= k && k < _Nel) {
        return _elst[k];
    }

    cout << "Error in Det_State::elst(); k=" << k << " _Nel=" << _Nel << endl;
    exit(0);

    return _elst[0];
}

bool Det_State::act_lmnus(double &d_mnus, const int k) {

    if (k < 0 || k >= Det_State::Nel()) {

        cout << "Error in Det_State::act_lmnus(); k=" << k;
        cout << " Nel=" << Det_State::Nel() << endl;
        exit(0);
    }

    d_mnus = 0;
    const int t_l  = _elst[k].l();
    const int t_ml = _elst[k].ml();

    if (t_ml == -t_l) return false;

    d_mnus = sqrt(t_l*(t_l + 1.0) - t_ml*(t_ml - 1.0));
    _elst[k].set_ml(t_ml - 1);
    _ML -= 1;

    if ( !_check_Pauli(k) ) {
        d_mnus = 0;
        return false;
    }
    sort();

    return true;
}

bool Det_State::act_lplus(double &d_plus, const int k) {

    if (k < 0 || k >= Det_State::Nel()) {

        cout << "Error in Det_State::act_lplus(); k=" << k;
        cout << " Nel=" << Det_State::Nel() << endl;
        exit(0);
    }

    d_plus = 0;
    const int t_l  = _elst[k].l();
    const int t_ml = _elst[k].ml();

    if (t_ml == t_l) return false;

    d_plus = sqrt(t_l*(t_l + 1.0) - t_ml*(t_ml + 1.0));
    _elst[k].set_ml(t_ml + 1);
    _ML += 1;

    if ( !_check_Pauli(k) ) {
        d_plus = 0;
        return false;
    }
    sort();

    return true;
}

bool Det_State::smnus_act(const int k) {

    if (k < 0 || k >= Det_State::Nel()) {
        cout << "Error in Det_State::smnus_act(); k=" << k;
        cout << " Nel=" << Det_State::Nel() << endl;
        exit(0);
    }

    if (_elst[k].ms() == SPIN[DOWN]) return false;
    _elst[k].set_ms(SPIN[DOWN]);
    _MS -= SPIN[STEP];

    if ( !_check_Pauli(k) ) {
        return false;
    }
    return true;
}

bool Det_State::splus_act(const int k) {

    if (k < 0 || k >= Det_State::Nel()) {

        cout << "Error in Det_State::splus_act(); k=" << k;
        cout << " Nel=" << Det_State::Nel() << endl;
        exit(0);
    }

    if (_elst[k].ms() == SPIN[UP]) return false;
    _elst[k].set_ms(SPIN[UP]);
    _MS += SPIN[STEP];

    if ( !_check_Pauli(k) ) {
        return false;
    }
    return true;
}

bool Det_State::hn_such_state(const nlmlms_State& Elst) const {

    for (int k = 0; k < _Nel; ++k) {
        if (Elst == _elst[k]) return false;
    }
    return true;
}

bool Det_State::is_ordered() const {

    for (int i = 0; i < _Nel-1; ++i) {
        if ( !(_elst[i+1] > _elst[i]) ) return false;
    }
    return true;
}

void Det_State::permute(const int i, const int j) {

    if(i < 0 || i >= _Nel || j < 0 || j >= _Nel) {

        cout << "Error in Det_State::permute(); i = " << i << " j = " << j << " ";
        cout << " Nel = " << Det_State::Nel() << endl;
        exit(0);
    }
    _sign *= -1;

    nlmlms_State buff_elst(_elst[i]);
    _elst[i] = _elst[j];
    _elst[j] = buff_elst;
}

int dfr_states(const Det_State& det_i, const Det_State& det_j,
               int& pos1i, int& pos1j, int& pos2i, int& pos2j) {

    if ( !(det_i.is_ordered()) || !(det_j.is_ordered()) ) {

        cout << "Error det_i or/and det_j in dfr_States(.. is not ordered..." << endl;
        det_i.print();
        det_j.print();
        exit(0);
    }

    pos1i = -1;
    pos1j = -1;
    pos2i = -1;
    pos2j = -1;

    int ind_i = 0, ind_j = 0;

    for (int k = 0; k < Det_State::Nel(); ++k) {

        if (det_j.hn_such_state(det_i._elst[k])) {

            if (ind_i == 0) {
                pos1i = k;
            } else if (ind_i == 1) {
                pos2i = k;
            }
            ++ind_i;
        }

        if ( det_i.hn_such_state(det_j._elst[k]) ) {

            if (ind_j == 0) {
                pos1j = k;
            } else if (ind_j == 1) {
                pos2j = k;
            }
            ++ind_j;
        }
    }

    if (ind_i != ind_j) {

        cout << "Fatal Error !! in Compare_Dets(... ";
        cout << "ind_i= " <<ind_i << " ind_j= "<< ind_j << endl;
        exit(0);
    }

    return ind_i;
}

bool operator== (const Det_State& DtSt1, const Det_State& DtSt2) {

    for (int k = 0; k < Det_State::Nel(); ++k) {

        if ( DtSt1.elst(k) != DtSt2.elst(k) ) return false;
    }
    return true;
}


void Det_State::_recalc_PLMLMS() {

    _Parity = 0;
    _sum_L  = 0;
    _ML     = 0;
    _MS     = 0;

    for(int k = 0; k < _Nel; ++k) {

        _MS += _elst[k].ms();
        _ML += _elst[k].ml();
        _sum_L += _elst[k].l();
    }
    _Parity = (int) fmod(double(_sum_L),2.0);
}

bool Det_State::_check_Pauli(const int k) {

    for (int i = 0; i < _Nel; ++i) {

        if (i == k) continue;

        if (_elst[k] == _elst[i]) return false;
    }
    return true;
}

bool Det_State::_check_Pauli_le(const int k) {

    for (int i = 0; i < k; ++i) {

        if (_elst[k] == _elst[i]) return false;
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////

// end of file

