#include <iostream>
#include <cstdlib>
#include "one_rbtree.h"
#include "ab_index.h"
#include "ccv_rbtree.h"
#include "one_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

One_RB_node::One_RB_node():
left(0), right(0), red(1), abInd(0), ccvTree(0) {
}

One_RB_node::~One_RB_node() {

    delete abInd;   abInd   = 0;
    delete ccvTree; ccvTree = 0;

    delete left;   left   = 0;
    delete right;  right  = 0;
}

////////////////////////////////////////////////////////////////////////////

One_RB_Tree::One_RB_Tree():_head(0), _size(0), _oneLst(0) {
}

One_RB_Tree::~One_RB_Tree() {

    _oneLst = 0;
    delete _head; _head = 0;
}

void One_RB_Tree::generate_OneList(One_List * const oneLst) const {

    _oneLst = oneLst;
    _catch_next_node(_head);
    _oneLst = 0;
}

void One_RB_Tree::add(const ab_Index& OneInd,
                      const int i, const int j, const double cVal) {

    _insert(OneInd);
    _RB_search(_head,OneInd)->ccvTree->add(i,j,cVal);
}

void One_RB_Tree::_rotR(One_RB_node* &h) {

    One_RB_node* x = h->left;
    h->left = x->right;
    x->right = h;
    h = x;
}

void One_RB_Tree::_rotL(One_RB_node* &h) {

    One_RB_node* x = h->right;
    h->right = x->left;
    x->left = h;
    h = x;
}

int One_RB_Tree::_is_red(One_RB_node *x) {

    if (0 == x) return 0;
    return x->red;
}

void One_RB_Tree::_insert(const ab_Index& oneInd) {

    _RB_insert(_head, oneInd,0);
    _head->red = 0;
}

void One_RB_Tree::_RB_insert(One_RB_node* &h,const ab_Index& insOneInd, int sw) {

    if (0 == h) {

        h = new One_RB_node;
        h->abInd = new ab_Index(insOneInd);
        h->ccvTree = new ccv_RB_Tree;

        ++_size;
    }

    if (_is_red(h->left) && _is_red(h->right)) {

        h->red = 1; 
        h->left->red  = 0; 
        h->right->red = 0;
    }

    if (insOneInd > (*(h->abInd)) ) {

        _RB_insert(h->right,insOneInd,1);

        if (_is_red(h) && _is_red(h->right) && !sw) {_rotL(h);}

        if (_is_red(h->right) && _is_red(h->right->right)) {

             _rotL(h);
            h->red = 0;
            h->left->red = 1;
        }
    } else if ( !( insOneInd == (*(h->abInd)) ) ) {

        _RB_insert(h->left,insOneInd,0);

        if (_is_red(h) && _is_red(h->left) && sw){_rotR(h);}

        if (_is_red(h->left) && _is_red(h->left->left)) {

            _rotR(h);
            h->red = 0;
            h->right->red = 1;
        }
    }
}

One_RB_node* One_RB_Tree::_RB_search(One_RB_node* h, const ab_Index& ptrnOneInd) {

    if (0 == h) {

        cout << "Error One_RB_Tree::_RB_find(" << endl;
        cout << "No such index: "; ptrnOneInd.print();
        exit(0);
    }

    if ( ptrnOneInd == (*(h->abInd)) ) {
        return h;
    }

    if ( ptrnOneInd > (*(h->abInd)) ) {
        return _RB_search(h->right, ptrnOneInd);
    } else {
        return _RB_search(h->left, ptrnOneInd);
    }
}

void One_RB_Tree::_catch_next_node(const One_RB_node* h) const {

    if ( 0 != h ) {

        _oneLst->add(h->abInd, h->ccvTree);

        _catch_next_node(h->left);
        _catch_next_node(h->right);
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

