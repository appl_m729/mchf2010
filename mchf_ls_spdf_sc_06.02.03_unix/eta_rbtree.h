#ifndef _ETA_RBTREE_H_
#define _ETA_RBTREE_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h" 

class eta_Index;
class Eta_List;


class Eta_rbNode : private Uncopyable {

public:

    Eta_rbNode *left, *right;
    int red;

    int linIndex;
    const eta_Index *eta_ind;

    Eta_rbNode();
    ~Eta_rbNode();
};


class Eta_rbTree : private Uncopyable {

public:

    Eta_rbTree();
    ~Eta_rbTree();

    void insert(const eta_Index& etaInd);
    int  linIndex(const eta_Index& etaInd) const;
    int  size() const {return _linInd + 1;}

    void linearize(Eta_List * const etaLst) const;

private:

    mutable Eta_List * _etaLst;

    Eta_rbNode * _head;
    int          _linInd;

    void _rotR(Eta_rbNode* &h);
    void _rotL(Eta_rbNode* &h);
    int  _is_red(Eta_rbNode *x);

    void _RB_insert(Eta_rbNode* &h, const eta_Index& EtaInd, int sw);
    Eta_rbNode* _RB_search(Eta_rbNode* h, const eta_Index& EtaInd) const;
    void _RB_traversal(Eta_rbNode* h) const;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _ETA_RBTREE_H_

// end of file

