writeFlags:
pnl_restore: no
pnl_dump:    ok   xe18_01_70_60_60_0p1_dump_01.txt
e_trace:     ok   xe18_01_70_60_60_0p1_e_trace_01.txt
rho:         ok   xe18_01_70_60_60_0p1_rho_01.txt
csf_basis:   no
one_list:    no
two_list:    no
E_mchf accuracy = 0.001
No hill electrons. Continue working.. ok
Atom_Conf:
Core Subshells: 
(4) 2 2 2 2 
(2) 6 6 
(1) 10 
core Nel = 30
(1,0)[2] (2,0)[2] (2,1)[6] (3,0)[2] (3,1)[6] (3,2)[10] (4,0)[2] 
walk_Nel = 6
(4,1) 
hill_Nel = 0

Nel_val = 6
Nel_tot = 36
Z = 54;  L ML S MS P: 0 0 0 0 0

Pnl_Index_Map:
length = 8
(1,0)->0
(2,0)->1
(3,0)->2
(4,0)->3
(2,1)->4
(3,1)->5
(4,1)->6
(3,2)->7
lmax = 2
llen (a_sta,a_end);
4 (0,3); 3 (4,6); 1 (7,7); 
max_llen = 4

start q4x_Convertor setup...
nl_length=1
(n,l) [w_max] [w_hill] [w_free]:
(4,1) [6] [0] [6]
 x_length=6
x -> (n,l,ml,ms) 
0->(4,1,-1,-0.5) 1->(4,1,-1,0.5) 2->(4,1,0,-0.5) 3->(4,1,0,0.5) 4->(4,1,1,-0.5) 5->(4,1,1,0.5) 
CSF_Basis: start setup...
collect nlw started..
 num_Conf = 1 ...collecting done ok
4,1(6)  
CSF_Basis: setup successfully done
CSF_Basis::N_csf = 1
Energy_Hash: start collecting dets... done ok
I_ab size = 8
Rk_abcd size = 81
qqHash: setup..  done ok
qBas_Conf: lmax = 2  Nlmax = 70
N[0] = 70  0.1
N[1] = 60  0.1
N[2] = 60  0.1
k_mixt = 0.3
dqIter_Conf:   0.001   0.001   0.001   0.001   0.001   0.001   0.001   0.001   
Variate_Hash: start generate ksiHash_list[8,8]... done ok
Variate_Hash: start generate etaHash_list[8,8]... done ok
Eta_rbTree: Start generate eta_List... done ok;  etaList->size = 63
etaList: start generate T,W _k_ab_rbTree... done ok; 
  Ttree->size = 63  Wtree->size = 63
s_k_ab_rbTree: start generate s_k_ab_List... done ok; T_list->size = 63
s_k_ab_rbTree: start generate s_k_ab_List... done ok; W_list->size = 63

qqHash:: start prepear tw_points...
T(63):  15