#include <fstream>
#include <iostream>
#include <iomanip>
#include "eta_list.h"
#include "eta_index.h"
#include "k_ab_index.h"
#include "s_k_ab_rbtree.h"
#include "s_k_ab_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Eta_DataBlock::Eta_DataBlock():next(0), lin_ind(-1), eta_ind(0) {
}

Eta_DataBlock::~Eta_DataBlock() {

    eta_ind = 0;
    delete next; next = 0;
}

////////////////////////////////////////////////////////////////////////////

Eta_List::Eta_List():_phead(new Eta_DataBlock),
_crnt_item(_phead), _ptail(_phead), _size(0) {
}

Eta_List::~Eta_List() {

    _crnt_item = 0;
    _ptail = 0;
    delete _phead;
}

void Eta_List::generate_2skabTrees(s_k_ab_rbTree * const M_sTree,
                                   s_k_ab_rbTree * const W_sTree) const {

    cout << "etaList: start generate T,W _k_ab_rbTree..";

    Eta_DataBlock * const tmpBuff = _crnt_item;
    s_k_ab_Index sInd(0,k_ab_Index(0,0,0));

    _crnt_item = _phead;
    while ( _crnt_item->next ) {

        _crnt_item = _crnt_item->next;

        const eta_Index * const eta(_crnt_item->eta_ind);
        const int l1 = eta->l1();
        const int l2 = eta->l2();
        const int k  = eta->kabInd().k();

        sInd.reset(l1+l2-k+1,eta->kabInd());
        M_sTree->insert(sInd);

        sInd.reset(l1+l2+k+2);
        W_sTree->insert(sInd);
    }
    _crnt_item = tmpBuff;

    cout << ". done ok; " << endl;
    cout << "  Ttree->size = " << M_sTree->size();
    cout << "  Wtree->size = " << W_sTree->size() << endl;
}

void Eta_List::clear() {

    if (_phead->next) {
        delete _phead->next; _phead->next = 0;
    }

    _crnt_item = _phead;
    _ptail = _phead;

    _size = 0;
}

void Eta_List::add(const eta_Index* EtaInd, const int linIndex) {

    _crnt_item->next = new Eta_DataBlock;
    _crnt_item = _crnt_item->next;

    _crnt_item->lin_ind = linIndex;
    _crnt_item->eta_ind = EtaInd;

    _size++;
}

void Eta_List::to_head() const {
    _crnt_item = _phead;
}

bool Eta_List::next() const {

    if (_crnt_item->next) {
        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

const eta_Index& Eta_List::etaInd() const {
    return *(_crnt_item->eta_ind);
}

int Eta_List::linIndex() const {
    return _crnt_item->lin_ind;
}

void Eta_List::write(const char file[]) const {

    cout << "eta_List: writing in "<< file << "..";
    ofstream fout(file);

    fout << "total size = " << size() << endl << endl;

    fout << "eta_Index: " << endl;
    fout << "position   linearIndex   l1 l2  k a b" << endl;

    int ii = 0;
    to_head();
    while(next()) {
        
        int l1,l2, k, a,b;
        l1 = etaInd().l1();
        l2 = etaInd().l2();
        etaInd().kabInd().k_ab(k, a,b);
    
        fout << endl << setw(4) << ii++ << setw(4) << linIndex();
        fout << setw(4) << l1 << setw(4) << l2;
        fout << setw(4) << k << setw(4) << a << setw(4) << b;
    }

    fout << endl << endl << "end of file" << endl;

    fout.close();
    cout << ". done ok" << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file

