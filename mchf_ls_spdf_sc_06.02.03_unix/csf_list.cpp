#include <iostream>
#include "csf_list.h"
#include "csf_ls.h"
#include "nlw_conf.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

CSF_DataBlock::CSF_DataBlock():
oneCnf(0), one_csf(0), next(0) {
//    cout << "csf_DataBlock constructor..." << endl;
}

CSF_DataBlock::~CSF_DataBlock() {

    oneCnf = 0;
    delete one_csf; one_csf = 0;
    delete next;    next    = 0;
//    cout << "csf_DataBlock destructor..." << endl;
}

////////////////////////////////////////////////////////////////////////////

CSFs_List::CSFs_List():
_phead(new CSF_DataBlock), 
_crnt_item_first(0), _crnt_item_secnd(0),
_prev_first_item(0), _size(0) {

//    cout << "csf_List constructor.." << endl;
    _crnt_item_first = _phead;
    _crnt_item_secnd = _phead;
}

CSFs_List::~CSFs_List() {

    delete _phead;

    _crnt_item_first = 0;
    _crnt_item_secnd = 0;
    _prev_first_item = 0;
//    cout << "csf_List destructor.." << endl;
}

const CSFunc_LS& CSFs_List::csf_first() const {
    return *(_crnt_item_first->one_csf);
}

const CSFunc_LS& CSFs_List::csf_secnd() const {
    return *(_crnt_item_secnd->one_csf);
}

const nlw_Conf* CSFs_List::pc_cnf() const {
    return _crnt_item_first->oneCnf;
}

void CSFs_List::to_head_first() const {
    _crnt_item_first = _phead;
}

void CSFs_List::to_first_item() const {
    _crnt_item_secnd = _prev_first_item;
}

void CSFs_List::to_first_item_sg() const {
    _crnt_item_secnd = _crnt_item_first;
}

bool CSFs_List::next_first() const {

    if (_crnt_item_first->next) {

        _prev_first_item = _crnt_item_first;
        _crnt_item_first = _crnt_item_first->next;

        return true;
    }
    return false;
}

bool CSFs_List::next_secnd() const {

    if (_crnt_item_secnd->next) {

        _crnt_item_secnd = _crnt_item_secnd->next;
        return true;
    }
    return false;
}

int CSFs_List::size() const {
    return _size;
}

void CSFs_List::catch_append(CSFunc_LS * const extCSF, 
                             const nlw_Conf* const pCnf) {

    _crnt_item_first->next = new CSF_DataBlock;
    _crnt_item_first = _crnt_item_first->next;

    _crnt_item_first->one_csf = extCSF;  // free memory in CSFs_List::~CSFs_List
    _crnt_item_first->oneCnf  = pCnf;    // free memory not in CSFs_List::~CSFs_List

    _size++;
}

////////////////////////////////////////////////////////////////////////////

// end of file

