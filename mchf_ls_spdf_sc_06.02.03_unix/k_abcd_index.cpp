#include <iostream>
#include <cstdlib>
#include "k_abcd_index.h"
#include "k_ab_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

k_abcd_Index::k_abcd_Index(const int k, const int a,
                           const int b, const int c, const int d):
_k(k), _a(a), _b(b), _c(c), _d(d) {

    if (_k < 0 || _a < 0 || _b < 0 || _c < 0 || _d < 0 ) { 

        cout << "Error in k_abcd_Index::k_abcd_Index(" << endl;
        print();
        exit(0);
    }
    _regulate();
}

k_abcd_Index::k_abcd_Index(const k_abcd_Index& rhs):
_k(rhs._k), _a(rhs._a), _b(rhs._b), _c(rhs._c), _d(rhs._d) {
}

k_abcd_Index::~k_abcd_Index() {
}

k_ab_Index k_abcd_Index::k_ac() const {
    return k_ab_Index(_k, _a, _c);
}

k_ab_Index k_abcd_Index::k_bd() const {
    return k_ab_Index(_k, _b, _d);
}

void k_abcd_Index::reset_k(const int k) {

    _k = k;

    if ( _k < 0 ) {

        cout << "Error in k_abcd_Index::reset_k(" << endl;
        print();
        exit(0);
    }
}

void k_abcd_Index::reset_abcd(const int a, const int b,
                              const int c, const int d) {

    _a = a; _b = b; _c = c; _d = d;

    if (_a < 0 || _b < 0 || _c < 0 || _d < 0 ) { 

        cout << "Error in k_abcd_Index::k_abcd_Index(" << endl;
        print();
        exit(0);
    }
    _regulate();
}

void k_abcd_Index::abcd(int &a,int &b,int &c,int &d) const {

    a = _a;
    b = _b;
    c = _c;
    d = _d;
}

void k_abcd_Index::k_abcd(int &k, int &a, int &b, int &c, int &d) const {

    k = _k;
    a = _a;
    b = _b;
    c = _c;
    d = _d;
}

void k_abcd_Index::print() const {

    cout << "k = " << _k << "  " ;
    cout << "a = " << _a << "  b = " << _b << "  ";
    cout << "c = " << _c << "  d = " << _d << endl;
}

// friend
bool operator== (const k_abcd_Index& lft, const k_abcd_Index& rht) {

    if (lft._k != rht._k) return false;

    if (lft._a != rht._a) return false;
    if (lft._b != rht._b) return false;
    if (lft._c != rht._c) return false;
    if (lft._d != rht._d) return false;
    // ==
    return true;
}

// friend
bool operator> (const k_abcd_Index& lft, const k_abcd_Index& rht) {

    if (lft._k > rht._k) return true;
    if (lft._k < rht._k) return false;

    if (lft._a > rht._a) return true;
    if (lft._a < rht._a) return false;                              

    if (lft._b > rht._b) return true;
    if (lft._b < rht._b) return false;                

    if (lft._c > rht._c) return true;
    if (lft._c < rht._c) return false;

    if (lft._d > rht._d) return true;
    if (lft._d < rht._d) return false;
     // ==
    return false;
}

void k_abcd_Index::_regulate() {

    // 1. min(a,c)
    if (_a > _c) _swap(_a,_c);

    // 2. min(b,d)
    if (_b > _d) _swap(_b,_d);

    // 3. min(a,b)
    if (_a > _b) {

        _swap(_a,_b);
        _swap(_c,_d);
    
    } else if ((_a == _b) && (_c > _d)) {
        
        _swap(_c,_d);    
    }
}

void k_abcd_Index::_swap(int &a, int &b) {

    const int tmp = a;
    a = b;
    b = tmp;
}

////////////////////////////////////////////////////////////////////////////

// end of file

