#ifndef _NUM_E_TRACE_H_
#define _NUM_E_TRACE_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"
#include "aconstants.h"


class HamiltonianMtrx;
class One_List;
class Two_List;
class NumE_List;


class NumE_Trace : private Uncopyable {

public:

    NumE_Trace(const One_List * const oneList,
               const Two_List * const twoList);
    ~NumE_Trace();

    void   update(const int k_iter, const HamiltonianMtrx * const hamMatr);

    void   print() const;
    double iter_dlt() const;
    void   write(const char file[]) const;

private:

    NumE_List      * const _eLst;
    const One_List * const _oneLst;
    const Two_List * const _twoLst;

    double  _E[NUM_E_LENGTH], _E_old[NUM_E_LENGTH];

    void _calc_one_E(const HamiltonianMtrx * const hamMatr);
    void _calc_two_E(const HamiltonianMtrx * const hamMatr);
};

////////////////////////////////////////////////////////////////////////////

#endif  //  _NUM_E_TRACE_H_

// end of file

