#include <iostream>
#include <cstdlib>
#include "shells_det.h"
#include "nlmlms_state.h"
#include "det_state.h"
#include "atom_conf.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Shells_Det::Shells_Det(const Atom_Conf * const aCnf):
_core_len(0), _length(0), _k_disl(-1), _elst(0) {

    if (!aCnf) {
        cout << "Error in Shells_Det; aCnf = 0.." << endl;
        exit(0);
    }

    _core_len = aCnf->core_Nel();
    _length = _core_len + Det_State::Nel();
    _k_disl = _length;

    if (!_length) {    
        cout << "Error in Shells_Det; _length = 0 .." << endl;
        exit(0);
    }

    _elst = new nlmlms_State[_length];
    _setup_elst(aCnf);
}

Shells_Det::~Shells_Det() {

    delete[] _elst; _elst = 0;
}

void Shells_Det::reset(const Det_State& ext_det) {

    _length = _core_len + Det_State::Nel();
    _k_disl = _length;

    if (_core_len) {

        if ((_elst[_core_len-1]  > ext_det.elst(0)) ||
            (_elst[_core_len-1] == ext_det.elst(0))) {

            cout << "Error in Shells_Det::reset; incorrect overlaps.." << endl;
            exit(0);
        }
    }

    for (int k = 0; k < Det_State::Nel(); ++k) {

        _elst[_core_len + k] = ext_det.elst(k);
    }
}

void Shells_Det::dislodge(const int k) {


    if (-1 == k) {

        _length = _core_len + Det_State::Nel();
        _k_disl = _length;
        return;
    }

    if (k >= 0 && k < Det_State::Nel()) {

        _k_disl = _core_len + k;
        _length = _core_len + Det_State::Nel() - 1;
        return;
    }

    cout << "Error in Shells_Det::dislodge(" << endl;
    cout << " k = " << k << endl;
    exit(0);
}

const nlmlms_State& Shells_Det::elst(const int k) const {

    if (k >= 0 && k < _k_disl) {

        return _elst[k];

    } else if (k >= _k_disl && k < _length) {

        return _elst[k+1];
    }

    cout << "Error in Shells_Det::elst" << " k = " << k << endl;
    exit(0);
}

void Shells_Det::_setup_elst(const Atom_Conf * const aCnf) {

    for (int v = 0, i = 0; v < aCnf->core_Nsubsh(); ++v) {

        const int n = aCnf->core_n(v);
        const int l = aCnf->core_l(v);

        for (int ml = -l; ml <= l; ++ml) {

            _elst[i++].set_nlmlms(n,l,ml,SPIN[DOWN]);
            _elst[i++].set_nlmlms(n,l,ml,SPIN[ UP ]);
        }
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

