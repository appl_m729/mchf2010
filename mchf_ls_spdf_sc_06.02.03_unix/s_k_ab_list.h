#ifndef _STEPEN_KAB_INDEX_LIST_H_
#define _STEPEN_KAB_INDEX_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class s_k_ab_Index;

class s_k_ab_DataBlock : private Uncopyable {

public:

    s_k_ab_DataBlock* next;

    const s_k_ab_Index* s_k_ab_ind;
    int linearIndex;

    s_k_ab_DataBlock();
    ~s_k_ab_DataBlock();
};


class s_k_ab_List : private Uncopyable {

public:

    s_k_ab_List();
    ~s_k_ab_List();

    void clear();

    void add(const s_k_ab_Index* skabInd, const int linearIndex);
    void to_head() const;
    bool next() const;

    const s_k_ab_Index& s_k_ab_Ind() const;
    int linInd() const;

    int size() const;
    void write(const char file[]) const;

private:

    s_k_ab_DataBlock * const _head;
    mutable s_k_ab_DataBlock* _crnt_item;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _STEPEN_KAB_INDEX_LIST_H_

// end of file
