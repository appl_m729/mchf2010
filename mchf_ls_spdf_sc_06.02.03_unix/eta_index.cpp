#include <iostream>
#include <cstdlib>
#include "eta_index.h"
#include "k_ab_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

eta_Index::eta_Index(const int l1, const int l2, const k_ab_Index& kabInd):
_l1(l1), _l2(l2), _kabInd(new k_ab_Index(kabInd)) {

    if (_l1 < 0 || _l2 < 0) {
        cout << "Error in eta_Index" << endl;
        cout << "l1 = " << _l1 << " l2 = " << _l2 << endl;
        exit(0);
    }    
    _regulate_ll();
}

eta_Index::eta_Index(const eta_Index& rhs):
_l1(rhs._l1), _l2(rhs._l2), _kabInd(new k_ab_Index(*(rhs._kabInd))) {
}

eta_Index::~eta_Index() {
    delete _kabInd;
}

eta_Index& eta_Index::operator= (const eta_Index& rhs) {

    if (&rhs == this) return *this;

    _l1 = rhs._l1;
    _l2 = rhs._l2;
    *(_kabInd) = *(rhs._kabInd);

    return *this;
}

void eta_Index::reset(const int l1, const int l2, const k_ab_Index& kabInd) {

    _l1 = l1;
    _l2 = l2;
    _regulate_ll();

    _kabInd->reset(kabInd);
}

void eta_Index::print() const {

    cout << "l1 = " << _l1 << " l2 = " << _l2 << " ";
    _kabInd->print();
}

//friend
bool operator==(const eta_Index& lhs, const eta_Index& rhs) {

    if (lhs._l1 != rhs._l1) return false;
    if (lhs._l2 != rhs._l2) return false;
    return (*(lhs._kabInd)) == (*(rhs._kabInd));
}

//friend
bool operator > (const eta_Index& lhs, const eta_Index& rhs) {

    if (lhs._l1 > rhs._l1) return true;
    if (lhs._l1 < rhs._l1) return false;

    if (lhs._l2 > rhs._l2) return true;
    if (lhs._l2 < rhs._l2) return false;

    return (*(lhs._kabInd)) > (*(rhs._kabInd));
}

void eta_Index::_regulate_ll() {

    if (_l1 > _l2) {
        const int buff = _l1;
        _l1 = _l2; _l2 = buff;
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

