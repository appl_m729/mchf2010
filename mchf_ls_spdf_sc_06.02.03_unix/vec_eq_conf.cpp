#include <iostream>
#include <cstdlib>
#include "vec_eq_conf.h"
#include "qbas_conf.h"
#include "pnl_index_map.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

VecEq_Conf::VecEq_Conf(const qBas_Conf * const basCnf,
                       const Pnl_IndexMap * const pnlInd):
_qBasCnf(basCnf), _pnlInd(pnlInd),
_x_sz(0), _x_pos(0), _A_sz(0), _A_pos(0),
_x_tsz(0), _A_tsz(0) {

    if (!_qBasCnf || !_pnlInd) {

        cout << "VecEq_Conf: Error zero pointers" << endl;
        cout << "basCnf = " << _qBasCnf << endl;
        cout << "pnlInd = " << _pnlInd << endl;

        exit(0);
    }

    const int n = _pnlInd->length();

    _x_sz  = new int[n];
    _x_pos = new int[n];
    _A_sz  = new int[(n*(n+1))/2];
    _A_pos = new int[(n*(n+1))/2];

    _x_tsz = 0;
    {for (int a = 0; a < n; ++a) {
        _x_sz[a] = _qBasCnf->Nmax(_pnlInd->l(a));
        _x_tsz += _x_sz[a];
    }}

    _x_pos[0] = 0;
    {for (int a = 1; a < n; ++a) {
        _x_pos[a] = _x_pos[a-1] + _x_sz[a-1];
    }}

    _A_tsz = 0;
    {for (int a = 0; a < n; ++a) {
     for (int b = a; b < n; ++b) {
        _A_sz[_trg_ind(n,a,b)] = _x_sz[a]*_x_sz[b];
        _A_tsz += _A_sz[_trg_ind(n,a,b)];
    }}}

    _A_pos[0] = 0;
    {for (int ind = 1; ind <= _trg_ind(n,n-1,n-1); ++ind) {
        _A_pos[ind] = _A_pos[ind-1] + _A_sz[ind-1];
    }}
}

VecEq_Conf::~VecEq_Conf() {

    delete[] _x_sz;  _x_sz  = 0;
    delete[] _x_pos; _x_pos = 0;
    delete[] _A_sz;  _A_sz  = 0;
    delete[] _A_pos; _A_pos = 0;
}

int VecEq_Conf::x_sz(const int a) const {

    if (a < 0 || a >= _pnlInd->length()) {

        cout << "Error in VecEq_Conf::x_sz(a = " << a << endl;
        cout << "pnlInd->length() = " << _pnlInd->length() << endl;
        exit(0);
    }
    return _x_sz[a];
}

int VecEq_Conf::x_pos(const int a) const {

    if (a < 0 || a >= _pnlInd->length()) {

        cout << "Error in VecEq_Conf::x_pos(a = " << a << endl;
        cout << "pnlInd->length() = " << _pnlInd->length() << endl;
        exit(0);
    }
    return _x_pos[a];
}

int VecEq_Conf::x_ind(const int a, const int i) const {

    if (a < 0 || a >= _pnlInd->length() || i < 0 || i >= _x_sz[a]) {

        cout << "Error in VecEq_Conf::x_ind(" << endl;
        cout << "a = " << a << " i = " << i << endl;

        exit(0);
    }
    return _x_pos[a] + i;
}

int VecEq_Conf::A_sz(const int a, const int b) const {

    const int n = _pnlInd->length();

    if (a < 0 || a >= n || b < 0 || b >= n) {

        cout << "Error in VecEq_Conf::A_sz(" << endl;
        cout << "a = " << a << "  b = " << b << endl;

        exit(0);
    }
    return _A_sz[_trg_ind(n,a,b)];
}

int VecEq_Conf::A_pos(const int a, const int b) const {

    const int n = _pnlInd->length();

    if (a < 0 || a >= n || b < 0 || b >= n) {

        cout << "Error in VecEq_Conf::A_pos(" << endl;
        cout << "a = " << a << "  b = " << b << endl;

        exit(0);
    }
    return _A_pos[_trg_ind(n,a,b)];
}

int VecEq_Conf::A_ind(const int a, const int b, const int i, const int j) const {

    const int n = _pnlInd->length();

    if (a < 0 || a >= n || b < 0 || b >= n ||
        i < 0 || i >= _x_sz[a] || j < 0 || j >= _x_sz[b]) {

        cout << "Error in VecEq_Conf::A_ind(" << endl;
        cout << "a = " << a << "  b = " << b << endl;
        cout << "i = " << i << "  j = " << j << endl;

        exit(0);
    }

    int i1(i), j1(j), wdth(_x_sz[b]);

    if (a > b) {

        wdth = _x_sz[a];
        i1 = j;
        j1 = i;
    }

    return _A_pos[_trg_ind(n,a,b)] + i1*wdth + j1;
}

void VecEq_Conf::print() const {

    cout << "VecEq_Conf:";

    cout << endl << "x_tsz = " << x_tsz();
    cout  << endl << "x_sz(a): ";
    {for (int a = 0; a < _pnlInd->length(); ++a) {
        cout << x_sz(a) << " ";
    }}

    cout  << endl << "x_pos(a): ";
    {for (int a = 0; a < _pnlInd->length(); ++a) {
        cout << x_pos(a) << " ";
    }}

    cout << endl << "A_tsz = " << A_tsz() << endl;
    cout << "A_sz(a,b): " << endl;
    {for (int a = 0; a < _pnlInd->length(); ++a) {
     for (int b = 0; b < _pnlInd->length(); ++b) {
        cout << A_sz(a,b) << " ";
    }cout << endl;}}

    cout << "A_pos(a,b): " << endl;
    {for (int a = 0; a < _pnlInd->length(); ++a) {
     for (int b = 0; b < _pnlInd->length(); ++b) {
        cout << A_pos(a,b) << " ";
    }cout << endl;}}

    cout << endl;
}

int VecEq_Conf::_trg_ind(const int sz, const int i, const int j) const {

    if (i <= j) {return ((2*sz - i - 1)*i)/2 + j;}
    return ((2*sz - j - 1)*j)/2 + i;
}

////////////////////////////////////////////////////////////////////////////

// end of file

