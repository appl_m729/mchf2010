#include <iostream>
#include <cstdlib>
#include "nlw_conf.h"
#include "q4x_convertor.h"
#include "coeffs.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

nlw_Conf::nlw_Conf():
_numShells(0), _numEl_all(0), _num_pos(0),
_n(0), _l(0), _w(0) {
}

nlw_Conf::nlw_Conf(const int * const extW, const q4x_Convertor * const q4xC):
_numShells(0), _numEl_all(0), _num_pos(0),
_n(0), _l(0), _w(0) {

    if (!extW || !q4xC) {
    
        cout << "Error in nlw_Conf.. extW = " << extW;
        cout << " q4xC = " << q4xC << endl;
        exit(0);
    }

    // 1. calc numShells
    _numShells = 0;
    {for (int k = 0; k < q4xC->nl_length(); ++k) {
        if ( extW[k] || q4xC->w_hill(k) ) ++_numShells;
    }}

    // 2. allocate memory
    _n = new int[_numShells];
    _l = new int[_numShells];
    _w = new int[_numShells];

    // 3. setup data
    {for (int k = 0, t = -1; k < q4xC->nl_length(); ++k) {

        if (extW[k] || q4xC->w_hill(k)) {

            _w[++t] = extW[k] + q4xC->w_hill(k);
            q4xC->nl(_n[t],_l[t], k);
        }
    }}

    // 4. all Nel
    _calc_numEl_all();

    // 5. n!/(k!(n-k)!)
    _calc_num_pos();
}

nlw_Conf::nlw_Conf(const nlw_Conf& rhs) {

    if (0 == rhs._numShells) {

        cout << "Error in nlw_Conf copy constructor;" << endl;
        cout << "numShells = 0; exiting..." << endl;
        exit(0);
    }

    _numShells = rhs._numShells;
    _numEl_all = rhs._numEl_all;
    _num_pos   = rhs._num_pos;

    _n = new int[_numShells];
    _l = new int[_numShells];
    _w = new int[_numShells];

    for(int k = 0; k < _numShells; ++k) {

        _n[k] = rhs._n[k];
        _l[k] = rhs._l[k];
        _w[k] = rhs._w[k];
    }
}

nlw_Conf::nlw_Conf(const int k, const int * const nn, 
                   const int * const ll, const int * const ww) {

    if (0 >= k) {

        cout << "Error in (nlw_Conf::nlw_Conf(k,n,l,w)" << endl;
        cout << "k = " << k << endl;
        exit(0);
    }

    // 1. calc numShells
    _numShells = k;

    // 2. allocate memory
    _n = new int[_numShells];
    _l = new int[_numShells];
    _w = new int[_numShells];

    // 3. setup data
    cout << "nlw_Conf constructor: start reading n,l,w from input.. ";
    for (int i = 0; i < _numShells; ++i) {

        cout << i << " ";

        _check_nlw(nn[i],ll[i],ww[i]);

        _n[i] = nn[i];
        _l[i] = ll[i];
        _w[i] = ww[i];
    }
    cout << ".ok; done." << endl;

    // 4. all Nel
    _calc_numEl_all();

    // 5. n!/(k!(n-k)!)
    _calc_num_pos();
}

nlw_Conf::~nlw_Conf() {

    delete[] _n;  _n = 0;
    delete[] _l;  _l = 0;
    delete[] _w;  _w = 0;
}

void nlw_Conf::nlw(int &n, int &l, int &w, const int k) const {

    if (k >= 0 && k < _numShells) {

        n = _n[k];
        l = _l[k];
        w = _w[k];
        return;
    }
    cout << "Error in nlw_Conf::nlw(... k=" << k << " _numShells=" << _numShells << endl;
    exit(0);
}

int nlw_Conf::n(const int k) const {

    if (k < 0 && k >= _numShells) {

        cout << "Error in nlw_Conf::n(.. k=" << k << " _numShells=" << _numShells << endl;
        exit(0);
    }
    return _n[k];
}

int nlw_Conf::l(const int k) const {

    if (k < 0 && k >= _numShells) {

        cout << "Error in nlw_Conf::l(.. k=" << k << " _numShells=" << _numShells << endl;
        exit(0);
    }
    return _l[k];
}

int nlw_Conf::w(const int k) const {

    if (k < 0 && k >= _numShells) {
        cout << "Error in nlw_Conf::w(.. k=" << k << " _numShells=" << _numShells << endl;
        exit(0);
    }
    return _w[k];
}

void nlw_Conf::print() const {

    for (int k = 0; k < _numShells; ++k) {
        cout << _n[k] << "," << _l[k] << "(" << _w[k] << ")" << "  ";
    }
    cout << endl;
}

void nlw_Conf::_calc_numEl_all() {

    _numEl_all = 0;
    for (int k = 0; k < _numShells; ++k) {
        _numEl_all += _w[k];
    }
}

void nlw_Conf::_calc_num_pos() {

    _num_pos = 1;
    for (int v = 0; v < _numShells; ++v) {
        _num_pos *= ccf().binomC(4*_l[v]+2, _w[v]);
    }
}

void nlw_Conf::_check_nlw(const int n, const int l, const int w) {

    if ((0 > l) || (l >= n) || (w <= 0) || (w > 2*(2*l+1))) {

        cout << "Error in nlw_Conf::_check_nlw(n,l,w)" << endl;
        cout << " n= " << n << " l= " << l << " w= " << w << endl;
        exit(0);
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

