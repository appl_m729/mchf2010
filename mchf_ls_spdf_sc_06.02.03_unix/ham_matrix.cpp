#include <iostream>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include "ham_matrix.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

HamiltonianMtrx::HamiltonianMtrx(const int Size):
_size(Size), _hsz((_size*(_size + 1))/2),
_A(0), _x(0), _mu(0), _Ax(0), _ksi(0) {

    if (_size < 1) {

        cout << "Error in constructor HamiltonianMtrx" << endl;
        cout << "size = " << _size << endl;
        exit(0);
    }

    _A = new double[_hsz];
    in_zero();

    _x    = new double[_size];
    _Ax   = new double[_size];
    _ksi  = new double[_size];


//    for (int i = 0; i < Size; ++i) {    
//        _x[i] = ((double) i + 1.0) / ((double) Size);
//    }

//    _x[0] =  1.4;
//    _x[1] =  0.8;
//    _x[2] =  6.17;
//    _x[3] = -2.22;
}

HamiltonianMtrx::~HamiltonianMtrx() {

    delete[] _A;    _A    = 0;
    delete[] _x;    _x    = 0;
    delete[] _Ax;   _Ax   = 0;
    delete[] _ksi;  _ksi  = 0;
}

void HamiltonianMtrx::in_zero() {

    for (int i = 0; i < _hsz; ++i) {
        _A[i] = 0;
    }
}

void HamiltonianMtrx::add(const int i, const int j, const double Value) {

    if (i <= j) {

        _A[_h_ind(i,j)] += Value;

    } else {

        cout << "Error in HamiltonianMtrx::add: i > j. H is upTriangle" << endl;
        exit(0);
    }
}

void HamiltonianMtrx::add(const int ind, const double value) {

    _A[ind] += value;
}

void HamiltonianMtrx::calc_ground_state() {

    if (1 == _size) {

        _x[0] = 1.0;
        _mu   = _A[0];
        return;
    }

    // initial setup
    const double const_C1 = 0.9;
    _x[_size-1] = const_C1;

    const double tail_C1 = sqrt((1.0 - const_C1*const_C1)/(_size - 1.0));

    {for (int i = 0; i < _size-1; ++i) {
        _x[i] = tail_C1;
    }}

    // find min eige val vec
    _ff_CG_EigVec_Sym();
}

void HamiltonianMtrx::print() const {

    for (int i = 0; i < _size; ++i) {
        for (int j = 0; j < _size; ++j) {
            cout  << setw(5) << _A[_h_ind(i,j)];
        }cout << endl;
    }
}

double HamiltonianMtrx::C(const int i) const {

    if (i < 0 || i > _size) {

        cout << " Error in HamiltonianMtrx::C(.. " << endl;
        cout << " i = " << i << ";  size = " << _size << endl;
        exit(0);
    }
    return _x[i];
}

int HamiltonianMtrx::_h_ind(const int i, const int j) const {

    if (i <= j) {return ((2*_size - i - 1)*i)/2 + j;}
    return ((2*_size - j - 1)*j)/2 + i;
}

double HamiltonianMtrx::_norm(const double * const x) const {

    const int i_max = _max_ind(x);
    double sum_sq = 0;

    {for (int i = 0; i < i_max; ++i) {

        const double t_xixm = x[i]/x[i_max];
        sum_sq += t_xixm*t_xixm;
    }}

    {for (int i = i_max+1; i < _size; ++i) {

        const double t_xixm = x[i]/x[i_max];
        sum_sq += t_xixm*t_xixm;
    }}
    sum_sq = fabs(x[i_max])*sqrt(sum_sq + 1.0);

    return sum_sq;
}

int HamiltonianMtrx::_max_ind(const double * const x) const {

    int i_max = 0;
    for (int i = 1; i < _size; ++i) {
        if (fabs(x[i]) > fabs(x[i_max])) {
            i_max = i;
        }
    }
    return i_max;
}

void HamiltonianMtrx::_ff_CG_EigVec_Sym() {

    int k_iter = 0;
    double mu_old;

    do {
        // 1)
        mu_old = _mu;
        _mu = 0;

        {for (int i = 0; i < _size; ++i) {

            _Ax[i] = 0;
            for (int j = 0; j < _size; ++j) {
                _Ax[i] += _A[_h_ind(i,j)]*_x[j];
            }
            _mu += _x[i]*_Ax[i];
        }}

        // 2)
        {for (int i = 0; i < _size; ++i) {
            _ksi[i] = _Ax[i] - _mu*_x[i];
        }}

        // 3)
        const double t0 = _norm(_ksi);

        if ( t0 < FF_CG_KSI_0_IS_ZERO) {
            break;
        }

        double mu_ksi = 0;
        {for (int i = 0; i < _size; ++i) {

            double Aksi_i = 0;
            for (int j = 0; j < _size; ++j) {
                Aksi_i += _A[_h_ind(i,j)]*_ksi[j]/t0;
            }
            mu_ksi += _ksi[i]/t0*Aksi_i;
        }}

        // 4)
        const double s0 = _mu - mu_ksi;
        const double eta = 2.0*t0/s0;
        const double a0 = -2.0/(fabs(s0)*sqrt(1+eta*eta) - s0);

        {for (int i = 0; i < _size; ++i) {
            _x[i] += a0*_ksi[i];
        }}

        // 5)
        const double norm1 = _norm(_x);
        {for (int i = 0; i < _size; ++i) {
            _x[i] /= norm1;
        }}

    } while((++k_iter <  FF_CG_MAX_ITER) && (fabs(mu_old - _mu) > FF_CG_EIG_DIAG_EPS));

    if (FF_CG_MAX_ITER == k_iter) {
    
        cout << endl << "HamiltonianMtrx: Warning in _ff_CG_EigVec_Sym()" << endl;
        cout << "k_iter = " << k_iter << endl;
        cout << "MAX_ITER limit achieved" << endl;
//        exit(0);
    }

    cout << "HamiltonianMtrx: FF_CG_k_iter = " << k_iter;
    cout << "  eige = " << setprecision(12) << _mu << setprecision(7) << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file

