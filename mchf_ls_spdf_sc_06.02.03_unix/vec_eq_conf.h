#ifndef _VECT_EQ_CONFIGURATE_H_
#define _VECT_EQ_CONFIGURATE_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class qBas_Conf;
class Pnl_IndexMap;

class VecEq_Conf : private Uncopyable {

public:

    VecEq_Conf(const qBas_Conf * const qBasCnf, const Pnl_IndexMap * const pnlInd);
    ~VecEq_Conf();

    int x_sz (const int a) const;
    int x_pos(const int a) const;
    int x_ind(const int a, const int i) const;

    int A_sz (const int a, const int b) const;
    int A_pos(const int a, const int b) const;
    int A_ind(const int a, const int b, const int i, const int j) const;

    int x_tsz() const {return _x_tsz;}  // total size
    int A_tsz() const {return _A_tsz;}

    void print() const;

private:

    const qBas_Conf    * const _qBasCnf;
    const Pnl_IndexMap * const _pnlInd;

    int *_x_sz, *_x_pos;
    int *_A_sz, *_A_pos;
    int  _x_tsz, _A_tsz;

    int _trg_ind(const int b_size, const int i, const int j) const;
};

////////////////////////////////////////////////////////////////////////////

#endif // _VECT_EQ_CONFIGURATE_H_

// end of file

