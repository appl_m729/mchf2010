#include <iostream>
#include <cstdlib>
#include "variate_hash.h"
#include "pnl_index_map.h"
#include "one_list.h"
#include "two_list.h"
#include "ab_index.h"
#include "k_ab_index.h"
#include "k_abcd_index.h"
#include "ksi_hash.h"
#include "eta_hash.h"
#include "eta_rbtree.h"
#include "eta_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Variate_Hash::Variate_Hash(const Pnl_IndexMap * const PnlInd,
                           const One_List * const OneLst,
                           const Two_List * const TwoLst):
_PnlInd(PnlInd), _oneLst(OneLst), _twoLst(TwoLst) {
}

Variate_Hash::~Variate_Hash() {
}

void Variate_Hash::generate_ksi(ksiHash_List * const ksiLst) const {

    const int plen = _PnlInd->length();

    cout << "Variate_Hash: start generate ksiHash_list[";
    cout << plen << "," << plen << "]..";

    _oneLst->to_head();
    while (_oneLst->next()) {
    
        const int a = _oneLst->abInd().a();
        const int b = _oneLst->abInd().b();

        const int la = _PnlInd->l(a);
        const int lb = _PnlInd->l(b);

        if (la != lb) {
        
            cout << "Error in Variate_Hash:: ksi" << endl;
            cout << "la = " << la << " lb = " << lb << endl;
            exit(0);
        }

        ksiLst[a*plen+b].add(_oneLst->p_ccvLst(),la);
        ksiLst[b*plen+a].add(_oneLst->p_ccvLst(),lb);
    }
    cout << ". done ok" << endl;
}

void Variate_Hash::generate_eta(etaHash_List * const etaLst,
                                Eta_rbTree * const etaTree) const {

    const int plen = _PnlInd->length();
    cout << "Variate_Hash: start generate etaHash_list[";
    cout << plen << "," << plen << "]..";

    _twoLst->to_head();
    while (_twoLst->next()) {

        const k_abcd_Index& t_twoInd = _twoLst->twoInd();

        const int a = t_twoInd.a();
        const int b = t_twoInd.b();
        const int c = t_twoInd.c();
        const int d = t_twoInd.d();

        const int la = _PnlInd->l(a);
        const int lb = _PnlInd->l(b);
        const int lc = _PnlInd->l(c);
        const int ld = _PnlInd->l(d);

        etaLst[a*plen+c].add(_twoLst->p_ccvLst(),la,lc,t_twoInd.k_bd());
        etaLst[c*plen+a].add(_twoLst->p_ccvLst(),lc,la,t_twoInd.k_bd());
        etaLst[b*plen+d].add(_twoLst->p_ccvLst(),lb,ld,t_twoInd.k_ac());
        etaLst[d*plen+b].add(_twoLst->p_ccvLst(),ld,lb,t_twoInd.k_ac());

        etaTree->insert(eta_Index(la,lc,t_twoInd.k_bd()));
        etaTree->insert(eta_Index(lb,ld,t_twoInd.k_ac()));
    }
    cout << ". done ok" << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file

