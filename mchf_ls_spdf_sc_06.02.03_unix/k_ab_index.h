#ifndef _K_AB_INDEX_H_
#define _K_AB_INDEX_H_

////////////////////////////////////////////////////////////////////////////
  
class k_ab_Index {

public:

    k_ab_Index(const int k, const int a,const int b);
    k_ab_Index(const k_ab_Index& rhs);
    ~k_ab_Index();

    void reset(const k_ab_Index& rhs);
    void reset(const int k, const int a,const int b);
    void reset_k(const int k);
    void print() const;

    int  k() const {return _k;}
    int  a() const {return _a;}
    int  b() const {return _b;}
    void k_ab(int& k, int& a, int& b) const;

    friend bool operator==(const k_ab_Index& lhs, const k_ab_Index& rhs);
    friend bool operator> (const k_ab_Index& lhs, const k_ab_Index& rhs);

private:

    int _k, _a,_b;

    void _regulate();
};

////////////////////////////////////////////////////////////////////////////

#endif  // _K_AB_INDEX_H_

// end of file

