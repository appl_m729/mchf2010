#include <iostream>
#include <cmath>
#include "qua3_index.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

qua3_Index::qua3_Index():
_ML(0), _MS(0), _P(0) {
}

qua3_Index::qua3_Index(const int ML, const double MS, const int P):
_ML(ML), _MS(MS), _P(P) {
}

qua3_Index::qua3_Index(const qua3_Index& rhs):
_ML(rhs._ML), _MS(rhs._MS), _P(rhs._P) {
}

qua3_Index::~qua3_Index() {
}

void qua3_Index::reset(const int extML, const double extMS, const int extP) {

    _ML = extML;
    _MS = extMS;
    _P  = extP;
}

void qua3_Index::reset(const qua3_Index& rhs) {

    _ML = rhs._ML;
    _MS = rhs._MS;
    _P  = rhs._P;
}

void qua3_Index::all(int &ML, double &MS, int &P) const {

    ML = _ML;
    MS = _MS;
    P  = _P;
}

void qua3_Index::print() const {
    cout << _ML << " " << _MS << " " << _P << endl;
}

qua3_Index& qua3_Index::operator= (const qua3_Index& rhs) {

    if (this == &rhs) return *this;

    _ML = rhs._ML;
    _MS = rhs._MS;
    _P  = rhs._P;

    return *this;
}

bool operator== (const qua3_Index& lhs, const qua3_Index& rhs) {

    if (lhs._ML != rhs._ML) return false;

    if (fabs(lhs._MS - rhs._MS) > MS_ACCURACY) return false;

    if (lhs._P != rhs._P ) return false;

    return true;
}

bool operator > (const qua3_Index& lhs, const qua3_Index& rhs) {

    if (lhs._ML > rhs._ML) return true;
    if (lhs._ML < rhs._ML) return false;

    if (fabs(lhs._MS - rhs._MS) > MS_ACCURACY) {

        if (lhs._MS > rhs._MS) return true;
        if (lhs._MS < rhs._MS) return false;
    }

    if (lhs._P > rhs._P ) return true;
    if (lhs._P < rhs._P ) return false;

    return false;
}

////////////////////////////////////////////////////////////////////////////

// end of file


