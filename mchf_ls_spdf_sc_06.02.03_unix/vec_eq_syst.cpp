#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include "vec_eq_syst.h"
#include "eta_rbtree.h"
#include "ksi_hash.h"
#include "eta_hash.h"
#include "pnl_index_map.h"
#include "qbas_conf.h"
#include "ham_matrix.h"
#include "vec_eq_conf.h"
#include "pnl_radial.h"
#include "pnl_buffer.h"
#include "ham_matrix.h"
#include "ccv_list.h"
#include "eta_index.h"
#include "k_ab_index.h"
#include "yacoby.h"
#include "aconstants.h"
#include "dq_iter_conf.h"
#include "mdiis_engine.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

VecEq_Syst::VecEq_Syst(const Eta_rbTree * const etaTree,
                       const ksiHash_List * const arr_ksihl,
                       const etaHash_List * const arr_etahl,
                       const Pnl_IndexMap * const pnlIndex,
                       const qBas_Conf * const qbasConf,
                       const dqIter_Conf * const dqItrCnf):

_etaTree(etaTree), _hash_ksi(arr_ksihl), _hash_eta(arr_etahl),
_pnlInd(pnlIndex), _qBasCnf(qbasConf), _dqIterCnf(dqItrCnf),
_veqCnf(new VecEq_Conf(_qBasCnf,_pnlInd)),
_etaInd(new eta_Index(0,0,k_ab_Index(0,0,0))),
_mDiisEngn(new mDIIS_Engine(3,_veqCnf)),
_A(0), _x(0), _lam(0), _x_old(0), _dx(0),
_tmpA(0), _tmpV(0), _tmpB(0),
_q(0), _dq(0), _dq_old(0), _dq_err(0), _sca(0) {

    if (!_etaTree || !_hash_ksi || !_hash_eta || !_pnlInd || !_qBasCnf) {

        cout << "VecEq_Syst: Error setup, zero input pointers" << endl;
        cout << "_etaTree  = " << _etaTree  << endl;
        cout << "_hash_ksi = " << _hash_ksi << endl;
        cout << "_hash_eta = " << _hash_eta << endl;
        cout << "_pnlInd = " << _pnlInd << endl;
        cout << "_basCnf = " << _qBasCnf << endl;

        exit(0);
    }

    _dqIterCnf->print();

    _A     = new double[_veqCnf->A_tsz()];
    _x     = new double[_veqCnf->x_tsz()];
    _lam   = new double[_pnlInd->square()];
    _x_old = new double[_veqCnf->x_tsz()];
    _dx    = new double[_veqCnf->x_tsz()];

    const int t_sz = _qBasCnf->Nlmax()*_qBasCnf->Nlmax();
    _tmpA = new double[t_sz];
    _tmpV = new double[t_sz];
    _tmpB = new double[t_sz];

    _q      = new double[_veqCnf->x_tsz()];
    _dq     = new double[_veqCnf->x_tsz()];
    _dq_old = new double[_veqCnf->x_tsz()];
    _dq_err = new double[_veqCnf->x_tsz()];

    _sca    = new double[_pnlInd->length()];
}

VecEq_Syst::~VecEq_Syst() {

    delete[] _sca;    _sca    = 0;
    delete[] _q;      _q      = 0;
    delete[] _dq;     _dq     = 0;
    delete[] _dq_err; _dq_err = 0;
    delete[] _dq_old; _dq_old = 0;

    delete[] _tmpA; _tmpA = 0;
    delete[] _tmpV; _tmpV = 0;
    delete[] _tmpB; _tmpB = 0;

    delete[] _A;     _A     = 0;
    delete[] _x;     _x     = 0;
    delete[] _lam;   _lam   = 0;
    delete[] _x_old; _x_old = 0;
    delete[] _dx;    _dx    = 0;

    delete _mDiisEngn;
    delete _dqIterCnf;
    delete _etaInd;
    delete _veqCnf;
}

void VecEq_Syst::recalc(const Radial_Pnl_Data * const radialPnl,
                        const HamiltonianMtrx * const hamMtrx) {

    cout << "VecEq_Syst: start recalc..." << endl;

    _set_A_in_zero();

    {for (int a = 0; a < _pnlInd->length(); ++a) {

        _scatter_diag_ksi(a,radialPnl,hamMtrx);
        _scatter_diag_eta(a,radialPnl,hamMtrx);

        for (int b = a+1; b < _pnlInd->length(); ++b) {

            _scatter_off_diag_ksi(a,b,radialPnl,hamMtrx);
            _scatter_off_diag_eta(a,b,radialPnl,hamMtrx);
        }
    }}

    _Solve_VecEqs(radialPnl);

    cout << "VecEq_Syst: recalc done ok." << endl;
}

double VecEq_Syst::b(const int a, const int i) const {

    if (i < 0 || i >= _veqCnf->x_sz(a)) {

        cout << "Error in VecEq_Syst::b(" << endl;
        cout << "Nmax = " << _veqCnf->x_sz(a);
        cout << " a = " << a << "  i = " << i << endl;
        exit(0);
    }

    return _x[_veqCnf->x_pos(a) + i];
}

void VecEq_Syst::_Solve_VecEqs(const Radial_Pnl_Data * const radialPnl) {

    _setup_first_appr_x(); // by diagonal matrices A_aa

    int k_iter;
    for (k_iter = 0; k_iter < VEC_EQ_MAX_ITER; ++k_iter) {

        _zero_every_q();
        _calc_every_q();
        _calc_every_dq();

        if ( !(k_iter % VEC_EQ_ITER_JMP_STEP) ) {

            cout << k_iter << "  ";
            _print_norm_every_dq();

            if ( _every_dq_is_small() ) break;
            if ( _any_dq_is_freeze()  ) break;
        }

        _save_every_x_in_x_old();

        _iterate_every_x_by_dq(radialPnl);
        _normalize_every_x();

//        _calc_every_dx();
//        _mDiisEngn->add(_x, _dx);
//        _mDiisEngn->produce(_x);
//        _normalize_every_x();
    }

    if (VEC_EQ_MAX_ITER == k_iter) {
    
        cout << endl << "VecEq_Syst: Warning!" << endl;
        cout << "k_iter = VEC_EQ_MAX_ITER = " << VEC_EQ_MAX_ITER << endl;
    }

    _calc_lams();
    _print_lams();
    _print_orthogonality();
}

void VecEq_Syst::_set_A_in_zero() {

    const int t_Asz = _veqCnf->A_tsz();

    for (int ind = 0; ind < t_Asz; ++ind) {_A[ind] = 0;}
}

void VecEq_Syst::_setup_first_appr_x() {

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int n = _veqCnf->x_sz(a);
        const int posA = _veqCnf->A_pos(a,a);

        // copy diagonal block(a,a) of A in tmpA matrix
        {for (int i = 0; i < n; ++i) {
         for (int j = 0; j < n; ++j) {
            _tmpA[i*n+j] = _A[posA+i*n+j];
        }}}

        diagonalize_Yacoby(n, _tmpA, _tmpV, _tmpB);

        // setup _x[a]
        const int xpos = _veqCnf->x_pos(a);
        const int sh   = _pnlInd->shift(a);

        for (int i = 0; i < n; ++i) {
        
            _x[xpos+i] = _tmpV[i*n + sh];
            _dq[xpos+i] = 0;
        }
    }
}

void VecEq_Syst::_zero_every_q() {

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int pos_a = _veqCnf->x_pos(a);
        const int sz_a  = _veqCnf->x_sz(a);

        for (int i = 0; i < sz_a; ++i) {
            _q[pos_a + i] = 0;
        }
    }
}

void VecEq_Syst::_calc_every_q() {

    for (int a = 0; a < _pnlInd->length(); ++a) {

   
        const int pos_a = _veqCnf->x_pos(a);
        const int sz_a  = _veqCnf->x_sz(a);

        {for (int b = 0; b < a; ++b) {
            
            const int pos_b = _veqCnf->x_pos(b);
            const int sz_b  = _veqCnf->x_sz(b);

            const int posA_ab = _veqCnf->A_pos(a,b);

            for (int i = 0; i < sz_a; ++i) {

                double sum = 0;
                for (int j = 0; j < sz_b; ++j) {
                    sum += _A[posA_ab + j*sz_a +i]*_x[pos_b+j];
                }
                _q[pos_a + i] += sum;
            }
        }}
        for (int b = a; b < _pnlInd->length(); ++b) {
            
            const int pos_b = _veqCnf->x_pos(b);
            const int sz_b  = _veqCnf->x_sz(b);

            const int posA_ab = _veqCnf->A_pos(a,b);

            for (int i = 0; i < sz_a; ++i) {

                double sum = 0;
                for (int j = 0; j < sz_b; ++j) {
                    sum += _A[posA_ab + i*sz_b +j]*_x[pos_b+j];
                }
                _q[pos_a + i] += sum;
            }
        }
    }
}

void VecEq_Syst::_calc_every_dq() {

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int pos_a = _veqCnf->x_pos(a);
        const int sz_a  = _veqCnf->x_sz(a);
        const int la    = _pnlInd->l(a);

        // update every_dq_start
        {for (int i = 0; i < sz_a; ++i) {

            _dq_old[pos_a + i] = _dq[pos_a + i];
            _dq[pos_a + i] = _q[pos_a + i];
        }}        

        // hash sca_product
        for (int b = _pnlInd->a_sta(la); b <= _pnlInd->a_end(la); ++b) {

            const int pos_xb = _veqCnf->x_pos(b);
            const double t_sca_ab = _sca_pr(&_q[pos_a],&_x[pos_xb],sz_a);

            for (int i = 0; i < sz_a; ++i) {
                _dq[pos_a + i] -= t_sca_ab*_x[pos_xb + i];
            }
        }

        {for (int i = 0; i < sz_a; ++i) {
            _dq_err[pos_a + i] = _dq[pos_a + i] - _dq_old[pos_a + i];
        }}
    }
}

void VecEq_Syst::_print_norm_every_dq() const {

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const double dqa_norm = _norm(&_dq[_veqCnf->x_pos(a)],_veqCnf->x_sz(a));

        cout << dqa_norm << "  ";
    }cout << endl;
}

bool VecEq_Syst::_every_dq_is_small() {

    double t_max_dq = _norm(&_dq[_veqCnf->x_pos(0)],_veqCnf->x_sz(0));

    for (int a = 1; a < _pnlInd->length(); ++a) {

        const double dqa_norm = _norm(&_dq[_veqCnf->x_pos(a)],_veqCnf->x_sz(a));

        if (t_max_dq > dqa_norm ) t_max_dq = dqa_norm;
    }

    if (t_max_dq < VEQ_EQ_dQ_ERR) return true;
    return false;
}

bool VecEq_Syst::_any_dq_is_freeze() {

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const double dq_err_nrm = _norm(&_dq_err[_veqCnf->x_pos(a)],_veqCnf->x_sz(a));

        if (fabs(dq_err_nrm) < VEQ_EQ_dQ_FREEZE) return true;
    }

    return false;
}

void VecEq_Syst::_iterate_every_x_by_dq(const Radial_Pnl_Data * const radialPnl) {

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int pos_x = _veqCnf->x_pos(a);
        const int sz_x  = _veqCnf->x_sz(a);
        const int la    = _pnlInd->l(a);
        const int na    = _pnlInd->l(a);


        for (int i = 0; i < sz_x; ++i) {

            const double tkin = fabs(radialPnl->kin(la,i,i));
            _x[pos_x+i] -= _dqIterCnf->alpha(a)*_dq[pos_x+i]/(1.0+tkin);

//            _x[pos_x+i] -= _dqIterCnf->alpha(a)*_dq[pos_x+i];

//            const double xp = (0.5*i)/radialPnl->kin(la,i,i);
//            _x[pos_x+i] -= _dqIterCnf->alpha(a)*_dq[pos_x+i]*f(xp);

//            const double znam = (i + pow(radialPnl->kin(la,i,i),na+la+1));
//            _x[pos_x+i] -= _dqIterCnf->alpha(a)*_dq[pos_x+i]/znam;
//            _x[pos_x+i] -= _dqIterCnf->alpha(a)*_dq[pos_x+i]/(1.0 + i*i);
        }
    }
}

double VecEq_Syst::f(const double x) const {

    const double eta = 2.0*x/(3.0 + 2.0*x);

    return 1.0/(1.0 + eta*eta*eta*2.0*x);
}

void VecEq_Syst::_normalize_every_x() {

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int sz_a  = _veqCnf->x_sz(a);
        const int pos_a = _veqCnf->x_pos(a);

        _normalize(&_x[pos_a],sz_a);
    }


    for (int l = 0; l <= _pnlInd->lmax(); ++l) {

        for (int a = _pnlInd->a_sta(l); a <= _pnlInd->a_end(l); ++a) {

            const int sz_a  = _veqCnf->x_sz(a);
            const int pos_a = _veqCnf->x_pos(a);

            {for (int b = _pnlInd->a_sta(l); b < a; ++b) {

                const int pos_b = _veqCnf->x_pos(b);
                _sca[b] = _sca_pr(&_x[pos_a],&_x[pos_b],sz_a);
            }}

            {for (int b = _pnlInd->a_sta(l); b < a; ++b) {

                const int pos_b = _veqCnf->x_pos(b);

                for (int i = 0; i < sz_a; ++i) {
                    _x[pos_a + i] -= _sca[b]*_x[pos_b+i];
                }
            }}
            _normalize(&_x[pos_a],sz_a);
        }
    }


}

void VecEq_Syst::_save_every_x_in_x_old() {

    for (int i = 0; i < _veqCnf->x_tsz(); ++i) {
        _x_old[i] = _x[i];
    }
}

void VecEq_Syst::_calc_every_dx() {

    for (int i = 0; i < _veqCnf->x_tsz(); ++i) {
        _dx[i] = _x[i] - _x_old[i];
    }
}

void VecEq_Syst::_calc_lams() {

    const int n = _pnlInd->length();

    for (int l = 0; l <= _pnlInd->lmax(); ++l) {

        for (int a = _pnlInd->a_sta(l); a <= _pnlInd->a_end(l); ++a) {
        for (int b = _pnlInd->a_sta(l); b <= _pnlInd->a_end(l); ++b) {

            _lam[a*n+b] = _lambda(a,b);
        }}
    }
}

double VecEq_Syst::_lambda(const int a, const int b) const {

    const int la = _pnlInd->l(a);
    const int lb = _pnlInd->l(b);

    if (la != lb) {

        cout << "Error in VecEq_Syst::_lambda(" << endl;
        cout << "la = " << la << " lb = " << lb << endl;

        exit(0);
    }

    double sum = 0;
    for (int ax = 0; ax < _pnlInd->length(); ++ax) {
        sum += _xAx(b,a,ax);
    }
    return sum;
}

double VecEq_Syst::_xAx(const int ax, const int aA, const int bx) const {

    const int xa_pos = _veqCnf->x_pos(ax);
    const int xb_pos = _veqCnf->x_pos(bx);

    double xAx(0);
    for (int i = 0; i < _veqCnf->x_sz(ax); ++i) {

        double Ax_i(0);
        for (int j = 0; j < _veqCnf->x_sz(bx); ++j) {

            Ax_i += _A[_veqCnf->A_ind(aA,bx,i,j)]*_x[xb_pos+j];
        }
        xAx += _x[xa_pos + i]*Ax_i;
    }
    return xAx;
}

void VecEq_Syst::_print_lams() const {

    cout << endl << "lambda:" << endl;

    const int n = _pnlInd->length();

    for (int l = 0; l <= _pnlInd->lmax(); ++l) {

        cout << "l = " << l << endl;

        for (int a = _pnlInd->a_sta(l); a <= _pnlInd->a_end(l); ++a) {
        for (int b = _pnlInd->a_sta(l); b <= _pnlInd->a_end(l); ++b) {

            cout << _lam[a*n+b] << " ";
        } cout << endl;}
    }
}

void VecEq_Syst::_print_orthogonality() const {

    cout << endl;
    for (int l = 0; l <= _qBasCnf->lmax(); ++l) {

        for (int a = _pnlInd->a_sta(l); a <= _pnlInd->a_end(l); ++a) {

            const int a_pos = _veqCnf->x_pos(a);
            for (int b = a; b <= _pnlInd->a_end(l); ++b) {

                const int b_pos  = _veqCnf->x_pos(b);
                const double scp = _sca_pr(&_x[b_pos],&_x[a_pos],_veqCnf->x_sz(b));

                const int na = _pnlInd->n(a);
                const int nb = _pnlInd->n(b);

                cout << "<" << na << "," << l << "|";
                cout << nb << "," << l << "> = " << scp << endl;
            }
        }
    }
    cout << endl;
}

double VecEq_Syst::_sca_pr(const double *y1, const double *y2, const int sz) const {

    double sum = 0;

    for (int k = 0; k < sz; ++k) {
        sum += y1[k]*y2[k];
    }
    return sum;
}

double VecEq_Syst::_norm(const double * const x, const int sz) const {

    const int i_max = _max_ind(x,sz);
    double sum_sq = 0;

    {for (int i = 0; i < i_max; ++i) {

        const double t_xixm = x[i]/x[i_max];
        sum_sq += t_xixm*t_xixm;
    }}

    {for (int i = i_max+1; i < sz; ++i) {

        const double t_xixm = x[i]/x[i_max];
        sum_sq += t_xixm*t_xixm;
    }}
    sum_sq = fabs(x[i_max])*sqrt(sum_sq + 1.0);

    return sum_sq;
}

int VecEq_Syst::_max_ind(const double * const x, const int sz) const {

    int i_max = 0;
    for (int i = 1; i < sz; ++i) {
        if (fabs(x[i]) > fabs(x[i_max])) {
            i_max = i;
        }
    }
    return i_max;
}

void VecEq_Syst::_normalize(double * x, const int sz) {

    const double v_nrm = _norm(x,sz);
    for (int i = 0; i < sz; ++i) x[i] /= v_nrm;
}

void VecEq_Syst::_scatter_diag_ksi(const int a,
                                   const Radial_Pnl_Data * const radialP,
                                   const HamiltonianMtrx * const hamMtrx) {

    const ksiHash_List& ksiLst = _hash_ksi[a*_pnlInd->length()+a];

    ksiLst.to_head();
    while (ksiLst.next()) {

        const ccv_List& ccvLst = ksiLst.ccvLst();

        double sum = 0;
        ccvLst.to_head();
        while (ccvLst.next()) {

            int i,j; double v;
            ccvLst.ijv(i,j,v);

            sum += v*hamMtrx->C(i)*hamMtrx->C(j);
        }

        const int l = ksiLst.l();
        const double rezlt = ksiLst.diag_factor()*sum;

        for (int s = 0; s < _veqCnf->x_sz(a); ++s) {

            _A[_veqCnf->A_ind(a,a,s,s)] += rezlt*radialP->ksi(l,s,s);

            for (int t = s+1; t < _veqCnf->x_sz(a); ++t) {

                const double aval = rezlt*radialP->ksi(l,s,t);
                _A[_veqCnf->A_ind(a,a,s,t)] += aval;
                _A[_veqCnf->A_ind(a,a,t,s)] += aval;
            }
        }
    }
}

void VecEq_Syst::_scatter_diag_eta(const int a,
                                   const Radial_Pnl_Data * const radialP,
                                   const HamiltonianMtrx * const hamMtrx) {

    const etaHash_List& etaLst = _hash_eta[a*_pnlInd->length()+a];

    etaLst.to_head();
    while (etaLst.next()) {

        const ccv_List& ccvLst = etaLst.ccvLst();

        double sum = 0;
        ccvLst.to_head();
        while (ccvLst.next()) {

            int i,j; double v;
            ccvLst.ijv(i,j,v);

            sum += v*hamMtrx->C(i)*hamMtrx->C(j);
        }

        const double rezlt = etaLst.diag_factor()*sum;

        const int l1 = etaLst.l1();
        const int l2 = etaLst.l2();
        _etaInd->reset(l1,l2,etaLst.kabInd());
        const int ind = _etaTree->linIndex(*_etaInd);

        if (l1 == l2) {

            for (int s = 0; s < _veqCnf->x_sz(a); ++s) {

                _A[_veqCnf->A_ind(a,a,s,s)] += rezlt*radialP->eta(ind,s,s);

                for (int t = s+1; t < _veqCnf->x_sz(a); ++t) {

                    const double aval = rezlt*radialP->eta(ind,s,t);

                    _A[_veqCnf->A_ind(a,a,s,t)] += aval;
                    _A[_veqCnf->A_ind(a,a,t,s)] += aval;
                }
            }

        } else {

            cout << "Error in vec_Eq_Syst. l1 != l2 in eta symm..." << endl;
            exit(0);
        }
    }
}

void VecEq_Syst::_scatter_off_diag_ksi(const int a, const int b,
                                       const Radial_Pnl_Data * const radialP,
                                       const HamiltonianMtrx * const hamMtrx) {

    const ksiHash_List& ksiLst = _hash_ksi[a*_pnlInd->length()+b];

    ksiLst.to_head();
    while (ksiLst.next()) {

        const ccv_List& ccvLst = ksiLst.ccvLst();

        double sum = 0;
        ccvLst.to_head();
        while (ccvLst.next()) {

            int i,j; double v;
            ccvLst.ijv(i,j,v);

            sum += v*hamMtrx->C(i)*hamMtrx->C(j);
        }

        const int l = ksiLst.l();
        const double rezlt = ksiLst.diag_factor()*sum;

        for (int s = 0; s < _veqCnf->x_sz(a); ++s) {
        for (int t = 0; t < _veqCnf->x_sz(b); ++t) {

            _A[_veqCnf->A_ind(a,b,s,t)] += rezlt*radialP->ksi(l,s,t);
        }}
    }
}

void VecEq_Syst::_scatter_off_diag_eta(const int a, const int b,
                                       const Radial_Pnl_Data * const radialP,
                                       const HamiltonianMtrx * const hamMtrx) {

    const etaHash_List& etaLst = _hash_eta[a*_pnlInd->length()+b];

    etaLst.to_head();
    while (etaLst.next()) {

        const ccv_List& ccvLst = etaLst.ccvLst();

        double sum = 0;
        ccvLst.to_head();
        while (ccvLst.next()) {

            int i,j; double v;
            ccvLst.ijv(i,j,v);

            sum += v*hamMtrx->C(i)*hamMtrx->C(j);
        }

        const double rezlt = etaLst.diag_factor()*sum;

        const int l1 = etaLst.l1();
        const int l2 = etaLst.l2();
        _etaInd->reset(l1,l2,etaLst.kabInd());
        const int ind = _etaTree->linIndex(*_etaInd);

        if (l1 <= l2) {

            for (int s = 0; s < _veqCnf->x_sz(a); ++s) {
            for (int t = 0; t < _veqCnf->x_sz(b); ++t) {

                _A[_veqCnf->A_ind(a,b,s,t)] += rezlt*radialP->eta(ind,s,t);
            }}

        } else {

            cout << "Error in vec_Eq_Syst. l1 > l2 in eta non_diag..." << endl;
            exit(0);
        }
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

