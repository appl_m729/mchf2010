#include <iostream>
#include <cmath>
#include <cstdlib>
#include "csf_ls.h"
#include "det_state.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

CSFunc_LS::CSFunc_LS():
_size(0), _A(0), _det(0),
_L(0), _ML(0), _P(0), _S(0), _MS(0) {
}

CSFunc_LS::CSFunc_LS(const CSFunc_LS& rhs):
_size(rhs._size), _A(0), _det(0),
_L(rhs._L), _ML(rhs._ML), _P(rhs._P), 
_S(rhs._S),_MS(rhs._MS) {

    if (_size > 0) {

        _A   = new double[_size];
        _det = new Det_State[_size];

        for (int ii = 0; ii < _size; ++ii) {

            _A[ii] = rhs._A[ii];
            _det[ii].clone(rhs._det[ii]);
        }
    }
}

CSFunc_LS::~CSFunc_LS() {

    delete[] _A;   _A   = 0;
    delete[] _det; _det = 0;
}

CSFunc_LS& CSFunc_LS::operator= (const CSFunc_LS& rhs) {

    if (&rhs == this) return *this;

    if ((_size != rhs._size) || (0 == rhs._size) ) {

        cout << "Error in CSFunc_LS& operator= ( ";
        cout << " _size = " << _size << "  rhs._size= " << rhs._size << endl;
        exit(0);
    }

    if ( !_A || !_det ) {

        cout << "Error in CSFunc_LS& operator= ( ";
        cout << " _A= " << _A << "  _det= " << _det << endl;
        exit(0);
    }

    for (int ii = 0; ii < _size; ++ii) {

        _A[ii] = rhs._A[ii];
        _det[ii].clone(rhs._det[ii]);
    }

    return *this;
}

int CSFunc_LS::size() const {
    return _size;
}

double CSFunc_LS::A(const int j) const {

    if ( (j < 0) || (j >= _size)) {

        cout << "Error in CSFunc_LS::A(j=" << j <<")..." << endl;
        exit(0);
    }

    return _A[j];
}

const Det_State& CSFunc_LS::det(const int j) const {

    if ( (j < 0) || (j >= _size) ) {

        cout << "Error in CSFunc_LS::det(j=" << j <<")..." << endl;
        exit(0);
    }

    return _det[j];
}

void CSFunc_LS::reduce() {

    int i = 0;
    while (i < _size-1) {

        if ( fabs(_A[i]) < CSF_TRANCATE_THRESHOLD ) {

            _A[i] = _A[_size-1];
            _det[i].clone(_det[_size-1]);
            _size--;

        } else {
            ++i;
        }
    }
    // i == size-1 (the last)
    if ( fabs(_A[_size-1]) < CSF_TRANCATE_THRESHOLD ) {
        _size--;
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

