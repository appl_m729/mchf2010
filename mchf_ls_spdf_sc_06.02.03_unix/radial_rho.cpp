#include <iostream>
#include <fstream>
#include <iomanip>
#include "radial_rho.h"
#include "grid_r.h"
#include "pnl_index_map.h"
#include "one_list.h"
#include "ccv_list.h"
#include "ab_index.h"
#include "ham_matrix.h"
#include "pnl_radial.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Radial_Rho::Radial_Rho(const char file[], const  Pnl_IndexMap * const PnlInd,
                       const One_List * const oneLst):
_rgrid(new grid_r(file)), _pnlInd(PnlInd), _oneLst(oneLst),
_Pnl_data(new double[(2*_rgrid->N()+1)*_pnlInd->length()]),
_rho_data(new double[(2*_rgrid->N()+1)]) {

}

Radial_Rho::~Radial_Rho() {

    delete   _rgrid;
    delete[] _Pnl_data;
    delete[] _rho_data;
}

void Radial_Rho::calc(const Radial_Pnl_Data * const radialPnl,
                      const HamiltonianMtrx * const hamltMtrx) {

    _zero_Pnl();
    _zero_rho();

    _calc_Pnl(radialPnl);
    _calc_rho(hamltMtrx);
}

double Radial_Rho::charge() const {

    double sum = 0;
    for (int i = 0; i < _rgrid->N(); ++i) {
        sum += _rgrid->h(i)*(_rho_data[2*i+0] + 4.0*_rho_data[2*i+1] + _rho_data[2*i+2]);
    }
    return sum/3.0;
}

void Radial_Rho::write(const char * const file)  const {

    cout << "Radial_Rho: writing in " << file << "..";
    ofstream fout(file);

    fout << "#grid:  N = " << _rgrid->N();
    fout << "   r_max = " << _rgrid->r_max() << endl;
    fout << "#Pnl(" << _pnlInd->length() << "):  ";
    for (int a = 0; a < _pnlInd->length(); ++a) {

        fout << "(" << _pnlInd->n(a) << ",";
        fout << _pnlInd->l(a) << ")  ";
    }

    fout << endl << "#r rho rho/(r*r) Pnl" << endl;

    for (int i = 0; i < 2*_rgrid->N()+1; ++i) {

        fout << setw(12) << _rgrid->r2(i);
        fout << setw(20) << _rho_data[i];
        fout << setw(20) << _rho_data[i]/(_rgrid->r2(i)*_rgrid->r2(i));

        for (int a = 0; a < _pnlInd->length(); ++a) {

            const int pos_a = a*(2*_rgrid->N()+1);
            fout << setw(20) << _Pnl_data[pos_a+i];
        }
        fout << endl;
    }

    fout.close();
    cout << ". done ok" << endl;
}

void Radial_Rho::_zero_Pnl() {

    const int apN = 2*_rgrid->N()+1; //ap - all points

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int pos_a = a*apN;

        for (int i = 0; i < apN; ++i) {
            _Pnl_data[pos_a + i] = 0;
        }
    }
}

void Radial_Rho::_zero_rho() {

    for (int i = 0; i < 2*_rgrid->N()+1; ++i) {
        _rho_data[i] = 0;
    }
}

void Radial_Rho::_calc_Pnl(const Radial_Pnl_Data * const radialPnl) {

    const int apN = 2*_rgrid->N()+1;

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int pos_a = a*apN;

        for (int i = 0; i < apN; ++i) {
            _Pnl_data[pos_a + i] = radialPnl->Pnl(a,_rgrid->r2(i));
        }
    }
}

void Radial_Rho::_calc_rho(const HamiltonianMtrx * const hamltMtrx) {

    _oneLst->to_head();
    while (_oneLst->next()) {

        const int a = _oneLst->abInd().a();
        const int b = _oneLst->abInd().b();

        const int pos_a = a*(2*_rgrid->N()+1);
        const int pos_b = b*(2*_rgrid->N()+1);

        const ccv_List * const ccvLst = _oneLst->p_ccvLst();
        double vCC = 0;
        ccvLst->to_head();
        while (ccvLst->next()) {

            int i,j; double v;
            ccvLst->ijv(i,j,v);

            vCC += v*hamltMtrx->C(i)*hamltMtrx->C(j);
        }

        for (int i = 0; i < 2*_rgrid->N()+1; ++i) {

            _rho_data[i] += vCC*_Pnl_data[pos_a+i]*_Pnl_data[pos_b+i];
        }
    }
}

////////////////////////////////////////////////////////////////////////////


// end of file
