#ifndef _CSF_DETS_BUILDER_H_
#define _CSF_DETS_BUILDER_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class q4x_Convertor;
class nlw_Conf;
class Det_State;

class CSF_DetsBuilder : private Uncopyable {

public:

    CSF_DetsBuilder();
    virtual ~CSF_DetsBuilder();

    void setup_q4xC(const q4x_Convertor * const ext_q4xC);

protected:

    const q4x_Convertor* _q4xC;      // convertor for mapping
    const nlw_Conf*      _nlw_cnf;   // current config from wich one builds dets
    Det_State * const _buffDet;
    int **_x;                        // discrete points
    int   _Nj_max, _Ni_max;          // size of box

    void _allocate_xij();
    void _delete_xij();
    void _print_xji() const;
    void _check_allocate_xij();
    void _initialize_xij();
    void _next_move(const int j, const int i);
    void _setup_buffDet();

    virtual void _process_next_det();        // should be redefined
};


class qua3_Index;
class qua3_List;

// catch all qua3-Indexes for nlw_conf
class CSF_qua3Ind_Catch : public CSF_DetsBuilder {

public:

    CSF_qua3Ind_Catch();
    virtual ~CSF_qua3Ind_Catch();

    void collect_Inds(const nlw_Conf* const p_Cnf, qua3_List * const q3Lst);

private:

    qua3_Index * const _q3Ind;
    qua3_List  * _q3Lst;

    virtual void _process_next_det();  // collect all qua3_Indexes here
};


class DetsList;

// for nlw_conf collect dets if ML, MS and P 
class CSF_Dets_Collection : public CSF_DetsBuilder {

public:

    CSF_Dets_Collection();
    virtual ~CSF_Dets_Collection();

    void create_list(const nlw_Conf * const cp_Cnf,
                     const qua3_Index * const cp_3Ind,
                     DetsList * const ext_pDetLst);
private:

    const qua3_Index * _cpq3Ind;
    DetsList * _detsLst;
    int _pract_index, _catch_index;

    virtual void _process_next_det();        // save dets if ML,MS and P;
};

////////////////////////////////////////////////////////////////////////////

#endif // _DETS_BUILDER_H_

//end of file
