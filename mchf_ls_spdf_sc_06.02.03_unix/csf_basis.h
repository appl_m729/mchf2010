#ifndef _CSF_BASIS_H_
#define _CSF_BASIS_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Atom_Conf;
class q4x_Convertor;
class CSFunc_LS;
class nlw_Builder;
class CSF_Dets_Collection;
class DetsList;
class CSF_Builder;
class CSFs_List;
class qua5_Index;


class CSF_Basis : private Uncopyable {

public:

    CSF_Basis(const Atom_Conf * const aCnf);
    ~CSF_Basis();

    const qua5_Index& q5Ind() const;

    int size() const;

    const CSFunc_LS&  csf_first() const; // logical const
    const CSFunc_LS&  csf_secnd() const; // logical const
    const CSFunc_LS* pcsf_first() const;

    void to_head_first() const;     // logical const
    void to_first_item() const;     // (i,i) 
    void to_first_item_sg() const;  // not (i,i) but (i,i+1) sg = sharp greater 

    bool next_first() const;        // logical const
    bool next_secnd() const;        // logical const

    void write(const char filename[]) const;

private:

    const Atom_Conf * const     _aCnf;
    const q4x_Convertor * const _q4xC;
    nlw_Builder * const         _nlwBldr;
    CSF_Dets_Collection * const _dtsClcn;
    DetsList * const            _dtsLst;
    CSF_Builder * const         _csfBldr;
    CSFs_List * const           _csfLst;
    qua5_Index * const          _q5Ind;

    void _setup();
};

////////////////////////////////////////////////////////////////////////////

#endif // _CSF_BASIS_H_

// end of file
