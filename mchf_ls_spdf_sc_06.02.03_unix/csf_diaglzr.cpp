#include <iostream>
#include <cmath>
#include <cstdlib>
#include "csf_diaglzr.h"
#include "det_state.h"
#include "dets_list.h"
#include "aconstants.h"
#include "qua3_index.h"
#include "qua5_index.h"
#include "qua5_rbtree.h"
#include "yacoby.h"
#include "csf_ls.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

CSF_Diaglzr::CSF_Diaglzr(): _dtsLst(0),
_Xmatr  (new double[Yacoby_MAX_SIZE*Yacoby_MAX_SIZE]), 
_R_buffL(new double[Yacoby_MAX_SIZE*Yacoby_MAX_SIZE]),
_T_buffS(new double[Yacoby_MAX_SIZE*Yacoby_MAX_SIZE]),
_buff_TR(new double[Yacoby_MAX_SIZE*Yacoby_MAX_SIZE]),
_eig_S(new double[Yacoby_MAX_SIZE]),
_eig_L(new int   [Yacoby_MAX_SIZE]),
_LL1_buff(new double[Yacoby_MAX_SIZE]),
_SS1_buff(new double[Yacoby_MAX_SIZE]) {
}

CSF_Diaglzr::~CSF_Diaglzr() {

    delete[] _Xmatr;   _Xmatr   = 0;
    delete[] _R_buffL; _R_buffL = 0;
    delete[] _T_buffS; _T_buffS = 0;
    delete[] _buff_TR; _buff_TR = 0;

    delete[] _eig_S; _eig_S = 0;
    delete[] _eig_L; _eig_L = 0;

    delete[] _LL1_buff; _LL1_buff = 0;
    delete[] _SS1_buff; _SS1_buff = 0;

    _dtsLst = 0;
}

void CSF_Diaglzr::_setup_X2mtr() {

    int i = -1;
    _dtsLst->to_head_1();
    while (_dtsLst->next_1()) {

        ++i;
        const Det_State * const tDet_i = _dtsLst->crnt_pDet_1();

        _L2mtr(i,i) = 0;
        _add_mLz_pLz2(tDet_i, i);
        _add_LpLm(tDet_i, tDet_i, i, i);

        _S2mtr(i,i) = 0;
        _add_mSz_pSz2(tDet_i, i);
        _add_SpSm(tDet_i, tDet_i, i, i);

        int j = i;
        _dtsLst->to_head_2_sg();
        while (_dtsLst->next_2()) {

            ++j;
            const Det_State * const tDet_j = _dtsLst->crnt_pDet_2();

            _L2mtr(i,j) = 0;
            _add_LpLm(tDet_i, tDet_j, i, j);

            _S2mtr(i,j) = 0;
            _add_SpSm(tDet_i, tDet_j, i, j);
        }
    }
}

void CSF_Diaglzr::_add_mLz_pLz2(const Det_State * const Det_i, const int i) {

    const int tML = Det_i->ML();
    _L2mtr(i,i) += double (tML*(tML - 1));
}

void CSF_Diaglzr::_add_LpLm(const Det_State * const Det_i,
                            const Det_State * const Det_j,
                            const int i, const int j) {
    Det_State t_Dtst;
    Det_State t_Dtst_2;

    for (int ki = 0; ki < Det_State::Nel(); ++ki) {

        t_Dtst = (*Det_j);
        double d_mnus = 0;

        if (t_Dtst.act_lmnus(d_mnus,ki)) {
            t_Dtst_2 = t_Dtst;

            for (int kj = 0; kj < Det_State::Nel() ; ++kj) {

                t_Dtst_2 = t_Dtst;
                double d_plus = 0;

                if (t_Dtst_2.act_lplus(d_plus,kj)) {

                    if ( (*Det_i) == t_Dtst_2) {
                        _L2mtr(i,j) += d_mnus*d_plus*t_Dtst_2.sign();
                    }
                }
            }
        }
    }
}

void CSF_Diaglzr::_add_mSz_pSz2(const Det_State * const Det_i, const int i) {

    const double tMS = Det_i->MS();
    _S2mtr(i,i) += tMS*(tMS - 1.0);
}

void CSF_Diaglzr::_add_SpSm(const Det_State * const Det_i,
                            const Det_State * const Det_j,
                            const int i, const int j) {
    Det_State t_Dtst;
    Det_State t_Dtst_2;

    for (int ki = 0; ki < Det_State::Nel(); ++ki) {

        t_Dtst = (*Det_j);

        if (t_Dtst.smnus_act(ki)) {
            t_Dtst_2 = t_Dtst;

            for (int kj = 0; kj < Det_State::Nel() ; ++kj) {

                t_Dtst_2 = t_Dtst;
                if (t_Dtst_2.splus_act(kj)) {

                    if ( (*Det_i) == t_Dtst_2) {
                        _S2mtr(i,j) += t_Dtst_2.sign();
                    }
                }
            }
        }
    }
}

void CSF_Diaglzr::_prepear_data() {

    const int t_sz = _dtsLst->size();

    _setup_X2mtr();
//    _print_L2_S2();

    if (1 < t_sz) {

        _create_R(PRINT_STUFF_TRUE);
        _rotate_S2();
        _create_T(PRINT_STUFF_TRUE);
        _create_TR();
        _setup_LS_basic(PRINT_STUFF_TRUE);

    } else if (1 == t_sz) {

        _create_1_eiges();

    } else {

        cout << "Unpredictable Error in CSF_Diaglzr:: " << endl;
        cout << " _dtsLst->size() = " << _dtsLst->size() << endl;
        exit(0);
    }
}

void CSF_Diaglzr::_create_R(PrintEigesParam param) {

    const int t_sz = _dtsLst->size();

    // copy L2 to _Xmatr
    for (int i = 0; i < t_sz; ++i) {
    for (int j = i; j < t_sz; ++j) {

         _Xmatr[i*t_sz+j] = _Xmatr[j*t_sz+i] = _L2mtr(i,j);
    }}

    // calc R-transformation
    diagonalize_Yacoby(t_sz, _Xmatr, _R_buffL, _buff_TR);

    // print L(L+1) eiges
    if (PRINT_STUFF_TRUE == param) {

        cout << "L(L+1) stuff from diag:" << endl;
        for (int i = 0; i < t_sz; ++i) {

            cout << _Xmatr[i*t_sz+i] << " ";
        }   cout << endl;
    }

    _trancate(_R_buffL);  // trancate uncorrect zero

//    cout << "L_Vec:" << endl; _print_X(_R_buffL);
}

void CSF_Diaglzr::_rotate_S2() {

    const int t_sz = _dtsLst->size();

    {for (int i = 0; i < t_sz; ++i) {
     for (int j = 0; j < t_sz; ++j) {

         double sum = 0;
         for (int k = 0; k < t_sz; ++k) {

             sum += _S2mtr(i,k)*_R_buffL[k*t_sz+j];
         }
         _buff_TR[i*t_sz+j] = sum;
    }}}

    // R'(S R)
    {for (int i = 0; i < t_sz; ++i) {
     for (int j = 0; j < t_sz; ++j) {

         double sum = 0;
         for (int k = 0; k < t_sz; ++k) {

             sum += _R_buffL[k*t_sz+i]*_buff_TR[k*t_sz+j];
         }
         _Xmatr[i*t_sz+j] = sum;
    }}}

    _trancate(_Xmatr);  // trancate uncorrect zero

//    cout << "Shx" << endl; _print_X(_Xmatr);
}

void CSF_Diaglzr::_create_T(PrintEigesParam param) {

    const int t_sz = _dtsLst->size();

    // calc T-transformation
    diagonalize_Yacoby(t_sz, _Xmatr, _T_buffS, _buff_TR);

    // print L(L+1) eiges
    if (PRINT_STUFF_TRUE == param) {

        cout << "S(S+1) stuff from diag:" << endl;
        for (int i = 0; i < t_sz; ++i) {

            cout << _Xmatr[i*t_sz+i] << " ";
        }   cout << endl;
    }

    _trancate(_T_buffS);  // trancate uncorrect zero

//    cout << "S_Vec:" << endl; _print_X(_T_buffS);
}

void CSF_Diaglzr::_create_TR() {

    const int t_sz = _dtsLst->size();

    {for (int i = 0; i < t_sz; ++i) {
     for (int j = 0; j < t_sz; ++j) {

         double sum = 0;
         for (int k = 0; k < t_sz; ++k) {

             sum += _R_buffL[i*t_sz+k]*_T_buffS[k*t_sz+j];
         }
         _buff_TR[i*t_sz+j] = sum;
    }}}

    _trancate(_buff_TR); // trancate uncorrect zero

    //cout << "TR:" << endl; _print_X(_buff_TR);
}

void CSF_Diaglzr::_setup_LS_basic(PrintEigesParam param) {

    _calc_diagEigVec_LS();
    _calc_norm_TRk_LS(param);
    _ballistic_catch_LS();
}

void CSF_Diaglzr::_calc_diagEigVec_LS() {

    const int t_sz = _dtsLst->size();

    // create L2*V = L(L+1)V, S2*V = S(S+1)V
    for (int i = 0; i < t_sz; ++i) {
    for (int j = 0; j < t_sz; ++j) {

        double l_sum_ij = 0;  // L(L+1)*V_ij
        double s_sum_ij = 0;  // S(S+1)*V_ij

        for (int k = 0; k < t_sz; ++k) {

            l_sum_ij += _L2mtr(i,k)*_buff_TR[k*t_sz+j];
            s_sum_ij += _S2mtr(i,k)*_buff_TR[k*t_sz+j];
        }

        if ( L2S2_ZERO_ITEM > fabs(l_sum_ij) ) {

            l_sum_ij = L2S2_TRANCATE;
        }

        if ( L2S2_ZERO_ITEM > fabs(s_sum_ij) ) {

            s_sum_ij = L2S2_TRANCATE;
        }

        _R_buffL[i*t_sz+j] = l_sum_ij;  // L(L+1)_Vec - buffer
        _T_buffS[i*t_sz+j] = s_sum_ij;  // S(S+1)_Vec - buffer
    }}

//    cout << "diag(L(L+1))Vec" << endl; _print_X(_R_buffL);
//    cout << "diag(S(S+1))Vec" << endl; _print_X(_T_buffS);
}

void CSF_Diaglzr::_calc_norm_TRk_LS(PrintEigesParam param) {

    const int t_sz = _dtsLst->size();

    {for (int k = 0; k < t_sz; ++k) {

        _LL1_buff[k] = _norm_TRk_X(_R_buffL,k);
        _SS1_buff[k] = _norm_TRk_X(_T_buffS,k);
    }}

    if (PRINT_STUFF_TRUE == param) {

        cout << "diag_Vec_norm L:" << endl;
        {for (int k = 0; k < t_sz; ++k) {

            cout << _LL1_buff[k] << " ";
        }} cout << endl;
        cout << "diag_Vec_norm S:" << endl;
        {for (int k = 0; k < t_sz; ++k) {

            cout << _SS1_buff[k] << " ";
        }} cout << endl;
    }
}

void CSF_Diaglzr::_trancate(double *arr_X) {

    const int t_sz = _dtsLst->size();

    for (int i = 0; i < t_sz; ++i) {
    for (int j = 0; j < t_sz; ++j) {

        if ( L2S2_ZERO_ITEM > fabs(arr_X[i*t_sz+j]) ) {

            arr_X[i*t_sz+j] = L2S2_TRANCATE;
        }
    }}
}

void CSF_Diaglzr::_ballistic_catch_LS() {

    const int t_sz = _dtsLst->size();

    for (int k = 0; k < t_sz; ++k) {
        // L
        bool flag_no_L = true;
        for (double tL = 0; tL <= EIG_L_MAX; tL += 1.0) {

            if ( fabs(tL*(tL+1.0) - _LL1_buff[k]) < LL1_ERROR ) {

                _eig_L[k] = ((int) tL);
                flag_no_L = false;
                break;
            }
        }
        if (flag_no_L) {

            cout << "Error in CSF_Diaglzr::_ballistic_catch_LS()" << endl;
            cout << "Cannot catch L.." << endl;
            exit(0);
        }
        //S
        bool flag_no_S = true;
        for (double tS = 0; tS <= EIG_S_MAX; tS += 0.5) {

            if ( fabs(tS*(tS+1.0) - _SS1_buff[k]) < SS1_ERROR ) {

                _eig_S[k] = tS;
                flag_no_S = false;
                break;
            }
        }
        if (flag_no_S) {

            cout << "Error in CSF_Diaglzr::_ballistic_catch_LS()" << endl;
            cout << "Cannot catch S.." << endl;
            exit(0);
        }
    }
}

double CSF_Diaglzr::_norm_TRk_X(double *X, const int k) {

    const int t_sz = _dtsLst->size();
    const int i_max = _find_max_vecX(X,k);
    const double max_Vk = fabs(X[i_max*t_sz+k]);

    if (max_Vk < XXp1_ZERO) return 0.0;

    double sum = 0;
    {for (int j = 0; j < i_max; ++j) {

        const double eta = X[j*t_sz+k]/max_Vk;
        sum += eta*eta;
    }}
    {for (int j = i_max+1; j < t_sz; ++j) {

        const double eta = X[j*t_sz+k]/max_Vk;
        sum += eta*eta;
    }}

    return max_Vk*sqrt(sum + 1.0);
}

int CSF_Diaglzr::_find_max_vecX(double *X, const int k) {

    const int t_sz = _dtsLst->size();

    int i_max = 0;
    double v_max = fabs(X[i_max*t_sz+k]);

    for (int i = 1; i < t_sz; ++i) {

        if (v_max < fabs(X[i*t_sz+k])) {

            i_max = i;
            v_max = fabs(X[i*t_sz+k]);
        }
    }
    return i_max;
}

void CSF_Diaglzr::_create_1_eiges() {

//    _print_L2_S2();

    _LL1_buff[0] = _L2mtr(0,0);
    _SS1_buff[0] = _S2mtr(0,0);

    _buff_TR[0] = 1.0;
    _ballistic_catch_LS();
}

void CSF_Diaglzr::_print_L2_S2() {

    const int t_sz = _dtsLst->size();

    cout << "L2:" << endl;
    for (int i = 0; i < t_sz; ++i) {
    for (int j = 0; j < t_sz; ++j) {

        cout << _L2mtr(i,j) << " ";

    }cout << endl;}

    cout << "S2:" << endl;
    for (int i = 0; i < t_sz; ++i) {
    for (int j = 0; j < t_sz; ++j) {

        cout << _S2mtr(i,j) << " ";

    }cout << endl;}
}

void CSF_Diaglzr::_print_X(double *arrX) {

    const int t_sz = _dtsLst->size();

    for (int i = 0; i < t_sz; ++i) {
    for (int j = 0; j < t_sz; ++j) {

        cout << arrX[i*t_sz+j] << "  ";
    }cout << endl;}
}

////////////////////////////////////////////////////////////////////////////

qua5_Collection::qua5_Collection():
CSF_Diaglzr(), _q5Ind(new qua5_Index), _q3Ind(new qua3_Index) {
}

qua5_Collection::~qua5_Collection() {

    delete _q3Ind;
    delete _q5Ind;
}

void qua5_Collection::process(const DetsList * const ext_dtsLst,
                              qua5_RbTree * const q5Tree) {

    _dtsLst = ext_dtsLst;

    if (0 == _dtsLst) {

        cout << "Error qua5_Collection:: detsList have not been setup.." << endl;
        exit(0);
    }

    if (0 == q5Tree) {

        cout << "Error qua5_Collection:: q5Tree have not been setup.." << endl;
        exit(0);
    }

    if (Yacoby_MAX_SIZE < _dtsLst->size() ) {

        cout << "Error in qua5_Collection::process(" << endl;
        cout << "_dtsLst->size() = " << _dtsLst->size();
        cout << "  >  Yacoby_MAX_SIZE = " << Yacoby_MAX_SIZE << endl;
        exit(0);
    }

    _prepear_data();        // build and diagonalize
    _put_in_5tree(q5Tree);  // save indexes

    _dtsLst = 0;
}

void qua5_Collection::_put_in_5tree(qua5_RbTree * const q5Tree) {

    const int t_sz = _dtsLst->size();

    const Det_State& tDet = _dtsLst->firstDet();
    _q3Ind->reset(tDet.ML(),tDet.MS(),tDet.Parity());

    _q5Ind->reset_qua3(*_q3Ind);

    for (int k = 0; k < t_sz; ++k) {

        _q5Ind->reset_LS(_eig_L[k],_eig_S[k]);
        q5Tree->add(*_q5Ind, _dtsLst->size());
    }
}

////////////////////////////////////////////////////////////////////////////

CSF_Builder::CSF_Builder():CSF_Diaglzr(),
_q5Ind(0), _num_csf(0), _ind_csf(0),
_init_flg(false) {
}

CSF_Builder::~CSF_Builder() {

    _q5Ind = 0;

    delete[] _ind_csf; _ind_csf = 0;
}

void CSF_Builder::init(const DetsList * const ext_dtsLst) {

    if (_init_flg) {

        cout << "Error CSF_Builder::init();" << endl;
        cout << " CSF_Builder::free() was not called" << endl;
        exit(0);
    }

    _dtsLst = ext_dtsLst;

    if (0 == _dtsLst) {

        cout << "Error CSF_Builder::init() detsList have not been setup.." << endl;
        exit(0);
    }

    _ind_csf = new int[_dtsLst->size()];
    _num_csf = 0;

    _init_flg = true;
}

void CSF_Builder::process(const qua5_Index * const ext_q5Ind) {

   if (!_init_flg) {

        cout << "Error CSF_Builder::process();" << endl;
        cout << " CSF_Builder::init() was not called" << endl;
        exit(0);
    }

   if (0 == ext_q5Ind) {

        cout << "Error CSF_Builder:: qua5_Index have not been setup.." << endl;
        exit(0);
    }

    _q5Ind = ext_q5Ind;

    _prepear_data();
    _hash_index_of_CSF_by_LS();
}

void CSF_Builder::setup_csf(CSFunc_LS * const empty_CSF, const int indFnc) {

   if (!_init_flg) {

        cout << "Error CSF_Builder::setup_csf();" << endl;
        cout << " CSF_Builder::init() was not called" << endl;
        exit(0);
    }

    if (indFnc >= 0 && indFnc < _num_csf && empty_CSF) {

        const int t_sz = _dtsLst->size();

        empty_CSF->_size = t_sz;
        empty_CSF->_A   = new double[t_sz];
        empty_CSF->_det = new Det_State[t_sz];

        _dtsLst->to_head_1();
        int kk = 0;
        while (_dtsLst->next_1()) {

            empty_CSF->_A[kk] = _buff_TR[kk*t_sz+_ind_csf[indFnc]];
            empty_CSF->_det[kk].clone( *(_dtsLst->crnt_pDet_1()) );
            ++kk;
        }

        empty_CSF->_L  = _q5Ind->L();
        empty_CSF->_ML = _q5Ind->ML();
        empty_CSF->_S  = _q5Ind->S();
        empty_CSF->_MS = _q5Ind->MS();
        empty_CSF->_P  = _q5Ind->P();

    } else {

        cout << "Error in CSF_Builder::setup_csf.." << endl;
        cout << "ind_func = " << _num_csf << "  indFns= " << indFnc << endl;
        cout << "to_setup_CSF = " << empty_CSF << endl;
        exit(0);
    }
}

void CSF_Builder::free() {

    if (!_init_flg) {

        cout << "Error CSF_Builder::free();" << endl;
        cout << " CSF_Builder::init() was not called" << endl;
        exit(0);
    }

    _dtsLst = 0;
    _q5Ind  = 0;

    delete[] _ind_csf;  _ind_csf = 0;

    _init_flg = false;
}

void CSF_Builder::_hash_index_of_CSF_by_LS() {

    const int tsz = _dtsLst->size();

    _num_csf = 0;
    for (int i = 0; i < tsz; ++i) {

        if ( (_q5Ind->L() == _eig_L[i]) &&
             (fabs(_q5Ind->S() - _eig_S[i]) < SPIN_THRESHOLD) ) {

            _ind_csf[_num_csf++] = i;
        }
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file
