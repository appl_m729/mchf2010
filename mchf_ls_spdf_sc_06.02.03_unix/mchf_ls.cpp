#include <iostream>
#include <iomanip>
#include <cmath>
#include "mchf_ls.h"
#include "wflags.h"
#include "atom_conf.h"
#include "pnl_index_map.h"
#include "det_state.h"
#include "shells_det.h"
#include "csf_basis.h"
#include "energy_hash.h"
#include "one_list.h"
#include "two_list.h"
#include "variate_hash.h"
#include "ksi_hash.h"
#include "eta_hash.h"
#include "eta_rbtree.h"
#include "qbas_conf.h"
#include "pnl_radial.h"
#include "pnl_buffer.h"
#include "ham_matrix.h"
#include "ccv_list.h"
#include "vec_eq_syst.h"
#include "aconstants.h"
#include "num_e_trace.h"
#include "radial_rho.h"
#include "dq_iter_conf.h"

#define EVER ;;

////////////////////////////////////////////////////////////////////////////

MCHF_Solver::MCHF_Solver(const char file[]):
_tt_start(time(NULL)),
_w_flags(new writeFlags(file)),
_aCnf(new Atom_Conf(file)),
_pnlInd(new Pnl_IndexMap(_aCnf)),
_csfBas(new CSF_Basis(_aCnf)),
_hash_E(new Energy_Hash(_aCnf,_csfBas,_pnlInd)),
_oneLst(new One_List), 
_twoLst(new Two_List),
_variate(new Variate_Hash(_pnlInd,_oneLst,_twoLst)),
_hashKsi(new ksiHash_List[_pnlInd->square()]),
_hashEta(new etaHash_List[_pnlInd->square()]),
_etaTree(new Eta_rbTree),
_qBasCnf(new qBas_Conf(file)),
_radialP(new Radial_Pnl_Data(_etaTree,_pnlInd,_qBasCnf,_aCnf->Z())),
_buff_Pnl(new Pnl_Buffer(_pnlInd,_qBasCnf)),
_hamlt(new HamiltonianMtrx(_csfBas->size())),
_vecEqSyst(new VecEq_Syst(_etaTree,_hashKsi,_hashEta,_pnlInd,_qBasCnf,
                          new dqIter_Conf(file,_pnlInd))),
_trace_E(new NumE_Trace(_oneLst,_twoLst)),
_radial_rho(new Radial_Rho(file,_pnlInd,_oneLst)) {

    _hash_E->generate_One(_oneLst);
    _hash_E->generate_Two(_twoLst);

    _variate->generate_ksi(_hashKsi);
    _variate->generate_eta(_hashEta, _etaTree);

    _radialP->generate_TW();
}

MCHF_Solver::~MCHF_Solver() {

    delete   _radial_rho;
    delete   _trace_E;
    delete   _vecEqSyst;
    delete   _hamlt;
    delete   _buff_Pnl;
    delete   _radialP;
    delete   _qBasCnf;
    delete   _etaTree;
    delete[] _hashEta;
    delete[] _hashKsi;
    delete   _variate;
    delete   _oneLst;
    delete   _twoLst;
    delete   _hash_E;
    delete   _csfBas;
    delete   _pnlInd;
    delete   _aCnf;
    delete   _w_flags;
}

void MCHF_Solver::solve() {

    cout << endl << "MCHF_Solver: start calculating..." << endl;

    if (_w_flags->is_restore()) {
        _buff_Pnl->restore(_w_flags->restore_name());
    } else {
        _buff_Pnl->init1();
//        _buff_Pnl->init_sample();
    }

    for(EVER) {

        cout << endl << "iter = " << _buff_Pnl->k_iter() << endl;

        _radialP->update(_buff_Pnl);

        _oneLst->wholeUpdate(_radialP);
        _twoLst->wholeUpdate(_radialP);

        _hamlt->in_zero();
        _scatter_one_on_hamlt();
        _scatter_two_on_hamlt();
        _hamlt->calc_ground_state();

        cout << "E0 = " << setprecision(E_PRECISION_PRINT) << _hamlt->E0() << endl;
        _trace_E->update(_buff_Pnl->k_iter(),_hamlt);
        _trace_E->print();

        if ( (fabs(_trace_E->iter_dlt()) < _w_flags->E_acc()) ||
             (_buff_Pnl->k_iter() >= MCHF_MAX_ITER) ) break;

        _vecEqSyst->recalc(_radialP,_hamlt);

        _buff_Pnl->save(_vecEqSyst);

        if (_w_flags->is_dump()) {
            _buff_Pnl->dump(_w_flags->dump_name());
        }
    }

    _radial_rho->calc(_radialP,_hamlt);
    cout << "charge = " << _radial_rho->charge() << endl;

    if (MCHF_MAX_ITER == _buff_Pnl->k_iter()) {

        cout << "Warning! In MCHF main loop k_iter == MAX_ITER == " << MCHF_MAX_ITER << endl;
        cout << "Maybe weak convergence.." << endl;
    }

    cout << "MCHF_Solver: calculation successfully done." << endl;
    cout << "number of iter = " << _buff_Pnl->k_iter() << endl;

    if (_w_flags->is_e_trace()) {_trace_E->write(_w_flags->e_trace_name());}
    if (_w_flags->is_oneList()) {_oneLst->write(_w_flags->oneList_name()); }
    if (_w_flags->is_twoList()) {_twoLst->write(_w_flags->twoList_name()); }
    if (_w_flags->is_csfBas())  {_csfBas->write(_w_flags->csfBas_name());  }
    if (_w_flags->is_rho())     {_radial_rho->write(_w_flags->rho_name()); }
}

void MCHF_Solver::mesureTime() {

  tm *pBuffTime = localtime(&_tt_start);
  cout << endl << "Start time: " << asctime(pBuffTime);

  _tt_stop  = time(NULL);
  pBuffTime = localtime(&_tt_stop);
  cout << "Stop  time: " << asctime(pBuffTime);
}

void MCHF_Solver::_scatter_one_on_hamlt() {

    cout << "Scatter One on hamlt..";

    _oneLst->to_head();
    while (_oneLst->next()) {

       const double Iab = _oneLst->value();
       const ccv_List * const ccvLst = _oneLst->p_ccvLst();

       ccvLst->to_head();
       while (ccvLst->next()) {

           int i,j; double v;
           ccvLst->ijv(i,j,v);

           if (i == j) {
               _hamlt->add(i,j,v*Iab);
           } else {
               _hamlt->add(i,j,0.5*v*Iab);           
           }
       }
    }

    cout << ". done ok" << endl;
}

void MCHF_Solver::_scatter_two_on_hamlt() {

    cout << "Scatter Two on hamlt..";

    _twoLst->to_head();
    while (_twoLst->next()) {

        const double Rk = _twoLst->value();
        const ccv_List * const ccvLst = _twoLst->p_ccvLst();

        ccvLst->to_head();
        while (ccvLst->next()) {
        
            int i,j; double v;
            ccvLst->ijv(i,j,v);

            if (i == j) {
                _hamlt->add(i,j,v*Rk);
            } else {
                _hamlt->add(i,j,0.5*v*Rk);            
            }
        }
    }

    cout << ". done ok" << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file

