#include <iostream>
#include <cstdlib>
#include "ccv_list.h"

////////////////////////////////////////////////////////////////////////////

ccv_DataBlock::ccv_DataBlock():
next(0), i(-1), j(-1), value(0) {

}

ccv_DataBlock::ccv_DataBlock(const int ext_i, const int ext_j,
                             const double ext_Val):
next(0), i(ext_i), j(ext_j), value(ext_Val) {

}

ccv_DataBlock::~ccv_DataBlock() {
    delete next; next = 0;
}

////////////////////////////////////////////////////////////////////////////

ccv_List::ccv_List():
_phead(new ccv_DataBlock), _crnt_item(_phead),
_ptail(_phead), _size(0) {

}

ccv_List::~ccv_List() {

    _crnt_item = 0;
    _ptail = 0;
    delete _phead;
}

void ccv_List::add(const int ext_i, const int ext_j, const double ext_Val) {

    _ptail->next = new ccv_DataBlock(ext_i,ext_j,ext_Val);
    _ptail = _ptail->next;
    ++_size;
}

void ccv_List::to_head() const {
    _crnt_item = _phead;
}

bool ccv_List::next() const {

    if ( _crnt_item->next ) {

        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int ccv_List::size() const {
    return _size;
}

int ccv_List::i() const {
    return _crnt_item->i;
}

int ccv_List::j() const {
    return _crnt_item->j;
}

double ccv_List::value() const {
    return _crnt_item->value;
}

void ccv_List::ijv(int &ext_i, int &ext_j, double &ext_val) const {

    ext_i   = _crnt_item->i;
    ext_j   = _crnt_item->j;
    ext_val = _crnt_item->value;
}

void ccv_List::clear() {

    if (_phead->next) {
        delete _phead->next; _phead->next = 0;
    }

    _crnt_item = _phead;
    _ptail     = _phead;

    _size = 0;
}

////////////////////////////////////////////////////////////////////////////

// end of file

