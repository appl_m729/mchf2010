#include <iostream>
#include <cstdlib>
#include "two_rbtree.h"
#include "k_abcd_index.h"
#include "ccv_rbtree.h"
#include "two_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Two_RB_node::Two_RB_node():
left(0), right(0), red(1), index_k_abcd(0), ccvTree(0) {
}

Two_RB_node::~Two_RB_node() {

    delete index_k_abcd; index_k_abcd = 0;
    delete ccvTree;      ccvTree      = 0;

    delete left;         left         = 0;
    delete right;        right        = 0;
}

////////////////////////////////////////////////////////////////////////////

Two_RB_Tree::Two_RB_Tree():_head(0), _size(0), _twoLst(0) {
}

Two_RB_Tree::~Two_RB_Tree() {

    _twoLst = 0;
    delete _head; _head = 0;
}

void Two_RB_Tree::generate_TwoList(Two_List * const twoLst) const {

    _twoLst = twoLst;
    _catch_next_node(_head);
    _twoLst = 0;
}

void Two_RB_Tree::add(const k_abcd_Index& twoInd, 
                      const int i, const int j, const double cVal) {

    _insert(twoInd);
    _RB_search(_head,twoInd)->ccvTree->add(i,j,cVal);
}

void Two_RB_Tree::_rotR(Two_RB_node* &h) {

    Two_RB_node* x = h->left;
    h->left = x->right;
    x->right = h;
    h = x;
}

void Two_RB_Tree::_rotL(Two_RB_node* &h) {

    Two_RB_node* x = h->right;
    h->right = x->left;
    x->left = h;
    h = x;
}

int Two_RB_Tree::_is_red(Two_RB_node *x) {

    if (0 == x) return 0;
    return x->red;
}

void Two_RB_Tree::_insert(const k_abcd_Index& twoInd) {

    _RB_insert(_head, twoInd,0);
    _head->red = 0;
}

void Two_RB_Tree::_RB_insert(Two_RB_node* &h,const k_abcd_Index& insTwoInd, int sw) {

    if (0 == h) {

        h = new Two_RB_node;
        h->index_k_abcd = new k_abcd_Index(insTwoInd);
        h->ccvTree = new ccv_RB_Tree;

        ++_size;
    }

    if (_is_red(h->left) && _is_red(h->right)) {

        h->red = 1; 
        h->left->red  = 0; 
        h->right->red = 0;
    }

    if (insTwoInd > (*(h->index_k_abcd)) ) {

        _RB_insert(h->right,insTwoInd,1);

        if (_is_red(h) && _is_red(h->right) && !sw) {_rotL(h);}

        if (_is_red(h->right) && _is_red(h->right->right)) {
             _rotL(h);
            h->red = 0;
            h->left->red = 1;
        }
    } else if ( !( insTwoInd == (*(h->index_k_abcd)) ) ) {

        _RB_insert(h->left,insTwoInd,0);

        if (_is_red(h) && _is_red(h->left) && sw){_rotR(h);}

        if (_is_red(h->left) && _is_red(h->left->left)) {
            _rotR(h);
            h->red = 0;
            h->right->red = 1;
        }
    }
}

Two_RB_node* Two_RB_Tree::_RB_search(Two_RB_node* h, const k_abcd_Index& ptrnTwoInd) {

    if (0 == h) {

        cout << "Error Two_RB_Tree::_RB_find(" << endl;
        cout << "No such index: "; ptrnTwoInd.print();
        exit(0);
    }

    if ( ptrnTwoInd == (*(h->index_k_abcd)) ) {
        return h;
    }

    if ( ptrnTwoInd > (*(h->index_k_abcd)) ) {
        return _RB_search(h->right, ptrnTwoInd);
    } else {
        return _RB_search(h->left, ptrnTwoInd);
    }
}

void Two_RB_Tree::_catch_next_node(const Two_RB_node* h) const {

    if ( 0 != h ) {

        _twoLst->add(h->index_k_abcd, h->ccvTree);

        _catch_next_node(h->left);
        _catch_next_node(h->right);
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

