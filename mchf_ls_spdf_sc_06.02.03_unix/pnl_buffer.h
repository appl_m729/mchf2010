#ifndef _Pnl_BUFFER_H_
#define _Pnl_BUFFER_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Pnl_IndexMap;
class qBas_Conf;
class VecEq_Syst;

class Pnl_Buffer : private Uncopyable {

public:

    Pnl_Buffer(const Pnl_IndexMap * const pnlIndex,
               const qBas_Conf * const basCnf);
    ~Pnl_Buffer();

    void restore(const char file[]);
    void init_zero();
    void init1();
    void init_sample();

    double b(const int a, const int i) const;
    int k_iter() const;

    void save(const VecEq_Syst* const veqSst);
    void dump(const char file[]) const;

private:

    const Pnl_IndexMap * const _pnlInd;
    const qBas_Conf  * const _basCnf;

    int _k_iter;
    double * const _b;

    void   _normilize(const int a);
    void   _print_b_data() const;
    int    _find_max(const int a, const int i1, const int i2) const;
    double _bsqrsum(const int a, const int i1, const int i2, const int i_max) const;
};

////////////////////////////////////////////////////////////////////////////

#endif // _Pnl_BUFFER_H_

// end of file

