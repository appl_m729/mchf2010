#ifndef _TWO_HASH_LIST_H_
#define _TWO_HASH_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class k_abcd_Index;
class ccv_RB_Tree;
class ccv_List;
class Radial_Pnl_Data;


class Two_DataBlock : private Uncopyable {

public:

    Two_DataBlock *next;

    const k_abcd_Index* index_k_abcd;
    double  value;
    ccv_List * ccvLst;

                                      
    Two_DataBlock();
    ~Two_DataBlock();
};


class Two_List : private Uncopyable {

public:

    Two_List();
    ~Two_List();

    void wholeUpdate(const Radial_Pnl_Data * const Radial_data);

    void add(const k_abcd_Index * const index_k_abcd,
             const ccv_RB_Tree * const ccvTree);

    void to_head() const;
    bool next() const;

    int size() const;

    const k_abcd_Index& twoInd() const;
    double value() const;
    const ccv_List& ccvLst() const;
    const ccv_List* p_ccvLst() const;

    void write(const char file[]) const;

private:

    Two_DataBlock * const _head_item;
    mutable Two_DataBlock* _crnt_item;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _TWO_HASH_LIST_H_

// end of file

