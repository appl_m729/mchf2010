#ifndef _Yk_HASH_INDEX_LIST_H_
#define _Yk_HASH_INDEX_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class k_ab_Index;

class k_ab_DataBlock : private Uncopyable {

public:

    k_ab_DataBlock* next;

    const k_ab_Index* k_ab_ind;
    int linearIndex;

    k_ab_DataBlock();
    ~k_ab_DataBlock();
};


class k_ab_List : private Uncopyable {

public:

    k_ab_List();
    ~k_ab_List();

    void clear();

    void add(const k_ab_Index* kabInd, const int linearIndex);
    void to_head() const;
    bool next() const;

    const k_ab_Index& k_ab_Ind() const;
    int linInd() const;

    int size() const;
    void write(const char file[]) const;

private:

    k_ab_DataBlock * const _head;
    mutable k_ab_DataBlock* _crnt_item;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _Yk_HASH_INDEX_LIST_H_

// end of file
