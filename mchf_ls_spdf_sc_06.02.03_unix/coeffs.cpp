#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include "coeffs.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

CompleteCoeffs::CompleteCoeffs():
_LengthPN(2005), _maxPPLength(0),
_PrimeNumber(new int[_LengthPN]), 
_PrimePower (new int[_LengthPN]) {

    _SetupPN();
}

CompleteCoeffs::~CompleteCoeffs() {

    delete[] _PrimeNumber;
    delete[] _PrimePower;
}

double CompleteCoeffs::clebsh(const double j1, const double mj1,
                              const double j2, const double mj2,
                              const double J,  const double MJ) {

    if ( fabs(mj1+mj2 - MJ) > PREC_OFF) return 0;
    if ( !_is_intn(j1+j2+J,5) ) return 0;

    if ( j1+j2-J < 0.0) return 0;
    if ( j1-j2+J < 0.0) return 0;
    if (-j1+j2+J < 0.0) return 0;

    if (fabs(mj1) > j1) return 0;
    if (fabs(mj2) > j2) return 0;
    if (fabs(MJ ) > J ) return 0;

    if (!_is_intn(j1+mj1,5)) return 0;
    if (!_is_intn(j2+mj2,5)) return 0;
    if (!_is_intn(J +MJ ,5)) return 0;

    if( ((int) fmod(J+mj1+mj2,2.0) ) == 0 ) {
        return  sqrt(2.0*J+1.0)*_V(j1,j2,J,mj1,mj2,-MJ);
    }else{
        return -sqrt(2.0*J+1.0)*_V(j1,j2,J,mj1,mj2,-MJ);
    }
}

double CompleteCoeffs::symb3j(const double j1, const double j2, const double j3,
                              const double m1, const double m2, const double m3) {

    if (fabs(m1 + m2 + m3) > PREC_OFF) {
        cout<<"error 3j: m1+m2+m3 != 0; exiting..."<<endl;
        exit(0);
    }

    if ( ((int) fmod(j3 + m3 + 2.0*j1, 2.0)) == 0) {
        return clebsh(j1,-m1,j2,-m2,j3,m3)/sqrt(2.0*j3 + 1.0);
    }else{
        return -1.0*clebsh(j1,-m1,j2,-m2,j3,m3)/sqrt(2.0*j3 + 1.0);
    }
}

double CompleteCoeffs::symb6j(const double j1, const double j2,
                              const double j3, const double j4,
                              const double j5, const double j6) {

    if ( ((int) fmod(2*(j1+j2+j3),2)) != 0) {
        cout << "Error in 6j-symb 1)" << endl;
        exit(0);
    }

    if ( ((int) fmod(2*(j1+j5+j6),2)) != 0) {
        cout << "Error in 6j-symb 2)" << endl;
        exit(0);
    }

    if ( ((int) fmod(2*(j4+j2+j6),2)) != 0) {
        cout << "Error in 6j-symb 3)" << endl;
        exit(0);
    }

    if ( ((int) fmod(2*(j4+j5+j3),2)) != 0) {
        cout << "Error in 6j-symb 4)" << endl;
        exit(0);
    }

    const int z_min = _max(_max(int(j1 + j2 + j3),int(j1 + j5 + j6)),
                           _max(int(j4 + j2 + j6),int(j4 + j5 + j3)));

    const int z_max = _min(_min(int(j1 + j2 + j4 + j5),int(j2 + j3 + j5 + j6)),
                           int(j3 + j1 + j6 + j4));

    double sum = 0.;
    for(int z = z_min; z <= z_max; ++z) {

        if( ((int) fmod(double(z),2.0)) == 0) {
            sum += _AddOneAdditive_6j(j1,j2,j3,j4,j5,j6,z);
        } else {
            sum -= _AddOneAdditive_6j(j1,j2,j3,j4,j5,j6,z);
        }
    }
    return sum;
}

double CompleteCoeffs::gonth(const int k, const int l1, const int m1,
                                          const int l2, const int m2) {

    if ( ((int) fmod(double(k + l1 + l2),2.0)) != 0) return 0;
    if (k > (l1+l2) ) return 0;
    if (k < fabs(l1-l2) ) return 0;
    if (fabs(m1) > l1) return 0;
    if (fabs(m2) > l2) return 0;

    if ( ((int) fmod(double(m1),2.0)) == 0) {
        return sqrt((2.*l1+1.)*(2.*l2+1.))*_V(l1,l2,k,-m1,m2,m1-m2)*_V(l1,l2,k,0,0,0);
    }else{
        return -sqrt((2.*l1+1.)*(2.*l2+1.))*_V(l1,l2,k,-m1,m2,m1-m2)*_V(l1,l2,k,0,0,0);
    }
}

int CompleteCoeffs::binomC(const int n, const int k) {

    _ZeroPrimePower();
    _AddFactOfNumberAsPrimePowers(n,1);
    _AddFactOfNumberAsPrimePowers(k,-1);
    _AddFactOfNumberAsPrimePowers(n-k,-1);

    double buff=1.;
    for(int i = 1; i <= _maxPPLength; ++i)
        buff *= _IntPow(_PrimeNumber[i],_PrimePower[i]);
    return int(buff);
}

void CompleteCoeffs::zero() {
    _ZeroPrimePower();
}

void CompleteCoeffs::add_fact(const int n, const int key) {
    _AddFactOfNumberAsPrimePowers(n,key);
}

void CompleteCoeffs::add_numb(const int n, const int key) {
    _AddOneNumberAsPrimePowers(n,key);
}

double CompleteCoeffs::get() {

    double buff=1.;
    for(int i = 1; i <= _maxPPLength; ++i) {
        buff *= _IntPow(_PrimeNumber[i],_PrimePower[i]);
    }
    return buff;
}

int CompleteCoeffs::_min(const int a, const int b) const {
    if ( a < b ) return a;
    return b;
}

int CompleteCoeffs::_max(const int a, const int b) const {
    if ( a > b ) return a;
    return b;
}

void CompleteCoeffs::_SetupPN() {

    _PrimeNumber[0] = 1;
    _PrimeNumber[1] = 2;
    for (int i = 2; i < _LengthPN; ++i) {
        _PrimeNumber[i] = _FindNewPrime(i);
    }
}

int CompleteCoeffs::_FindNewPrime(const int i) {

    int Value = _PrimeNumber[i-1];
    bool flag;

    do{
        flag = false;
        ++Value;
        for(int j=1; j < i; ++j) {
            if ( ((int) fmod(double(Value),double(_PrimeNumber[j])) ) == 0) {
                flag = true;
                break;
            }
        }
    } while (flag);

    return Value;
}

// This function is only for evaluating by formula in Bethe's book.
// It is equivalent to the 3j-symbol and differ from
// the last by the phase (-1)^... factor.
double CompleteCoeffs::_V(const double a, const double b, const double c,
                          const double a_,const double b_,const double c_) {

    if ( ((int) fmod(2*(a+b+c),2) ) != 0){
        cout << "Error _V(): j1+j2+j3 is odd; exiting..." << endl;
        exit(0);
    }
    const double dec = 10.0;
    if ( ((int) fmod((c-c_)*dec,dec) ) != 0){
        cout << "power pow(-1.0,c-c_+z) in _V() is not integer; exiting.." << endl;
        exit(0);
    }
    const int z_min = -_min(_min(int(-b+c+a_),int(-a+c-b_)),0);
    const int z_max =  _min(_min(int(a+b-c),int(a-a_)),int(b+b_));
    double sum=0.;
    for(int z=z_min; z <= z_max; z++){
        if( ((int) fmod(c-c_+z,2.0) ) == 0){
            sum += _AddOneAdditive_3j(a,b,c,a_,b_,c_,z);
        }else{
            sum -= _AddOneAdditive_3j(a,b,c,a_,b_,c_,z);
        }
    }
    return sum;
}

void CompleteCoeffs::_AddTrngl(const double a, const double b, const double c) {
/* Warning! Use this function carefully.                         *
 * It adds powers in Power-array >without preliminary zeroing<.  *
 * But if need, use zeroing() in another "basic function" before *
 * using this one.                                               */

    _AddFactOfNumberAsPrimePowers(int(a+b-c),1);
    _AddFactOfNumberAsPrimePowers(int(a-b+c),1);
    _AddFactOfNumberAsPrimePowers(int(-a+b+c),1);
    _AddFactOfNumberAsPrimePowers(int(a+b+c)+1,-1);
}

double CompleteCoeffs::_AddOneAdditive_3j(const double a, const double b, 
                                          const double c, const double a_,
                                          const double b_,const double c_, const int z) {
    _ZeroPrimePower();
    _AddFactOfNumberAsPrimePowers(int(a+b-c),1);
    _AddFactOfNumberAsPrimePowers(int(a-b+c),1);
    _AddFactOfNumberAsPrimePowers(int(-a+b+c),1);
    _AddFactOfNumberAsPrimePowers(int(a+b+c)+1,-1);
    _AddFactOfNumberAsPrimePowers(int(a+a_),1);
    _AddFactOfNumberAsPrimePowers(int(a-a_),1);
    _AddFactOfNumberAsPrimePowers(int(b+b_),1);
    _AddFactOfNumberAsPrimePowers(int(b-b_),1);
    _AddFactOfNumberAsPrimePowers(int(c+c_),1);
    _AddFactOfNumberAsPrimePowers(int(c-c_),1);
    _AddFactOfNumberAsPrimePowers(z,-2);
    _AddFactOfNumberAsPrimePowers(int(a+b-c-z),-2);
    _AddFactOfNumberAsPrimePowers(int(a-a_-z),-2);
    _AddFactOfNumberAsPrimePowers(int(b+b_-z),-2);
    _AddFactOfNumberAsPrimePowers(int(-b+c+a_+z),-2);
    _AddFactOfNumberAsPrimePowers(int(-a+c-b_+z),-2);

    double buff=1.;
    for(int i=1; i <= _maxPPLength; ++i)
        buff *= _IntPow(_PrimeNumber[i],_PrimePower[i]);
    return sqrt(buff);
}

double CompleteCoeffs::_AddOneAdditive_6j(const double j1, const double j2,
                                          const double j3, const double j4,
                                          const double j5, const double j6, const int z) {
    _ZeroPrimePower();
    _AddTrngl(j1,j2,j3);
    _AddTrngl(j1,j5,j6);
    _AddTrngl(j4,j2,j6);
    _AddTrngl(j4,j5,j3);
    _AddFactOfNumberAsPrimePowers(z+1,2);
    _AddFactOfNumberAsPrimePowers(int(z-j1-j2-j3),-2);
    _AddFactOfNumberAsPrimePowers(int(z-j1-j5-j6),-2);
    _AddFactOfNumberAsPrimePowers(int(z-j4-j2-j6),-2);
    _AddFactOfNumberAsPrimePowers(int(z-j4-j5-j3),-2);
    _AddFactOfNumberAsPrimePowers(int(j1+j2+j4+j5-z),-2);
    _AddFactOfNumberAsPrimePowers(int(j2+j3+j5+j6-z),-2);
    _AddFactOfNumberAsPrimePowers(int(j3+j1+j6+j4-z),-2);

    double buff = 1.;
    for(int i=1; i <= _maxPPLength; ++i)
        buff *= _IntPow(_PrimeNumber[i],_PrimePower[i]);
    return sqrt(buff);
}

void CompleteCoeffs::_ZeroPrimePower() {

    for(int i=0; i < _LengthPN; ++i) {
        _PrimePower[i] = 0;
    }
    _maxPPLength=0;
}

double CompleteCoeffs::_IntPow(const int numb, const int power) {

    if(power > 0){
        double temp = 1.;
        for(int k=1; k <= power; ++k){
            temp *= numb;
        }
        return temp;
    }else if(power<0){
        double temp=1.;
        for(int k=1; k <= -power; ++k){
            temp*=numb;
        }
        return 1.0/temp;
    }
    return 1.0;
}

void CompleteCoeffs::_AddFactOfNumberAsPrimePowers(const int Number, const int key) {

    for(int k=2; k <= Number; ++k){
        _AddOneNumberAsPrimePowers(k,key);
    }
}

void CompleteCoeffs::_AddOneNumberAsPrimePowers(const int k, const int key) {

    int Val = k; int i = 1;
    do {
        if( ((int) fmod(double(Val),double(_PrimeNumber[i]))) ==0) {
            Val /= _PrimeNumber[i];
            _PrimePower[i] += key;
        } else {
            ++i;
        }
    } while ( (Val != 1) && (i < _LengthPN) );
    if (_maxPPLength < i) _maxPPLength = i;
}

bool CompleteCoeffs::_is_intn(double val, const int nd) {

    if(nd < 1 || nd > 14) {
        cout << "Error in _is_intn(double val, int nd){... nd is out of range " << endl;
        return false;
    }
    val = fabs(val);
    //cout << "val= " << val << " nd= " << nd << endl;

    double frac = val - floor(val);
    //cout << "frac= " << frac << endl;

    int rest = (int) ceil(frac*_IntPow(10, nd) - c05);
    //cout << setprecision(20) << "rest= " << rest << endl;

    if ( rest == 0) {
        return true;
    }

    return false;
}

////////////////////////////////////////////////////////////////////////////

CompleteCoeffs& ccf() {

    static CompleteCoeffs t_ccf;

    return t_ccf;
}

////////////////////////////////////////////////////////////////////////////

// end of file

