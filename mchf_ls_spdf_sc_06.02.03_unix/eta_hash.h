#ifndef _ETA_HASH_H_
#define _ETA_HASH_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class ccv_List;
class k_ab_Index;

class etaHash_DataBlock : private Uncopyable {

public:

    etaHash_DataBlock * next;

    int l1,l2, diag_factor;
    const k_ab_Index * kabInd;
    const ccv_List * ccvLst;

    etaHash_DataBlock();
    ~etaHash_DataBlock();
};


class etaHash_List : private Uncopyable {

public:

    etaHash_List();
    ~etaHash_List();

    void clear();

    void add(const ccv_List * ccvLst,
             const int l1, const int l2, const k_ab_Index& kabInd);

    void to_head() const;
    bool next() const;

    int diag_factor() const;
    const ccv_List& ccvLst() const;
    int l1() const;
    int l2() const;
    const k_ab_Index& kabInd() const;

    int size() const {return _size;}

private:

    etaHash_DataBlock * const _phead;
    mutable etaHash_DataBlock * _crnt_item;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif // _ETA_HASH_H_

// end of file

