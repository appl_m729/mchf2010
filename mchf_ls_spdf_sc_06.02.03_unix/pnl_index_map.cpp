#include <iostream>
#include <cstdlib>
#include "pnl_index_map.h"
#include "atom_conf.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Pnl_IndexMap::Pnl_IndexMap(const Atom_Conf * const aCnf):
_length(0), _n(0), _l(0), _lmax(0),
_llen(0), _max_llen(0), _a_sta(0), _a_end(0) {

    if (!aCnf) {
        cout << "Error in Pnl_IndexMap;  aCnf = 0" << endl;
        exit(0);
    }

    _length = aCnf->core_Nsubsh() + aCnf->walk_Nsubsh() + aCnf->hill_Nsubsh();
    _n = new int[_length];
    _l = new int[_length];
    _setup_nl(aCnf);
    _sort_nl();

    _lmax = _l[_length-1];
    _llen  = new int[_lmax+1];
    _a_sta = new int[_lmax+1];
    _a_end = new int[_lmax+1];
    _setup_llen_aa();

    print();
}

Pnl_IndexMap::~Pnl_IndexMap() {

    delete[] _n;     _n     = 0;
    delete[] _l;     _l     = 0;
    delete[] _llen;  _llen  = 0;
    delete[] _a_sta; _a_sta = 0;
    delete[] _a_end; _a_end = 0;
}

int Pnl_IndexMap::a(const int ext_n, const int ext_l) const {

    for (int a = 0; a <= _length/2; ++a) {

        if (ext_n == _n[a] && ext_l == _l[a]) return a;
        if (ext_n == _n[_length-a] && ext_l == _l[_length-a]) return _length-a;
    }

    cout << "Error in Pnl_Index_Map::a;  nl- is out of range" << endl;
    cout << "n = " << ext_n << " l = " << ext_l << endl;
    exit(0);
}

int Pnl_IndexMap::n(const int i) const {

    _check_i(i);
    return _n[i];
}

int Pnl_IndexMap::l(const int i) const {

    _check_i(i);
    return _l[i];
}

int Pnl_IndexMap::shift(const int a) const {

    _check_i(a);
    return _n[a] - _l[a] - 1;
//    return a - a_sta(_l[a]);
}

int Pnl_IndexMap::len(const int l) const {

    _check_l(l);
    return _llen[l];
}

int Pnl_IndexMap::a_sta(const int l) const {

    _check_l(l);
    return _a_sta[l];
}

int Pnl_IndexMap::a_end(const int l) const {

    _check_l(l);
    return _a_end[l];
}

void Pnl_IndexMap::print() const {

    cout << endl << "Pnl_Index_Map:" << endl;
    cout << "length = " << length() << endl;

    for (int a = 0; a < length(); ++a) {
        cout << "(" << n(a) << "," << l(a) << ")->" << a << endl;
    }

    cout << "lmax = " << lmax() << endl;
    cout << "llen (a_sta,a_end);" << endl;
    for (int ll = 0; ll <= lmax(); ++ll) {
    
        cout << len(ll) << " (" << a_sta(ll) << "," << a_end(ll) << "); ";
    }
    cout << endl << "max_llen = " << max_llen() << endl;
}

void Pnl_IndexMap::_setup_nl(const Atom_Conf * const aCnf) {

    int pos = 0;

    {for (int i = 0; i < aCnf->core_Nsubsh(); ++i, ++pos) {

        _n[pos] = aCnf->core_n(i);
        _l[pos] = aCnf->core_l(i);
    }}

    {for (int i = 0; i < aCnf->walk_Nsubsh(); ++i, ++pos) {

        _n[pos] = aCnf->walk_n(i);
        _l[pos] = aCnf->walk_l(i);
    }}

    {for (int i = 0; i < aCnf->hill_Nsubsh(); ++i, ++pos) {

        _n[pos] = aCnf->hill_n(i);
        _l[pos] = aCnf->hill_l(i);
    }}
}

void Pnl_IndexMap::_setup_llen_aa() {

    int ll = 0;

    // a_sta, a_end
    _a_sta[0] = 0;
    for (int a = 0; a < _length-1; ++a) {
                                         
        if (_l[a] != _l[a+1]) {

            _a_end[ll++] = a;
            _a_sta[ll]   = a+1;
        }
    }
    _a_end[_lmax] = _length - 1;

    // _llen, _max_llen
    _max_llen = 0;
    for (ll = 0; ll <= _lmax; ++ll) {

        _llen[ll] = _a_end[ll] - _a_sta[ll] + 1;

        if (_llen[ll] > _max_llen) _max_llen = _llen[ll];
    }
}

void Pnl_IndexMap::_sort_nl() {

    for (int i = 0;   i < _length - 1; ++i) {
    for (int j = i+1; j < _length;     ++j) {

        if ( (_l[i] > _l[j]) || (_l[i] == _l[j] && _n[i] > _n[j]) ) {

            // swap
            const int tn = _n[i];
            const int tl = _l[i];

            _n[i] = _n[j];
            _l[i] = _l[j];

            _n[j] = tn;
            _l[j] = tl;
        }    
    }}
}

void Pnl_IndexMap::_check_i(const int i) const {

    if (i < 0 || i >= _length) {

        cout << "Error in Pnl_IndexMap::_check_i( i is out of range...  ";
        cout << "_length = " << _length << " i = " << i << endl;
        exit(0);
    }
}

void Pnl_IndexMap::_check_l(const int l) const {

    if (l < 0 || l > _lmax) {

        cout << "Error in Pnl_IndexMap::_check_l(..  l = ";
        cout << l << " lmax = " << _lmax << endl;
        exit(0);
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

