#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include "mdiis_engine.h"
#include "qr_decomp.h"
#include "vec_eq_conf.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

mDIIS_Engine::mDIIS_Engine(const int max_depth, const VecEq_Conf * const VeqCnf):
_veqCnf(VeqCnf), _qrDecomp(new QR_Decompose(max_depth+1)),
_max_Depth(max_depth), _crnt_Depth(0),
_ind(0), _x(0), _dx(0), _dxdx(0), _c(0) {

    if (_max_Depth < 1) {

        cout << "Error in mDIIS_Engine" << endl;
        cout << "max_Depth = " << _max_Depth << endl;
        exit(0);
    }

    if (!_veqCnf) {

        cout << "Error in mDIIS_Engine.." << endl;
        cout << "  _veqCnf = " << _veqCnf << endl;
        exit(0);
    }

    _ind  = new int[_max_Depth];

    _x    = new double[_max_Depth*_veqCnf->x_tsz()];
    _dx   = new double[_max_Depth*_veqCnf->x_tsz()];

    _dxdx = new double[_max_Depth*_max_Depth];
    _c    = new double[_max_Depth+1];

    // setup indexes
    for (int k = 0; k < _max_Depth; ++k) {_ind[k] = k;}
}

mDIIS_Engine::~mDIIS_Engine() {

    delete   _qrDecomp;

    delete[] _ind;  _ind  = 0;
    delete[] _x;    _x    = 0;
    delete[] _dx;   _dx   = 0;
    delete[] _dxdx; _dxdx = 0;
    delete[] _c;    _c    = 0;
}

void mDIIS_Engine::add(const double * const x, const double * const dq) {

    if (_crnt_Depth < _max_Depth) {

        _crnt_Depth++;

    } else {

        _shift_indexes();
    }    
    _put_x_dq_in_array(x,dq);
}

void mDIIS_Engine::produce(double * const x_new) {

    _calc_new_dxidxj_values();
    _setup_qr();
    _calc_ck();
    _calc_new_x(x_new);
}

void mDIIS_Engine::_shift_indexes() {

    const int tmp_ind = _ind[0];

    for (int k = 0; k < _max_Depth-1; ++k) {
        _ind[k] = _ind[k+1];
    }
    _ind[_max_Depth-1] = tmp_ind;
}

void mDIIS_Engine::_put_x_dq_in_array(const double * const x, const double * const dq) {

    const int pos = _ind[_crnt_Depth-1]*_veqCnf->x_tsz();

    for (int i = 0; i < _veqCnf->x_tsz(); ++i) {

        _x[pos + i] =  x[i];
       _dx[pos + i] = dq[i];
    }
}

void mDIIS_Engine::_calc_new_dxidxj_values() {

    const int pos_n = _ind[_crnt_Depth-1]*_veqCnf->x_tsz();

    for (int k = 0; k < _crnt_Depth; ++k) {

        const int pos_k = _ind[k]*_veqCnf->x_tsz();

        double sum = 0;
        for (int i = 0; i < _veqCnf->x_tsz(); ++i) {
            sum += _dx[pos_n +i]*_dx[pos_k +i];
        }
        
        _dxdx[_ind[k]*_max_Depth+_ind[_crnt_Depth-1]] = sum;
        _dxdx[_ind[_crnt_Depth-1]*_max_Depth+_ind[k]] = sum;
    }
}

void mDIIS_Engine::_setup_qr() {

    _qrDecomp->resize(_crnt_Depth+1);

    for (int k1 = 0; k1 < _crnt_Depth; ++k1) {
    for (int k2 = 0; k2 < _crnt_Depth; ++k2) {

        _qrDecomp->set(k1,k2,_dxdx[k1*_max_Depth+k2]);
    }}

    for (int k = 0; k < _crnt_Depth; ++k) {

        _qrDecomp->set(k,_crnt_Depth,1);
        _qrDecomp->set(_crnt_Depth,k,1);
    }
    _qrDecomp->set(_crnt_Depth,_crnt_Depth,0);
}

void mDIIS_Engine::_calc_ck() {

    _qrDecomp->decompose();

    for (int i = _crnt_Depth; i >=0; --i) {

        if (fabs(_qrDecomp->R(i,i)) < 1e-300) {

            cout << "Error in mDIIS_Engine:  _qrDecomp->R(i,i)" << endl;
            cout << "i = " << i << " _qrDecomp->R(i,i) = " << _qrDecomp->R(i,i) << endl;
            exit(0);
        }

        double sum = 0;
        for (int j = i+1; j <= _crnt_Depth; ++j) {

            sum += _c[j]*_qrDecomp->R(i,j);
        }
        _c[i] = (_qrDecomp->Q(_crnt_Depth,i) - sum)/_qrDecomp->R(i,i);
    }
}

void mDIIS_Engine::_calc_new_x(double * const x) const {

    for (int i = 0; i < _veqCnf->x_tsz(); ++i) {

        double sum_xi  = 0;
        double sum_dxi = 0;

        for (int k = 0; k < _crnt_Depth; ++k) {

            const int pos = k*_veqCnf->x_tsz();

            sum_xi  += _c[k]* _x[pos + i];
            sum_dxi += _c[k]*_dx[pos + i];
        }
        x[i] = sum_xi + sum_dxi;
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file
