#ifndef _STEPEN_K_AB_HASHINDEX_RBTREE_H_
#define _STEPEN_K_AB_HASHINDEX_RBTREE_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class s_k_ab_Index;
class s_k_ab_List;

class s_k_ab_rbNode : private Uncopyable {

public:

    s_k_ab_rbNode *left, *right;
    int red;

    int linearIndex;
    s_k_ab_Index * s_k_ab_ind;

    s_k_ab_rbNode();
    ~s_k_ab_rbNode();
};


class s_k_ab_rbTree : private Uncopyable {

public:

    s_k_ab_rbTree();
    ~s_k_ab_rbTree();

    void insert(const s_k_ab_Index& skabInd);
    int  linIndex(const s_k_ab_Index& skabInd) const;
    int  size() const {return _linInd + 1;}
    void clear();

    void linearize(s_k_ab_List * const skabLst, const char * stxt);

private:

    s_k_ab_List   * _skabLst;
    s_k_ab_rbNode * _head;
    int             _linInd;

    void _rotR(s_k_ab_rbNode* &h);
    void _rotL(s_k_ab_rbNode* &h);
    int  _is_red(s_k_ab_rbNode *x);

    void _RB_insert(s_k_ab_rbNode* &h, const s_k_ab_Index& ins_skabInd, int sw);
    const s_k_ab_rbNode * const _RB_search(const s_k_ab_rbNode * const h, 
                                           const s_k_ab_Index& prnt_skabInd) const;
    void _RB_traversal(s_k_ab_rbNode * h);
};

////////////////////////////////////////////////////////////////////////////

#endif  // _STEPEN_K_AB_HASHINDEX_RBTREE_H_

// end of file

