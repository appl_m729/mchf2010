#ifndef _CSF_DIAGONALIZATOR_H_
#define _CSF_DIAGONALIZATOR_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"
#include "sym_matrix.h"


enum PrintEigesParam {PRINT_STUFF_FALSE = 0, PRINT_STUFF_TRUE = 1};

class DetsList;
class Det_State;
class SymMatrix;


class CSF_Diaglzr : private Uncopyable {

public:

    CSF_Diaglzr();
    ~CSF_Diaglzr();

protected:

    SymMatrix  _L2mtr, _S2mtr;
    const DetsList* _dtsLst;   // One CSF_Dets_Collection
    double *_Xmatr, *_R_buffL, *_T_buffS, *_buff_TR;
    double *_eig_S;
    int    *_eig_L;
    double *_LL1_buff, *_SS1_buff;


    void   _setup_X2mtr();
    void   _add_mLz_pLz2(const Det_State * const Det_i, const int i);
    void   _add_LpLm(const Det_State * const Det_i,
                     const Det_State * const Det_j, const int i, const int j);
    void   _add_mSz_pSz2(const Det_State * const tDet_i, const int i);
    void   _add_SpSm(const Det_State * const Det_i,
                     const Det_State * const Det_j, const int i, const int j);
    void   _prepear_data();

    void   _create_R(PrintEigesParam param = PRINT_STUFF_TRUE);
    void   _rotate_S2();
    void   _create_T(PrintEigesParam param = PRINT_STUFF_TRUE);
    void   _create_TR();
    void   _setup_LS_basic(PrintEigesParam param = PRINT_STUFF_TRUE);

    void   _calc_diagEigVec_LS();
    void   _calc_norm_TRk_LS(PrintEigesParam param = PRINT_STUFF_TRUE);
    void   _ballistic_catch_LS();

    void   _trancate(double *X);
    double _norm_TRk_X(double *X, const int k);
    int    _find_max_vecX(double *X, const int k);

    void   _create_1_eiges();
    void   _print_L2_S2();
    void   _print_X(double *X);
};


class qua5_RbTree;
class qua5_Index;
class qua3_Index;

class qua5_Collection : public CSF_Diaglzr {

public:

    qua5_Collection();
    ~qua5_Collection();

    void process(const DetsList* const ext_dtsLst, qua5_RbTree * const q5Tree);

private:

    qua5_Index * const _q5Ind;
    qua3_Index * const _q3Ind;

    void _put_in_5tree(qua5_RbTree * const q5Tree);
};


class CSFunc_LS;
class qua5_Index;

class CSF_Builder : public CSF_Diaglzr {

public:

    CSF_Builder();
    ~CSF_Builder();

    void init(const DetsList * const ext_dtsLst);

    void process(const qua5_Index * const ext_q5Ind);

    int  num_csf() const {return _num_csf;}
    void setup_csf(CSFunc_LS * const to_setup_CSF, const int indFnc);

    void free();

private:

    const qua5_Index * _q5Ind;

    int   _num_csf;
    int  *_ind_csf;

    bool  _init_flg;

    void  _hash_index_of_CSF_by_LS();
};

////////////////////////////////////////////////////////////////////////////

#endif // _CSF_DIAGONALIZATOR_H_

// end of file


