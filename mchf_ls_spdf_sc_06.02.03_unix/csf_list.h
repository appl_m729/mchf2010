#ifndef _CSF_LIST_H_
#define _CSF_LIST_H_

////////////////////////////////////////////////////////////////////////////

class CSFunc_LS;
class nlw_Conf;

class CSF_DataBlock {

public:

    const nlw_Conf* oneCnf;
    CSFunc_LS     *one_csf;
    CSF_DataBlock *next;

    CSF_DataBlock();
    ~CSF_DataBlock();
};


class CSFs_List {

public:

    CSFs_List();
    ~CSFs_List();

    const CSFunc_LS& csf_first() const; // logical const
    const CSFunc_LS& csf_secnd() const; // logical const
    const nlw_Conf*  pc_cnf()    const;

    void to_head_first() const; // logical const
    void to_first_item() const; // (i,i)
    void to_first_item_sg() const;  // not (i,i) but (i,i+1); sg - strictly greater

    bool next_first() const; // logical const
    bool next_secnd() const; // logical const

    int  size() const;
    void catch_append(CSFunc_LS * const extCSF, const nlw_Conf* const pCnf);

private:

    CSF_DataBlock * const _phead;

    mutable CSF_DataBlock *_crnt_item_first;
    mutable CSF_DataBlock *_crnt_item_secnd;
    mutable CSF_DataBlock *_prev_first_item;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif // _CSF_LIST_H_

// end of file

