#ifndef _HAMILTONIAN_MATRIX_H_
#define _HAMILTONIAN_MATRIX_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class HamiltonianMtrx : private Uncopyable {

public:

    HamiltonianMtrx(const int Size);
    ~HamiltonianMtrx();

    void in_zero();
    void add(const int i, const int j, const double Value);
    void add(const int ind, const double value);
    void calc_ground_state();

    double C(const int i) const;
    double E0() const {return _mu;}
    double h(const int i, const int j) const {return _A[_h_ind(i,j)];}
    double h(const int ind) const {return _A[ind];}

    int  size() const {return _size;}
    void print() const;

private:

    int     _size, _hsz; // half size
    double *_A, *_x, _mu;
    double *_Ax, *_ksi;  // temp variable

    int    _h_ind(const int i, const int j) const;

    int    _max_ind(const double * const x) const;
    double _norm(const double * const x) const;

    void   _ff_CG_EigVec_Sym();
};

////////////////////////////////////////////////////////////////////////////

#endif  // _HAMILTONIAN_MATRIX_H_

// end of file

