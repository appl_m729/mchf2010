#ifndef _ETA_INDEX_H_
#define _ETA_INDEX_H_

////////////////////////////////////////////////////////////////////////////

class k_ab_Index;

class eta_Index {

public:

    eta_Index(const int l1, const int l2, const k_ab_Index& kabInd);
    eta_Index(const eta_Index& rhs);
    ~eta_Index();

    eta_Index& operator= (const eta_Index& rhs);

    int l1() const {return _l1;}
    int l2() const {return _l2;}
    const k_ab_Index* p_kabInd() const {return _kabInd;}
    const k_ab_Index& kabInd() const {return *_kabInd;}

    void reset(const int l1, const int l2, const k_ab_Index& kabInd);

    void print() const;

    friend bool operator==(const eta_Index& lhs, const eta_Index& rhs);
    friend bool operator >(const eta_Index& lhs, const eta_Index& rhs);

private:

    int _l1,_l2;
    k_ab_Index * const _kabInd;

    void _regulate_ll();
};

////////////////////////////////////////////////////////////////////////////

#endif // _ETA_INDEX_H_

// end of file

