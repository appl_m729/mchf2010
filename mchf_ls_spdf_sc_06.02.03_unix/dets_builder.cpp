#include <iostream>
#include <cmath>
#include <cstdlib>
#include "dets_builder.h"
#include "q4x_convertor.h"
#include "nlw_conf.h"
#include "det_state.h"
#include "nlmlms_state.h"
#include "qua3_index.h"
#include "qua3_list.h"
#include "dets_list.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

//               +-+-+-+-+-+-+-+-+-+-+
//               |*|*|*| | | | | | |x| n(0)l(0)   nlw(0) = 3, nlw_len(0) = 9
//               +-+-+-+-+-+-+-+-+-+-+
//            /\ |*|*| | | |x|x|x|x|x| n(1)l(1)   nlw(1) = 2, nlw_len(1) = 5
//            || +-+-+-+-+-+-+-+-+-+-+
// _Nj_max = 5   |*|*|*|*| | | | | | | n(2)l(2)   nlw(2) = 4, nlw_len(2) = 10
//            || +-+-+-+-+-+-+-+-+-+-+
//            \/ |*| | | | | | | |x|x| n(3)l(3)   nlw(3) = 1, nlw_len(3) = 8
//               +-+-+-+-+-+-+-+-+-+-+
//               |*|*| | | | | |x|x|x| n(4)l(4)   nlw(4) = 2, nlw_len(4) = 7
//               +-+-+-+-+-+-+-+-+-+-+
//                       <= =>            * = el_state = (n,l,ml,ms)
//                   _Ni_max = 10         x = can't move here

////////////////////////////////////////////////////////////////////////////

CSF_DetsBuilder::CSF_DetsBuilder():
_q4xC(0), _nlw_cnf(0), _buffDet(new Det_State),
_x(0), _Nj_max(0), _Ni_max(0) {

    _Nj_max = 10;
    _Ni_max = 18 + 2; //0(+1) < ... < N+1 (+1) =>(+2)
    _allocate_xij();
}

//virtual
CSF_DetsBuilder::~CSF_DetsBuilder() {

    _delete_xij();

    delete _buffDet;

    _q4xC    = 0;
    _nlw_cnf = 0;
}

void CSF_DetsBuilder::setup_q4xC(const q4x_Convertor * const ext_q4xC) {
    _q4xC = ext_q4xC;
}

void CSF_DetsBuilder::_allocate_xij() {

    _x = new int*[_Nj_max];
    for (int j = 0; j < _Nj_max; ++j) {

        _x[j] = new int[_Ni_max];

        for (int q = 0; q < _Ni_max; ++q) {
            _x[j][q] = -2;
        }
    }
}

void CSF_DetsBuilder::_delete_xij() {

    for (int j = 0; j < _Nj_max; ++j) {

        delete[] _x[j]; _x[j] = 0;
    }
    delete[] _x; _x = 0;
}

void CSF_DetsBuilder::_print_xji() const {

    cout << " | "; 
    for (int j = 0; j < _nlw_cnf->numShells(); ++j) {

        for (int q = 0; q < _nlw_cnf->w(j); ++q) {
           cout << _x[j][q+1] << " ";
        }
        cout << " "; 
    }
}

void CSF_DetsBuilder::_check_allocate_xij() {

    const int tNj = _nlw_cnf->numShells();
    const int tNi = _q4xC->max_wmax() + 2;

    if (_Nj_max < tNj || _Ni_max < tNi) {

        cout << "reallocate x_ij..." << endl;
        _delete_xij();
        _Nj_max = tNj;
        _Ni_max = tNi;
        _allocate_xij();
        cout << "new allocate x_ij done ok" << endl;
    }
}

void CSF_DetsBuilder::_initialize_xij() {

    for (int j = 0; j < _nlw_cnf->numShells(); ++j) {

        const int xj_start = _q4xC->x_sta(*_nlw_cnf,j);
        _x[j][0] = xj_start - 1;

        for (int i = 1; i <= _nlw_cnf->w(j); ++i) {

            _x[j][i] = (xj_start - 1) + i;
        }

        _x[j][_nlw_cnf->w(j) + 1] = _q4xC->x_end(*_nlw_cnf,j) + 1;
    }
}

void CSF_DetsBuilder::_next_move(const int j, const int i) {

    if (j < _nlw_cnf->numShells()) {

        if (i <= _nlw_cnf->w(j)) {

            for (_x[j][i] = _x[j][i-1]+1; _x[j][i] < _x[j][i+1]; ++_x[j][i]) {

                _next_move(j,i+1);
            }
        } else {
            _next_move(j+1,1);
        }
    } else {
        _process_next_det();
    }
}

void CSF_DetsBuilder::_setup_buffDet() {

    int k = -1;
    for (int j = 0; j < _nlw_cnf->numShells(); ++j) {

        for (int q = 0; q < _nlw_cnf->w(j); ++q) {

            _buffDet->set_elst_le(_q4xC->elst(_x[j][q+1]), ++k);
        }
    }
}

//virtual
void CSF_DetsBuilder::_process_next_det() {

    cout << "Error in CSF_DetsBuilder base class;" << endl;
    cout << "there is no redefinition of _process_next_det()..." << endl;
    exit(0);
}

////////////////////////////////////////////////////////////////////////////

CSF_qua3Ind_Catch::CSF_qua3Ind_Catch():
CSF_DetsBuilder(), _q3Ind(new qua3_Index), _q3Lst(0) {
}

//virtual
CSF_qua3Ind_Catch::~CSF_qua3Ind_Catch() {

    delete _q3Ind;
}

void CSF_qua3Ind_Catch::collect_Inds(const nlw_Conf* const p_Cnf, 
                                     qua3_List * const ext_q3Lst) {

    _nlw_cnf = p_Cnf;
    _q3Lst   = ext_q3Lst;

    // check errors
    if (0 == _q4xC) {

        cout << "Error Dets_qua3_Catch:: q4xC = 0" << endl;
        exit(0);
    }

    if (0 == _nlw_cnf) {

        cout << "Error Dets_qua3_Catch:: nlw_Conf have not been setup..." << endl;
        exit(0);
    }

    if (0 == _q3Lst) {

        cout << "Error Dets_qua3_Catch:: qua3_List have not been setup..." << endl;
        exit(0);
    }

    // start calc
//    cout << "starting catching process for..." << endl;
//    _nlw_cnf->print();

    _q3Lst->clear();

    _check_allocate_xij();
    _initialize_xij();

    _next_move(0,1);

    _q3Lst   = 0;
    _nlw_cnf = 0;
}

//virtual
void CSF_qua3Ind_Catch::_process_next_det() {

    // 1. setup determinant for current configuration
    _setup_buffDet();

    // 2. save (ML,MS,P)-index
    _q3Ind->reset(_buffDet->ML(), _buffDet->MS(), _buffDet->Parity());
    _q3Lst->add(*_q3Ind);
}


////////////////////////////////////////////////////////////////////////////

CSF_Dets_Collection::CSF_Dets_Collection():
CSF_DetsBuilder(), _cpq3Ind(0), _detsLst(0),
_pract_index(0), _catch_index(0) {
}

//virtual
CSF_Dets_Collection::~CSF_Dets_Collection() {

    _cpq3Ind = 0;
    _detsLst = 0;
}

void CSF_Dets_Collection::create_list(const nlw_Conf * const cp_Cnf,
                                      const qua3_Index * const ext_q3Ind, 
                                      DetsList * const ext_pDetLst) {
    _nlw_cnf = cp_Cnf;
    _cpq3Ind = ext_q3Ind;
    _detsLst = ext_pDetLst;

    // check errors
    if (0 == _q4xC) {

        cout << "Error CSF_Dets_Collection:: q4xC = 0" << endl;
        exit(0);
    }

    if (0 == _cpq3Ind) {

        cout << "Error CSF_Dets_Collection:: ML,MS,P have not been setup..." << endl;
        exit(0);
    }

    if (0 == _nlw_cnf) {

        cout << "Error CSF_Dets_Collection:: nlw_Conf have not been setup..." << endl;
        exit(0);
    }

    if (0 == _detsLst) {

        cout << "Error CSF_Dets_Collection:: detsList have not been setup..." << endl;
        exit(0);
    }

//    start calc
//    cout << "starting dets collecting process for..." << endl;
//    _nlw_cnf->print();

    _detsLst->clear();

    _check_allocate_xij();
    _initialize_xij();

    _pract_index = _catch_index = 0;
    _next_move(0,1);

    _detsLst = 0;
    _cpq3Ind = 0;
    _nlw_cnf = 0;
}

//virtual
void CSF_Dets_Collection::_process_next_det() {

    _pract_index++;

    // 1. setup determinant for current configuration
    _setup_buffDet();

    // 2. add, if satisfy the following conditions
    if ( (_buffDet->ML() == _cpq3Ind->ML()) && 
         (fabs(_buffDet->MS() - _cpq3Ind->MS()) < SPIN_THRESHOLD) && 
         (_buffDet->Parity() == _cpq3Ind->P()) ) {

        _catch_index++;
        _detsLst->add(*_buffDet);
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

