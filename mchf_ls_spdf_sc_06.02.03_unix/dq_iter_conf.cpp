#include <iostream>
#include <cstdlib>
#include "dq_iter_conf.h"
#include "pnl_index_map.h"
#include "read.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

dqIter_Conf::dqIter_Conf(const char * const file,
                         const Pnl_IndexMap * const pnlInd):
_N(pnlInd->length()), _alpha(new double[_N]) {

    for (int a = 0; a < _N; ++a) {

        if (ReadReal(file, "iter_dq_prm", a, _alpha[a])) {

            cout << "dqIter_Conf: Error in read params;  a = " << a << endl;
            exit(0);
        }
    }
}

dqIter_Conf::~dqIter_Conf() {

    delete[] _alpha;
}

void dqIter_Conf::print() const {

    cout << "dqIter_Conf:   ";
    for (int a = 0; a < _N; ++a) {

        cout << _alpha[a] << "   " ;
    }
    cout << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file
