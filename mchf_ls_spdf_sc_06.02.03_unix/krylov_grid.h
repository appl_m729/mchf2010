#ifndef _KRYLOV_GRID_H_
#define _KRYLOV_GRID_H_

////////////////////////////////////////////////////////////////////////////

class KLaguerreGrid {

public:

    KLaguerreGrid();
    ~KLaguerreGrid();

    static int length() {return 151;}

    int    index(const double alf) const;
    void   activate(const int index) const;
    double alpha() const;

    double A(const int k) const;
    double x(const int k) const;

    int Nalf() const {return _Nalf;}

private:

    const int _Nalf;
    double * const _alfa;

    mutable int _crnt_index, _crnt_pos;

    double * const _A;
    double * const _x;

    void _setup_x_0();
    void _setup_x_1();
    void _setup_x_2();
    void _setup_x_3();
    void _setup_x_4();
    void _setup_x_5();
    void _setup_x_6();
    void _setup_x_7();
    void _setup_x_8();
    void _setup_x_9();
    void _setup_x_10();
    void _setup_x_11();
    void _setup_x_12();
    void _setup_x_13();
    void _setup_x_14();

    void _setup_A_0();
    void _setup_A_1();
    void _setup_A_2();
    void _setup_A_3();
    void _setup_A_4();
    void _setup_A_5();
    void _setup_A_6();
    void _setup_A_7();
    void _setup_A_8();
    void _setup_A_9();
    void _setup_A_10();
    void _setup_A_11();
    void _setup_A_12();
    void _setup_A_13();
    void _setup_A_14();
};

////////////////////////////////////////////////////////////////////////////

#endif // _KRYLOV_GRID_H_

// end of file

