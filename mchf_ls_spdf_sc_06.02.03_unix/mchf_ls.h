#ifndef _MCHF_LS_SOLVER_H_
#define _MCHF_LS_SOLVER_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

using namespace::std;

class writeFlags;
class Atom_Conf;
class Pnl_IndexMap;
class CSF_Basis;
class Energy_Hash;
class One_List;
class Two_List;
class Variate_Hash;
class ksiHash_List;
class etaHash_List;
class Eta_rbTree;
class qBas_Conf;
class Radial_Pnl_Data;
class Pnl_Buffer;
class HamiltonianMtrx;
class VecEq_Syst;
class NumE_Trace;
class Radial_Rho;


class MCHF_Solver : private Uncopyable {

public:

    MCHF_Solver(const char file[]);
    ~MCHF_Solver();

    void solve();
    void mesureTime();

private:

    time_t  _tt_start, _tt_stop;

    const writeFlags   * const _w_flags;
    const Atom_Conf    * const _aCnf;
    const Pnl_IndexMap * const _pnlInd;
    const CSF_Basis    * const _csfBas;
    const Energy_Hash  * const _hash_E;

    One_List           * const _oneLst;
    Two_List           * const _twoLst;    
    const Variate_Hash * const _variate;
    ksiHash_List       * const _hashKsi;
    etaHash_List       * const _hashEta;
    Eta_rbTree         * const _etaTree;
    const qBas_Conf    * const _qBasCnf;
    Radial_Pnl_Data    * const _radialP;
    Pnl_Buffer         * const _buff_Pnl;
    HamiltonianMtrx    * const _hamlt;
    VecEq_Syst         * const _vecEqSyst;
    NumE_Trace         * const _trace_E;
    Radial_Rho         * const _radial_rho;

    void _scatter_one_on_hamlt();
    void _scatter_two_on_hamlt();
};

////////////////////////////////////////////////////////////////////////////

#endif  // _MCHF_LS_SOLVER_H_

// end of file

