#include <iostream>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include "pnl_radial.h"
#include "eta_rbtree.h"
#include "pnl_index_map.h"
#include "qbas_conf.h"
#include "qq_hash.h"
#include "s_k_ab_index.h"
#include "s_k_ab_rbtree.h"
#include "s_k_ab_list.h"
#include "eta_list.h"
#include "two_list.h"
#include "pnl_buffer.h"
#include "k_ab_index.h"
#include "ab_index.h"
#include "k_abcd_index.h"
#include "eta_index.h"
#include "krylov_grid.h"
#include "laguerre.h"
#include "eta_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Radial_Pnl_Data::Radial_Pnl_Data(const Eta_rbTree * const etarbTree,
                                 const Pnl_IndexMap * const pnlInd,
                                 const qBas_Conf * const qBasCnf,
                                 const double extZ):

_etaTree(etarbTree), _pnlInd(pnlInd), _qBasCnf(qBasCnf), _Z(extZ), 
_kLgr(new KLaguerreGrid), _qqHash(new qqHash(_qBasCnf,pnlInd)),
_etaLst(new Eta_List), _Ttree(new s_k_ab_rbTree), _Wtree(new s_k_ab_rbTree),
_Tlist(new s_k_ab_List), _Wlist(new s_k_ab_List),
_sInd(new s_k_ab_Index(0,k_ab_Index(0,0,0))),
_pL1(new PLaguerre(KLaguerreGrid::length())),
_pL2(new PLaguerre(KLaguerreGrid::length())),
_etaInd(new eta_Index(0,0,k_ab_Index(0,0,0))),
_bnl(0), _tv(0), _wv(0), _Tr(0), _Wr(0) {

    if (!etarbTree || !pnlInd || !qBasCnf) {

        cout << "Radial_Pnl_Data: Error setup, zero input pointers" << endl;
        cout << "etarbTree = " << etarbTree << endl;
        cout << "pnlInd = "    << pnlInd    << endl;
        cout << "qBasConf = "  << qBasCnf   << endl;

        exit(0);
    }

    if (_pnlInd->lmax() != _qBasCnf->lmax()) {

        cout << "Pnl_Radial_Data Error" << endl;
        cout << "PnlInd::lmax  = " << _pnlInd->lmax()  << endl;
        cout << "qBasCnf::lmax = " << _qBasCnf->lmax() << endl;

        exit(0);
    }

    if (KLaguerreGrid::length() < _qBasCnf->Nlmax()) {

        cout << "Pnl_Radial_Data Error" << endl;
        cout << "kLgr::length  = " << KLaguerreGrid::length() << endl;
        cout << "qBasCnf::Nlmax = " << _qBasCnf->Nlmax() << endl;

        exit(0);
    }

    _qBasCnf->print();
}

Radial_Pnl_Data::~Radial_Pnl_Data() {

    delete _kLgr;
    delete _qqHash;
    delete _etaLst;
    delete _Ttree;
    delete _Wtree;
    delete _Tlist;
    delete _Wlist;
    delete _sInd;
    delete _pL1;
    delete _pL2;
    delete _etaInd;

    _free_5arr();
}

void Radial_Pnl_Data::_free_5arr() {

    delete[] _bnl;  _bnl = 0;
    delete[] _tv;   _tv  = 0;
    delete[] _wv;   _wv  = 0;
    delete[] _Tr;   _Tr  = 0;
    delete[] _Wr;   _Wr  = 0;
}

void Radial_Pnl_Data::generate_TW() {

    _etaLst->clear();
    _etaTree->linearize(_etaLst);

    _Ttree->clear();
    _Wtree->clear();
    _etaLst->generate_2skabTrees(_Ttree,_Wtree);

    _Tlist->clear();
    _Ttree->linearize(_Tlist,"T_");

    _Wlist->clear();
    _Wtree->linearize(_Wlist,"W_");

    _free_5arr();

    _tv  = new double[_etaLst->size()*_qBasCnf->Nlmax()*_qBasCnf->Nlmax()];
    _wv  = new double[_etaLst->size()*_qBasCnf->Nlmax()*_qBasCnf->Nlmax()];

    _bnl = new double[_pnlInd->length()*_qBasCnf->Nlmax()];

    _Tr  = new double[_Ttree->size()*KLaguerreGrid::length()];
    _Wr  = new double[_Wtree->size()*KLaguerreGrid::length()];

    _qqHash->prepear_tw(_Tlist, _Wlist);
}

void Radial_Pnl_Data::update(const Pnl_Buffer * const pnlBuff) {

    cout << endl << "Radial_Pnl_Data:: start update...";

    _bnl_update(pnlBuff);

    cout << endl << "T(" << _Tlist->size() << "): ";
    _Tlist->to_head();
    while (_Tlist->next() ) {

        cout << " " << _Tlist->linInd();
        _recalc_Tr(_Tlist->s_k_ab_Ind(), _Tlist->linInd());
    }

    cout << endl << "W(" << _Wlist->size() << "): ";
    _Wlist->to_head();
    while (_Wlist->next() ) {

        cout << " " << _Wlist->linInd();
        _recalc_Wr(_Wlist->s_k_ab_Ind(), _Wlist->linInd());
    }

    _t_w_to_zero();

    cout << endl << "eta(" << _etaLst->size() << "): ";
    _etaLst->to_head();
    while (_etaLst->next() ) {

        cout << " " << _etaLst->linIndex();
        _recalc_tw(_etaLst->etaInd(),_etaLst->linIndex(), T_POINTS);
        _recalc_tw(_etaLst->etaInd(),_etaLst->linIndex(), W_POINTS);
    }

    cout << endl;
    cout << "Radial_Pnl_Data:: update done ok" << endl;
}

double Radial_Pnl_Data::Iab(const ab_Index& oneInd) const {

    const int la = _pnlInd->l(oneInd.a());
    const int lb = _pnlInd->l(oneInd.b());

    if (la != lb) {

        cout << "Error in Radial_Pnl_Data::Iab" << endl;
        cout << "la = " << la << " lb = " << lb << endl;
        exit(0);
    }

    const int ind1 = oneInd.a()*_qBasCnf->Nlmax();
    const int ind2 = oneInd.b()*_qBasCnf->Nlmax();

    double sum = 0;

    for (int k1 = 0; k1 < _qBasCnf->Nmax(la); ++k1) {
    for (int k2 = 0; k2 < _qBasCnf->Nmax(la); ++k2) {

        sum += _bnl[ind1 +k1]*_bnl[ind2 +k2]*ksi(la,k1,k2);
    }}

    return sum;
}

double Radial_Pnl_Data::Rk(const k_abcd_Index& twoInd) const {

    const double l1 = _pnlInd->l(twoInd.a());
    const double l3 = _pnlInd->l(twoInd.c());

    const int ind1 = twoInd.a()*_qBasCnf->Nlmax();
    const int ind3 = twoInd.c()*_qBasCnf->Nlmax();

    _etaInd->reset(l1,l3, twoInd.k_bd());
    const int eta_ind = _etaTree->linIndex(*_etaInd);

    double sum = 0;

    for (int k1 = 0; k1 < _qBasCnf->Nmax(l1); ++k1) {
    for (int k3 = 0; k3 < _qBasCnf->Nmax(l3); ++k3) {

        sum += _bnl[ind1 +k1]*_bnl[ind3 +k3]*eta(eta_ind,k1,k3);
    }}

    return sum;
}

double Radial_Pnl_Data::ksi(const int l, const int i, const int j) const {

    if (0 == l) return 0.5*_qqHash->dQdQ(i,l,j) - _Z*_qqHash->QQr(i,l,j);

    return 0.5*(_qqHash->dQdQ(i,l,j) + l*(l+1)*_qqHash->QQrr(i,l,j)) - _Z*_qqHash->QQr(i,l,j);
}

double Radial_Pnl_Data::kin(const int l, const int i, const int j) const {

    if (0 == l) return 0.5*_qqHash->dQdQ(i,l,j);

    return 0.5*(_qqHash->dQdQ(i,l,j) + l*(l+1)*_qqHash->QQrr(i,l,j));
}

double Radial_Pnl_Data::eta(const int ind, const int i, const int j) const {

    const int pos = ind;
    const int N = _qBasCnf->Nlmax();

    const int fin_ind = pos*N*N + i*N + j;

    return (_tv[fin_ind] + _wv[fin_ind])/_qBasCnf->zeta(0);
}

double Radial_Pnl_Data::eta(const eta_Index& etaInd, const int i, const int j) const {

    const int pos = _etaTree->linIndex(etaInd);
    const int N = _qBasCnf->Nlmax();

    const int fin_ind = pos*N*N + i*N + j;

    return (_tv[fin_ind] + _wv[fin_ind])/_qBasCnf->zeta(0);
}
/*
double Radial_Pnl_Data::tv(const int ind, const int i, const int j) const {

    const int pos = ind;
    const int N = _qBasCnf->Nlmax();

    return _tv[pos*N*N + i*N + j];
}

double Radial_Pnl_Data::tv(const eta_Index& etaInd, const int i, const int j) const {

    const int pos = _etaTree->linIndex(etaInd);
    const int N   = _qBasCnf->Nlmax();

    return _tv[pos*N*N + i*N + j];
}

double Radial_Pnl_Data::wv(const int ind, const int i, const int j) const {

    const int pos = ind;
    const int N = _qBasCnf->Nlmax();

    return _wv[pos*N*N + i*N + j];
}

double Radial_Pnl_Data::wv(const eta_Index& etaInd, const int i, const int j) const {

    const int pos = _etaTree->linIndex(etaInd);
    const int N   = _qBasCnf->Nlmax();

    return _wv[pos*N*N + i*N + j];
}
*/
double Radial_Pnl_Data::Pnl(const int a, const double r) const {

    const int la = _pnlInd->l(a);
    const int ind_a = a*_qBasCnf->Nlmax();
    const double rz = r/_qBasCnf->zeta(la);

    _pL1->calc(_qBasCnf->Nmax(la),2.0*la+2.0,rz);

    double val_Pnl = 0;
    for (int k = 0; k < _qBasCnf->Nmax(la); ++k) {

        val_Pnl += _bnl[ind_a + k]*sqrt(_factor(k,la)/_qBasCnf->zeta(la))*
                                 _pL1->value(k)*pow(rz,la+1)*exp(-0.5*rz);
    }
    return val_Pnl;
}

void Radial_Pnl_Data::_recalc_Tr(const s_k_ab_Index& skabInd, const int linInd) {

    const int a = skabInd.kabInd().a();
    const int b = skabInd.kabInd().b();

    const int l1 = _pnlInd->l(a);
    const int l2 = _pnlInd->l(b);

    const int ind1 = a*_qBasCnf->Nlmax();
    const int ind2 = b*_qBasCnf->Nlmax();

    const int pos_t = linInd*KLaguerreGrid::length();

    for (int i = 0; i < KLaguerreGrid::length(); ++i) {

        double sum = 0;

        for (int k1 = 0; k1 < _qBasCnf->Nmax(l1); ++k1) {
        for (int k2 = 0; k2 < _qBasCnf->Nmax(l2); ++k2) {
    
            sum += _bnl[ind1+k1]*_bnl[ind2+k2]*_qqHash->tau(linInd, k1,k2,i);
        }}
        _Tr[pos_t+i] = sum;
    }
}

void Radial_Pnl_Data::_recalc_Wr(const s_k_ab_Index& skabInd, const int linInd) {

    const int a = skabInd.kabInd().a();
    const int b = skabInd.kabInd().b();

    const int l1 = _pnlInd->l(a);
    const int l2 = _pnlInd->l(b);

    const int ind1 = a*_qBasCnf->Nlmax();
    const int ind2 = b*_qBasCnf->Nlmax();

    const int pos_w = linInd*KLaguerreGrid::length();

    for (int i = 0; i < KLaguerreGrid::length(); ++i) {

        double sum = 0;

        for (int k1 = 0; k1 < _qBasCnf->Nmax(l1); ++k1) {
        for (int k2 = 0; k2 < _qBasCnf->Nmax(l2); ++k2) {
    
            sum += _bnl[ind1+k1]*_bnl[ind2+k2]*_qqHash->wnu(linInd, k1,k2,i);
        }}
        _Wr[pos_w+i] = sum;
    }
}

void Radial_Pnl_Data::_t_w_to_zero() {

    const int i_max = _etaLst->size()*_qBasCnf->Nlmax()*_qBasCnf->Nlmax();

    for (int i = 0; i < i_max; ++i) {

        _tv[i] = 0;
        _wv[i] = 0;
    }
}

void Radial_Pnl_Data::_recalc_tw(const eta_Index & etaInd, const int linInd, const TW tw_flag) {

    const int N   = _qBasCnf->Nlmax();
    const int pos = linInd*N*N;

    const int l1 = etaInd.l1();
    const int l2 = etaInd.l2();

    const s_k_ab_rbTree * twTree = 0;
    int s_tw(-1);
    double * TW(0);
    double * et_tw(0);

    if (T_POINTS == tw_flag) {

        s_tw   = l1+l2-etaInd.kabInd().k()+1;
        twTree = _Ttree;
        TW     = _Tr;
        et_tw  = _tv;

    } else if (W_POINTS == tw_flag) {
    
        s_tw   = l1+l2+etaInd.kabInd().k()+2;
        twTree = _Wtree;
        TW     = _Wr;
        et_tw  = _wv;

    } else {

        cout << "Error in _recalc_tw flag: incorrect case tw_flag = " << tw_flag << endl;
        exit(0);
    }

    _sInd->reset(s_tw, etaInd.kabInd());

    const int lin_skab = twTree->linIndex(*_sInd);
    const int tw_pos   = lin_skab*KLaguerreGrid::length();

    _kLgr->activate(_kLgr->index(s_tw));

    for (int i = 0; i < KLaguerreGrid::length(); ++i) {

        _pL1->calc(_qBasCnf->Nmax(l1),2.0*l1+2.0,_kLgr->x(i));
        _pL2->calc(_qBasCnf->Nmax(l2),2.0*l2+2.0,_kLgr->x(i));

        const double Ai = _kLgr->A(i);

        for (int k1 = 0; k1 < _qBasCnf->Nmax(l1); ++k1) {
        for (int k2 = 0; k2 < _qBasCnf->Nmax(l2); ++k2) {

            et_tw[pos + k1*N + k2] += Ai*_pL1->value(k1)*_pL2->value(k2)*TW[tw_pos+i];
        }}
    }

    for (int k1 = 0; k1 < _qBasCnf->Nmax(l1); ++k1) {
    for (int k2 = 0; k2 < _qBasCnf->Nmax(l2); ++k2) {

        const double factr = sqrt(_factor(k1,l1)*_factor(k2,l2));
        et_tw[pos + k1*N + k2] *= factr;
    }}
}

double Radial_Pnl_Data::_factor(const int k, const int l) const {

    double pr = 1.0;
    for (int z = k + 1; z <= (k + 2*l + 2); ++z) {    
        pr /= (double) z;
    }
    return pr;
}

void Radial_Pnl_Data::_bnl_update(const Pnl_Buffer * const pnlBuff) {

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int la = _pnlInd->l(a);
        for (int i = 0; i < _qBasCnf->Nmax(la); ++i) {
        
            _bnl[a*_qBasCnf->Nlmax() + i] = pnlBuff->b(a,i);
        }
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file
