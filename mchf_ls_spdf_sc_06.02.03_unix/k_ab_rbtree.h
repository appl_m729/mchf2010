#ifndef _K_AB_HASHINDEX_RBTREE_H_
#define _K_AB_HASHINDEX_RBTREE_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class k_ab_Index;
class k_ab_List;

class k_ab_rbNode : private Uncopyable {

public:

    k_ab_rbNode *left, *right;
    int red;

    int linearIndex;
    k_ab_Index* k_ab_ind;

    k_ab_rbNode();
    ~k_ab_rbNode();
};


class k_ab_rbTree : private Uncopyable {

public:

    k_ab_rbTree();
    ~k_ab_rbTree();

    void insert(const k_ab_Index& kabInd);
    int  linIndex(const k_ab_Index& kabInd);
    int  size() const {return _linInd + 1;}

    void linearize(k_ab_List * const kabLst);

private:

    k_ab_List   * _kabLst;
    k_ab_rbNode * _head;
    int           _linInd;

    void _rotR(k_ab_rbNode* &h);
    void _rotL(k_ab_rbNode* &h);
    int  _is_red(k_ab_rbNode *x);

    void         _RB_insert(k_ab_rbNode* &h, const k_ab_Index& ins_kabInd, int sw);
    k_ab_rbNode* _RB_search(k_ab_rbNode* h, const k_ab_Index& prnt_kabInd);
    void         _RB_traversal(k_ab_rbNode* h);
};

////////////////////////////////////////////////////////////////////////////

#endif  // _K_AB_HASHINDEX_RBTREE_H_

// end of file

