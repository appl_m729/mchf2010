#ifndef _K_ABCD_INDEX_H_
#define _K_ABCD_INDEX_H_

////////////////////////////////////////////////////////////////////////////

class k_ab_Index;
class eta_Index;

class k_abcd_Index {

public:

    k_abcd_Index(const int k, const int a, const int b,const int c, const int d);
    k_abcd_Index(const k_abcd_Index& rhs);
    ~k_abcd_Index();

    k_ab_Index k_ac() const;
    k_ab_Index k_bd() const;

    void reset_k(const int k);
    void reset_abcd(const int a, const int b, const int c, const int d);

    int k() const {return _k;}
    int a() const {return _a;}
    int b() const {return _b;}
    int c() const {return _c;}
    int d() const {return _d;}
    void abcd(int &a, int &b, int &c, int &d) const;
    void k_abcd(int &k, int &a, int &b, int &c, int &d) const;

    void print() const;

    friend bool operator==(const k_abcd_Index& lft, const k_abcd_Index& rht);
    friend bool operator> (const k_abcd_Index& lft, const k_abcd_Index& rht);

private:

    int _k, _a, _b, _c, _d;

    void _regulate();
    void _swap(int &a, int &b);
};

////////////////////////////////////////////////////////////////////////////

#endif  // _K_ABCD_INDEX_H_

// end of file

