#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include "csf_basis.h"
#include "csf_ls.h"
#include "q4x_convertor.h"
#include "nlw_builder.h"
#include "dets_builder.h"
#include "dets_list.h"
#include "csf_diaglzr.h"
#include "csf_list.h"
#include "qua5_index.h"
#include "qua3_index.h"
#include "nlw_conf.h"
#include "det_state.h"
#include "atom_conf.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

CSF_Basis::CSF_Basis(const Atom_Conf * const aCnf):
_aCnf(aCnf), 
_q4xC(new q4x_Convertor(_aCnf)),
_nlwBldr(new nlw_Builder(_aCnf,_q4xC)), 
_dtsClcn(new CSF_Dets_Collection),
_dtsLst(new DetsList), 
_csfBldr(new CSF_Builder),
_csfLst(new CSFs_List), 
_q5Ind(new qua5_Index) {

    _dtsClcn->setup_q4xC(_q4xC);

    _q5Ind->reset_LS(_aCnf->L(),_aCnf->S());
    _q5Ind->reset_qua3(qua3_Index(_aCnf->ML(),_aCnf->MS(),_aCnf->P()));

    if (_q5Ind->is_not_valid()) {

        cout << "Error in CSF_Basis::q5Ind.." << endl;
        _q5Ind->print();
        exit(0);
    }

    _setup();
}

CSF_Basis::~CSF_Basis() {

    delete _q4xC;
    delete _q5Ind;
    delete _csfLst;
    delete _csfBldr;
    delete _dtsClcn;
    delete _dtsLst;
    delete _nlwBldr;
}

int CSF_Basis::size() const {
    return _csfLst->size();
}

const CSFunc_LS& CSF_Basis::csf_first() const {
    return _csfLst->csf_first();
}

const CSFunc_LS& CSF_Basis::csf_secnd() const {
    return _csfLst->csf_secnd();
}

const CSFunc_LS* CSF_Basis::pcsf_first() const {
    return &(_csfLst->csf_first());
}

void CSF_Basis::to_head_first() const {
    _csfLst->to_head_first();
}

void CSF_Basis::to_first_item() const {
    _csfLst->to_first_item();
}

void CSF_Basis::to_first_item_sg() const {
    _csfLst->to_first_item_sg();
}

bool CSF_Basis::next_first() const {
    return _csfLst->next_first();
}

bool CSF_Basis::next_secnd() const {
    return _csfLst->next_secnd();
}

void CSF_Basis::write(const char filename[]) const {

    cout << "CSF_Basis: writing in " << filename << "..";

    ofstream fout(filename);
    fout << "CSF_basis functions... " << endl;

    {
    fout << "walk_Nel = " << _aCnf->walk_Nel() << endl;
    for (int s = 0; s < _aCnf->walk_Nsubsh(); ++s) {
        fout << "(" << _aCnf->walk_n(s) << "," << _aCnf->walk_l(s) << ")  ";
    }fout << endl;

    fout << "hill_Nel = " << _aCnf->hill_Nel() << endl;
    for (int i = 0; i < _aCnf->hill_Nsubsh(); ++i) {
        fout << "(" << _aCnf->hill_n(i) << "," << _aCnf->hill_l(i) << ")";
        fout << "[" << _aCnf->hill_w(i) << "] ";
    } fout << endl;
    }

    fout << endl;
    fout << "Nel_val = " << _aCnf->Nel_val() << endl;
    fout << "Nel_tot = " << _aCnf->Nel_tot() << endl;

    fout << endl << "qua5_Index:  ";
    fout << _q5Ind->L() << " " << _q5Ind->ML() << " ";
    fout << _q5Ind->S() << " " << _q5Ind->MS() << " ";
    fout << _q5Ind->P() << endl;

    fout << endl << "basis size = " << _csfLst->size() << endl;

    to_head_first();
    int ii = 0;
    while(next_first()) {

        fout << endl;
        fout << ii++ << ": (size = " << csf_first().size() << ")  ";
        fout <<  "L=" << csf_first().L() << " ML=" << csf_first().ML();
        fout << " S=" << csf_first().S() << " MS=" << csf_first().MS();
        fout << " P=" << csf_first().P() << endl;

        fout << "cnf:" << endl;
        int k;
        for (k = 0; k < _csfLst->pc_cnf()->numShells(); ++k) {

            int n,l,w;
            _csfLst->pc_cnf()->nlw(n,l,w,k);
            if (k) fout << " ";
            fout << n << "," << l << "(" << w << ")";
        } fout << endl;

        fout << "Ak" << endl;
        double sumAk = 0;
        for (k = 0; k < csf_first().size(); ++k) {

            const double tAk = csf_first().A(k);
            sumAk += tAk*tAk;
            fout << tAk << endl;
        }
        fout << "sum_Ak2 = " << sumAk << endl;

        for (k = 0; k < csf_first().size(); ++k) {

            fout << "sum_L="   << csf_first().det(k).sum_L();
            fout << " ML="     << csf_first().det(k).ML();
            fout << " MS="     << csf_first().det(k).MS();
            fout << " Parity=" << csf_first().det(k).Parity();
            fout << " sign="   << csf_first().det(k).sign() << endl;

            for (int u = 0; u < Det_State::Nel(); ++u) {

                int nn,ll,ml; double ms;
                csf_first().det(k).nlmlms(nn, ll, ml, ms, u);
                fout << nn <<" "<< ll <<" "<< ml <<" "<< ms << endl;
            }
        }
    }

    fout << endl << endl << "end of file" << endl << endl;
    fout.close();
    cout << ". done ok" << endl;
}

void CSF_Basis::_setup() {

    cout << "CSF_Basis: start setup..." << endl;

    _nlwBldr->collect();

    _nlwBldr->to_head();
    while (_nlwBldr->next_nlw()) {

        const nlw_Conf* pCnf = _nlwBldr->crnt_pnlw();
        pCnf->print();

        _dtsClcn->create_list(pCnf, &(_q5Ind->q3()), _dtsLst);
        if (0 == _dtsLst->size()) continue;

        _csfBldr->init(_dtsLst);
        _csfBldr->process(_q5Ind);

        for (int i = 0; i < _csfBldr->num_csf(); ++i) {

            CSFunc_LS *empty_CSF = new CSFunc_LS;
            _csfBldr->setup_csf(empty_CSF,i);
            empty_CSF->reduce();
            _csfLst->catch_append(empty_CSF,pCnf);
            //empty_CSF = 0;
        }
        _csfBldr->free();
    }

    cout << "CSF_Basis: setup successfully done" << endl;
    cout << "CSF_Basis::N_csf = " << _csfLst->size() << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file


