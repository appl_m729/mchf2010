#ifndef _QUA5_LMLSMSP_INDEX_H_
#define _QUA5_LMLSMSP_INDEX_H_

////////////////////////////////////////////////////////////////////////////

class qua3_Index;

class qua5_Index {

public:

    qua5_Index();
    qua5_Index(const int L, const double S);
    qua5_Index(const qua3_Index& q3Ind);
    qua5_Index(const qua5_Index& rhs);
    ~qua5_Index();

    qua5_Index& operator= (const qua5_Index& rhs);

    void reset_LS(const int L, const double S);
    void reset_qua3(const qua3_Index& q3Ind);
    bool is_not_valid() const;

    int    L () const {return _L;}
    double S () const {return _S;}
    int    ML() const;
    double MS() const;
    int    P () const;

    void   all(int& L, int &ML, double& S, double& MS, int& P) const;
    const qua3_Index& q3() const {return *_q3Ind;}

    void print() const;

    friend bool operator== (const qua5_Index& lhs, const qua5_Index& rhs);
    friend bool operator > (const qua5_Index& lhs, const qua5_Index& rhs);

private:

    qua3_Index * const _q3Ind;
    int     _L;
    double  _S;
};

////////////////////////////////////////////////////////////////////////////

#endif //_QUA5_LMLSMSP_INDEX_H_

// end of file

