#ifndef _Q_BASIS_CONFIGURATION_H_
#define _Q_BASIS_CONFIGURATION_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class qBas_Conf : private Uncopyable {

public:

    qBas_Conf(const char file[]);
    ~qBas_Conf();

    int    lmax() const;
    int    Nlmax() const;
    int    Nmax(const int l) const;
    double zeta(const int l) const;

    double kappa() const {return _kappa;}

    void   print() const;

private:

    int      _lmax;
    int     *_Nmax;  // _Nmax[lmax] -  maximum length of ( ,d,dd)Q(n)-basis for fixed l;
    int      _Nlmax; // auxiliary variable; Is max of the previous ones;
    double  *_zeta;
    double   _kappa;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _RBAS_CONFIGURATION_H_

// end of file

