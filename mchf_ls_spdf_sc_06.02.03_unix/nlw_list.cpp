#include <iostream>
#include <cstdlib>
#include "nlw_conf.h"
#include "nlw_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

nlw_DataBlock::nlw_DataBlock():one_nlw(0), next(0) {
//    cout << "  nlw_DataBlock constructor." << endl;
}

nlw_DataBlock::~nlw_DataBlock() {

//    cout << "  nlw_DataBlock destructor." << endl;
    delete one_nlw; one_nlw = 0;
    delete next;    next    = 0;
}

////////////////////////////////////////////////////////////////////////////

nlw_Conf_List::nlw_Conf_List():
_phead(new nlw_DataBlock), _crnt_item(_phead),
_ptail(_phead), _size(0) {

//    cout << "nlw_Conf_List constructor." << endl;
}

nlw_Conf_List::~nlw_Conf_List() {

//    cout << "nlw_Conf_List destructor." << endl;
    _crnt_item = 0;
    _ptail     = 0;

    delete _phead;
}

const nlw_Conf* nlw_Conf_List::crnt_pnlw() const {
    return _crnt_item->one_nlw;
}

void nlw_Conf_List::to_head() {
    _crnt_item = _phead;
}

bool nlw_Conf_List::next() {

    if (_crnt_item->next) {
        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int nlw_Conf_List::size() const {
    return _size;
}

void nlw_Conf_List::add(nlw_Conf* const One_nlw) {

    _ptail->next = new nlw_DataBlock;
    _ptail = _ptail->next;
    _ptail->one_nlw = One_nlw;

    _size++;
}

void nlw_Conf_List::clear() {

    if (_phead->next) {
        delete _phead->next; _phead->next = 0;
    }

    _crnt_item = _phead;
    _ptail     = _phead;

    _size = 0;
}

////////////////////////////////////////////////////////////////////////////

// end of file

