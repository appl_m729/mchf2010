#ifndef _RADIAL_RHO_H_
#define _RADIAL_RHO_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class grid_r;
class Pnl_IndexMap;
class One_List;
class Radial_Pnl_Data;
class HamiltonianMtrx;


class Radial_Rho : private Uncopyable {

public:

    Radial_Rho(const char file[], const Pnl_IndexMap * const PnlInd,
               const One_List * const oneLst);
    ~Radial_Rho();

    void calc(const Radial_Pnl_Data * const radialPnl,
              const HamiltonianMtrx * const hamltMtrx);

    double charge() const;
    void   write(const char * const file)  const;

private:

    const grid_r       * const _rgrid;
    const Pnl_IndexMap * const _pnlInd;
    const One_List     * const _oneLst;

    double * const _Pnl_data;
    double * const _rho_data;

    void _zero_Pnl();
    void _zero_rho();
    void _calc_Pnl(const Radial_Pnl_Data * const radialPnl);
    void _calc_rho(const HamiltonianMtrx * const hamltMtrx);
};

////////////////////////////////////////////////////////////////////////////

#endif // _RADIAL_RHO_H_

// end of file
