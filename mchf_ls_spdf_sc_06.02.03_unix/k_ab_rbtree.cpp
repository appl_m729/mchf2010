#include <iostream>
#include <cstdlib>
#include "k_ab_rbtree.h"
#include "k_ab_index.h"
#include "k_ab_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

k_ab_rbNode::k_ab_rbNode():
left(0), right(0), red(1), k_ab_ind(0), linearIndex(-1) {
}

k_ab_rbNode::~k_ab_rbNode() {

    delete k_ab_ind; k_ab_ind = 0;
    delete left;     left     = 0;
    delete right;    right    = 0;
}

////////////////////////////////////////////////////////////////////////////

k_ab_rbTree::k_ab_rbTree():_kabLst(0), _head(0), _linInd(-1) {
}

k_ab_rbTree::~k_ab_rbTree() {

    _kabLst = 0;
    delete _head; _head = 0;
}

void k_ab_rbTree::insert(const k_ab_Index& kabInd) {

    _RB_insert(_head, kabInd,0);
    _head->red = 0;
}

int k_ab_rbTree::linIndex(const k_ab_Index& kabInd) {
    return (_RB_search(_head, kabInd))->linearIndex;
}

void k_ab_rbTree::linearize(k_ab_List* ext_kabLst) {

    cout << "k_ab_rbTree: start generate k_ab_List..";

    _kabLst = ext_kabLst;
    _RB_traversal(_head);
    _kabLst = 0;

    cout << ". done ok; Ylist->size = " << ext_kabLst->size() << endl;
}

void k_ab_rbTree::_rotR(k_ab_rbNode* &h) {

    k_ab_rbNode* x = h->left;
    h->left = x->right;
    x->right = h;
    h = x;
}

void k_ab_rbTree::_rotL(k_ab_rbNode* &h) {

    k_ab_rbNode* x = h->right;
    h->right = x->left;
    x->left = h;
    h = x;
}

int k_ab_rbTree::_is_red(k_ab_rbNode *x) {

    if (0 == x) return 0;
    return x->red;
}

void k_ab_rbTree::_RB_insert(k_ab_rbNode* &h,const k_ab_Index& ins_kabInd, int sw) {

    if (0 == h) {

        h = new k_ab_rbNode;
        h->k_ab_ind = new k_ab_Index(ins_kabInd);
        h->linearIndex = ++_linInd;
        return;
    }

    if (_is_red(h->left) && _is_red(h->right)) {

        h->red = 1; 
        h->left->red  = 0; 
        h->right->red = 0;
    }

    if (ins_kabInd > (*(h->k_ab_ind)) ) {

        _RB_insert(h->right,ins_kabInd,1);

        if (_is_red(h) && _is_red(h->right) && !sw) {_rotL(h);}

        if (_is_red(h->right) && _is_red(h->right->right)) {
             _rotL(h);
            h->red = 0;
            h->left->red = 1;
        }
    } else if ( !( ins_kabInd == (*(h->k_ab_ind)) ) ) {

        _RB_insert(h->left,ins_kabInd,0);

        if (_is_red(h) && _is_red(h->left) && sw){_rotR(h);}

        if (_is_red(h->left) && _is_red(h->left->left)) {
            _rotR(h);
            h->red = 0;
            h->right->red = 1;
        }
    }
}

k_ab_rbNode* k_ab_rbTree::_RB_search(k_ab_rbNode* h, const k_ab_Index& ptrn_kabInd) {

    if (0 == h) {

        cout << "Error k_ab_rbTree::_RB_search(" << endl;
        cout << "No such index: "; 
        ptrn_kabInd.print();
        exit(0);
    }

    if ( ptrn_kabInd == (*(h->k_ab_ind)) ) {
        return h;
    }

    if (ptrn_kabInd > (*(h->k_ab_ind))) {
        return _RB_search(h->right, ptrn_kabInd);
    } else {
        return _RB_search(h->left, ptrn_kabInd);
    }
}

void k_ab_rbTree::_RB_traversal(k_ab_rbNode* h) {

    if (0 == h) return;

    _kabLst->add(h->k_ab_ind, h->linearIndex);

    _RB_traversal(h->right);
    _RB_traversal(h->left);
}

////////////////////////////////////////////////////////////////////////////

// end of file

