#include <iostream>
#include <fstream>
#include <cstdlib>
#include "wflags.h"
#include "read.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

writeFlags::writeFlags(const char file[]): 
_E_acc(1.0),
_is_restore(false),
_is_dump(false),
_is_e_trace(false),
_is_rho(false),
_is_csfBas(false),
_is_oneList(false),
_is_twoList(false) {

    if (ReadReal(file,"E_acc",0,_E_acc)) {
        cout << "wFlags: Error read E_acc" << endl;
        exit(0);
    }

    _restore_name[0] = '\0';
    _dump_name[0]    = '\0';
    _e_trace_name[0] = '\0';
    _rho_name[0]     = '\0';
    _csfBas_name[0]  = '\0';
    _oneList_name[0] = '\0';
    _twoList_name[0] = '\0';

    cout << "writeFlags:" << endl;

    _read(file,"pnl_restore: ok   ","pnl_restore: no",_is_restore, "pnl_restore",_restore_name);
    _read(file,"pnl_dump:    ok   ","pnl_dump:    no",_is_dump,    "pnl_dump",   _dump_name);
    _read(file,"e_trace:     ok   ","e_trace:     no",_is_e_trace, "e_trace",    _e_trace_name);
    _read(file,"rho:         ok   ","rho:         no",_is_rho,     "rho",        _rho_name);
    _read(file,"csf_basis:   ok   ","csf_basis:   no",_is_csfBas,  "csf_basis",  _csfBas_name);
    _read(file,"one_list:    ok   ","one_list:    no",_is_oneList, "one_list",   _oneList_name);
    _read(file,"two_list:    ok   ","two_list:    no",_is_twoList, "two_list",   _twoList_name);

    if ( _is_restore && !ifstream(_restore_name,ios::in)) {

        cout << "writeFlags: Error open file " << _restore_name << endl;
        exit(0);
    }

    cout << "E_mchf accuracy = " << _E_acc << endl;
}

writeFlags::~writeFlags() {
}

void writeFlags::_read(const char file[], const char * const text_ok, 
                       const char * const text_no, bool & prm_flag,
                       const char * const param, char * const data) {

    int fkey;

    if (!ReadInt(file, param, 0, fkey)) {

        if (1 == fkey) {

            prm_flag = true;
       
            if (!ReadStr(file, param, 1, data) ) {
           
                cout << text_ok << data << endl;

            } else {

                cout << "Error in writeFlags.. No pos_1 param = " << param << endl;
                exit(0);
            }

        } else {
           cout << text_no << endl;
        }
    
    } else {
    
        cout << "Error in writeFlags.. No pos_0 param = " << param << endl;
        exit(0);
    }

}

////////////////////////////////////////////////////////////////////////////

// end of file

