#include <iostream>
#include <cstdlib>
#include "qbas_conf.h"
#include "read.h" 

using namespace::std;

////////////////////////////////////////////////////////////////////////////

qBas_Conf::qBas_Conf(const char file[]):
_lmax(0), _Nmax(0), _Nlmax(0), _zeta(0), _kappa(0) {

    if (ReadReal(file,"k_mixt",0,_kappa)) {
        cout << "qBas_Conf: Error read k_mixt" << endl;
        exit(0);
    }

    if (_kappa < 0.0 || _kappa > 1.0) {
        cout << "qBas_Conf: Error range k_mixt = " << _kappa << endl;
        exit(0);    
    }

    if (ReadInt(file,"qBas",0,_lmax)) { 
        cout << "qBas_Conf: Error read l_max" << endl; 
        exit(0);
    }

    if (_lmax < 0) { 
        cout << "qBas_Conf: Error l_max = " << _lmax << endl; 
        exit(0);
    }

    _Nmax = new int   [_lmax+1];
    _zeta = new double[_lmax+1];

    for (int l = 0; l <= _lmax; ++l) {

        if (ReadInt(file,"qBas",2*l+1,_Nmax[l])) {
            cout << "qBas_Conf: Error read N_max" << endl;
            exit(0);
        }

        if (ReadReal(file,"qBas",2*l+2,_zeta[l])) {
            cout << "qBas_Conf: Error read zeta" << endl;
            exit(0);
        }

        if (_zeta[l] < 1e-9) {
            cout << "qBas_Conf: Error zeta = " << _zeta[l] << endl;
            exit(0);        
        }

        if (_zeta[l] != _zeta[0]) {
            cout << "qBas_Conf: Error not eqviv zeta:   0 != " << l << endl;
            exit(0);        
        }

        if (_Nlmax < _Nmax[l]) _Nlmax = _Nmax[l];
    }
}

qBas_Conf::~qBas_Conf() {
    delete[] _Nmax; _Nmax = 0;
    delete[] _zeta; _zeta = 0;
}

int qBas_Conf::lmax() const {
    return _lmax;
}

int qBas_Conf::Nlmax() const {
    return _Nlmax;
}

int qBas_Conf::Nmax(const int l) const {

    if (l < 0 || l > _lmax) {    
        cout << "qBas_Conf: Error Nmax( l = " << l << endl;
        exit(0);
    }
    return _Nmax[l];
}

double qBas_Conf::zeta(const int l) const {

    if (l < 0 || l > _lmax) {    
        cout << "qBas_Conf: Error zeta( l = " << l << endl;
        exit(0);
    }
    return _zeta[l];
}

void qBas_Conf::print() const {

    cout << "qBas_Conf: lmax = " << lmax() << " ";
    cout << " Nlmax = " << Nlmax() << endl;

    for (int ll = 0; ll <= lmax(); ++ll) {    
        cout << "N[" << ll << "] = "<< Nmax(ll) << "  ";
        cout << _zeta[ll] << endl;
    }
    cout << "k_mixt = " << _kappa;
    cout << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file

