#ifndef _UNCOPYABLE_BASE_H_
#define _UNCOPYABLE_BASE_H_

////////////////////////////////////////////////////////////////////////////

class Uncopyable {

protected:

    Uncopyable() {}
    ~Uncopyable() {}

private:

    Uncopyable(const Uncopyable&);
    Uncopyable& operator= (const Uncopyable&);
};

////////////////////////////////////////////////////////////////////////////

#endif // _UNCOPYABLE_BASE_H_

//end of file

