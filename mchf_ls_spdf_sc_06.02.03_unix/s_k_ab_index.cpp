#include <iostream>
#include <cstdlib>
#include "k_ab_index.h"
#include "s_k_ab_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

s_k_ab_Index::s_k_ab_Index(const int s_ext, const k_ab_Index& kabInd):
_s(s_ext), _kabInd(new k_ab_Index(kabInd)) {

    if (0 > _s) {
    
        cout << "s_k_ab_Index: Error in constructor; s = " << _s << endl;
        exit(0);
    }
}

s_k_ab_Index::s_k_ab_Index(const s_k_ab_Index& rhs):
_s(rhs._s), _kabInd(new k_ab_Index(*(rhs._kabInd))) {
}

s_k_ab_Index::~s_k_ab_Index() {
    delete _kabInd;
}

s_k_ab_Index& s_k_ab_Index::operator= (const s_k_ab_Index& rhs) {

    if (&rhs == this) return *this;

    _s = rhs._s;
    *(_kabInd) = *(rhs._kabInd);

    return *this;
}

void s_k_ab_Index::reset(const int ext_s) {

    _s = ext_s;

    if (0 > _s) {
    
        cout << "s_k_ab_Index: Error in reset; s = " << _s << endl;
        exit(0);
    }
}

void s_k_ab_Index::reset(const int ext_s, const k_ab_Index& kabInd) {

    reset(ext_s);
    _kabInd->reset(kabInd);
}

void s_k_ab_Index::print() const {

    cout << "s = " << _s << " ";
    _kabInd->print();
}

//friend 
bool operator == (const s_k_ab_Index& lhs, const s_k_ab_Index& rhs) {

    if (lhs._s != rhs._s) return false;
    return (*(lhs._kabInd)) == (*(rhs._kabInd));
}

//friend 
bool operator > (const s_k_ab_Index& lhs, const s_k_ab_Index& rhs) {

    if (lhs._s > rhs._s) return true;
    if (lhs._s < rhs._s) return false;

    return (*(lhs._kabInd)) > (*(rhs._kabInd));
}

////////////////////////////////////////////////////////////////////////////

// end of file

