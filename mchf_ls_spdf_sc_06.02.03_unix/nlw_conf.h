#ifndef _MANYSHELL_nlw_ELECTR_CONF_H_
#define _MANYSHELL_nlw_ELECTR_CONF_H_

////////////////////////////////////////////////////////////////////////////

class q4x_Convertor;

class nlw_Conf {

public:

    nlw_Conf();
    nlw_Conf(const int * const w, const q4x_Convertor * const q4xC);
    nlw_Conf(const nlw_Conf& rhs);
    nlw_Conf(const int k, const int * const n, const int * const l, const int * const w);
    ~nlw_Conf();

    nlw_Conf& operator= (const nlw_Conf& rhs); // = 0

    int numShells() const {return _numShells;} //20(1),21(4),43(2) = 3
    int numEl_all() const {return _numEl_all;} //                  = 7
    int num_pos()   const {return _num_pos;  } // =C(1,2)*C(4,6)*C(2,14)

    void nlw(int &n,int &l,int &w, const int k) const;
    int  n(const int k) const;
    int  l(const int k) const;
    int  w(const int k) const;

    void print() const;

private:

    int  _numShells;
    int  _numEl_all;
    int  _num_pos;

    int *_n;
    int *_l;
    int *_w;

    void _calc_numEl_all();
    void _calc_num_pos();
    void _check_nlw(const int n, const int l, const int w);
};

////////////////////////////////////////////////////////////////////////////

#endif // _MANYSHELL_nlw_ELECTR_CONF_H_

// end of file

