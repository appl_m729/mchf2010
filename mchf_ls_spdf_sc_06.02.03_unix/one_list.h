#ifndef _ONE_IAB_HASH_LIST_H_
#define _ONE_IAB_HASH_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class ab_Index;
class ccv_List;
class ccv_RB_Tree;
class Radial_Pnl_Data;


class One_DataBlock : private Uncopyable {

public:

    One_DataBlock * next;

    double value;
    const ab_Index * index_ab;
    ccv_List * ccvLst;

    One_DataBlock();
    ~One_DataBlock();
};


class One_List : private Uncopyable {

public:

    One_List();
    ~One_List();

    void wholeUpdate(const Radial_Pnl_Data * const Radial_data);

    void add(const ab_Index * const index_ab,
             const ccv_RB_Tree * const ccvTree);
    void to_head() const;
    bool next() const;

    int size() const;

    double value() const;
    const ab_Index& abInd() const;
    const ccv_List& ccvLst() const;
    const ccv_List * p_ccvLst() const;

    void write(const char file[]) const;

private:

    One_DataBlock * const _phead;
    mutable One_DataBlock* _crnt_item;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _ONE_IAB_HASH_LIST_H_

// end of file

