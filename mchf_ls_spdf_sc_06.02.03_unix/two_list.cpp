#include <fstream>
#include <iostream>
#include <iomanip>
#include "two_list.h"
#include "k_abcd_index.h"
#include "ccv_rbtree.h"
#include "ccv_list.h"
#include "k_ab_index.h"
#include "pnl_radial.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Two_DataBlock::Two_DataBlock():
next(0), index_k_abcd(0), value(0), ccvLst(0) {
}

Two_DataBlock::~Two_DataBlock() {

    index_k_abcd = 0;
    delete ccvLst;

    delete next; next = 0;
}

////////////////////////////////////////////////////////////////////////////

Two_List::Two_List(): _head_item(new Two_DataBlock),
_crnt_item(_head_item), _size(0) {
    
}

Two_List::~Two_List() {

    _crnt_item = 0;
    delete _head_item;
}

void Two_List::wholeUpdate(const Radial_Pnl_Data * const RadialData) {

    Two_DataBlock * const tmpBuff = _crnt_item;

    _crnt_item = _head_item;
    while ( _crnt_item->next ) {

        _crnt_item = _crnt_item->next;
        _crnt_item->value = RadialData->Rk(*(_crnt_item->index_k_abcd));
    }

    _crnt_item = tmpBuff;
}

void Two_List::add(const k_abcd_Index * const Ind_k_abcd, 
                   const ccv_RB_Tree * const ext_ccvTree) {

    ccv_List * const t_list = new ccv_List;
    ext_ccvTree->generate_ccvList(t_list);

    if (t_list->size() > 0) {

        _crnt_item->next = new Two_DataBlock;
        _crnt_item = _crnt_item->next;

        _crnt_item->index_k_abcd = Ind_k_abcd;
        _crnt_item->value = 0;

        _crnt_item->ccvLst = t_list;
        ++_size;

    } else {
    
        delete t_list;
    }
}

void Two_List::to_head() const {
    _crnt_item = _head_item;
}

bool Two_List::next() const {

    if ( _crnt_item->next ) {

        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int Two_List::size() const {
    return _size;
}

const k_abcd_Index& Two_List::twoInd() const {
    return *(_crnt_item->index_k_abcd);
}

double Two_List::value() const {
    return _crnt_item->value;
}

const ccv_List& Two_List::ccvLst() const {
    return *(_crnt_item->ccvLst);
}

const ccv_List* Two_List::p_ccvLst() const {
    return _crnt_item->ccvLst;
}

void Two_List::write(const char file[]) const {

    cout << "Two_List: writing in " << file << "..";
    ofstream fout(file);

    fout << "two_List data,  size = " << _size << endl;
    Two_DataBlock * const tmpBuff = _crnt_item;

    _crnt_item = _head_item;
    while ( _crnt_item->next ) {

        _crnt_item = _crnt_item->next;

        fout << endl << "k_abcd_Index:  ";
        fout << _crnt_item->index_k_abcd->k() << "  ";
        fout << _crnt_item->index_k_abcd->a() << " ";
        fout << _crnt_item->index_k_abcd->b() << " ";
        fout << _crnt_item->index_k_abcd->c() << " ";
        fout << _crnt_item->index_k_abcd->d() << endl;
        fout << "R_value = " << setprecision(12) << _crnt_item->value << endl;

        _crnt_item->ccvLst->to_head();
        while(_crnt_item->ccvLst->next()) {

            fout << "i = " << _crnt_item->ccvLst->i();
            fout << " j = " << _crnt_item->ccvLst->j();
            fout << " value = " << _crnt_item->ccvLst->value() << endl;
       }
    }

    fout.close();

    _crnt_item = tmpBuff;

    cout << ". done ok" << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file


