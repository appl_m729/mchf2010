#ifndef _HASH_ENERGY_H_
#define _HASH_ENERGY_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Det_State;
class nlmlms_State;
class CSF_Basis;
class Shells_Det;
class Pnl_IndexMap;
class CompleteCoeffs;
class ab_Index;
class k_abcd_Index;
class One_RB_Tree;
class One_List;
class Two_RB_Tree;
class Two_List;
class Atom_Conf;


class Energy_Hash : private Uncopyable {

public:

    Energy_Hash(const Atom_Conf * const aCnf,
                const CSF_Basis * const CSFBas,
                const Pnl_IndexMap * const pnlInd);
    ~Energy_Hash();

    void generate_One(One_List * const oneLst) const;
    void generate_Two(Two_List * const twoLst) const;

private:

    const CSF_Basis * const    _csfBas;
    const Pnl_IndexMap * const _pnlInd;
    Shells_Det * const         _shells;
    CompleteCoeffs * const     _cft;
    ab_Index * const           _indOne;
    k_abcd_Index * const       _indTwo;
    One_RB_Tree * const        _oneTree;
    Two_RB_Tree * const        _twoTree;

    void _setup();

    void _collect_f_0(const int i, const int j, const int ki, const int sj);
    void _collect_f_1(const int i, const int j, const int ki, const int sj,
                      const int pos1ki, const int pos1sj);

    void _collect_g_0(const int i, const int j, const int ki, const int sj);
    void _collect_g_1(const int i, const int j, const int ki, const int sj,
                      const int pos1ki, const int pos1sj);
    void _collect_g_2(const int i, const int j, const int ki, const int sj,
                      const int pos1ki, const int pos1sj, 
                      const int pos2ki, const int pos2sj);

    int _sign_one(const int pos1ki, const int pos1sj) const;

    int _sign_two(const int pos1ki, const int pos1sj,
                  const int pos2ki, const int pos2sj) const;

    void _add_two_any(const double fctr_sign,
                      const int i, const int j, const int ki, const int sj,
                      const nlmlms_State& el1, const nlmlms_State& el2,
                      const nlmlms_State& el3, const nlmlms_State& el4);

    int _max(const int a, const int b) const;
    int _min(const int a, const int b) const;
    int _int_abs(const int a) const;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _HASH_ENERGY_H_

//end of file

