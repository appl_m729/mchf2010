#include <iostream>
#include <cstdlib>
#include "ksi_hash.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

ksiHash_DataBlock::ksiHash_DataBlock():
next(0), l(-1), diag_factor(0), ccvLst(0) {
}

ksiHash_DataBlock::~ksiHash_DataBlock() {

    ccvLst = 0;
    delete next; next = 0;
}

////////////////////////////////////////////////////////////////////////////

ksiHash_List::ksiHash_List(): _phead(new ksiHash_DataBlock),
_crnt_item(_phead), _size(0) {
}

ksiHash_List::~ksiHash_List() {

    _crnt_item = 0;
    delete _phead;
}

void ksiHash_List::clear() {

    if (_phead->next) {
        delete _phead->next; _phead->next = 0;
    }
    _crnt_item = _phead;
    _size = 0;
}

void ksiHash_List::add(const ccv_List * ext_ccv_Lst, const int ext_l) {

    _crnt_item = _phead;
    while (_crnt_item->next) {

        _crnt_item = _crnt_item->next;

        if (ext_l == _crnt_item->l) {

            if (ext_ccv_Lst != _crnt_item->ccvLst) {

                cout << "Error in ksiHash_List::add." << endl;
                cout << "Incorrect identical ext_l = " << ext_l << endl;
                exit(0);            
            }

            _crnt_item->diag_factor++;

            if (2 < _crnt_item->diag_factor) {

                cout << "Error in ksiHash_List::add." << endl;
                cout << "diag_factor > 2" << endl;
                exit(0);            
            }

            return;
        }
    }

    _crnt_item->next = new ksiHash_DataBlock;
    _crnt_item = _crnt_item->next;

    _crnt_item->diag_factor = 1;
    _crnt_item->ccvLst = ext_ccv_Lst;
    _crnt_item->l = ext_l;

    _size++;

    if (1 < _size) {    
        cout << "Error in ksi_List::add; _size > 1" << endl;
        exit(0);
    }
}

void ksiHash_List::to_head() const {
    _crnt_item = _phead;
}

bool ksiHash_List::next() const {

    if (_crnt_item->next) {
        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int ksiHash_List::diag_factor() const {
    return _crnt_item->diag_factor;
}

const ccv_List& ksiHash_List::ccvLst() const {
    return *(_crnt_item->ccvLst);
}

int ksiHash_List::l() const {

    return _crnt_item->l;
}

////////////////////////////////////////////////////////////////////////////

// end of file

