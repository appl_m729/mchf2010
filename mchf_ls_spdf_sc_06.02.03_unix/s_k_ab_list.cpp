#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "s_k_ab_index.h"
#include "s_k_ab_list.h"
#include "k_ab_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

s_k_ab_DataBlock::s_k_ab_DataBlock():
next(0), s_k_ab_ind(0), linearIndex(-1) {
}

s_k_ab_DataBlock::~s_k_ab_DataBlock() {

    s_k_ab_ind = 0;
    delete next; next = 0;
}

////////////////////////////////////////////////////////////////////////////

s_k_ab_List::s_k_ab_List():
_head(new s_k_ab_DataBlock), _crnt_item(_head), _size(0) {

}

s_k_ab_List::~s_k_ab_List() {

    delete _head;
    _crnt_item = 0;
}

void s_k_ab_List::clear() {

    if (_head->next) {
        delete _head->next; _head->next = 0;
    }

    _crnt_item = _head;
    _size = 0;
}

void s_k_ab_List::add(const s_k_ab_Index* skabInd, const int ext_linIndex) {

    _crnt_item->next = new s_k_ab_DataBlock;
    _crnt_item = _crnt_item->next;
    _crnt_item->s_k_ab_ind = skabInd;
    _crnt_item->linearIndex = ext_linIndex;

    ++_size;
}

void s_k_ab_List::to_head() const {
    _crnt_item = _head;
}

bool s_k_ab_List::next() const {

    if (_crnt_item->next) {
        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int s_k_ab_List::size() const {
    return _size;
}

const s_k_ab_Index& s_k_ab_List::s_k_ab_Ind() const {

    return *(_crnt_item->s_k_ab_ind);
}

int s_k_ab_List::linInd() const {

    return _crnt_item->linearIndex;
}

void s_k_ab_List::write(const char file[]) const {

    cout << "s_k_ab_List: writing in "<< file << "..";
    ofstream fout(file);

    fout << "total size = " << size() << endl << endl;

    fout << "s_k_ab_Index: " << endl;
    fout << "position   linearIndex   s  k  a  b" << endl;

    int ii = 0;
    to_head();
    while(next()) {

        int k,a,b, s(s_k_ab_Ind().s());
        s_k_ab_Ind().kabInd().k_ab(k,a,b);

        fout << endl << setw(4) << ii++ << setw(4) << linInd();
        fout << setw(4) << s << setw(4) << k;
        fout << setw(4) << a << setw(4) << b;
    }

    fout << endl << endl << "end of file" << endl;

    fout.close();
    cout << ". done ok" << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file
