#ifndef _CCV_REDBLACK_TREE_H_
#define _CCV_REDBLACK_TREE_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class ab_Index;
class ccv_List;


class ccv_RB_node : private Uncopyable {

public:

    ccv_RB_node *left, *right;
    int red;

    ab_Index * abInd;
    double value;

    ccv_RB_node();
    ~ccv_RB_node();
};


class ccv_RB_Tree : private Uncopyable {

public:

    ccv_RB_Tree();
    ~ccv_RB_Tree();

    void generate_ccvList(ccv_List * const ccvLst) const;
    void add(const int i, const int j, const double Val);

    int  size() const {return _size;} 

private:

    ccv_RB_node *_head;
    int          _size;

    mutable ccv_List * _ccvLst;

    void _rotR(ccv_RB_node* &h);
    void _rotL(ccv_RB_node* &h);
    int  _is_red(ccv_RB_node *x);

    void _insert(const ab_Index& Ind_ab); 
    void _RB_insert(ccv_RB_node* &h,const ab_Index& insOneInd, int sw);
    ccv_RB_node* _RB_search(ccv_RB_node* h, const ab_Index& ptrnOneInd);

    void _catch_next_node(const ccv_RB_node* h) const;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _CCV_REDBLACK_TREE_H_

// end of file
