#ifndef _ITERATION_CONVERGE_CONGIG_H_
#define _ITERATION_CONVERGE_CONGIG_H_

////////////////////////////////////////////////////////////////////////////

class Pnl_IndexMap;

class dqIter_Conf {

public:

    dqIter_Conf(const char * const file, const Pnl_IndexMap * const pnlInd);
    ~dqIter_Conf();

    int N() const {return _N;}
    double alpha(const int a) const {return _alpha[a];}

    void print() const;

private:

    const int _N;
    double * const _alpha;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _ITERATION_CONVERGE_CONGIG_H_

// end of file
