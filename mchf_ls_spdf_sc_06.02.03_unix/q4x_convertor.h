#ifndef _nlmlms_x_CONVERTOR_H_
#define _nlmlms_x_CONVERTOR_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Atom_Conf;
class nlmlms_State;
class nlw_Conf;


class q4x_Convertor : private Uncopyable {

public:

    q4x_Convertor(const Atom_Conf * const p_aCnf);
    ~q4x_Convertor();

    int  nl_length() const;
    int  n_ss(const int v) const;
    int  l_ss(const int v) const;
    void nl(int &n, int &l, const int v) const;  // v - index of subshell
    int  v_ind(const int n, const int l) const;

    int  w_max(const int v) const;               // max number = 2(2l+1)
    int  w_max(const int n, const int l) const;
    int  max_wmax() const {return _max_wmax;}
    int  w_hill(const int v) const;
    int  w_free(const int v) const; // w_max - w_hill

    int  x(const int n, const int l, const int ml, const double ms) const;
    int  x(const nlmlms_State& elSt) const;
    int  x_last() const;
    int  x_sta(const nlw_Conf& Cnf_nlw, const int v) const; //(n1,l1)w1,..,(nv,lv)wv,..
    int  x_end(const nlw_Conf& Cnf_nlw, const int v) const;

    int  n(const int x) const;  // 0 <= x < Length
    int  l(const int x) const;
    int  ml(const int x) const;
    double ms(const int x) const;
    nlmlms_State elst(const int x) const;

    int  x_length() const;
    void print() const;

private:

    const Atom_Conf * const _aCnf;

    int     _nl_length;  // number of all different (nl)_i subshells
    int    *_wmax_v; // max nl occupation numbers
    int    *_whill_v;
    int    *_n_v;        // n of nl_i's subshell
    int    *_l_v;        // l of nl_i's subshell
    int     _max_wmax, _lmax;

    int    *_n, *_l, *_ml, *_x; // quantum numbers x <-> (n,l,m,ms)
    double *_ms;
    int     _x_length;


    void  _setup_wv_nv_lv();
    void  _calc_x_length_new_nlmlmsx();
    void  _reset_map();
    void  _fill_segment(const int n, const int l, int &i);

    int   _v_ind(const int n, const int l) const;
    int   _ind(const int n, const int l, const int ml, const double ms) const;
    int   _min_i(const int a, const int b);
    void  _swap_nl(const int si, const int sj);
};

#endif // _nlmlms_x_CONVERTOR_H_

////////////////////////////////////////////////////////////////////////////

// end of file

