#ifndef _ETA_LIST_H_
#define _ETA_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class eta_Index;
class s_k_ab_rbTree;

class Eta_DataBlock : private Uncopyable {

public:

    Eta_DataBlock * next;

    int lin_ind;
    const eta_Index * eta_ind;

    Eta_DataBlock();
    ~Eta_DataBlock();
};


class Eta_List : private Uncopyable {

public:

    Eta_List();
    ~Eta_List();

    void generate_2skabTrees(s_k_ab_rbTree * const M_sTree,
                             s_k_ab_rbTree * const W_sTree) const;
    void clear();

    void add(const eta_Index* EtaInd, const int linIndex);

    void to_head() const;
    bool next() const;

    const eta_Index& etaInd() const;
    int linIndex() const;

    int size() const {return _size;}

    void write(const char file[]) const;

private:

    Eta_DataBlock * const _phead;
    mutable Eta_DataBlock * _crnt_item;
    Eta_DataBlock * _ptail;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif // _ETA_LIST_H_

// end of file

