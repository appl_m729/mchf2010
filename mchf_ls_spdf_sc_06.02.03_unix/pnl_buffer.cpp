#include <iostream>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <fstream>
#include "pnl_buffer.h"
#include "pnl_index_map.h"
#include "qbas_conf.h"
#include "vec_eq_syst.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Pnl_Buffer::Pnl_Buffer(const Pnl_IndexMap * const pnlIndex,
                       const qBas_Conf * const basCnf):
_pnlInd(pnlIndex), _basCnf(basCnf), _k_iter(0),
_b(new double[_pnlInd->length()*_basCnf->Nlmax()]) {
}

Pnl_Buffer::~Pnl_Buffer() {
    delete[] _b;
}

void Pnl_Buffer::restore(const char file[]) {

    init_zero();

    cout << "Pnl_Buffer: restore from " << file ;

    ifstream fin(file);

    for (int l = 0; l <= _pnlInd->lmax(); ++l) {    
        for (int al = _pnlInd->a_sta(l); al <= _pnlInd->a_end(l); ++al) {

            int t_sz(32000);
            fin >> t_sz;
 
            if (t_sz > _basCnf->Nmax(l)) {

                cout << "Pnl_Buffer: Error in restore.. " << endl;
                cout << "t_sz = " << t_sz << " Nmax(" << l << ") = " << _basCnf->Nmax(l) << endl;
                exit(0);
            }

            for (int nq = 0; nq < t_sz; ++nq) {

                fin  >> _b[al*_basCnf->Nlmax() + nq];
            }
        }
    }

    fin.close();

    cout << " ...done ok" << endl;
}

void Pnl_Buffer::init_zero() {

    cout << "Pnl_Buffer: init_zero()" << endl;

    for (int l = 0; l <= _pnlInd->lmax(); ++l) {    
        for (int al = _pnlInd->a_sta(l); al <= _pnlInd->a_end(l); ++al) {

            for (int nq = 0; nq < _basCnf->Nmax(l); ++nq) {
                _b[al*_basCnf->Nlmax() + nq] = 0;
            }
        }
    }
}

void Pnl_Buffer::init1() {

    cout << "Pnl_Buffer: init1()" << endl;

    for (int l = 0; l <= _pnlInd->lmax(); ++l) {    
        for (int al = _pnlInd->a_sta(l); al <= _pnlInd->a_end(l); ++al) {

            for (int nq = 0; nq < _basCnf->Nmax(l); ++nq) {
                _b[al*_basCnf->Nlmax() + nq] = 0;
            }
            _b[al*_basCnf->Nlmax() + _pnlInd->n(al)-l-1] = 1;
        }
    }
}

void Pnl_Buffer::init_sample() {

    cout << "Pnl_Buffer: init_sample()" << endl;

    for (int l = 0; l <= _pnlInd->lmax(); ++l) {
        for (int al = _pnlInd->a_sta(l); al <= _pnlInd->a_end(l); ++al) {

            for (int nq = 0; nq < _basCnf->Nmax(l); ++nq) {
                _b[al*_basCnf->Nlmax() + nq] = 0;
            }
        }
    }

    _b[0*_basCnf->Nlmax() + 0] =  1/sqrt(2.0);
    _b[0*_basCnf->Nlmax() + 1] = -1/sqrt(3.0);
    _b[0*_basCnf->Nlmax() + 2] =  1/sqrt(4.0);

    _b[1*_basCnf->Nlmax() + 1] =  1/sqrt(5.0);
    _b[1*_basCnf->Nlmax() + 2] = -1/sqrt(6.0);
    _b[1*_basCnf->Nlmax() + 4] =  2/sqrt(7.0);

    _b[3*_basCnf->Nlmax() + 3] =  1/sqrt(5.0);
    _b[3*_basCnf->Nlmax() + 6] =  2/sqrt(11.0);
    _b[3*_basCnf->Nlmax() + 9] =  3/sqrt(17.0);
}

double Pnl_Buffer::b(const int a, const int i) const {

    const int la = _pnlInd->l(a);

    if (i < 0 || i >= _basCnf->Nmax(la)) {
    
        cout << "Error in Pnl_Buffer::b(" << endl;
        cout << "Nmax = " << _basCnf->Nmax(la);
        cout << " a = " << a << "  i = " << i << endl;
        exit(0);
    }

    return _b[a*_basCnf->Nlmax() + i];
}

int Pnl_Buffer::k_iter() const {
    return _k_iter;
}

void Pnl_Buffer::save(const VecEq_Syst* const veqSst) {

    const double t_mixt = _basCnf->kappa();

    cout << "Pnl_Buffer: save b.." << endl;

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int la = _pnlInd->l(a);
        const int pos_a = a*_basCnf->Nlmax();

        for (int i = 0; i < _basCnf->Nmax(la); ++i) {
                
            _b[pos_a + i] = (1.0-t_mixt)*_b[pos_a + i] + t_mixt*veqSst->b(a,i);
        }
        _normilize(a);
    }

    _print_b_data();

    _k_iter++;

    cout << "Pnl_Buffer:  save b done ok." << endl;
}

void Pnl_Buffer::dump(const char file[]) const {

    cout << "Pnl_Buffer: start dumping in " << file ;

    ofstream fout(file);

    for (int l = 0; l <= _pnlInd->lmax(); ++l) {    
        for (int al = _pnlInd->a_sta(l); al <= _pnlInd->a_end(l); ++al) {

            fout << _basCnf->Nmax(l) << endl;

            for (int nq = 0; nq < _basCnf->Nmax(l); ++nq) {

                fout << setprecision(16) << _b[al*_basCnf->Nlmax() + nq] << endl;
            }
        }
    }

    fout.close();

    cout << " ...done ok" << endl;
}

void Pnl_Buffer::_normilize(const int a) {

    const int la = _pnlInd->l(a);
    const int sz = _basCnf->Nmax(la);
    const int pos_a = a*_basCnf->Nlmax();

    const int i_max = _find_max(a, 0, sz-1);

    const double v_nrm = _bsqrsum(a, 0, sz-1, i_max);

    for (int i = 0; i < sz; ++i) {

        _b[pos_a + i] /= v_nrm;
    }
}

/*
double VecEq_Syst::_norm(const double * const x, const int sz) const {

    const int i_max = _max_ind(x,sz);
    double sum_sq = 0;

    {for (int i = 0; i < i_max; ++i) {

        const double t_xixm = x[i]/x[i_max];
        sum_sq += t_xixm*t_xixm;
    }}

    {for (int i = i_max+1; i < sz; ++i) {

        const double t_xixm = x[i]/x[i_max];
        sum_sq += t_xixm*t_xixm;
    }}
    sum_sq = fabs(x[i_max])*sqrt(sum_sq + 1.0);

    return sum_sq;
}

int VecEq_Syst::_max_ind(const double * const x, const int sz) const {

    int i_max = 0;
    for (int i = 1; i < sz; ++i) {
        if (fabs(x[i]) > fabs(x[i_max])) {
            i_max = i;
        }
    }
    return i_max;
}

void VecEq_Syst::_normalize(double * x, const int sz) {

    const double v_nrm = _norm(x,sz);
    for (int i = 0; i < sz; ++i) x[i] /= v_nrm;
}

*/

void Pnl_Buffer::_print_b_data() const {

    cout << "a i_max_head i_max_tail sqrt(sum_(head_half)_of_b2) sqrt(sum_(tail_half)_of_b2)  ratio" << endl;

    for (int a = 0; a < _pnlInd->length(); ++a) {

        const int la(_pnlInd->l(a));
        const int index_05(_basCnf->Nmax(la)/2);

        const int i_max_0(_find_max(a, 0, index_05-1));
        const int i_max_N(_find_max(a, index_05, _basCnf->Nmax(la)-1));

        const double sum0(_bsqrsum(a, 0, index_05, i_max_0));
        const double sum1(_bsqrsum(a, index_05+1, _basCnf->Nmax(la)-1, i_max_N));

        cout << a << "   " << i_max_0 << "  " << i_max_N << "  ";
        cout << sum0 << "  " << sum1 << "  " << sum1/sum0 << endl;
    } cout << endl;

    for (int l = 0; l <= _pnlInd->lmax(); ++l) {    

        cout << "l = " << l << endl;

        cout << "a = ";
        for (int al = _pnlInd->a_sta(l); al <= _pnlInd->a_end(l); ++al) {
            cout << al << " ";
        }cout << endl;

        for (int nq = 0; nq < _basCnf->Nmax(l); ++nq) {

            cout << setw(3) << nq;
            for (int al = _pnlInd->a_sta(l); al <= _pnlInd->a_end(l); ++al) {

                cout << setw(16) << setprecision(10) << _b[al*_basCnf->Nlmax() + nq];
            }cout << endl;
        }
        cout << endl;
    }
}

int Pnl_Buffer::_find_max(const int a, const int i1, const int i2) const {

    const int pos_a = a*_basCnf->Nlmax();
    int i_max = i1;

    for (int j = i1+1; j <= i2; ++j) {
    
        if (fabs(_b[pos_a + i_max]) < fabs(_b[pos_a + j])) {i_max = j;}    
    }

    return i_max;
}

double Pnl_Buffer::_bsqrsum(const int a, const int i1, const int i2, const int i_max) const {

    const int pos_a = a*_basCnf->Nlmax();
    double sum = 0;

    for (int j = i1; j <= i2; ++j) {

        if (i_max == j) continue;
    
        const double val = _b[pos_a + j]/_b[pos_a + i_max];
        sum += val*val;    
    }

    return fabs(_b[pos_a+i_max])*sqrt(1.0+sum);
}

////////////////////////////////////////////////////////////////////////////

// end of file

