#ifndef _RADIAL_Pnl_DATA_H_
#define _RADIAL_Pnl_DATA_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Pnl_IndexMap;
class Pnl_Buffer;
class ab_Index;
class k_ab_Index;
class k_abcd_Index;
class eta_Index;
class qBas_Conf;
class qqHash;
class s_k_ab_Index;
class s_k_ab_rbTree;
class s_k_ab_List;
class Two_List;
class Eta_rbTree;
class Eta_List;
class KLaguerreGrid;
class eta_Index;
class PLaguerre;
class eta_Index;


enum TW {T_POINTS = 1, W_POINTS = 2};


class Radial_Pnl_Data : private Uncopyable {

public:

    Radial_Pnl_Data(const Eta_rbTree * const etarbTree,
                    const Pnl_IndexMap * const pnlInd,
                    const qBas_Conf * const basCnf, 
                    const double extZ);

    ~Radial_Pnl_Data();

    void generate_TW();
    void update(const Pnl_Buffer * const pnlBuff);

    double Iab(const ab_Index& oneInd) const;
    double Rk (const k_abcd_Index& twoInd) const;

    double ksi(const int l, const int i, const int j) const;
    double kin(const int l, const int i, const int j) const;

    double eta(const int ind, const int i, const int j) const;
    double eta(const eta_Index& etaInd, const int i, const int j) const;

//    double tv(const int ind, const int i, const int j) const;
//    double tv(const eta_Index& etaInd, const int i, const int j) const;

//    double wv(const int ind, const int i, const int j) const;
//    double wv(const eta_Index& etaInd, const int i, const int j) const;

    double Pnl(const int a, const double r) const;

private:

    const Eta_rbTree    * const _etaTree;
    const Pnl_IndexMap  * const _pnlInd;
    const qBas_Conf     * const _qBasCnf;
    const double                _Z;
    const KLaguerreGrid * const _kLgr;

    qqHash        * const _qqHash;
    Eta_List      * const _etaLst;
    s_k_ab_rbTree * const _Ttree;
    s_k_ab_rbTree * const _Wtree;
    s_k_ab_List   * const _Tlist;
    s_k_ab_List   * const _Wlist;
    s_k_ab_Index  * const _sInd;
    PLaguerre     * const _pL1;
    PLaguerre     * const _pL2;
    eta_Index     * const _etaInd;

    double *_bnl;
    double *_tv, *_wv;
    double *_Tr;
    double *_Wr;

    void   _free_5arr();

    void   _recalc_Tr(const s_k_ab_Index& skabInd, const int linInd);
    void   _recalc_Wr(const s_k_ab_Index& skabInd, const int linInd);

    void   _t_w_to_zero();
    void   _recalc_tw(const eta_Index & etaInd, const int linInd, const TW tw_flag);
    double _factor(const int k, const int l) const;

    void   _bnl_update(const Pnl_Buffer * const pnlBuff);
};

////////////////////////////////////////////////////////////////////////////

#endif // _RADIAL_Pnl_DATA_H_

// end of file

