#ifndef _MLMSP_INDEX_H_
#define _MLMSP_INDEX_H_

////////////////////////////////////////////////////////////////////////////

class qua3_Index {

public:

    qua3_Index();
    qua3_Index(const int ML, const double MS, const int P);
    qua3_Index(const qua3_Index& rhs);
    ~qua3_Index();

    void reset(const int ML, const double MS, const int P);
    void reset(const qua3_Index& rhs);

    int    ML() const {return _ML;}
    double MS() const {return _MS;}
    int    P () const {return _P; }
    void   all(int &ML, double& MS, int& P) const;

    void print() const;

    qua3_Index& operator= (const qua3_Index& rhs);

    friend bool operator== (const qua3_Index& lhs, const qua3_Index& rhs);
    friend bool operator > (const qua3_Index& lhs, const qua3_Index& rhs);

private:

    int    _ML;
    double _MS;
    int    _P;
};

////////////////////////////////////////////////////////////////////////////

#endif // _MLMSP_INDEX_H_

// end of file

