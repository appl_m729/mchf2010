#ifndef _DETERMINANT_STATE_H_
#define _DETERMINANT_STATE_H_

////////////////////////////////////////////////////////////////////////////

class nlmlms_State;

class Det_State {

public:

    Det_State();
    Det_State(const Det_State& rhs);
    ~Det_State();

    static void set_Nel(const int Nel);
    static int Nel();

    void set_elst_le(const nlmlms_State& elst, const int k);
    void set_elst(const nlmlms_State& elst, const int k);
    void set_nlmlms(const int n, const int l, const int ml, const double ms, const int k);
    void set_n (const int n, const int k);
    void set_l (const int l, const int k);
    void set_ml(const int ml, const int k);
    void set_ms(const double ms, const int k);
    void clone(const Det_State& elst);

    Det_State& operator= (const Det_State& rhs);

    void recalc();
    void sort();
    void print() const;

    int    Parity() const; // l1+l2+...+lN is odd or even
    int    sum_L() const;  // l1+l2+...+lN
    int    ML() const;     // m1 +m2 +...+mN
    double MS() const;     // ms1+ms2+...+msN
    int    sign() const;   // +-1
    void   set_plus_sign();

    void   nlmlms(int &n, int &l, int &ml, double &ms, const int k) const;
    void   nl(int &n, int &l, const int k) const;
    int    n (const int k) const;
    int    l (const int k) const;
    int    ml(const int k) const;
    double ms(const int k) const;
    const nlmlms_State& elst(const int k) const;

    bool   act_lmnus(double &d_mnus, const int k);
    bool   act_lplus(double &d_plus, const int k);
    bool   smnus_act(const int k);
    bool   splus_act(const int k);

    bool   hn_such_state(const nlmlms_State& elst) const; // hn == have no
    bool   is_ordered() const;
    void   permute(const int i, const int j);

    friend int dfr_states(const Det_State& det_i, const Det_State& det_j,
                          int& pos1i, int& pos1j, int& pos2i, int& pos2j);
    friend bool operator== (const Det_State& dtst1, const Det_State& dtst2);

private:

    static int _Nel;

    nlmlms_State * _elst;    // array of one-electron states in one determinant.
    int     _Parity;
    int     _sum_L;
    int     _ML;
    double  _MS; 
    int     _sign;

    void _recalc_PLMLMS();
    bool _check_Pauli(const int k);
    bool _check_Pauli_le(const int k);
};

////////////////////////////////////////////////////////////////////////////

#endif //_DETERMINANT_STATE_H_


// end of file

