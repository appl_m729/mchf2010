#include <iostream>
#include <cstdlib>
#include "s_k_ab_rbtree.h"
#include "s_k_ab_index.h"
#include "s_k_ab_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

s_k_ab_rbNode::s_k_ab_rbNode():
left(0), right(0), red(1), s_k_ab_ind(0), linearIndex(-1) {
}

s_k_ab_rbNode::~s_k_ab_rbNode() {

    delete s_k_ab_ind; s_k_ab_ind = 0;
    delete left;       left       = 0;
    delete right;      right      = 0;
}

////////////////////////////////////////////////////////////////////////////

s_k_ab_rbTree::s_k_ab_rbTree():
_skabLst(0), _head(0), _linInd(-1) {
}

s_k_ab_rbTree::~s_k_ab_rbTree() {

    _skabLst = 0;
    delete _head; _head = 0;
}

void s_k_ab_rbTree::insert(const s_k_ab_Index& skabInd) {

    _RB_insert(_head, skabInd,0);
    _head->red = 0;
}

void s_k_ab_rbTree::clear() {

    _linInd  = -1;
    _skabLst =  0;
    delete _head; _head = 0;
}

int s_k_ab_rbTree::linIndex(const s_k_ab_Index& skabInd) const {
    return (_RB_search(_head, skabInd))->linearIndex;
}

void s_k_ab_rbTree::linearize(s_k_ab_List* ext_skabLst, const char * stxt) {

    cout << "s_k_ab_rbTree: start generate s_k_ab_List..";

    _skabLst = ext_skabLst;
    _RB_traversal(_head);
    _skabLst = 0;

    cout << ". done ok; ";
    cout << stxt << "list->size = " << ext_skabLst->size() << endl;
}

void s_k_ab_rbTree::_rotR(s_k_ab_rbNode* &h) {

    s_k_ab_rbNode* x = h->left;
    h->left = x->right;
    x->right = h;
    h = x;
}

void s_k_ab_rbTree::_rotL(s_k_ab_rbNode* &h) {

    s_k_ab_rbNode* x = h->right;
    h->right = x->left;
    x->left = h;
    h = x;
}

int s_k_ab_rbTree::_is_red(s_k_ab_rbNode *x) {

    if (0 == x) return 0;
    return x->red;
}

void s_k_ab_rbTree::_RB_insert(s_k_ab_rbNode* &h, const s_k_ab_Index& skabInd, int sw) {

    if (0 == h) {

        h = new s_k_ab_rbNode;
        h->s_k_ab_ind = new s_k_ab_Index(skabInd);
        h->linearIndex = ++_linInd;
        return;
    }

    if (_is_red(h->left) && _is_red(h->right)) {

        h->red = 1; 
        h->left->red  = 0; 
        h->right->red = 0;
    }

    if (skabInd > (*(h->s_k_ab_ind)) ) {

        _RB_insert(h->right,skabInd,1);

        if (_is_red(h) && _is_red(h->right) && !sw) {_rotL(h);}

        if (_is_red(h->right) && _is_red(h->right->right)) {

            _rotL(h);
            h->red = 0;
            h->left->red = 1;
        }
    } else if ( !( skabInd == (*(h->s_k_ab_ind)) ) ) {

        _RB_insert(h->left,skabInd,0);

        if (_is_red(h) && _is_red(h->left) && sw){_rotR(h);}

        if (_is_red(h->left) && _is_red(h->left->left)) {

            _rotR(h);
            h->red = 0;
            h->right->red = 1;
        }
    }
}

const s_k_ab_rbNode * const s_k_ab_rbTree::_RB_search(const s_k_ab_rbNode* const h,
                                                      const s_k_ab_Index& skabInd) const {

    if (0 == h) {

        cout << "Error s_k_ab_rbTree::_RB_search(" << endl;
        cout << "No such index: "; 
        skabInd.print();
        exit(0);
    }

    if ( skabInd == (*(h->s_k_ab_ind)) ) {
        return h;
    }

    if (skabInd > (*(h->s_k_ab_ind))) {
        return _RB_search(h->right, skabInd);
    } else {
        return _RB_search(h->left, skabInd);
    }
}

void s_k_ab_rbTree::_RB_traversal(s_k_ab_rbNode* h) {

    if (0 == h) return;

    _skabLst->add(h->s_k_ab_ind, h->linearIndex);

    _RB_traversal(h->right);
    _RB_traversal(h->left);
}

////////////////////////////////////////////////////////////////////////////

// end of file

