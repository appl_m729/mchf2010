#ifndef _COEFFS_IAB_RKABCD_LIST_H_
#define _COEFFS_IAB_RKABCD_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

// E = sum { (Ci*Cj*Aik*Ajs*Val + ..) Iab + (..)Rkabcd }

class ccv_DataBlock : private Uncopyable {

public:

    ccv_DataBlock *next;

    int i,j;
    double value;  // = Aik*Ajs*Val

    ccv_DataBlock();
    ccv_DataBlock(const int i, const int j, const double Val);
    ~ccv_DataBlock();
};


class ccv_List : private Uncopyable {

public:

    ccv_List();
    ~ccv_List();

    void add(const int i, const int j, const double Val);

    void to_head() const;
    bool next() const;

    int    size() const;
    int    i() const;
    int    j() const;
    double value() const;
    void   ijv(int & i, int & j, double & value) const;

    void   clear();

private:

    ccv_DataBlock * const _phead;
    mutable ccv_DataBlock* _crnt_item;
    ccv_DataBlock * _ptail;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _COEFFS_IAB_RKABCD_LIST_H_

// end of file
