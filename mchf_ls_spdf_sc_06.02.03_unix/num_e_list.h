#ifndef _NUM_ENERGY_LIST_H_
#define _NUM_ENERGY_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"
#include "aconstants.h"


class NumE_DataBlock : private Uncopyable {

public:

    NumE_DataBlock * next;

    int k_iter;
    double E[NUM_E_LENGTH];
                                      
    NumE_DataBlock();
    ~NumE_DataBlock();
};


class NumE_List : private Uncopyable {

public:

    NumE_List();
    ~NumE_List();

    void add(const int k_iter, double *ext_numE);
    void to_head() const;
    bool next() const;

    int    size() const;
    double E(NumE_Part key_E) const;    
    int    k_iter() const;

    void write(const char file[]) const;

private:

    int _size;
    NumE_DataBlock * const _head_item;
    mutable NumE_DataBlock * _crnt_item;
};

////////////////////////////////////////////////////////////////////////////

#endif // _NUM_ENERGY_LIST_H_

// end of file

