#include <iostream>
#include <cstdlib>
#include "nlmlms_state.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

nlmlms_State::nlmlms_State():_n(0),_l(0),_ml(0),_ms(0) {
    //Actually, ms = +-1/2; ms=0- wrong initizlization for errors debugging
}

nlmlms_State::nlmlms_State(const nlmlms_State& rhs):
_n(rhs._n), _l(rhs._l), _ml(rhs._ml), _ms(rhs._ms) {
}

nlmlms_State::~nlmlms_State() {
   // cout << "ElctrState destructor" << endl;
}

void nlmlms_State::nlmlms(int& n, int& l, int& ml, double& ms) const {

    n  = this->_n;
    l  = this->_l;
    ml = this->_ml;
    ms = this->_ms;
}

void nlmlms_State::nl(int& n, int& l) const {
    n = this->_n;
    l = this->_l;
}

int nlmlms_State::n() const {
    return this->_n;
}

int nlmlms_State::l() const {
    return this->_l;
}

int nlmlms_State::ml() const {
    return this->_ml;
}

double nlmlms_State::ms() const {
    return this->_ms;
}

void nlmlms_State::clone(const nlmlms_State& rhs) {

    if (&rhs == this) return;

    this->_n  = rhs._n;
    this->_l  = rhs._l;
    this->_ml = rhs._ml;
    this->_ms = rhs._ms;
}

void nlmlms_State::set_nlmlms(const int n, const int l, const int ml, const double ms) {

    if (n >= 1 && l >= 0 && n > l && ml >= -l && ml <= l &&
               ((ms == SPIN[DOWN]) || (ms == SPIN[UP])) ) {

        this->_n  = n;
        this->_l  = l;
        this->_ml = ml;
        this->_ms = ms;

    } else {

        cout << "Error in nlmlms_State::nlmlms(.. wrong setup; ";
        cout << "n=" << n << " l=" << l;
        cout << " ml=" << ml << " ms=" << ms << endl;

        exit(0);
    }
}

void nlmlms_State::set_n(const int n_in) {

    if (n_in >= 1) {

        this->_n = n_in;

    } else {

        cout << "Error in nlmlms_State::set_n(.. wrong setup; ";
        cout << "n=" << n_in << " _l=" << _l;
        cout << " _ml=" << _ml << " _ms=" << _ms << endl;

        exit(0);
    }

    if (this->_l >= n_in) {

        cout << "auto reset l by n setup; old state: " << endl;
        print();
        this->set_l(0);
        cout << "current state after reset: " << endl;
        print();
    }
}

void nlmlms_State::set_l(const int l_in) {

    if (l_in >= 0 && l_in < this->_n) {

        this->_l = l_in;

    } else {

        cout << "Error in nlmlms_State::set_l(.. wrong setup; ";
        cout << "_n=" << _n << " l=" << l_in;
        cout << " _ml=" << _ml << " _ms=" << _ms << endl;

        exit(0);
    }

    if (this->_ml < l_in || this->_ml > l_in) {

        cout << "auto reset ml by l setup; old state: " << endl;
        print();
        this->set_ml(-(this->_l));
        cout << "current state: " << endl;
        print();
    }
}

void nlmlms_State::set_ml(const int ml_in) {

    if (ml_in >= -(this->_l) && ml_in <= this->_l) {

        this->_ml = ml_in;

    } else {

        cout << "Error in nlmlms_State::set_m(.. wrong setup; ";
        cout << "_n=" << _n << " _l=" << _l;
        cout << " ml=" << ml_in << " _ms=" << _ms << endl;

        exit(0);
    }
}

void nlmlms_State::set_ms(const double ms_in) {

    if (ms_in == SPIN[UP] || ms_in == SPIN[DOWN]) {

        this->_ms = ms_in;

    } else {

        cout << "Error in nlmlms_State::set_ms(.. wrong setup; ";
        cout << "_n=" << _n << " _l=" << _l;
        cout << " _ml=" << _ml << " ms=" << ms_in << endl;

        exit(0);
    }
}

void nlmlms_State::print() const {

    cout << "n=" << _n << " l=" << _l;
    cout << " ml=" << _ml << " ms=" << _ms << endl;
}

nlmlms_State& nlmlms_State::operator= (const nlmlms_State& rhs) {

    if (this == &rhs) return *this;

    this->_n  = rhs._n;
    this->_l  = rhs._l;
    this->_ml = rhs._ml;
    this->_ms = rhs._ms;

    return *this;
}

// friend
bool operator != (const nlmlms_State& st1, const nlmlms_State& st2) {

    if (st1._n  != st2._n)  return true;
    if (st1._l  != st2._l)  return true;
    if (st1._ml != st2._ml) return true;
    if (st1._ms != st2._ms) return true;

    return false;
}

// friend
bool operator == (const nlmlms_State& st1, const nlmlms_State& st2) {

    if (st1._n  != st2._n)  return false;
    if (st1._l  != st2._l)  return false;
    if (st1._ml != st2._ml) return false;
    if (st1._ms != st2._ms) return false;

    return true;
}

// friend
bool operator > (const nlmlms_State& st_gr, const nlmlms_State& st_sm) {

    if (st_gr._n > st_sm._n) {
        return true;
    } else if (st_gr._n == st_sm._n) {

        if (st_gr._l > st_sm._l) {
            return true;
        } else if (st_gr._l == st_sm._l) {        

            if (st_gr._ml > st_sm._ml) {
                return true;
            } else if (st_gr._ml == st_sm._ml) {

                if ( (st_gr._ms == SPIN[UP]) && (st_sm._ms == SPIN[DOWN]) ) {
                    return true;
                }
                return false;
            }
            return false;
        } 
        return false;
    } 
    return false;
}

bool iseq_barring_n (const nlmlms_State& st1, const nlmlms_State& st2) {

    if (st1._ml != st2._ml)  return false;
    if (st1._l  != st2._l)   return false;
    if (st1._ms != st2._ms)  return false;

    return true;
}

////////////////////////////////////////////////////////////////////////////

// end of file
