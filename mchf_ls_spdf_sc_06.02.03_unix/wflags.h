#ifndef _WRITE_FLAGS_H_
#define _WRITE_FLAGS_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

const int sz256 = 256;


class writeFlags : private Uncopyable {

public:

    writeFlags(const char file[]);
    ~writeFlags();

    double E_acc() const {return _E_acc;}

    bool is_restore() const {return _is_restore;}
    bool is_dump()    const {return _is_dump;   }
    bool is_e_trace() const {return _is_e_trace;}
    bool is_rho()     const {return _is_rho;    }
    bool is_csfBas()  const {return _is_csfBas; }
    bool is_oneList() const {return _is_oneList;}
    bool is_twoList() const {return _is_twoList;}

    const char * const restore_name() const {return _restore_name;}
    const char * const dump_name()    const {return _dump_name;   }
    const char * const e_trace_name() const {return _e_trace_name;}
    const char * const rho_name()     const {return _rho_name;    }
    const char * const csfBas_name()  const {return _csfBas_name; }
    const char * const oneList_name() const {return _oneList_name;}
    const char * const twoList_name() const {return _twoList_name;}

private:

    double _E_acc;

    bool _is_restore;
    bool _is_dump;
    bool _is_e_trace;
    bool _is_rho;
    bool _is_csfBas;
    bool _is_oneList;
    bool _is_twoList;

    char _restore_name[sz256];
    char _dump_name   [sz256];
    char _e_trace_name[sz256];
    char _rho_name    [sz256];
    char _csfBas_name [sz256];
    char _oneList_name[sz256];
    char _twoList_name[sz256];

    void _read(const char file[], const char * const text_ok,
               const char * const text_no, bool & prm_flag,
               const char * const param, char * const data);
};

////////////////////////////////////////////////////////////////////////////

#endif // _WRITE_FLAGS_H_

// end of file

