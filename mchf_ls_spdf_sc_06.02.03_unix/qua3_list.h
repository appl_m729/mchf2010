#ifndef _QUA3_LIST_H_
#define _QUA3_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class qua3_Index;

class qua3_DataBlock : private Uncopyable {

public:

    int             count;
    qua3_Index     *index;
    qua3_DataBlock *next;

    qua3_DataBlock();
    ~qua3_DataBlock();
};


class qua3_List : private Uncopyable {
public:

    qua3_List();
    ~qua3_List();
 
    const qua3_Index* crnt_index() const;
    int crnt_count() const;

    void to_head();
    bool next(); 
    void add(const qua3_Index& qua3Ind);
    void clear();

    int  size()  const {return _size; }
    int  count() const {return _count;}

private:

    qua3_DataBlock * const _phead;
    qua3_DataBlock *_crnt_item;

    int _size;
    int _count;
};

////////////////////////////////////////////////////////////////////////////

#endif // _QUA3_LIST_H_

// end of file

