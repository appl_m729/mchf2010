#include <iostream>
#include <cstdlib>
#include <cmath>
#include "laguerre.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

PLaguerre::PLaguerre(const int Nmax): _Nmax(Nmax), 
 _PL(new double[_Nmax+1]), 
_dPL(new double[_Nmax+1]) {
}

PLaguerre::~PLaguerre() {

    delete[] _PL;   _PL  = 0;
    delete[] _dPL;  _dPL = 0;
}

void PLaguerre::calc(const int n_max, double alpha, const double x) {

    if (n_max > _Nmax || n_max < 0) {

        cout << "Error in void PLaguerre::calculate(int n_max,.. " << endl;
        cout << "n_max = " << n_max << " Nmax= " << _Nmax << endl;
        cout << "x = " << x << endl;
        exit(0);
    }

    // Laguerre
    _PL[0] = 1.0;
    _PL[1] = 1.0 + alpha - x;

    {for (int n = 1; n < n_max; ++n) {

        _PL[n+1] = -(n + alpha)/(n + 1.0)*_PL[n-1] + (2.0*n + 1.0 + alpha - x)/(n + 1.0)*_PL[n];
    }}

    //dLaguerre
    ++alpha;
    _dPL[0] = 1.0;
    _dPL[1] = 1.0 + alpha - x;

    {for (int n = 1; n < n_max; ++n) {

        _dPL[n+1] = -(n + alpha)/(n + 1.0)*_dPL[n-1] + (2.0*n + 1.0 + alpha - x)/(n + 1.0)*_dPL[n];
    }}
}

double PLaguerre::value(const int n) {

    if (n < 0 || n > _Nmax) {
        cout << "Error in double PLaguerre::PL(" << endl;
        cout << "n = " << n << " Nmax= " << _Nmax << endl;
        exit(0);
    }
    return _PL[n];
}

double PLaguerre::dValue(const int n) {

    if (n < 0 || n > _Nmax) {
        cout << "Error in double PLaguerre::dPL(" << endl;
        cout << "n = " << n << " Nmax= " << _Nmax << endl;
        exit(0);
    }

    if (n < 1) return 0.0;
    return -_dPL[n-1];
}


////////////////////////////////////////////////////////////////////////////

// end of file

