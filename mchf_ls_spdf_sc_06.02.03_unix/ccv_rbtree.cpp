#include <iostream>
#include <cmath>
#include <cstdlib>
#include "ccv_rbtree.h"
#include "ab_index.h"
#include "ccv_list.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

ccv_RB_node::ccv_RB_node():
left(0), right(0), red(1), abInd(0), value(0) {

}

ccv_RB_node::~ccv_RB_node() {

    delete abInd;  abInd  = 0;

    delete left;   left   = 0;
    delete right;  right  = 0;
}

////////////////////////////////////////////////////////////////////////////

ccv_RB_Tree::ccv_RB_Tree():_head(0), _size(0), _ccvLst(0) {
}

ccv_RB_Tree::~ccv_RB_Tree() {

    _ccvLst = 0;
    delete _head; _head = 0;
}

void ccv_RB_Tree::generate_ccvList(ccv_List * const ccvLst) const {

    _ccvLst = ccvLst;
    _catch_next_node(_head);
    _ccvLst = 0;
}

void ccv_RB_Tree::add(const int i, const int j, const double ext_Val) {

    const ab_Index ab_ind(i,j);
    _insert(ab_ind);
    _RB_search(_head,ab_ind)->value += ext_Val;
}

void ccv_RB_Tree::_rotR(ccv_RB_node* &h) {

    ccv_RB_node* x = h->left;
    h->left = x->right;
    x->right = h;
    h = x;
}

void ccv_RB_Tree::_rotL(ccv_RB_node* &h) {

    ccv_RB_node* x = h->right;
    h->right = x->left;
    x->left = h;
    h = x;
}

int ccv_RB_Tree::_is_red(ccv_RB_node *x) {

    if (0 == x) return 0;
    return x->red;
}

void ccv_RB_Tree::_insert(const ab_Index& oneInd) {

    _RB_insert(_head, oneInd,0);
    _head->red = 0;
}

void ccv_RB_Tree::_RB_insert(ccv_RB_node* &h,const ab_Index& insOneInd, int sw) {

    if (0 == h) {

        h = new ccv_RB_node;
        h->abInd = new ab_Index(insOneInd);
        h->value = 0;

        ++_size;
    }

    if (_is_red(h->left) && _is_red(h->right)) {

        h->red = 1; 
        h->left->red  = 0; 
        h->right->red = 0;
    }

    if (insOneInd > (*(h->abInd)) ) {

        _RB_insert(h->right,insOneInd,1);

        if (_is_red(h) && _is_red(h->right) && !sw) {_rotL(h);}

        if (_is_red(h->right) && _is_red(h->right->right)) {

             _rotL(h);
            h->red = 0;
            h->left->red = 1;
        }
    } else if ( !( insOneInd == (*(h->abInd)) ) ) {

        _RB_insert(h->left,insOneInd,0);

        if (_is_red(h) && _is_red(h->left) && sw){_rotR(h);}

        if (_is_red(h->left) && _is_red(h->left->left)) {

            _rotR(h);
            h->red = 0;
            h->right->red = 1;
        }
    }
}

ccv_RB_node* ccv_RB_Tree::_RB_search(ccv_RB_node* h, const ab_Index& ptrnOneInd) {

    if (0 == h) {

        cout << "Error in ccv_RB_Tree::_RB_find(" << endl;
        cout << "No such index: "; ptrnOneInd.print();
        exit(0);
    }

    if ( ptrnOneInd == (*(h->abInd)) ) {
        return h;
    }

    if ( ptrnOneInd > (*(h->abInd)) ) {
        return _RB_search(h->right, ptrnOneInd);
    } else {
        return _RB_search(h->left, ptrnOneInd);
    }
}

void ccv_RB_Tree::_catch_next_node(const ccv_RB_node* h) const {

    if ( 0 != h ) {

        if (fabs(h->value) > CCV_TRANCATE_COEFF) {
            _ccvLst->add(h->abInd->a(), h->abInd->b(), h->value);
        }

        _catch_next_node(h->left);
        _catch_next_node(h->right);
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

