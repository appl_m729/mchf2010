#ifndef _NLW_BUILDER_H_
#define _NLW_BUILDER_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Atom_Conf;
class q4x_Convertor;
class nlw_Conf;
class nlw_Conf_List;


class nlw_Builder : private Uncopyable {

public:

    nlw_Builder(const Atom_Conf * const aCnf, const q4x_Convertor* const q4xC);
    ~nlw_Builder();

    void collect();

    void to_head();
    bool next_nlw();
    const nlw_Conf * crnt_pnlw() const;

    int  num_Cnfs() const;

private:

    const Atom_Conf * const _aCnf;
    const q4x_Convertor * const  _q4xC;
    int *_wc;    // w_counter
    int  _numCnfs;
    nlw_Conf_List * const _nlwLst;

    void _initialize();
    void _move_nextpoint(const int i);
    bool _w_isle_Nel(const int k);
    bool _w_iseq_Nel();
    void _save_config();
};

////////////////////////////////////////////////////////////////////////////

#endif // _NLW_BUILDER_H_

// end of file

