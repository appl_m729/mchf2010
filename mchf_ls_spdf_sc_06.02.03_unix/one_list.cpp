#include <fstream>
#include <iostream>
#include <iomanip>
#include "one_list.h"
#include "ab_index.h"
#include "ccv_list.h"
#include "ccv_rbtree.h"
#include "pnl_radial.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

One_DataBlock::One_DataBlock():
next(0), value(0), index_ab(0), ccvLst(0) {
}

One_DataBlock::~One_DataBlock() {

    index_ab = 0;
    delete ccvLst;

    delete next;   next = 0;
}

////////////////////////////////////////////////////////////////////////////

One_List::One_List(): _phead(new One_DataBlock),
_crnt_item(_phead), _size(0) {

}

One_List::~One_List() {

    _crnt_item = 0;
    delete _phead;
}

void One_List::wholeUpdate(const Radial_Pnl_Data * const RadialData) {

    One_DataBlock * const tmpBuff = _crnt_item;

    _crnt_item = _phead;
    while ( _crnt_item->next ) {

        _crnt_item = _crnt_item->next;
        _crnt_item->value = RadialData->Iab(*(_crnt_item->index_ab));
    }

    _crnt_item = tmpBuff;
}

void One_List::add(const ab_Index * const Ind_ab, 
                   const ccv_RB_Tree * const ext_ccvTree) {

    ccv_List * const t_list = new ccv_List;
    ext_ccvTree->generate_ccvList(t_list);

    if (t_list->size() > 0) {

        _crnt_item->next = new One_DataBlock;
        _crnt_item = _crnt_item->next;

        _crnt_item->index_ab = Ind_ab;
        _crnt_item->value = 0;

        _crnt_item->ccvLst = t_list;
        ++_size;

    } else {
    
        delete t_list;
    }
}

void One_List::to_head() const {
    _crnt_item = _phead;
}

bool One_List::next() const {

    if ( _crnt_item->next ) {

        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int One_List::size() const {
    return _size;
}

double One_List::value() const {
    return _crnt_item->value;
}

const ab_Index& One_List::abInd() const {
    return *(_crnt_item->index_ab);
}

const ccv_List& One_List::ccvLst() const {
    return *(_crnt_item->ccvLst);
}

const ccv_List * One_List::p_ccvLst() const {
    return _crnt_item->ccvLst;
}

void One_List::write(const char file[]) const {

    cout << "One_List: writing in " << file << "..";
    ofstream fout(file);

    fout << "one_List data,  size = " << _size << endl;
    One_DataBlock * const tmpBuff = _crnt_item;

    _crnt_item = _phead;
    while ( _crnt_item->next ) {

        _crnt_item = _crnt_item->next;

        fout << endl << "ab_Index:  ";
        fout << _crnt_item->index_ab->a() << " ";
        fout << _crnt_item->index_ab->b() << endl;
        fout << "I_value = " << setprecision(12) << _crnt_item->value << endl;

        _crnt_item->ccvLst->to_head();
        while(_crnt_item->ccvLst->next()) {

            fout << "i = " << _crnt_item->ccvLst->i();
            fout << " j = " << _crnt_item->ccvLst->j();
            fout << " value = " << _crnt_item->ccvLst->value() << endl;
       }
    }

    fout << endl << "End of file" << endl;
    fout.close();

    _crnt_item = tmpBuff;

    cout << ". done ok" << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file


