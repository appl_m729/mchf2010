#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include "num_e_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

NumE_DataBlock::NumE_DataBlock():next(0), k_iter(-1) {

    E[E_ONE] = 0;
    E[E_TWO] = 0;
    E[E_TOT] = 0;
}

NumE_DataBlock::~NumE_DataBlock() {

    delete next; next = 0;
}

////////////////////////////////////////////////////////////////////////////

NumE_List::NumE_List(): _size(0),
_head_item(new NumE_DataBlock), _crnt_item(_head_item) {
}

NumE_List::~NumE_List() {
    delete _head_item;
}

void NumE_List::add(const int kIter, double *ext_E) {

    _crnt_item->next = new NumE_DataBlock;
    _crnt_item = _crnt_item->next;

    _crnt_item->k_iter = kIter;

    _crnt_item->E[E_ONE] = ext_E[E_ONE];
    _crnt_item->E[E_TWO] = ext_E[E_TWO];
    _crnt_item->E[E_TOT] = ext_E[E_TOT];
}

void NumE_List::to_head() const {
    _crnt_item = _head_item;
}

bool NumE_List::next() const {

    if (_crnt_item->next) {

        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int NumE_List::size() const {
    return _size;
}

double NumE_List::E(NumE_Part key_E) const {
    return _crnt_item->E[key_E];
}

int NumE_List::k_iter() const {
    return _crnt_item->k_iter;
}

void NumE_List::write(const char file[]) const {

    cout << "writing energy_mchf in: " << file << " ..";
    ofstream fout(file);
    fout << " k_iter   E_one  E_two  E_tot  E_dlt " << endl;
    fout << setprecision(16);
                            
    to_head();
    NumE_DataBlock *t_prev = _crnt_item;
    while ( next() ) {

        fout << k_iter() << "  ";
        fout << E(E_ONE) << "  " << E(E_TWO) << "  " << E(E_TOT) << "  ";
        fout << fabs(E(E_TOT) - t_prev->E[E_TOT]) << endl;

        t_prev = _crnt_item;
    }

    fout << "End of file" << endl;
    fout.close();
    cout << ".done ok" << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file
