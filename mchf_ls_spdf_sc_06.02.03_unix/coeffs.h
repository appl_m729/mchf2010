#ifndef _CLEBSHGORD_GONTH_H_
#define _CLEBSHGORD_GONTH_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"


class CompleteCoeffs : private Uncopyable {

public:

    CompleteCoeffs();
    ~CompleteCoeffs();

    double  clebsh(const double j1, const double m1, const double j2, const double m2,
                   const double J,  const double MJ);
    double  symb3j(const double j1, const double j2, const double j3, const double m1,
                   const double m2, const double m3);
    double  symb6j(const double j1, const double j2, const double j3, const double j4,
                   const double j5, const double j6); // xx
    double  gonth (const int k, const int l1, const int m1, const int l2, const int m2);
    int     binomC(const int n, const int k);

    void    zero();
    void    add_fact(const int n, const int key);
    void    add_numb(const int n, const int key);
    double  get();

    bool    is_intn(const double val, const int nd){return _is_intn(val,nd);}

private:

    int         _LengthPN;      //number of primes (~1000 primes)
    int         _maxPPLength;   //temp buffer variable;
    int * const _PrimeNumber;   //array of prime numbers up to the ~17000
    int * const _PrimePower;    //corresponding powers of this primes

    int     _min(const int a, const int b) const;
    int     _max(const int a, const int b) const;
    void    _SetupPN();
    int     _FindNewPrime(const int i);
    double  _V(const double a, const double b, const double c,
               const double a_,const double b_,const double c_);
    void    _AddTrngl(const double a, const double b, const double c);
    double  _AddOneAdditive_3j(const double a, const double b, const double c,
                               const double a_,const double b_,const double c_, const int z);
    double  _AddOneAdditive_6j(const double j1, const double j2, const double j3,
                               const double j4, const double j5, const double j6, const int z);
    void    _ZeroPrimePower();
    double  _IntPow(const int numb, const int power);
    void    _AddFactOfNumberAsPrimePowers(const int Number, const int key);
    void    _AddOneNumberAsPrimePowers(const int k, const int key); // key 1 Numerator, -1 Denominator;

    bool   _is_intn(double val, const int num_comma_digit);
};

////////////////////////////////////////////////////////////////////////////

CompleteCoeffs& ccf();

////////////////////////////////////////////////////////////////////////////

#endif // _CLEBSHGORD_GONTH_H_

// end of file

