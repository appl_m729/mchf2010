#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include "qua5_index.h"
#include "qua5_rbtree.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

qua5_RB_node::qua5_RB_node():
left(0), right(0), red(1),
index_q5(0), count(0), min_size(-1), max_size(0) {
}

qua5_RB_node::~qua5_RB_node() {

    delete index_q5; index_q5 = 0;
    delete left;     left     = 0;
    delete right;    right    = 0;
}

void qua5_RB_node::re_mmSizes(const int ext_Size) {

    if (max_size < ext_Size) max_size = ext_Size;
    if ( (min_size > ext_Size) || (-1 == min_size) ) min_size = ext_Size;
}

////////////////////////////////////////////////////////////////////////////

qua5_RbTree::qua5_RbTree():
_head(0), _size(0), _count(0), _oldL(0), _oldS(0) {
}

qua5_RbTree::~qua5_RbTree() {
    delete _head; _head = 0;
}

void qua5_RbTree::add(const qua5_Index& Ind_q5, const int Size) {
    _insert(Ind_q5, Size);
}

void qua5_RbTree::clear() {

    delete _head; _head = 0;

    _size  = 0;
    _count = 0;

    _oldL = 0;
    _oldS = 0;
}

void qua5_RbTree::write(const char file[]) const {

//    ofstream fout(file,ios::app);
    ofstream fout(file);

    fout <<"L ML S MS P (number) min_size max_size" << endl;

    if (_head) {

        _oldL = _head->index_q5->L();
        _oldS = _head->index_q5->S();

        _write_next_node(_head, &fout);
    }

    fout << endl << "size = " << size() << endl;
    fout << "count = " << count() << endl;

    fout << endl << "End of file" << endl;
    fout.close();
}

void qua5_RbTree::_rotR(qua5_RB_node* &h) {

    qua5_RB_node* x = h->left;
    h->left = x->right;
    x->right = h;
    h = x;
}

void qua5_RbTree::_rotL(qua5_RB_node* &h) {

    qua5_RB_node* x = h->right;
    h->right = x->left;
    x->left = h;
    h = x;
}

int qua5_RbTree::_is_red(qua5_RB_node *x) {

    if (0 == x) return 0;
    return x->red;
}

void qua5_RbTree::_insert(const qua5_Index& q5_Ind, const int Size) {

    _RB_insert(_head, q5_Ind, Size, 0);
    _head->red = 0;
}

void qua5_RbTree::_RB_insert(qua5_RB_node* &h, const qua5_Index& insq5Ind,
                             const int Size, const int sw) {
    if (0 == h) {

        h = new qua5_RB_node;
        h->index_q5 = new qua5_Index(insq5Ind);
        h->count = 1;
        h->re_mmSizes(Size);

        ++_count;
        ++_size;
        return;
    }

    if (_is_red(h->left) && _is_red(h->right)) {

        h->red = 1; 
        h->left->red  = 0; 
        h->right->red = 0;
    }

    if (insq5Ind > (*(h->index_q5)) ) {

        _RB_insert(h->right,insq5Ind, Size,1);

        if (_is_red(h) && _is_red(h->right) && !sw) {_rotL(h);}

        if (_is_red(h->right) && _is_red(h->right->right)) {

            _rotL(h);
            h->red = 0;
            h->left->red = 1;
        }

    } else if ( insq5Ind == (*(h->index_q5)) ) {

        _count++;
        h->count++;
        h->re_mmSizes(Size);

    } else { // <

        _RB_insert(h->left,insq5Ind, Size,0);

        if (_is_red(h) && _is_red(h->left) && sw){_rotR(h);}

        if (_is_red(h->left) && _is_red(h->left->left)) {

            _rotR(h);
            h->red = 0;
            h->right->red = 1;
        }
    }
}

void qua5_RbTree::_write_next_node(const qua5_RB_node* h, ofstream* tfout) const {

    if ( 0 != h ) {

        _write_next_node(h->left, tfout);

        if ( (h->index_q5->L() != _oldL) ||
             (fabs(h->index_q5->S() - _oldS) > SPIN_THRESHOLD) ) {

            _oldL = h->index_q5->L();
            _oldS = h->index_q5->S();

            (*tfout) << endl;
        }

        (*tfout) << h->index_q5->L()  << setw(5);
        (*tfout) << h->index_q5->ML() << setw(5);
        (*tfout) << h->index_q5->S()  << setw(5);
        (*tfout) << h->index_q5->MS() << setw(3);
        (*tfout) << h->index_q5->P()  << setw(3);
        (*tfout) << setw(2) <<"(" << setw(3)<< h->count << ")";
        (*tfout) << setw(6) << h->min_size;
        (*tfout) << setw(6) << h->max_size << endl;

        _write_next_node(h->right, tfout);
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

