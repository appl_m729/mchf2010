#ifndef _RADIAL_GRID_H_
#define _RADIAL_GRID_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class grid_r : private Uncopyable {

public:

    grid_r(const char file[]);
    ~grid_r();

    double r(const int i) const;
    double r(const int i, const int key) const;
    double r2(const int i2) const;  //  i2=2*i+key
    double h(const int i) const;
    int    N() const;
    double r_max() const;

    void print() const;

private:

    int     _N;
    double  _t32;
    double *_data_r;
    double *_data_h;  // 2*h_i = r_(i+1) - r_i;
    double  _r_max;

    void    _setup();
};

////////////////////////////////////////////////////////////////////////////

#endif // _RADIAL_GRID_H_

// end of file

