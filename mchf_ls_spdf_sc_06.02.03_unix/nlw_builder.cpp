#include <iostream>
#include <cstdlib>
#include "nlw_conf.h"
#include "q4x_convertor.h"
#include "atom_conf.h"
#include "nlw_list.h"
#include "nlw_builder.h"
#include "coeffs.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

nlw_Builder::nlw_Builder(const Atom_Conf * const ext_aCnf, 
                         const q4x_Convertor* const ext_q4xC):
_aCnf(ext_aCnf), _q4xC(ext_q4xC),
_wc(0), _numCnfs(0), _nlwLst(new nlw_Conf_List) {

    if (!_aCnf || !_q4xC) {

        cout << "Error in nlw_Builder::" << endl;
        cout << "aCnf = " << _aCnf << " q4xC = " << _q4xC << endl;
        exit(0);
    }

    if ( 0 == _q4xC->nl_length() ) {

        cout << "Error in nlw_Builder::" << endl;
        cout << "q4xC->nlw_length() = " << _q4xC->nl_length() << endl;
        exit(0);
    }

    _wc = new int[_q4xC->nl_length()];
}

nlw_Builder::~nlw_Builder() {

    delete   _nlwLst;
    delete[] _wc;  _wc = 0;
}

void nlw_Builder::collect() {

    cout << "collect nlw started.." << endl;

    _initialize();
    _move_nextpoint(0);

    cout << " num_Conf = " << _nlwLst->size() << " ";
    cout << "...collecting done ok" << endl;
}

void nlw_Builder::to_head() {
    _nlwLst->to_head();
}

bool nlw_Builder::next_nlw() {
    return _nlwLst->next();
}

const nlw_Conf* nlw_Builder::crnt_pnlw() const {
    return _nlwLst->crnt_pnlw();
}

int nlw_Builder::num_Cnfs() const {
    return _nlwLst->size();
}

void nlw_Builder::_initialize() {

    _nlwLst->clear();

    for (int k = 0; k < _q4xC->nl_length(); ++k) _wc[k] = 0;
}

void nlw_Builder::_move_nextpoint(const int k) {

    if (k < _q4xC->nl_length()) {

        for (_wc[k] = 0; _wc[k] <= _q4xC->w_free(k); _wc[k]++) {

            if ( _w_isle_Nel(k) ) _move_nextpoint(k+1);
        }
    } else {

        if (_w_iseq_Nel()) {
            _save_config();
        }
    }
}

bool nlw_Builder::_w_isle_Nel(const int K) {

    if (K < 0 || K >= _q4xC->nl_length()) {

        cout << "Error in nlw_Builder::_w_isle_Nel(.. k= " << K << endl;
        exit(0);
    }

    int sum = 0;
    for (int ik = 0; ik <= K; ++ik) {

        sum += _wc[ik] + _q4xC->w_hill(ik);
        if (sum > _aCnf->Nel_val()) return false;
    }
    return true;
}

bool nlw_Builder::_w_iseq_Nel() {
        
    int sum = 0;
    for(int k = 0; k < _q4xC->nl_length(); ++k) {
        sum += _wc[k] + _q4xC->w_hill(k);
    }

    if (sum == _aCnf->Nel_val()) return true;
    return false;
}

void nlw_Builder::_save_config() {
    _nlwLst->add(new nlw_Conf(_wc,_q4xC));
}

////////////////////////////////////////////////////////////////////////////

// end of file

