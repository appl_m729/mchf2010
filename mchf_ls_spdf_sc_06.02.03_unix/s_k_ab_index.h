#ifndef _STEPEN_K_AB_INDEX_H_
#define _STEPEN_K_AB_INDEX_H_

////////////////////////////////////////////////////////////////////////////

class k_ab_Index;

class s_k_ab_Index {

public:

    s_k_ab_Index(const int s, const k_ab_Index& kabInd);
    s_k_ab_Index(const s_k_ab_Index& rhs);
    ~s_k_ab_Index();

    s_k_ab_Index& operator= (const s_k_ab_Index& rhs);

    int s() const {return _s;}
    const k_ab_Index* p_kabInd() const {return  _kabInd;}
    const k_ab_Index&   kabInd() const {return *_kabInd;}

    void reset(const int s);
    void reset(const int s, const k_ab_Index& kabInd);

    void print() const;

    friend bool operator==(const s_k_ab_Index& lhs, const s_k_ab_Index& rhs);
    friend bool operator >(const s_k_ab_Index& lhs, const s_k_ab_Index& rhs);

private:

    int _s;
    k_ab_Index * const _kabInd;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _STEPEN_K_AB_INDEX_H_

// end of file

