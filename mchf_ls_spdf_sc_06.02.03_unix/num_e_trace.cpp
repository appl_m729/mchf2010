#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include "num_e_trace.h"
#include "num_e_list.h"
#include "ham_matrix.h"
#include "one_list.h"
#include "two_list.h"
#include "ccv_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

NumE_Trace::NumE_Trace(const One_List * const oneList, 
                       const Two_List * const twoList):
_eLst(new NumE_List), _oneLst(oneList), _twoLst(twoList) {
}

NumE_Trace::~NumE_Trace() {
    delete _eLst;
}

void NumE_Trace::update(const int k_iter, const HamiltonianMtrx * const hamMatr) {

    _E_old[E_ONE] = _E[E_ONE];
    _E_old[E_TWO] = _E[E_TWO];
    _E_old[E_TOT] = _E[E_TOT];

    _E[E_ONE] = _E[E_TWO] = 0;

    _calc_one_E(hamMatr);
    _calc_two_E(hamMatr);

    _E[E_TOT] = _E[E_ONE] + _E[E_TWO];

    _eLst->add(k_iter,_E);
}

void NumE_Trace::print() const {

    cout << "E_old: E_one E_two E_tot  " << setprecision(E_PRECISION_PRINT);
    cout << _E_old[E_ONE] << " " << _E_old[E_TWO] << " " << _E_old[E_TOT] << endl;

    cout << "E_new: E_one E_two E_tot  ";
    cout << _E[E_ONE] << " " << _E[E_TWO] << " " << _E[E_TOT] << endl;
}

double NumE_Trace::iter_dlt() const {
    return fabs(_E_old[E_TOT] - _E[E_TOT]);
}

void NumE_Trace::write(const char file[]) const {
    _eLst->write(file);
}

void NumE_Trace::_calc_one_E(const HamiltonianMtrx * const hamMatr) {

    _oneLst->to_head();
    while (_oneLst->next()) {

       const double Iab = _oneLst->value();
       const ccv_List * const ccvLst = _oneLst->p_ccvLst();

       ccvLst->to_head();
       while (ccvLst->next()) {

           int i,j; double v;
           ccvLst->ijv(i,j,v);

           _E[E_ONE] += hamMatr->C(i)*hamMatr->C(j)*v*Iab;
       }
    }
}

void NumE_Trace::_calc_two_E(const HamiltonianMtrx * const hamMatr) {

    _twoLst->to_head();
    while (_twoLst->next()) {

        const double Rk = _twoLst->value();
        const ccv_List * const ccvLst = _twoLst->p_ccvLst();

        ccvLst->to_head();
        while (ccvLst->next()) {

            int i,j; double v;
            ccvLst->ijv(i,j,v);

           _E[E_TWO] += hamMatr->C(i)*hamMatr->C(j)*v*Rk;
        }
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file
