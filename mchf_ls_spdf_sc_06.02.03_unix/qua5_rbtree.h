#ifndef _QUA5_LMLSMSP_RBTREE_H_
#define _QUA5_LMLSMSP_RBTREE_H_

////////////////////////////////////////////////////////////////////////////

#include <fstream>
#include "uncopyable.h"

using namespace::std;

class qua5_Index;

class qua5_RB_node : private Uncopyable {

public:

    qua5_RB_node *left, *right;
    int red;

    const qua5_Index *index_q5;
    int count;                   // Number of CSF_functions (in general, different ones)
                                 // with the same qua5_Index.
    int min_size, max_size;      // length of CSF_func.


    qua5_RB_node();
    ~qua5_RB_node();

    void re_mmSizes(const int Size);
};


class qua5_RbTree : private Uncopyable {

public:

    qua5_RbTree();
    ~qua5_RbTree();

    void add(const qua5_Index& Ind_q5, const int Size);
    void clear();

    int size() const {return _size;}
    int count() const {return _count;}

    void write(const char file[]) const;

private:

    qua5_RB_node  *_head;
    int            _size;    // number of all different q5_indexes
    int            _count;   // number of all q5_indexes with repeat

    mutable int    _oldL;   // to insert space-line when write
    mutable double _oldS;

    void _rotR(qua5_RB_node* &h);
    void _rotL(qua5_RB_node* &h);
    int  _is_red(qua5_RB_node *x);

    void _insert(const qua5_Index& q5_Ind, const int Size);
    void _RB_insert(qua5_RB_node* &h, const qua5_Index& q5_Ind,
                    const int Size, const int sw);
    void _write_next_node(const qua5_RB_node* h, ofstream* tfout) const;
};

////////////////////////////////////////////////////////////////////////////

#endif // _QUA5_LMLSMSP_RBTREE_H_

// end of file

