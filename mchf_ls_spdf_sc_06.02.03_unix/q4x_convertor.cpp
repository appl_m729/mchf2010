#include <iostream>
#include <cstdlib>
#include "q4x_convertor.h"
#include "atom_conf.h"
#include "nlmlms_state.h"
#include "nlw_conf.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

q4x_Convertor::q4x_Convertor(const Atom_Conf * const p_aCnf):
_aCnf(p_aCnf), _nl_length(0), 
_wmax_v(0), _whill_v(0), _n_v(0), _l_v(0), 
_max_wmax(0), _lmax(0),
_n(0), _l(0), _ml(0), _x(0), _ms(0), _x_length(0) {

    if (!_aCnf) {

        cout << "Error in nlw_Builder::" << endl;
        cout << "aCnf = " << _aCnf << endl;
        exit(0);
    }

    cout << endl << "start q4x_Convertor setup..." << endl;
    _setup_wv_nv_lv();
    _calc_x_length_new_nlmlmsx();
    _reset_map();       //  x, n,l,ml,ms initialization

    print();
}

q4x_Convertor::~q4x_Convertor() {

    delete[] _wmax_v;  _wmax_v  = 0;
    delete[] _whill_v; _whill_v = 0;
    delete[] _n_v;     _n_v     = 0;
    delete[] _l_v;     _l_v     = 0;

    delete[] _n;  _n  = 0;
    delete[] _l;  _l  = 0;
    delete[] _ml; _ml = 0;
    delete[] _ms; _ms = 0;
    delete[] _x;  _x  = 0;
}

int q4x_Convertor::nl_length() const {
    return _nl_length;
}

int q4x_Convertor::n_ss(const int v) const {

    if (0 > v || v >= _nl_length) {
    
        cout << "Error in q4x_Convertor::n_ss;" << endl;
        cout << "v = " << v << endl;
        exit(0);
    }
    return _n_v[v];
}

int q4x_Convertor::l_ss(const int v) const {

    if (0 > v || v >= _nl_length) {
    
        cout << "Error in q4x_Convertor::l_ss;" << endl;
        cout << "v = " << v << endl;
        exit(0);
    }
    return _l_v[v];
}

void q4x_Convertor::nl(int &n, int &l, int const v) const {

    if (v < 0 || v >= _nl_length) {

        cout << "Error in q4x_Convertor::nl.. incorrect v=" << v << endl;
        exit(0);
    }

    n = _n_v[v];
    l = _l_v[v];
}

int q4x_Convertor::v_ind(const int n, const int l) const {
    return _v_ind(n,l);
}

int q4x_Convertor::w_max(const int v) const {

    if (v < 0 || v >= _nl_length) {

        cout << "Error in q4x_Convertor::w_max.. incorrect v=" << v << endl;
        exit(0);
    }
    return _wmax_v[v];
}

int q4x_Convertor::w_hill(const int v) const {

    if (v < 0 || v >= _nl_length) {

        cout << "Error in q4x_Convertor::w_hill.. incorrect v=" << v << endl;
        exit(0);
    }
    return _whill_v[v];
}

int q4x_Convertor::w_free(const int v) const {

    if (v < 0 || v >= _nl_length) {

        cout << "Error in q4x_Convertor::w_free.. incorrect v=" << v << endl;
        exit(0);
    }
    return _wmax_v[v] - _whill_v[v];
}

int q4x_Convertor::w_max(const int n, const int l) const {
    return _wmax_v[_v_ind(n,l)];
}

int q4x_Convertor::x(const int n, const int l, const int ml, const double ms) const {
    return _ind(n,l,ml,ms);
}

int q4x_Convertor::x(const nlmlms_State& elSt) const {

    int tn,tl,tml; double tms;
    elSt.nlmlms(tn,tl,tml,tms);

    return x(tn,tl,tml,tms);
}

int q4x_Convertor::x_last() const {
    return _x_length - 1;
}

int q4x_Convertor::x_sta(const nlw_Conf& Cnf_nlw, const int k) const {

    if (k < 0 || k >= Cnf_nlw.numShells() ) {

        cout << "Error in q4x_Convertor::x_start(..; k=" << k << endl;
        cout << "numShells = " << Cnf_nlw.numShells() << endl;
        exit(0);
    }
    const int tl = Cnf_nlw.l(k);

    return x(Cnf_nlw.n(k),tl,-tl,SPIN[DOWN]);
}

int q4x_Convertor::x_end(const nlw_Conf& Cnf_nlw, const int k) const {

    if (k < 0 || k >= Cnf_nlw.numShells() ) {

        cout << "Error in q4x_Convertor::x_end(..; k=" << k << endl;
        cout << "numShells = " << Cnf_nlw.numShells() << endl;
        exit(0);
    }
    const int tl = Cnf_nlw.l(k);

    return this->x(Cnf_nlw.n(k),tl,tl,SPIN[UP]);
}

int q4x_Convertor::n(const int x) const {

    if (x < 0 || x >= _x_length) {

        cout << "Error in q4x_Convertor::n(x=" << x;
        cout << " _x_length=" << _x_length << endl;
        exit(0);
    }
    return _n[x];
}

int q4x_Convertor::l(const int x) const {

    if (x < 0 || x >= _x_length) {

        cout << "Error in q4x_Convertor::l(x=" << x;
        cout <<" _x_length=" << _x_length << endl;
        exit(0);
    }
    return _l[x];
}

int q4x_Convertor::ml(const int x) const {

    if (x < 0 || x >= _x_length) {

        cout << "Error in q4x_Convertor::ml(x=" << x;
        cout << " _x_length=" << _x_length << endl;
        exit(0);
    }
    return _ml[x];
}

double q4x_Convertor::ms(const int x) const {

    if (x < 0 || x >= _x_length) {

        cout << "Error in q4x_Convertor::ms(x=" << x;
        cout << " _x_length=" << _x_length << endl;
        exit(0);
    }
    return _ms[x];
}

nlmlms_State q4x_Convertor::elst(const int x) const {

    if (x < 0 || x >= _x_length) {

        cout << "Error in q4x_Convertor::elst(x=" << x;
        cout << " _x_length=" << _x_length << endl;
        exit(0);
    }

    nlmlms_State t_elst;
    t_elst.set_nlmlms(_n[x],_l[x],_ml[x],_ms[x]);

    return t_elst;
}

int q4x_Convertor::x_length() const {
    return _x_length;
}

void q4x_Convertor::print() const {

    cout << "x -> (n,l,ml,ms) " << endl;
    for (int k = 0; k < _x_length; ++k) {

        cout << _x[k] << "->(" << _n[k] << ","    << _l[k];
        cout << ","   << _ml[k] << ","   << _ms[k] << ") ";
    } 
    cout << endl;
}

void q4x_Convertor::_setup_wv_nv_lv() {

    _nl_length = _aCnf->walk_Nsubsh() + _aCnf->hill_Nsubsh();
    
    cout << "nl_length=" << _nl_length << endl;
    if (!_nl_length) return;

    _wmax_v = new int[_nl_length];
    _whill_v= new int[_nl_length];
    _n_v    = new int[_nl_length];
    _l_v    = new int[_nl_length];

    // copy walk
    int pos = 0;
    for (int ws = 0; ws < _aCnf->walk_Nsubsh(); ++ws, ++pos) {

        _n_v[pos] = _aCnf->walk_n(ws);
        _l_v[pos] = _aCnf->walk_l(ws);
    }
    // copy hill
    for (int hs = 0; hs < _aCnf->hill_Nsubsh(); ++hs, ++pos) {

        _n_v[pos] = _aCnf->hill_n(hs);
        _l_v[pos] = _aCnf->hill_l(hs);
    }

    // sort
    {for (int vi = 0;  vi < _nl_length - 1; ++vi) {
     for (int vj = vi; vj < _nl_length; ++vj)
    
         if ( ( _n_v[vi] >  _n_v[vj]) || 
              ((_n_v[vi] == _n_v[vj]) && (_l_v[vi] > _l_v[vj])) ) {
         
              _swap_nl(vi,vj);
         }
    }}

    // setup w_max, w_hill
    _max_wmax = 0;
    _lmax    = 0;
    {for (int i = 0; i < _nl_length; ++i) {

        _wmax_v[i] = 4*_l_v[i] + 2;
        if (_max_wmax < _wmax_v[i]) {

            _lmax = _l_v[i];
            _max_wmax = _wmax_v[i];
        }

        const int k = _aCnf->hill_find(_n_v[i],_l_v[i]);
        if (k == -1) {
            _whill_v[i] = 0;
        } else {
            _whill_v[i] = _aCnf->hill_w(k);
        }
    }}

    cout << "(n,l) [w_max] [w_hill] [w_free]:" << endl;
    for (int k = 0; k < _nl_length; ++k) {
        cout << "(" << _n_v[k] << "," << _l_v[k] << ") ";
        cout << "[" << _wmax_v[k] << "] ";
        cout << "[" << _whill_v[k] << "] ";
        cout << "[" << w_free(k) << "]" << endl;
    }
}

void q4x_Convertor::_calc_x_length_new_nlmlmsx() {

    _x_length = 0;

    for (int v = 0; v < _nl_length; ++v) {    
        _x_length += 4*_l_v[v] + 2;
    }
    cout << " x_length=" << _x_length << endl;

    if (!_x_length) return;

    _n  = new int   [_x_length];
    _l  = new int   [_x_length];
    _ml = new int   [_x_length];
    _ms = new double[_x_length];
    _x  = new int   [_x_length];
}

void q4x_Convertor::_reset_map() {

    for (int v = 0, i = -1; v < _nl_length; ++v) {    
        _fill_segment(_n_v[v], _l_v[v], i);
    }
}

void q4x_Convertor::_fill_segment(const int n_s, const int l_s, int &i) {

    for (int mm = -l_s; mm <= l_s; ++mm) {

        _x[++i] = i;
        _n [i]  = n_s;
        _l [i]  = l_s;
        _ml[i]  = mm;
        _ms[i]  = SPIN[DOWN];

        _x[++i] = i;
        _n [i]  = n_s;
        _l [i]  = l_s;
        _ml[i]  = mm;
        _ms[i]  = SPIN[UP];
    }
}

int q4x_Convertor::_v_ind(const int n, const int l) const {

    for (int k = 0; k <= _nl_length/2; ++k) {

        if (n == _n_v[k] && l == _l_v[k]) return k;
        if (n == _n_v[_nl_length-k] && l == _l_v[_nl_length-k]) return _nl_length-k;
    }

    cout << "Error in q4x_Convertor::_v_ind(" << endl;
    exit(0);
}

int q4x_Convertor::_ind(const int n, const int l, const int ml, const double ms) const {

    for (int xx = 0; xx <= _x_length/2; ++xx) {

        if (n == _n[xx] && l == _l[xx] && ml == _ml[xx] && ms == _ms[xx]) return xx;

        if (n  == _n [_x_length-xx] && l  == _l [_x_length-xx] &&
            ml == _ml[_x_length-xx] && ms == _ms[_x_length-xx]) return _x_length-xx;
    }

    cout << "Error in q4x_Convertor::_ind(... " << endl;
    exit(0);
}

int q4x_Convertor::_min_i(const int a, const int b) {

    if (a < b) return a;
    return b;
}

void q4x_Convertor::_swap_nl(const int si, const int sj) {

    const int tn = _n_v[si];
    const int tl = _l_v[si];

    _n_v[si] = _n_v[sj];
    _l_v[si] = _l_v[sj];

    _n_v[sj] = tn;
    _l_v[sj] = tl;
}

////////////////////////////////////////////////////////////////////////////

// end of file


