#include <cstdlib>
#include <cmath>
#include "yacoby.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

const int N_max_phi = Yacoby_MAX_SIZE;

const double dZERO  = 0.0;
const double dONE   = 1.0;
const double dBAR = 1e-20;

int diaganalazing(int N, double* A, double* D, double* V) {

    static double B[N_max_phi*N_max_phi], Z[N_max_phi*N_max_phi];

// Initialize to the indentity matrix.
    int ip,iq;
    for (ip = 0; ip < N; ip++) {

        for ( iq = 0; iq < N; iq++) { V[ip*N + iq] = dZERO;}
        V[ip*N+ip] = dONE;
    }

    for (ip = 0; ip < N; ip++) {

        B[ip] = A[ip*N+ip];
        D[ip] = B[ip];
        Z[ip] = dZERO;
    }

    int nrot = 0;
    for (nrot = 0; nrot < 1000; nrot++) {

        // Sum off-diagonal elements
        double sm = dZERO, tresh;

        for (ip = 0;    ip < N-1; ip++) {
        for (iq = ip+1; iq < N;   iq++) {

            sm += fabs(A[ip*N + iq]);
        }}

        if (sm < dBAR) break; // the normal return

        if (nrot < 4) {
            tresh = 0.2*sm/(N*N);
        } else {
            tresh = dZERO;
        }

        for (ip = 0;     ip < N-1;ip++) {
        for (iq = ip + 1;iq < N;  iq++) {

            const double g = 100.0*fabs(A[ip*N + iq]);

            if ( (nrot > 4) && (fabs(D[ip]) + g == fabs(D[ip])) && 
                 (fabs(D[iq]) + g == fabs(D[iq])) ) {

                A[ip*N+iq] = dZERO;

            } else {

                if (fabs(A[ip*N+iq]) > tresh) { //111

                    double t, theta, h = D[iq] - D[ip];

                    if (fabs(h) + g == fabs(h)) {

                        t = A[ip*N+iq]/h;

                    } else {

                        theta = 0.5*h/A[ip*N+iq];
                        t = dONE/(fabs(theta) + sqrt(dONE + theta*theta));
                        if (theta < dZERO) t = -t;
                    }

                    const double c = dONE/sqrt(dONE + t*t);
                    const double s = t*c;
                    const double tau = s/(dONE + c);

                    h     = t*A[ip*N+iq];
                    Z[ip] = Z[ip] - h;
                    Z[iq] = Z[iq] + h;
                    D[ip] = D[ip] - h;
                    D[iq] = D[iq] + h;
                    A[ip*N+iq] = dZERO;

                    int j;
                    for (j = 0; j < ip; j++) {

                        const double g  = A[j*N + ip];
                        const double h  = A[j*N + iq];
                        A[j*N+ip] = g - s*(h + g*tau);
                        A[j*N+iq] = h + s*(g - h*tau);     
                    }

                    for (j = ip + 1; j < iq; j++) {
               
                        const double g  = A[ip*N+j];
                        const double h  = A[j*N+iq];
                        A[ip*N+j] = g - s*(h + g*tau);
                        A[j*N+iq] = h + s*(g - h*tau);     
                    }

                    for (j = iq + 1; j < N; j++) {

                        const double g  = A[ip*N + j];
                        const double h  = A[iq*N + j];
                        A[ip*N+j] = g - s*(h + g*tau);
                        A[iq*N+j] = h + s*(g - h*tau);     
                    }

                    for (j = 0; j < N; j++) {

                        const double g = V[j*N + ip];
                        const double h = V[j*N + iq];
                        V[j*N+ip] = g - s*(h + g*tau);
                        V[j*N+iq] = h + s*(g - h*tau);
                    }
                } // endif 111
            } // endif 
        }} //ip,iq

        for (ip = 0; ip < N; ip++) {

            B[ip] += Z[ip];
            D[ip]  = B[ip];
            Z[ip]  = dZERO;
        }
    } //main loop

    return nrot;
}


void diagonalize_Yacoby(int N_phi, double* cw0, double* cw1, double* cw2) {

    diaganalazing(N_phi,cw0,cw1,cw2);
    int i_phi, j_phi;

    // sort eigenvectors
    int in_E[N_max_phi];
    for (i_phi = 0; i_phi < N_phi; i_phi++) in_E[i_phi] = i_phi;

    bool key;
    do {

        key = false;

        for (i_phi = 1; i_phi < N_phi; i_phi++) {
    
            if (cw1[i_phi-1] > cw1[i_phi]) {

                const double w = cw1[i_phi-1];
                cw1[i_phi-1]   = cw1[i_phi];
                cw1[i_phi]     = w;

                const int k   = in_E[i_phi];
                in_E[i_phi]   = in_E[i_phi-1];
                in_E[i_phi-1] = k;

                key = true;
            }
        }
    } while(key);
 
    for (i_phi = 0; i_phi < N_phi; i_phi++) cw0[N_phi*i_phi + i_phi] = cw1[i_phi];

    for (i_phi = 0; i_phi < N_phi; i_phi++) {
    for (j_phi = 0; j_phi < N_phi; j_phi++) {

        cw1[N_phi*j_phi + i_phi] = cw2[N_phi*j_phi + in_E[i_phi]];
    }}
}

////////////////////////////////////////////////////////////////////////////

// end of file



