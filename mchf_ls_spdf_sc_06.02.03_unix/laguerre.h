#ifndef _LAGUERRE_POLINOMS_H_
#define _LAGUERRE_POLINOMS_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class PLaguerre : Uncopyable {

public:

    PLaguerre(const int Nmax);
    ~PLaguerre();

    void   calc(const int n_max, double alpha, const double x); 
    double value(const int n);
    double dValue(const int n);

private:

    int     _Nmax;  // max n for Laguerre L_nl(x)
    double *_PL;    // Laguerre polinom,
    double *_dPL;   // its first derivative
};

////////////////////////////////////////////////////////////////////////////

#endif // _LAGUERRE_POLINOMS_H_

// end of file

