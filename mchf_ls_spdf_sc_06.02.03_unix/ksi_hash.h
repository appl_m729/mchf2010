#ifndef _KSI_HASH_H_
#define _KSI_HASH_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class ccv_List;

class ksiHash_DataBlock : private Uncopyable {

public:

    ksiHash_DataBlock * next;

    int l, diag_factor;
    const ccv_List * ccvLst;

    ksiHash_DataBlock();
    ~ksiHash_DataBlock();
};


class ksiHash_List : private Uncopyable {

public:

    ksiHash_List();
    ~ksiHash_List();

    void clear();

    void add(const ccv_List * ccvLst, const int l);

    void to_head() const;
    bool next() const;

    int diag_factor() const;
    const ccv_List& ccvLst() const;
    int l() const;

    int size() const {return _size;}

private:

    ksiHash_DataBlock * const _phead;
    mutable ksiHash_DataBlock * _crnt_item;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _KSI_HASH_H_

// end of file

