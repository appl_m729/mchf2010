#ifndef _ONE_IAB_REDBLACK_TREE_H_
#define _ONE_IAB_REDBLACK_TREE_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class ab_Index;
class ccv_RB_Tree;
class One_List;


class One_RB_node : private Uncopyable {

public:

    One_RB_node *left, *right;
    int red;

    ab_Index * abInd;
    ccv_RB_Tree * ccvTree;

    One_RB_node();
    ~One_RB_node();
};


class One_RB_Tree : private Uncopyable {
public:

    One_RB_Tree();
    ~One_RB_Tree();

    void generate_OneList(One_List * const oneLst) const;

    void add(const ab_Index& OneInd, const int i, const int j,
             const double Val);

    int  size() const {return _size;} 

private:

    One_RB_node *_head;
    int          _size;

    mutable One_List *_oneLst;

    void _rotR(One_RB_node* &h);
    void _rotL(One_RB_node* &h);
    int  _is_red(One_RB_node *x);

    void _insert(const ab_Index& oneInd); 
    void _RB_insert(One_RB_node* &h,const ab_Index& insOneInd, int sw);
    One_RB_node* _RB_search(One_RB_node* h, const ab_Index& ptrnOneInd);

    void _catch_next_node(const One_RB_node* h) const;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _ONE_IAB_REDBLACK_TREE_H_

// end of file
