#ifndef _DETS_LIST_H_
#define _DETS_LIST_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class Det_State;

class Det_DataBlock : private Uncopyable {

public:

    Det_State     *one_Det;
    Det_DataBlock *next;

    Det_DataBlock();
    ~Det_DataBlock();
};


class DetsList : private Uncopyable {

public:

    DetsList();
    ~DetsList();

    void to_head_1() const;
    void to_head_2_sg() const;   // sg = strictly greate = ">" = i+1;

    bool next_1() const; 
    bool next_2() const;

    const Det_State* crnt_pDet_1() const;
    const Det_State* crnt_pDet_2() const;

    const Det_State& firstDet() const;
 
    void add(const Det_State& extDet);
    void clear();

    int  size() const;
    bool is_empty() const;

    bool find(int &d_pos, const Det_State& extDet) const;

private:

    Det_DataBlock * const _phead;

    mutable Det_DataBlock *_crnt_item_1, *_crnt_item_2;
    Det_DataBlock * _ptail;

    int _size;
};

////////////////////////////////////////////////////////////////////////////

#endif // _DETS_LIST_H_

// end of file


