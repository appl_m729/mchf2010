#include <iostream>
#include <cstdlib>
#include "k_ab_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

k_ab_Index::k_ab_Index(const int ext_k, const int ext_a,const int ext_b):
_k(ext_k), _a(ext_a), _b(ext_b) {

    if (0 > _k || 0 > _a || 0 > _b) {

        cout << "Error in k_ab_Index::k_ab_Index(" << endl;
        print();
        exit(0);
    }
    _regulate();
}

k_ab_Index::k_ab_Index(const k_ab_Index& rhs):
_k(rhs._k), _a(rhs._a), _b(rhs._b) {
}

k_ab_Index::~k_ab_Index() {
}

void k_ab_Index::reset(const k_ab_Index& rhs) {

    _k = rhs._k;
    _a = rhs._a;
    _b = rhs._b;
}

void k_ab_Index::reset(const int ext_k, const int ext_a, const int ext_b) {

    _k = ext_k; _a = ext_a; _b = ext_b;

    if (0 > _k || 0 > _a || 0 > _b) {

        cout << "Error in k_ab_Index::reset(" << endl;
        print();
        exit(0);
    }
    _regulate();
}

void k_ab_Index::reset_k(const int ext_k) {

    _k = ext_k;

    if (0 > ext_k) {

        cout << "Error in k_ab_Index::reset_k(" << endl;
        print();
        exit(0);
    }
}

void k_ab_Index::print() const {

    cout << "k = " << _k << " a = " << _a << " b = " << _b << endl;
}

void k_ab_Index::k_ab(int& ext_k, int& ext_a, int& ext_b) const {

    ext_k = _k;
    ext_a = _a;
    ext_b = _b;
}

//friend
bool operator==(const k_ab_Index& lhs, const k_ab_Index& rht) {

    if (lhs._k != rht._k) return false;

    if (lhs._a != rht._a) return false;
    if (lhs._b != rht._b) return false;

    return true;
}

//friend
bool operator> (const k_ab_Index& lft, const k_ab_Index& rht) {

    if (lft._k < rht._k ) return false;
    if (lft._k > rht._k ) return true;

    if (lft._a < rht._a) return false;
    if (lft._a > rht._a) return true;

    if (lft._b < rht._b) return false;
    if (lft._b > rht._b) return true;

    // ==
    return false;
}

void k_ab_Index::_regulate() {

    if ( _a > _b ) {

        const int buff_Ind = _b; 
        _b = _a;
        _a = buff_Ind;
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

