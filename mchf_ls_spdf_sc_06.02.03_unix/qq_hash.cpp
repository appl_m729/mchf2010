#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include "qbas_conf.h"
#include "krylov_grid.h"
#include "qq_hash.h"
#include "laguerre.h"
#include "s_k_ab_list.h"
#include "s_k_ab_index.h"
#include "pnl_index_map.h"
#include "k_ab_index.h"
#include "coeffs.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

qqHash::qqHash(const qBas_Conf* const qbCnf, const Pnl_IndexMap * const pnlInd):
_qbCnf(qbCnf),
_pnlInd(pnlInd),
_kLgr1(new KLaguerreGrid), 
_kLgr2(new KLaguerreGrid),
_pL1(new PLaguerre(KLaguerreGrid::length())),
_pL2(new PLaguerre(KLaguerreGrid::length())),
_cc(new CompleteCoeffs),
_QQrr(0), _QQr(0), _dQdQ(0), _tau(0), _wnu(0),
_kk_buff(0), _Kbuff_a_inf(0), _Kbuff_0_a(0),
_lmax(0),  _Nlmax(0), _Nmax(0) {

    cout << "qqHash: setup.. ";

    if (!pnlInd) {
        cout << "qqHash: Error pnlInd = 0" << endl;
        exit(0);    
    }

    if (!_qbCnf) {    
        cout << "qqHash: Error qBas_Conf = 0" << endl;
        exit(0);
    }

    if (_qbCnf->Nlmax() > KLaguerreGrid::length()) {

        cout << endl << "qqHash: Error KLaguerreGrid::length() = " << KLaguerreGrid::length();
        cout << " < " << _qbCnf->Nlmax() << " = qbCnf->Nlmax()" << endl;

        exit(0);    
    }

    _Nlmax = _qbCnf->Nlmax();
    _lmax  = _qbCnf->lmax();

    _Nmax  = new int[_lmax+1];

    for (int ll = 0; ll <= _lmax; ++ll) {    
        _Nmax[ll] = _qbCnf->Nmax(ll);
    }

    _QQr  = new double[(_lmax + 1)*_Nlmax*_Nlmax];
    _QQrr = new double[(_lmax + 1)*_Nlmax*_Nlmax];
    _dQdQ = new double[(_lmax + 1)*_Nlmax*_Nlmax];

    _kk_buff     = new double[_Nlmax*_Nlmax];
    _Kbuff_a_inf = new double[_Nlmax*_Nlmax];
    _Kbuff_0_a   = new double[_Nlmax*_Nlmax];

    _setup();

    cout << " done ok" << endl;
}

qqHash::~qqHash() {

    delete _kLgr1;
    delete _kLgr2;
    delete _pL1;
    delete _pL2;
    delete _cc;

    delete[] _QQr;  _QQr  = 0;
    delete[] _QQrr; _QQrr = 0;
    delete[] _dQdQ; _dQdQ = 0;

    delete[] _tau;  _tau  = 0;
    delete[] _wnu;  _wnu  = 0;

    delete[] _kk_buff;     _kk_buff     = 0;
    delete[] _Kbuff_a_inf; _Kbuff_a_inf = 0;
    delete[] _Kbuff_0_a;   _Kbuff_0_a   = 0;

    delete[] _Nmax; _Nmax = 0;
}
                        
double qqHash::QQr(const int n1, const int l, const int n2) const {
    return _QQr[_ind(n1,l,n2)]/_qbCnf->zeta(l);
}

double qqHash::QQrr(const int n1, const int l, const int n2) const {

    const double t_ze2 = _qbCnf->zeta(l)*_qbCnf->zeta(l);
    return _QQrr[_ind(n1,l,n2)]/t_ze2;
}

double qqHash::dQdQ(const int n1, const int l, const int n2) const {

    const double t_ze2 = _qbCnf->zeta(l)*_qbCnf->zeta(l);
    return _dQdQ[_ind(n1,l,n2)]/t_ze2;
}

void qqHash::prepear_tw(const s_k_ab_List * const tLst, 
                        const s_k_ab_List * const wLst) {

    if (!tLst || !wLst) {
    
        cout << "Error qqHash::prepear(";
        cout << "tLst = " << tLst << ", ";
        cout << "wLst = " << wLst << ").." << endl;    
        exit(0);
    }

    cout << endl << "qqHash:: start prepear tw_points..." << endl;

    delete[] _tau;
    delete[] _wnu;

    const int sz_t = tLst->size()*KLaguerreGrid::length()*_Nlmax*_Nlmax;
    const int sz_w = wLst->size()*KLaguerreGrid::length()*_Nlmax*_Nlmax;

    _tau = new double[sz_t];
    _wnu = new double[sz_w];

    _wt_in_zero(sz_t,sz_w);

    cout << "T(" << tLst->size() << "): ";
    tLst->to_head();
    while (tLst->next()) {

        cout << " " << tLst->linInd();

        _kLgr1->activate(_kLgr1->index((double) tLst->s_k_ab_Ind().s()));
        for (int i_r = 0; i_r < KLaguerreGrid::length(); ++i_r) {

            _calc_tau(i_r, tLst->linInd(),tLst->s_k_ab_Ind());
        }
    }

    cout << endl << "W(" << wLst->size() << "): ";
    wLst->to_head();
    while (wLst->next()) {

        cout << " " << wLst->linInd();

        _kLgr1->activate(_kLgr1->index((double) wLst->s_k_ab_Ind().s()));
        for (int i_r = 0; i_r < KLaguerreGrid::length(); ++i_r) {

            _calc_wnu(i_r, wLst->linInd(),wLst->s_k_ab_Ind());
        }
    }

    cout << endl << "qqHash:: prepear tw_grid done ok" << endl;
}

double qqHash::tau(const int lin_ind_skab, const int k1,
                   const int k2, const int i_point) const {

    const int len = KLaguerreGrid::length();
    const int pos = len*_Nlmax*_Nlmax*lin_ind_skab;

    return _tau[pos + len*(k1*_Nlmax + k2) + i_point];
}

double qqHash::wnu(const int lin_ind_skab,
                   const int k1, const int k2, const int i_point) const {
                   
    const int len = KLaguerreGrid::length();
    const int pos = len*_Nlmax*_Nlmax*lin_ind_skab;

    return _wnu[pos + len*(k1*_Nlmax + k2) + i_point];
}

void qqHash::_wt_in_zero(const int sz_t, const int sz_w) {

    {for (int i = 0; i < sz_t; ++i) _tau[i] = 0;}
    {for (int i = 0; i < sz_w; ++i) _wnu[i] = 0;}
}

void qqHash::_calc_tau(const int i_r, const int lin_ind, const s_k_ab_Index& skabInd) {

    const double rr = _kLgr1->x(i_r);

    const int len = KLaguerreGrid::length();
    const int l1  = _pnlInd->l(skabInd.kabInd().a());
    const int l2  = _pnlInd->l(skabInd.kabInd().b());
    const int k   = skabInd.kabInd().k();

    const int ss = l1+l2+k+2;

    _K_0_a(ss,rr,l1,l2);

    const int pos = len*_Nlmax*_Nlmax*lin_ind + i_r;

    for (int k1 = 0; k1 < _Nmax[l1]; ++k1) {
    for (int k2 = 0; k2 < _Nmax[l2]; ++k2) {

        const double fctr = sqrt(_factor(k1,l1)*_factor(k2,l2));

        const int ind_kk = k1*_Nlmax + k2;
        _tau[pos + len*ind_kk] = fctr*_Kbuff_0_a[ind_kk];
    }}
}

void qqHash::_calc_wnu(const int i_r, const int lin_ind, const s_k_ab_Index& skabInd) {

    const double rr = _kLgr1->x(i_r);

    const int len = KLaguerreGrid::length();
    const int l1  = _pnlInd->l(skabInd.kabInd().a());
    const int l2  = _pnlInd->l(skabInd.kabInd().b());
    const int k   = skabInd.kabInd().k();

    const int ss = l1+l2-k+1;

    _K_a_inf(ss,rr,l1,l2);

    const int pos = len*_Nlmax*_Nlmax*lin_ind + i_r;

    for (int k1 = 0; k1 < _Nmax[l1]; ++k1) {
    for (int k2 = 0; k2 < _Nmax[l2]; ++k2) {

        const double fctr = sqrt(_factor(k1,l1)*_factor(k2,l2));

        const int ind_kk = k1*_Nlmax + k2;
        _wnu[pos + len*ind_kk] = fctr*_Kbuff_a_inf[ind_kk];
    }}
}

void qqHash::_K_0_a(const double alf, const double a, const int l1, const int l2) {

    _K_a_inf(alf,a,l1,l2);
    _Kmfa(alf,0,l1,l2);

    for (int k1 = 0; k1 < _Nmax[l1]; ++k1) {
    for (int k2 = 0; k2 < _Nmax[l2]; ++k2) {

        const int pos = k1*_Nlmax+k2;
        _Kbuff_0_a[pos] = _kk_buff[pos] - _Kbuff_a_inf[pos];
    }}
}

void qqHash::_K_a_inf(const double alf, const double a, const int l1, const int l2) {

    const int alf_int = (int) alf;

    const int sz = _Nlmax*_Nlmax;
    for (int kk = 0; kk < sz; ++kk) _Kbuff_a_inf[kk] = 0;

    for (int m = 0; m <= alf_int; ++m) {    

        _Kmfa(m,a,l1,l2);
        const double factr = _cc->binomC(alf_int,m)*pow(a,alf_int - m)*exp(-a);

        for (int k1 = 0; k1 < _Nmax[l1]; ++k1) {
        for (int k2 = 0; k2 < _Nmax[l2]; ++k2) {

            const int pos = k1*_Nlmax+k2;
            _Kbuff_a_inf[pos] += factr*_kk_buff[pos];
        }}
    }
}

void qqHash::_Kmfa(const double m, const double rr, const int l1, const int l2) {

    _kLgr2->activate(_kLgr2->index(m));

    const int sz = _Nlmax*_Nlmax;
    for (int kk = 0; kk < sz; ++kk) _kk_buff[kk] = 0;

    for (int i = 0; i < KLaguerreGrid::length(); ++i) {

        _pL1->calc(_Nmax[l1],2.0*l1+2.0,_kLgr2->x(i)+rr);
        _pL2->calc(_Nmax[l2],2.0*l2+2.0,_kLgr2->x(i)+rr);
        const double Ai = _kLgr2->A(i);

        for (int k1 = 0; k1 < _Nmax[l1]; ++k1) {
        for (int k2 = 0; k2 < _Nmax[l2]; ++k2) {

            _kk_buff[k1*_Nlmax + k2] += Ai*_pL1->value(k1)*_pL2->value(k2);
        }}
    }
}

int qqHash::_ind(const int n1, const int l, const int n2) const {
    return _Nlmax*(_Nlmax*l + n2) + n1;
}

void qqHash::_setup() {

    // zero init
    {for (int l = 0; l <= _lmax; ++l) {

        for (int n1 = 0; n1 < _Nmax[l]; ++n1) {
        for (int n2 = 0; n2 < _Nmax[l]; ++n2) {

            const int t_ind = _ind(n1,l,n2);

            _QQr [t_ind] = 0;
            _QQrr[t_ind] = 0;
            _dQdQ[t_ind] = 0;
        }}
    }}

    // integrate
    {for (int l = 0; l <= _lmax; ++l) {

        for (int i = 0; i < KLaguerreGrid::length(); ++i) {

            _calc_QQr (l,i);
            _calc_QQrr(l,i);
            _calc_dQdQ(l,i);
        }
    }}

    // normalize
    {for (int l = 0; l <= _lmax; ++l) {

        for (int n1 = 0; n1 < _Nmax[l]; ++n1) {
        for (int n2 = 0; n2 < _Nmax[l]; ++n2) {

            const int t_ind = _ind(n1,l,n2);
            const double fctr = sqrt(_factor(n1,l)*_factor(n2,l));

            _QQr [t_ind] *= fctr;
            _QQrr[t_ind] *= fctr;
            _dQdQ[t_ind] *= fctr;
        }}
    }}
}

double qqHash::_factor(const int n, const int l) const {

    double pr = 1.0;
    for (int z = n + 1; z <= (n + 2*l + 2); ++z) {    
        pr /= (double) z;
    }
    return pr;
}

void qqHash::_calc_QQr(const int l, const int i_point) {

    const int ind = _kLgr1->index(2.0*l + 1.0);
    _kLgr1->activate(ind);

    const double Ai = _kLgr1->A(i_point);

    _pL1->calc(_Nmax[l],2.0*l+2.0,_kLgr1->x(i_point));

    for (int n1 = 0; n1 < _Nmax[l]; ++n1) {
    for (int n2 = 0; n2 < _Nmax[l]; ++n2) {

        _QQr[_ind(n1,l,n2)] += Ai*_pL1->value(n1)*_pL1->value(n2);
    }}
}

void qqHash::_calc_QQrr(const int l, const int i_point) {

    const int ind = _kLgr1->index(2.0*l);
    _kLgr1->activate(ind);

    const double Ai = _kLgr1->A(i_point);

    _pL1->calc(_Nmax[l],2.0*l+2.0,_kLgr1->x(i_point));

    for (int n1 = 0; n1 < _Nmax[l]; ++n1) {
    for (int n2 = 0; n2 < _Nmax[l]; ++n2) {

        _QQrr[_ind(n1,l,n2)] += Ai*_pL1->value(n1)*_pL1->value(n2);
    }}
}

void qqHash::_calc_dQdQ(const int l, const int i_point) {

    _calc_2l_0(l,i_point);
    _calc_2l_1(l,i_point);
    _calc_2l_2(l,i_point);
}

void qqHash::_calc_2l_0(const int l, const int i_point) {

    const double lp1sq = (l+1.0)*(l+1.0);

    const int ind = _kLgr1->index(2.0*l);
    _kLgr1->activate(ind);

    const double Ai = _kLgr1->A(i_point);

    _pL1->calc(_Nmax[l],2.0*l+2.0,_kLgr1->x(i_point));

    for (int n1 = 0; n1 < _Nmax[l]; ++n1) {
    for (int n2 = 0; n2 < _Nmax[l]; ++n2) {

        _dQdQ[_ind(n1,l,n2)] += Ai*_pL1->value(n1)*_pL1->value(n2)*lp1sq;
    }}
}

void qqHash::_calc_2l_1(const int l, const int i_point) {

    const double lp1 = (l+1.0);

    const int ind = _kLgr1->index(2.0*l+1);
    _kLgr1->activate(ind);

    const double Ai = _kLgr1->A(i_point);

    _pL1->calc(_Nmax[l],2.0*l+2.0,_kLgr1->x(i_point));

    for (int n1 = 0; n1 < _Nmax[l]; ++n1) {
    for (int n2 = 0; n2 < _Nmax[l]; ++n2) {

        const double v1 = -_pL1->value(n1) *_pL1->value(n2);
        const double v2 =  _pL1->dValue(n1)*_pL1->value(n2);
        const double v3 =  _pL1->value(n1) *_pL1->dValue(n2);

        _dQdQ[_ind(n1,l,n2)] += Ai*(v1+v2+v3)*lp1;
    }}
}

void qqHash::_calc_2l_2(const int l, const int i_point) {

    const int ind = _kLgr1->index(2.0*l+2);
    _kLgr1->activate(ind);

    const double Ai = _kLgr1->A(i_point);

    _pL1->calc(_Nmax[l],2.0*l+2.0,_kLgr1->x(i_point));

    for (int n1 = 0; n1 < _Nmax[l]; ++n1) {
    for (int n2 = 0; n2 < _Nmax[l]; ++n2) {

        const double v1 = 0.25*_pL1->value(n1)*_pL1->value(n2);
        const double v2 = -0.5*_pL1->value(n1)*_pL1->dValue(n2);
        const double v3 = -0.5*_pL1->dValue(n1)*_pL1->value(n2);
        const double v4 =      _pL1->dValue(n1)*_pL1->dValue(n2);

        _dQdQ[_ind(n1,l,n2)] += Ai*(v1+v2+v3+v4);
    }}
}

////////////////////////////////////////////////////////////////////////////

// end of file
