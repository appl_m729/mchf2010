#include <iostream>
#include <fstream>
#include <cstdlib>
#include "atom_conf.h"
#include "read.h"
#include "det_state.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Atom_Conf::Atom_Conf(const char file[]):
_Z(0), _core_lmax(0), _core_maxNsubsh(0),
_core_Nsubsh(0), _core_n(0), _core_w(0), _core_Nel(0),
_core_Nsubsh_tot(0),
_core_n_tot(0), _core_l_tot(0),_core_w_tot(0),
_walk_Nel(0), _walk_Nsubsh(0), _walk_n(0), _walk_l(0),
_hill_Nel(0), _hill_Nsubsh(0), _hill_n(0),
_hill_l(0), _hill_w(0),
_L(0), _ML(0), _P(0), _S(0), _MS(0) {

    _read_core(file);
    _read_hill(file);
    _read_walk(file);
    _check_overlaps_cwh();
    _read_LMLSMLP(file);

    if (!Nel_tot()) {
        cout << "Error in Atom_Conf; Nel_tot = 0" << endl;
        exit(0);
    }

    Det_State::set_Nel(Nel_val());
    print();
}

Atom_Conf::~Atom_Conf() {

    delete[] _core_Nsubsh; _core_Nsubsh = 0;
    delete[] _core_n;      _core_n      = 0;
    delete[] _core_w;      _core_w      = 0;

    delete[] _core_n_tot;  _core_n_tot  = 0;  
    delete[] _core_l_tot;  _core_l_tot  = 0;  
    delete[] _core_w_tot;  _core_w_tot  = 0;
    
    delete[] _walk_n;      _walk_n      = 0;
    delete[] _walk_l;      _walk_l      = 0;

    delete[] _hill_n;      _hill_n      = 0;
    delete[] _hill_l;      _hill_l      = 0;
    delete[] _hill_w;      _hill_w      = 0;
}

int Atom_Conf::core_Nsubsh(const int ext_l) const {

    if (ext_l < 0 || ext_l > _core_lmax) {
        cout << "Error in Atom_Conf::core_Nsubsh" << endl;
        cout << " ext_l = " << ext_l << endl;
        exit(0);    
    }
    return _core_Nsubsh[ext_l];
}

int Atom_Conf::core_n(const int ext_l, const int ext_i) const {

    if (ext_l < 0 || ext_l > _core_lmax) {
        cout << "Error in Atom_Conf::core_n" << endl;
        cout << " ext_l = " << ext_l << endl;
        exit(0);    
    }
    if (ext_i < 0 || ext_i >= _core_Nsubsh[ext_l]) {    
        cout << "Error in Atom_Conf::core_n; ext_i = " << ext_i << endl;
        exit(0);
    }
    return _core_n[ext_l*_core_maxNsubsh + ext_i];
}

int Atom_Conf::core_w(const int ext_l, const int ext_i) const {

    if (ext_l < 0 || ext_l > _core_lmax) {
        cout << "Error in Atom_Conf::core_w" << endl;
        cout << " ext_l = " << ext_l << endl;
        exit(0);    
    }
    if (ext_i < 0 || ext_i >= _core_Nsubsh[ext_l]) {    
        cout << "Error in Atom_Conf::core_w; ext_i = " << ext_i << endl;
        exit(0);
    }
    return _core_w[ext_l*_core_maxNsubsh + ext_i];
}

int Atom_Conf::core_n(const int i) const {

    if (0 > i || i >= _core_Nsubsh_tot) {    
        cout << "Error in Atom_Conf::core_n" << endl;
        exit(0);
    }
    return _core_n_tot[i];
}

int Atom_Conf::core_l(const int i) const {

    if (0 > i || i >= _core_Nsubsh_tot) {    
        cout << "Error in Atom_Conf::core_l" << endl;
        exit(0);
    }
    return _core_l_tot[i];
}

int Atom_Conf::core_w(const int i) const {

    if (0 > i || i >= _core_Nsubsh_tot) {    
        cout << "Error in Atom_Conf::core_w" << endl;
        exit(0);
    }
    return _core_w_tot[i];
}

int Atom_Conf::walk_n(const int i) const {

    if (i < 0 || i >= _walk_Nsubsh) {    
        cout << "Error in Atom_Conf::walk_n" << endl;
        exit(0);
    }
    return _walk_n[i];
}

int Atom_Conf::walk_l(const int i) const {

    if (i < 0 || i >= _walk_Nsubsh) {    
        cout << "Error in Atom_Conf::walk_l" << endl;
        exit(0);
    }
    return _walk_l[i];
}

int Atom_Conf::hill_n(const int i) const {

    if (i < 0 || i >= _hill_Nsubsh) {    
        cout << "Error in Atom_Conf::hill_n" << endl;
        exit(0);
    }
    return _hill_n[i];
}

int Atom_Conf::hill_l(const int i) const {

    if (i < 0 || i >= _hill_Nsubsh) {    
        cout << "Error in Atom_Conf::hill_l" << endl;
        exit(0);
    }
    return _hill_l[i];
}

int Atom_Conf::hill_w(const int i) const {

    if (i < 0 || i >= _hill_Nsubsh) {    
        cout << "Error in Atom_Conf::hill_w" << endl;
        exit(0);
    }
    return _hill_w[i];
}

int Atom_Conf::hill_find(const int n, const int l) const {

    int reslt = -1;
    for (int k = 0; k < _hill_Nsubsh; ++k) {
    
        if ((n == _hill_n[k]) && (l == _hill_l[k])) return k; 
    }
    return reslt;
}

void Atom_Conf::print() const {

    cout << "Atom_Conf:" << endl;

    cout << "Core Subshells: " << endl;
    for (int ll = 0; ll <= core_lmax(); ++ll) {
    
        cout << "("<< core_Nsubsh(ll) << ") ";
        for (int i = 0; i < core_Nsubsh(ll); ++i) {        
            cout << core_w(ll,i) << " ";
        } cout << endl;
    }

    cout << "core Nel = " << core_Nel() << endl;
    for (int ic = 0; ic < core_Nsubsh(); ++ic) {
    
        cout << "(" << core_n(ic) << "," << core_l(ic) << ")";
        cout << "[" << core_w(ic) << "] ";    
    } cout << endl;

    cout << "walk_Nel = " << walk_Nel() << endl;
    for (int s = 0; s < walk_Nsubsh(); ++s) {
        cout << "(" << walk_n(s) << "," << walk_l(s) << ") ";
    }cout << endl;

    cout << "hill_Nel = " << hill_Nel() << endl;
    for (int i = 0; i < hill_Nsubsh(); ++i) {
        cout << "(" << hill_n(i) << "," << hill_l(i) << ")";
        cout << "[" << hill_w(i) << "] ";
    } cout << endl;

    cout << "Nel_val = " << Nel_val() << endl;
    cout << "Nel_tot = " << Nel_tot() << endl;

    cout << "Z = " << Z() << ";  ";
    cout << "L ML S MS P: " << L() << " " << ML() << " ";
    cout << S() << " " << MS() << " " << P() << endl;
}

void Atom_Conf::_read_core(const char file[]) {

    if (ReadReal(file,"Z",0,_Z)) {
        cout << "Error reading in Atom_Conf:  Z " << endl;
        exit(0);
    }

    if (ReadInt(file,"core_lmax",0,_core_lmax)) {
        cout << "Error reading in Atom_Conf: core_lmax" << endl;
        exit(0);
    }

    if (0 > _core_lmax) {

        cout << "Atom_Conf:  No core shells; continue working... ok" << endl;

        _core_lmax = -1;
        _core_maxNsubsh = 0;
        _core_Nsubsh = 0;
        _core_n = 0;
        _core_w = 0;
        _core_Nel = 0;
        _core_Nsubsh_tot = 0;
        _core_n_tot = 0;
        _core_l_tot = 0;
        _core_w_tot = 0;

        return;
    }

    // setup
    _core_Nsubsh = new int[_core_lmax + 1];
    _core_Nsubsh_tot = 0;

    {for (int ll = 0; ll <= _core_lmax; ++ll) {

        if (ReadInt(file,"core_lmax",ll+1, _core_Nsubsh[ll])) {
            cout << "Error reading in Atom_Conf: core_Nsubsh[" << ll << "]" << endl;
            exit(0);
        }
        if (_core_Nsubsh[ll] <= 0) {
            cout << "Error in Atom_Conf: core_Nsubsh[" << ll << "] <= 0" << endl;
            exit(0);        
        }

        _core_Nsubsh_tot += _core_Nsubsh[ll];

        if (_core_maxNsubsh < _core_Nsubsh[ll]) _core_maxNsubsh = _core_Nsubsh[ll];
    }}

    _core_n = new int[_core_maxNsubsh*(_core_lmax + 1)];
    _core_w = new int[_core_maxNsubsh*(_core_lmax + 1)];
    
    _core_Nel = 0;
    {for (int ll = 0; ll <= _core_lmax; ++ll) {
        
        for (int i = 0; i < _core_Nsubsh[ll]; ++i) {        

            _core_w[ll*_core_maxNsubsh+i] = 4*ll+2;
            _core_n[ll*_core_maxNsubsh+i] = ll + 1 + i;

            _core_Nel += _core_w[ll*_core_maxNsubsh+i];
        }
    }}
    _setup_core_tot();
}

void Atom_Conf::_setup_core_tot() {

    _core_w_tot = new int[_core_Nsubsh_tot];
    _core_n_tot = new int[_core_Nsubsh_tot];
    _core_l_tot = new int[_core_Nsubsh_tot];

    // initialize
    {for (int ll = 0, pos = 0; ll <= _core_lmax; ++ll) {
        for (int i = 0; i < _core_Nsubsh[ll]; ++i, ++pos) {
        
            _core_w_tot[pos] = _core_w[ll*_core_maxNsubsh + i];
            _core_n_tot[pos] = _core_n[ll*_core_maxNsubsh + i];
            _core_l_tot[pos] = ll;
        }
    }}

    // sort
    for (int ci = 0; ci < _core_Nsubsh_tot - 1; ++ci) {
    for (int cj = ci+1; cj < _core_Nsubsh_tot; ++cj) {

        if ( (_core_n_tot[ci] > _core_n_tot[cj]) ||
             ((_core_n_tot[ci] == _core_n_tot[cj]) && 
              (_core_l_tot[ci] >  _core_l_tot[cj])) ) {

            _core_tot_swap(ci,cj);
        }    
    }}
}

void Atom_Conf::_read_walk(const char file[]) {

    if (ReadInt(file,"walk_Nel",0,_walk_Nel)) {
        cout << "Error reading in Atom_Conf:  _walk_Nel" << endl;
        exit(0);
    }
    if (0 >= _walk_Nel) {    

        cout << "Atom_Conf:: no walk_Nel; continue working... ok." << endl;

        _walk_Nel = 0;
        _walk_Nsubsh = 0;
        _walk_n = 0;
        _walk_l = 0;

        return;
    }

    if (ReadInt(file,"walk_Nel",1,_walk_Nsubsh)) {
        cout << "Error reading in Atom_Conf:  _walk_Nsubsh" << endl;
        exit(0);    
    }
    if (_walk_Nsubsh <= 0) {    
        cout << "Error reading in Atom_Conf:  _walk_Nsubsh <= 0" << endl;
        exit(0);
    }

    _walk_n = new int[_walk_Nsubsh];
    _walk_l = new int[_walk_Nsubsh];

    for (int wi = 0; wi < _walk_Nsubsh; ++wi) {

        if (ReadInt(file,"walk_Nel",2*wi+2,_walk_n[wi])) {
            cout << "Error reading in Atom_Conf:  _walk_n" << endl;
            exit(0);
        }
        if (ReadInt(file,"walk_Nel",2*wi+3,_walk_l[wi])) {
            cout << "Error reading in Atom_Conf:  _walk_l" << endl;
            exit(0);
        }    
    }
    _check_walk();
}

void Atom_Conf::_read_hill(const char file[]) {

    if (ReadInt(file,"hill_Nsubsh",0,_hill_Nsubsh)) {
        cout << "Error reading in Atom_Conf:  hill_Nsubsh" << endl;
        exit(0);
    }

    if (_hill_Nsubsh <= 0) {

        cout << "No hill electrons. Continue working.. ok" << endl;

        _hill_Nel = 0;
        _hill_Nsubsh =0;
        _hill_n = 0;
        _hill_l = 0;
        _hill_w = 0;

        return;
    }

    _hill_n = new int[_hill_Nsubsh];
    _hill_l = new int[_hill_Nsubsh];
    _hill_w = new int[_hill_Nsubsh];

    _hill_Nel = 0;
    for (int ih = 0; ih < _hill_Nsubsh; ++ih) {

        if (ReadInt(file,"hill_Nsubsh",3*ih+1,_hill_w[ih])) {
            cout << "Error reading in Atom_Conf:  _hill_w" << endl;
            exit(0);
        }
        _hill_Nel += _hill_w[ih];

        if (ReadInt(file,"hill_Nsubsh",3*ih+2,_hill_n[ih])) {
            cout << "Error reading in Atom_Conf:  _hill_n" << endl;
            exit(0);
        }
        if (ReadInt(file,"hill_Nsubsh",3*ih+3,_hill_l[ih])) {
            cout << "Error reading in Atom_Conf:  _hill_l" << endl;
            exit(0);
        }
    }
    _check_hill();
}

void Atom_Conf::_check_overlaps_cwh() {

    bool flag_jmp(false), flag_cw(false), flag_ch(false), flag_wh(false);

    if (core_Nsubsh() > 0) {

        const int icn = core_Nsubsh()-1;

        if (walk_Nsubsh() > 0) {

            if (_core_n_tot[icn] > _walk_n[0]) flag_jmp = true;

            if ((_core_n_tot[icn] == _walk_n[0]) &&
                (_core_l_tot[icn] >  _walk_l[0]) ) flag_jmp = true;
        }

        if (hill_Nsubsh() > 0) {

            if (_core_n_tot[icn] > _hill_n[0]) flag_jmp = true;

            if ((_core_n_tot[icn] == _hill_n[0]) &&
                (_core_l_tot[icn] >  _hill_l[0]) ) flag_jmp = true;
        }
    }

    for (int ci = 0; ci < core_Nsubsh(); ++ci) {
        for (int wj = 0; wj < walk_Nsubsh(); ++wj) {
        
            if ( (_core_n_tot[ci] == _walk_n[wj]) && (_core_l_tot[ci] == _walk_l[wj]) ) {
                flag_cw = true;
                break;
            }
        }
        for (int hj = 0; hj < hill_Nsubsh(); ++hj) {
        
            if ( (_core_n_tot[ci] == _hill_n[hj]) && (_core_l_tot[ci] == _hill_l[hj]) ) {
                flag_ch = true;
                break;
            }
        }
    }

    for (int wi = 0; wi < walk_Nsubsh(); ++wi) {        
    for (int hj = 0; hj < hill_Nsubsh(); ++hj) {
        
        if ( (_walk_n[wi] == _hill_n[hj]) && (_walk_l[wi] == _hill_l[hj]) ) {        
            flag_wh = true;
            break;
        }
    }}

    if (flag_jmp || flag_cw || flag_ch || flag_wh) {
    
        cout << "Error in Atom_Conf:: overlaps; flags are:" << endl;
        cout << flag_jmp << " " << flag_cw << " " << flag_ch << " " << flag_wh << endl;
        exit(0);
    }
}

void Atom_Conf::_read_LMLSMLP(const char file[]) {

    if (ReadInt(file,"L", 0, _L)) {    
        cout << "Error reading in Atom_Conf: L" << endl;
        exit(0);
    }
    if (ReadInt(file,"ML", 0, _ML)) {    
        cout << "Error reading in Atom_Conf: ML" << endl;
        exit(0);
    }
    if (ReadReal(file,"S", 0, _S)) {    
        cout << "Error reading in Atom_Conf: S" << endl;
        exit(0);
    }
    if (ReadReal(file,"MS", 0, _MS)) {    
        cout << "Error reading in Atom_Conf: MS" << endl;
        exit(0);
    }
    if (ReadInt(file,"P", 0, _P)) {
        cout << "Error reading in Atom_Conf: P" << endl;
        exit(0);
    }
}

void Atom_Conf::_check_walk() {

    {for (int wi = 0; wi < _walk_Nsubsh; ++wi) {

        if (0 >= _walk_n[wi] || 0 > _walk_l[wi] || _walk_n[wi] <= _walk_l[wi]) {
        
            cout << "Error in Atom_Conf:: incorrect walk_data.." << endl;
            exit(0);
        }
    }}

    {for (int wi = 0; wi < _walk_Nsubsh - 1; ++wi) {

        if (_walk_n[wi] > _walk_n[wi+1]) {
        
            cout << "Error in Atom_Conf:: walk_data not sorted.." << endl;
            exit(0);

        } else if ( (_walk_n[wi] == _walk_n[wi+1]) &&
                    (_walk_l[wi] >= _walk_l[wi+1]) ) {
        
            cout << "Error in Atom_Conf:: walk_data not sorted.." << endl;
            exit(0);
        }
    }}

    {int sum = 0;
     for (int wi = 0; wi < _walk_Nsubsh; ++wi) sum += 4*_walk_l[wi] + 2;
     for (int hi = 0; hi < _hill_Nsubsh; ++hi) sum += 4*_hill_l[hi] + 2 - _hill_w[hi];

     if (sum < _walk_Nel) {
         cout << "Error in Atom_Conf:: exceeding walk_Nel.." << endl;
         exit(0);     
     }
    }
}

void Atom_Conf::_check_hill() {

    {for (int hi = 0; hi < _hill_Nsubsh; ++hi) {

        if ( (0 >= _hill_n[hi]) || (0 > _hill_l[hi]) ||
             (_hill_n[hi] <= _hill_l[hi]) || (_hill_w[hi] <= 0) ||
             (_hill_w[hi] > (4*_hill_l[hi]+2)) ) {
        
            cout << "Error in Atom_Conf:: incorrect hill_data.." << endl;
            exit(0);
        }
    }}

    {for (int hi = 0; hi < _hill_Nsubsh - 1; ++hi) {

        if (_hill_n[hi] > _hill_n[hi+1]) {
        
            cout << "Error in Atom_Conf:: hill_data not sorted.." << endl;
            exit(0);

        } else if ( (_hill_n[hi] == _hill_n[hi+1]) && 
                    (_hill_l[hi] >  _hill_l[hi+1]) ) {

            cout << "Error in Atom_Conf:: hill_data not sorted.." << endl;
            exit(0);
        }
    }}
}

void Atom_Conf::_core_tot_swap(const int i, const int j) {

    const int tw = _core_w_tot[i];
    const int tn = _core_n_tot[i];
    const int tl = _core_l_tot[i];

    _core_w_tot[i] = _core_w_tot[j];
    _core_n_tot[i] = _core_n_tot[j];
    _core_l_tot[i] = _core_l_tot[j];
     
    _core_w_tot[j] = tw;
    _core_n_tot[j] = tn;
    _core_l_tot[j] = tl;
}

////////////////////////////////////////////////////////////////////////////

// end of file

