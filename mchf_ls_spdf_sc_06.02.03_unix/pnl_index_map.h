#ifndef _PNL_INDEX_MAP_H_
#define _PNL_INDEX_MAP_H_

////////////////////////////////////////////////////////////////////////////

class Atom_Conf;

class Pnl_IndexMap {

public:

    Pnl_IndexMap(const Atom_Conf * const aCnf);
    ~Pnl_IndexMap();

    int a(const int ext_n, const int ext_l) const;

    // Pnl data but sorted as 10,20,30,40...21,31,41..32,42,..
    int length() const {return _length;}
    int square() const {return _length*_length;}
    int n(const int a) const;
    int l(const int a) const;
    int shift(const int a) const;
    // l
    int lmax() const {return _lmax;}
    int len(const int l) const;
    int max_llen() const {return _max_llen;}
    int a_sta(const int l) const;
    int a_end(const int l) const;

    void print() const;

private:

    int  _length, *_n, *_l;
    int  _lmax, *_llen, _max_llen, *_a_sta, *_a_end;

    void _setup_nl(const Atom_Conf * const aCnf);
    void _setup_llen_aa();

    void _sort_nl();
    void _check_i(const int i) const;
    void _check_l(const int l) const;
};

////////////////////////////////////////////////////////////////////////////

#endif // _PNL_INDEX_MAP_H_

// end of file

