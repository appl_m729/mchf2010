#ifndef _CONFIGURATION_STATE_FUNCTION_LS_H_
#define _CONFIGURATION_STATE_FUNCTION_LS_H_

////////////////////////////////////////////////////////////////////////////

class Det_State;

class CSFunc_LS {

public:

    CSFunc_LS();
    CSFunc_LS(const CSFunc_LS& rhs);
    ~CSFunc_LS();

    CSFunc_LS& operator= (const CSFunc_LS& rhs);

    int    L () const {return _L; }
    int    ML() const {return _ML;}
    double S () const {return _S; }
    double MS() const {return _MS;}
    int    P () const {return _P; }

    int size() const;
    double A(const int j) const;
    const Det_State& det(const int j) const;

    void reduce();

private:

    int        _size;
    double    *_A;
    Det_State *_det;

    int    _L, _ML, _P;
    double _S, _MS;

    friend class CSF_Builder;
};

////////////////////////////////////////////////////////////////////////////

#endif // _CONFIGURATION_STATE_FUNCTION_LS_H_

// end of file

