#ifndef _ALL_CONSTANTS_H_
#define _ALL_CONSTANTS_H_

////////////////////////////////////////////////////////////////////////////

const double PREC_OFF = 1e-14;      // coeffs
const double c05 = 0.5 - PREC_OFF;  // coeffs

const double SPIN_THRESHOLD = 1e-4; // qua5_Index, qua5_rbtree, dets_builder
const double MS_ACCURACY = 1e-10;   // qua3_Index

enum SpinState {DOWN = 0, UP = 1, STEP = 2};
const double SPIN[3] = {-0.5, 0.5, 1.0};      // nlmlms_state, q4x_convertor

const double ACCURATE_0 = 1e-27;    // qua5_Index

const double CSF_TRANCATE_THRESHOLD = 1e-14; // csf_ls

const int SymMatrix_MAX_SIZE = 100; // sym_matrix
const int Yacoby_MAX_SIZE = 300;    // yacoby

const double XXp1_ZERO = 1e-7;  // csf_diaglzr   1/(length of basis <~ 10^4)
const double EIG_L_MAX = 30;    // csf_diaglzr
const double EIG_S_MAX = 30;
const double LL1_ERROR = 1e-9;  // csf_diaglzr
const double SS1_ERROR = 1e-9;  // csf_diaglzr
const double L2S2_ZERO_ITEM = 1e-14; // csf_diaglzr
const double L2S2_TRANCATE  = 0; // csf_diaglzr

const double CCV_TRANCATE_COEFF = 1e-14; // ccv_list for two_list

const double FF_CG_EIG_DIAG_EPS  = 1e-31;  // ham_matrix
const double FF_CG_KSI_0_IS_ZERO = 1e-15;  // ham_matrix
const int    FF_CG_MAX_ITER      = 30000;  // ham_matrix

const int MCHF_MAX_ITER = 1000;  // mchf_ls

const int VEC_EQ_MAX_ITER      = 5000000;  // vec_eq_syst
const int VEC_EQ_ITER_JMP_STEP = 200;      // vec_eq_syst
const double VEQ_EQ_dQ_ERR     = 1e-13;    // vec_eq_syst
const double VEQ_EQ_dQ_FREEZE  = 1e-14;    // vec_eq_syst

const int NUM_E_LENGTH = 3;                       // num_e_trace
enum NumE_Part {E_TOT = 0, E_ONE = 1, E_TWO = 2}; // num_e_trace

const int E_PRECISION_PRINT = 10; // mchf_ls, num_e_trace;

////////////////////////////////////////////////////////////////////////////

#endif // _ALL_CONSTANTS_H_

// end of file

