#ifndef _QR_DECOMPOSITION_H_
#define _QR_DECOMPOSITION_H_

////////////////////////////////////////////////////////////////////////////

class QR_Decompose {

public:

    QR_Decompose(const int size);
    ~QR_Decompose();

    void set(const int i, const int j, const double Val) {_R[i*_n+j] = Val;}

    void resize(const int sz);
    void decompose();
    void print() const;

    double Q(const int i, const int j) const {return _Q[i*_n+j];}
    double R(const int i, const int j) const {return _R[i*_n+j];}

private:

    const double QR_ERROR_PREC;
    const int _max_n;
    int _n;
    double * const _Q, * const _R;
    double * const _u, * const _Buff;

    void   _hausV(const int k, const double * const M, double * const u_ext);
    double _sign(const int k, const double * const M) const;
    void   _mult_hausP(const int k, const double * const v, double * const M);
    void   _mult_Q(const int k, const double * const v, double * const M);
    void   _init_Q(const double * const v);
    double _deltaKro(const int i, const int j) const;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _QR_DECOMPOSITION_H_


// end of file
