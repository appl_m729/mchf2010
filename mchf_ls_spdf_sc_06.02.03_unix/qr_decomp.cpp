#include <iostream>
#include <cmath>
#include <cstdlib>
#include "qr_decomp.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

QR_Decompose::QR_Decompose(const int size):
QR_ERROR_PREC(1e-15), _max_n(size), _n(size),
_Q(new double[_n*_n]), _R(new double[_n*_n]),
_u(new double[_n*_n]), _Buff(new double[_n*_n]) {

}

QR_Decompose::~QR_Decompose() {

    delete[] _Q;
    delete[] _R;
    delete[] _u;
    delete[] _Buff;
}

void QR_Decompose::resize(const int Size) {

    if (1 < Size && Size <= _max_n) {

        _n = Size;
        return;
    }

    cout << "QR_Decompose: Error in resize" << endl;
    cout << "Size = " << Size << "  _max_n = " << _max_n << endl;
    exit(0);
}

void QR_Decompose::decompose() {

    _hausV(0,_R,_u);
    _init_Q(_u);
    _mult_hausP(0,_u,_R);

    for (int k = 1; k < _n-1; ++k) {

       _hausV(k,_R,_u);
       _mult_hausP(k,_u,_R);
       _mult_Q(k,_u,_Q);
    }
}

void QR_Decompose::print() const {

    for (int i = 0; i < _n; ++i) {
    for (int j = 0; j < _n; ++j) {

        cout << _R[i*_n+j] << " ";
    }cout << endl;}
    cout << endl;
}

void QR_Decompose::_hausV(const int k, const double * const M, double * const u_ext) {

// A. Golub, Ch. Van Loun;  p.184,  sect 5.1.3, alg. 5.1.1

    double vi_max = fabs(M[k*_n+k]);

    {for (int i = k+1; i < _n; ++i) {
        if (vi_max < fabs(M[i*_n+k])) {vi_max = fabs(M[i*_n+k]);}
    }}

    if (vi_max < QR_ERROR_PREC) {

        for (int i = k+1; i < _n; ++i) {u_ext[i] = 0;}
        u_ext[k] = 1.0;

        return;
    }

    // calc norm
    double mu = 0;    
    {for (int i = k; i < _n; ++i) {
        const double buff = M[i*_n+k]/vi_max;
        mu += buff*buff;
    }}
    mu = vi_max*sqrt(mu);
//    cout << "mu = " << mu << endl;

    const double beta = M[k*_n+k] + _sign(k,M)*mu;
    for (int i = k+1; i < _n; ++i) {
        u_ext[i] = M[i*_n+k]/beta;
    }
    u_ext[k] = 1.0;
}

double QR_Decompose::_sign(const int k, const double * const M) const {

    if (M[k*_n+k] > 0.0) return 1.0;
    return -1.0;
}

void QR_Decompose::_mult_hausP(const int k, const double * const v, double * const M) {

    double gam = 0;
    {for (int i = k; i < _n; ++i) {gam += v[i]*v[i];}}
    gam = 2.0/gam;

    {for (int i = k; i < _n; ++i) {
     for (int j = k; j < _n; ++j) {

         _Buff[i*_n+j] = 0;
         for (int s = k; s < _n; ++s) {
             _Buff[i*_n+j] += (_deltaKro(i,s) - gam*v[i]*v[s])*M[s*_n+j];
         }
     }}}

    {for (int i = k; i < _n; ++i) {
         for (int j = k; j < _n; ++j) {
             M[i*_n+j] = _Buff[i*_n+j];
         }
    }}
    {for (int i = k+1; i < _n; ++i) {M[i*_n+k] = 0;}}
}

void QR_Decompose::_mult_Q(const int k, const double * const v, double * const M) {

    double gam = 0;
    {for (int i = k; i < _n; ++i) {gam += v[i]*v[i];}}
    gam = 2.0/gam;

    {for (int i = 0; i < _n; ++i) {
     for (int j = k; j < _n; ++j) {

         double sum = 0;
         for (int s = k; s < _n; ++s) {
             sum += M[i*_n+s] * (_deltaKro(s,j) - gam*v[s]*v[j]);
         }
         _Buff[i*_n+j] = sum;
     }}}

    {for (int i = 0; i < _n; ++i) {
     for (int j = k; j < _n; ++j) {
         M[i*_n+j] = _Buff[i*_n+j];
     }}}
}

void QR_Decompose::_init_Q(const double * const v) {

    double gam = 0;
    {for (int i = 0; i < _n; ++i) {gam += v[i]*v[i];}}
    gam = 2.0/gam;

    {for (int i = 0; i < _n; ++i) {
     for (int j = 0; j < _n; ++j) {

         _Q[i*_n+j] = _deltaKro(i,j) - gam*v[i]*v[j];
     }}}
}

double QR_Decompose::_deltaKro(const int i, const int j) const {

    if (i == j) return 1.0;
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////

// end of file
