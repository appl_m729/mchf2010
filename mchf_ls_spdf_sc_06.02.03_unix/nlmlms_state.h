#ifndef _ELECTRON_n_l_ml_ms_STATE_H_
#define _ELECTRON_n_l_ml_ms_STATE_H_

////////////////////////////////////////////////////////////////////////////

class nlmlms_State {

public:

    nlmlms_State();
    nlmlms_State(const nlmlms_State& rhs);
    ~nlmlms_State();

    void   nlmlms(int& n, int& l, int& m, double& ms) const;
    void   nl(int& n, int& l) const;
    int    n()  const;
    int    l()  const;
    int    ml() const;
    double ms() const;

    void   clone(const nlmlms_State& rhs);
    void   set_nlmlms(const int n, const int l, const int ml, const double ms);
    void   set_n(const int n);
    void   set_l(const int l);
    void   set_ml(const int ml);
    void   set_ms(const double ms);

    void   print() const;

    nlmlms_State& operator= (const nlmlms_State& rhs);

    friend bool operator !=    (const nlmlms_State& st_lft, const nlmlms_State& st_rgh);
    friend bool operator ==    (const nlmlms_State& st_lft, const nlmlms_State& st_rgh);
    friend bool operator >     (const nlmlms_State& st_gre, const nlmlms_State& st_sma);
    friend bool iseq_barring_n (const nlmlms_State& st_lft, const nlmlms_State& st_rgh);

private:

    int    _n,_l,_ml; // quantum numbers;
    double _ms;
};

////////////////////////////////////////////////////////////////////////////

#endif // _ELECTRON_n_l_ml_ms_STATE_H_

// end of file

