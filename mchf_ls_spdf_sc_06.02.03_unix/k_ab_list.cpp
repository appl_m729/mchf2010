#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "k_ab_index.h"
#include "k_ab_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

k_ab_DataBlock::k_ab_DataBlock():
next(0), k_ab_ind(0), linearIndex(-1) {
}

k_ab_DataBlock::~k_ab_DataBlock() {
    k_ab_ind = 0;
    delete next; next = 0;
}

////////////////////////////////////////////////////////////////////////////

k_ab_List::k_ab_List():
_head(new k_ab_DataBlock), _crnt_item(_head), _size(0) {

}

k_ab_List::~k_ab_List() {

    delete _head;
    _crnt_item = 0;
}

void k_ab_List::clear() {

    if (_head->next) {
        delete _head->next; _head->next = 0;
    }

    _crnt_item = _head;
    _size = 0;
}

void k_ab_List::add(const k_ab_Index* kabInd, const int ext_linIndex) {

    _crnt_item->next = new k_ab_DataBlock;
    _crnt_item = _crnt_item->next;
    _crnt_item->k_ab_ind = kabInd;
    _crnt_item->linearIndex = ext_linIndex;

    ++_size;
}

void k_ab_List::to_head() const {
    _crnt_item = _head;
}

bool k_ab_List::next() const {

    if (_crnt_item->next) {
        _crnt_item = _crnt_item->next;
        return true;
    }
    return false;
}

int k_ab_List::size() const {
    return _size;
}

const k_ab_Index& k_ab_List::k_ab_Ind() const {

    return *(_crnt_item->k_ab_ind);
}

int k_ab_List::linInd() const {

    return _crnt_item->linearIndex;
}

void k_ab_List::write(const char file[]) const {

    cout << "k_ab_List: writing in "<< file << "..";
    ofstream fout(file);

    fout << "total size = " << size() << endl << endl;

    fout << "k_ab_Index: " << endl;
    fout << "position   linearIndex   k  a b" << endl;

    int ii = 0;
    to_head();
    while(next()) {

        int k, a,b;
        k_ab_Ind().k_ab(k, a,b);

        fout << endl << setw(4) << ii++ << setw(4) << linInd();
        fout << setw(4) << k << setw(4) << a << setw(4) << b;
    }

    fout << endl << endl << "end of file" << endl;

    fout.close();
    cout << ". done ok" << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file
