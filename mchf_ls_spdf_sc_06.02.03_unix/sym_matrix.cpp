#include <iostream>
#include <cstdlib>
#include "sym_matrix.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

SymMatrix::SymMatrix():
_MAX_size(SymMatrix_MAX_SIZE), _size(SymMatrix_MAX_SIZE), _A(0) {

    _allocate_A(_MAX_size);
}

SymMatrix::SymMatrix(const int extSize):
_MAX_size(SymMatrix_MAX_SIZE), _size(extSize), _A(0) {

    _check_size(extSize);

    if (_MAX_size < extSize) {

        _print_maxresize(extSize);
        _MAX_size = _size;
    }

    _allocate_A(_MAX_size);
}

SymMatrix::SymMatrix(const SymMatrix& rhs):
_MAX_size(rhs._MAX_size), _size(rhs._size), _A(0) {

    _allocate_A(_MAX_size);
    _copy_rhs_A(rhs);
}

SymMatrix::~SymMatrix() {

    delete[] _A;  _A = 0;
}

SymMatrix& SymMatrix::operator= (const SymMatrix& rhs) {

    if (&rhs == this) return *this;

    if ( (!_A) || (_MAX_size < rhs._MAX_size) ) {

        delete[] _A; _A = 0;

        _MAX_size = rhs._MAX_size;
        _allocate_A(_MAX_size);
    }

    _size = rhs._size;
    _copy_rhs_A(rhs);

    return *this;
}

double& SymMatrix::operator()(const int i, const int j) {

    if (i < 0 || i >= _MAX_size) {

        cout << "Error in SymMatrix::operator(  i = " << i << endl;
        cout << "MAX_size= " << _MAX_size << "  size= " << _size << endl;
        exit(0);
    }

    if (j < 0 || j >= _MAX_size) {

        cout << "Error in SymMatrix::operator(  j = " << j << endl;
        cout << "MAX_size= " << _MAX_size << "  size= " << _size << endl;
        exit(0);
    }

    return _A[_ind(i,j)];
}

void SymMatrix::resize(const int extSize) {

    _check_size(extSize);

    if ( _MAX_size < extSize) {

        _print_maxresize(extSize);

        delete[] _A; _A = 0;

        _MAX_size = extSize;
        _allocate_A(_MAX_size);
    }

    _size = extSize;
}

void SymMatrix::print() const {

    for (int i = 0; i < _size; ++i) {
    for (int j = i; j < _size; ++j) {

        cout << _A[_ind(i,j)] << "  ";
    } cout << endl;}
}

int SymMatrix::_ind(const int i, const int j) const {

    const int two2 = 2;

    if (i > j) return j*_size - (j*j+j)/two2 + i;
    return i*_size - (i*i+i)/two2 + j;
}

void SymMatrix::_allocate_A(const int max_size) {

    const int two2 = 2;
    const int length = (max_size*(max_size + 1))/two2;

    _A = new double[length];
}

void SymMatrix::_copy_rhs_A(const SymMatrix& rhs) {

    const int two2 = 2;
    const int length = (_size*(_size + 1))/two2;

    for (int k = 0; k < length/two2; ++k) {

        _A[2*k  ] = rhs._A[2*k  ];
        _A[2*k+1] = rhs._A[2*k+1];
    }
        _A[length-1] = rhs._A[length-1];
}

void SymMatrix::_check_size(const int extSize) {

    if (extSize < 1) {

        cout << "Error in SymMatrix:: " << endl;
        cout << "extSize = " << extSize << endl; 
        exit(0);
    }
}

void SymMatrix::_print_maxresize(const int extSize) {

    cout << "SymMatrix::resize() MAXsize changed:" << endl;
    cout << "old  MAX_size = " << _MAX_size << "  ";
    cout << "crnt MAX_size = " << extSize << endl;
}

////////////////////////////////////////////////////////////////////////////

// end of file

