#include <iostream>
#include <cstdlib>
#include "eta_rbtree.h"
#include "eta_index.h"
#include "eta_list.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Eta_rbNode::Eta_rbNode():
left(0), right(0), red(1), linIndex(-1), eta_ind(0) {
}

Eta_rbNode::~Eta_rbNode() {

    delete eta_ind; eta_ind = 0;
    delete left;    left    = 0;
    delete right;   right   = 0;
}

////////////////////////////////////////////////////////////////////////////

Eta_rbTree::Eta_rbTree():_etaLst(0), _head(0),
_linInd(-1){
}

Eta_rbTree::~Eta_rbTree() {

    _etaLst = 0;
    delete _head; _head = 0;
}

void Eta_rbTree::insert(const eta_Index& EtaInd) {

    _RB_insert(_head, EtaInd,0);
    _head->red = 0;
}

int Eta_rbTree::linIndex(const eta_Index& EtaInd) const {
    return (_RB_search(_head, EtaInd))->linIndex;
}

void Eta_rbTree::linearize(Eta_List * const EtaLst) const {

    cout << "Eta_rbTree: Start generate eta_List..";

    _etaLst = EtaLst;
    _RB_traversal(_head);
    _etaLst = 0;

    cout << ". done ok;  etaList->size = " << EtaLst->size() << endl;
}

void Eta_rbTree::_rotR(Eta_rbNode* &h) {

    Eta_rbNode* x = h->left;
    h->left = x->right;
    x->right = h;
    h = x;
}

void Eta_rbTree::_rotL(Eta_rbNode* &h) {

    Eta_rbNode* x = h->right;
    h->right = x->left;
    x->left = h;
    h = x;
}

int Eta_rbTree::_is_red(Eta_rbNode *x) {

    if (0 == x) return 0;
    return x->red;
}

void Eta_rbTree::_RB_insert(Eta_rbNode* &h, const eta_Index& EtaInd, int sw) {

    if (0 == h) {

        h = new Eta_rbNode;
        h->eta_ind = new eta_Index(EtaInd);
        h->linIndex = ++_linInd;
        return;
    }

    if (_is_red(h->left) && _is_red(h->right)) {

        h->red = 1;
        h->left->red  = 0;
        h->right->red = 0;
    }

    if (EtaInd > (*(h->eta_ind)) ) {

        _RB_insert(h->right,EtaInd,1);

        if (_is_red(h) && _is_red(h->right) && !sw) {_rotL(h);}

        if (_is_red(h->right) && _is_red(h->right->right)) {
            _rotL(h);
            h->red = 0;
            h->left->red = 1;
        }
    } else if ( !( EtaInd == (*(h->eta_ind)) ) ) {

        _RB_insert(h->left,EtaInd,0);

        if (_is_red(h) && _is_red(h->left) && sw) {_rotR(h);}

        if (_is_red(h->left) && _is_red(h->left->left)) {
            _rotR(h);
            h->red = 0;
            h->right->red = 1;
        }
    }
}

Eta_rbNode* Eta_rbTree::_RB_search(Eta_rbNode* h, const eta_Index& EtaInd) const {

    if (0 == h) {

        cout << "Error Eta_rbTree::_RB_search(" << endl;
        cout << "No such index: ";
        EtaInd.print();
        exit(0);
    }

    if ( EtaInd == (*(h->eta_ind)) ) {
        return h;
    }

    if (EtaInd > (*(h->eta_ind))) {
        return _RB_search(h->right, EtaInd);
    } else {
        return _RB_search(h->left, EtaInd);
    }
}

void Eta_rbTree::_RB_traversal(Eta_rbNode* h) const {

    if (0 == h) return;

    _etaLst->add(h->eta_ind, h->linIndex);

    _RB_traversal(h->right);
    _RB_traversal(h->left);
}

////////////////////////////////////////////////////////////////////////////

// end of file


