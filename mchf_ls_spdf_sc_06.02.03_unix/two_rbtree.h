#ifndef _TWO_HASH_REDBLACK_TREE_H_
#define _TWO_HASH_REDBLACK_TREE_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class k_abcd_Index;
class ccv_RB_Tree;
class Two_List;


class Two_RB_node : private Uncopyable {

public:

    Two_RB_node *left, *right;
    int red;

    k_abcd_Index * index_k_abcd;
    ccv_RB_Tree  * ccvTree;

    Two_RB_node();
    ~Two_RB_node();
};


class Two_RB_Tree : private Uncopyable {

public:

    Two_RB_Tree();
    ~Two_RB_Tree();

    void generate_TwoList(Two_List * const twoLst) const;
    void add(const k_abcd_Index& twoInd, const int i, const int j,
             const double Val);

    int  size() const {return _size;}

private:

    Two_RB_node *_head;
    int          _size;

    mutable Two_List* _twoLst;

    void _rotR(Two_RB_node* &h);
    void _rotL(Two_RB_node* &h);
    int  _is_red(Two_RB_node *x);

    void _insert(const k_abcd_Index& twoInd); 
    void _RB_insert(Two_RB_node* &h,const k_abcd_Index& insTwoInd, int sw);
    Two_RB_node* _RB_search(Two_RB_node* h, const k_abcd_Index& ptrnTwoInd);

    void _catch_next_node(const Two_RB_node* h) const;
};

////////////////////////////////////////////////////////////////////////////

#endif  // _TWO_HASH_REDBLACK_TREE_H_

// end of file
