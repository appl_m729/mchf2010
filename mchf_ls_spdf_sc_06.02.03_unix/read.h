#ifndef _READ_ATOM_H_
#define _READ_ATOM_H_

// input servise routine
// input data from conf file

int ReadInt( const char* const file_name, const char* const parameter_name,
             int position, int &input_value);

int ReadReal(const char* const file_name, const char* const parameter_name, 
             int position, double &input_value);

int ReadStr( const char* const file_name, const char* const parameter_name, 
             int position, char* input_value);

int ReadIntStr(const char* const file_name, const char* const parameter_name, 
               int position, int &input_value);

const char *Error_read(int nErr);

#endif // _READ_ATOM_H_

