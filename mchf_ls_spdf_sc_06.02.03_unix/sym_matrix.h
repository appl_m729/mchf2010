#ifndef _SYMMETRICAL_MATRIX_H_
#define _SYMMETRICAL_MATRIX_H_

////////////////////////////////////////////////////////////////////////////
  
class SymMatrix {

public:

    SymMatrix();
    SymMatrix(const int extSize);
    SymMatrix(const SymMatrix& rhs);
    ~SymMatrix();

    SymMatrix& operator= (const SymMatrix& rhs);
    double&    operator()(const int i, const int j);

    int  size() const {return _size;}
    void resize(const int ext_Size);

    void print() const;

private:

    int _MAX_size, _size;
    double *_A;


    inline int _ind(const int i, const int j) const;

    void _allocate_A(const int max_size);
    void _copy_rhs_A(const SymMatrix& rhs);
    void _check_size(const int extSize);
    void _print_maxresize(const int extSize);
};

////////////////////////////////////////////////////////////////////////////

#endif  // _SYMMETRICAL_MATRIX_H_

// end of file

