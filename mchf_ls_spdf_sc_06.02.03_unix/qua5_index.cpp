#include <iostream>
#include <cmath>
#include <cstdlib>
#include "qua3_index.h"
#include "qua5_index.h"
#include "aconstants.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

qua5_Index::qua5_Index():
_q3Ind(new qua3_Index), _L(0), _S(0) {
}

qua5_Index::qua5_Index(const int L, const double S):
_q3Ind(new qua3_Index), _L(L), _S(S) {
}

qua5_Index::qua5_Index(const qua3_Index& q3Ind):
_q3Ind(new qua3_Index(q3Ind)), _L(0), _S(0) {
}

qua5_Index::qua5_Index(const qua5_Index& rhs):
_q3Ind(new qua3_Index(*(rhs._q3Ind))), _L(rhs._L), _S(rhs._S) {
}

qua5_Index::~qua5_Index() {
    delete _q3Ind;
}

qua5_Index& qua5_Index::operator= (const qua5_Index& rhs) {

    if (&rhs == this) return *this;

    (*_q3Ind) = *(rhs._q3Ind);
    _L = rhs._L;
    _S = rhs._S;

    return *this;
}

void qua5_Index::reset_LS(const int ext_L, const double ext_S) {

    if (ext_L < 0) {

        cout << "Error in qua5_Index: ext_L = " << ext_L << endl;
        exit(0);
    }

    if (ext_S < -ACCURATE_0) {

        cout << "Error in qua5_Index: ext_S = " << ext_S << endl;
        exit(0);
    }

    _L = ext_L;
    _S = ext_S;
}

void qua5_Index::reset_qua3(const qua3_Index& q3Ind) {
    _q3Ind->reset(q3Ind);
}

bool qua5_Index::is_not_valid() const {

    if ((_q3Ind->ML() > _L) || (_q3Ind->ML() < -_L)) return true;
    if ((_q3Ind->MS() > _S) || (_q3Ind->MS() < -_S)) return true;

    return false;
}

int qua5_Index::ML() const {
    return _q3Ind->ML();
}

double qua5_Index::MS() const {
    return _q3Ind->MS();
}

int qua5_Index::P() const {
    return _q3Ind->P();
}

void qua5_Index::all(int& L, int &ML, double& S, double& MS, int& P) const {

    L  = _L; 
    S  = _S;
    _q3Ind->all(ML,MS,P);
}

void qua5_Index::print() const {

    cout << _L << " " << _q3Ind->ML() << " ";
    cout << _S << " " << _q3Ind->MS() << " " << _q3Ind->P() << endl;
}

// friend
bool operator== (const qua5_Index& lhs, const qua5_Index& rhs) {

    if (lhs._L != rhs._L ) return false;
    if (fabs(lhs._S - rhs._S) > SPIN_THRESHOLD) return false;

    return (lhs.q3() == rhs.q3());
}

// friend
bool operator > (const qua5_Index& lhs, const qua5_Index& rhs) {

    if (lhs._L > rhs._L ) return true;
    if (lhs._L < rhs._L ) return false;

    if (fabs(lhs._S - rhs._S) > SPIN_THRESHOLD) {

        if (lhs._S  > rhs._S ) return true;
        if (lhs._S  < rhs._S ) return false;
    }

    return (lhs.q3() > rhs.q3());
}

////////////////////////////////////////////////////////////////////////////

// end of file


