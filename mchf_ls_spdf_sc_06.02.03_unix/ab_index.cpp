#include <iostream>
#include <cstdlib>
#include "ab_index.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

ab_Index::ab_Index(const int ext_a, const int ext_b):
_a(ext_a), _b(ext_b) {

    if ( _a < 0 || _b < 0 ) {

        cout << "Error in ab_Index::ab_Index(" << endl;
        print();
        exit(0);
    }
    _regulate();
}

ab_Index::ab_Index(const ab_Index& rhs):
_a(rhs._a), _b(rhs._b) {
}

ab_Index::~ab_Index() {
}

ab_Index& ab_Index::operator= (const ab_Index& rhs) {

    if (&rhs == this) return *this;

    _a = rhs._a;
    _b = rhs._b;

    return *this;
}

void ab_Index::reset(const int ext_a, const int ext_b) {

    _a = ext_a; _b = ext_b;

    if ( _a < 0 || _b < 0 ) {

        cout << "Error in ab_Index::reset(" << endl;
        print();
        exit(0);
    }
    _regulate();
}

void ab_Index::print() const {
    cout << "a = " << _a << " b = " << _b << endl;
}

void ab_Index::ab(int& ext_a, int& ext_b) const {
    ext_a = _a;  ext_b = _b;
}

//friend 
bool operator==(const ab_Index& lft, const ab_Index& rht) {

    if (lft._a != rht._a) return false;
    if (lft._b != rht._b) return false;

    return true;
}

//friend 
bool operator > (const ab_Index& lft, const ab_Index& rht) {

    if (lft._a < rht._a) return false;
    if (lft._a > rht._a) return true;

    if (lft._b < rht._b) return false;
    if (lft._b > rht._b) return true;
    // ==             
    return false;
}

void ab_Index::_regulate() {

    if (_a > _b) {

        const int buff = _b; 
        _b = _a;
        _a = buff;
    }
}

////////////////////////////////////////////////////////////////////////////

// end of file

