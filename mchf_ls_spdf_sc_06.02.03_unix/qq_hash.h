#ifndef _QQ_HASH_H_
#define _QQ_HASH_H_

////////////////////////////////////////////////////////////////////////////

#include "uncopyable.h"

class qBas_Conf;
class KLaguerreGrid;
class PLaguerre;
class Pnl_IndexMap;
class s_k_ab_List;
class s_k_ab_Index;
class CompleteCoeffs;


class qqHash : private Uncopyable {

public:

    qqHash(const qBas_Conf* const qbCnf, const Pnl_IndexMap * const pnlInd);
    ~qqHash();
                        
    double QQr (const int n1, const int l, const int n2) const;
    double QQrr(const int n1, const int l, const int n2) const;
    double dQdQ(const int n1, const int l, const int n2) const;

    void prepear_tw(const s_k_ab_List * const tLst, 
                    const s_k_ab_List * const wLst);

    double tau(const int lin_ind_skab,
               const int k1, const int k2, const int i_point) const;

    double wnu(const int lin_ind_skab,
               const int k1, const int k2, const int i_point) const;

private:

    const qBas_Conf     * const _qbCnf;
    const Pnl_IndexMap  * const _pnlInd;
    const KLaguerreGrid * const _kLgr1;
    const KLaguerreGrid * const _kLgr2;
    PLaguerre * const _pL1;
    PLaguerre * const _pL2;
    CompleteCoeffs * const _cc;

    double *_QQr, *_QQrr, *_dQdQ;
    double *_tau, *_wnu;
    double *_kk_buff, *_Kbuff_a_inf, *_Kbuff_0_a;

    int  _lmax, _Nlmax;
    int *_Nmax;


    void   _wt_in_zero(const int sz_t, const int sz_w);
    void   _calc_tau(const int i_r, const int lin_ind, const s_k_ab_Index& skabInd);
    void   _calc_wnu(const int i_r, const int lin_ind, const s_k_ab_Index& skabInd);

    void   _K_0_a(const double alf, const double a, const int l1, const int l2);
    void   _K_a_inf(const double alf, const double a, const int l1, const int l2);
    void   _Kmfa(const double m, const double rr, const int l1, const int l2);

    inline int _ind(const int n1,const int l,const int n2) const;
    
    void   _setup();
    double _factor(const int n, const int l) const;

    void   _calc_QQr (const int l, const int i);
    void   _calc_QQrr(const int l, const int i);
    void   _calc_dQdQ(const int l, const int i);

    void   _calc_2l_0(const int l, const int i);
    void   _calc_2l_1(const int l, const int i);
    void   _calc_2l_2(const int l, const int i);
};

////////////////////////////////////////////////////////////////////////////

#endif  // _QQ_HASH_H_

// end of file

