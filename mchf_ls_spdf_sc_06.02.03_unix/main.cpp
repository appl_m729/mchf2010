#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include "laguerre.h"
#include "krylov_grid.h"
#include "qbas_conf.h"
#include "qq_hash.h"
#include "s_k_ab_index.h"
#include "k_ab_index.h"
#include "s_k_ab_list.h"
#include "s_k_ab_rbtree.h"
#include "pnl_radial.h"
#include "atom_conf.h"
#include "pnl_index_map.h"
#include "eta_rbtree.h"
#include "energy_hash.h"
#include "csf_basis.h"
#include "one_list.h"
#include "two_list.h"
#include "variate_hash.h"
#include "eta_hash.h"
#include "ksi_hash.h"
#include "s_k_ab_rbtree.h"
#include "s_k_ab_list.h"
#include "eta_list.h"
#include "pnl_buffer.h"
#include "eta_index.h"
#include "ab_index.h"
#include "vec_eq_conf.h"
#include "ham_matrix.h"
#include "vec_eq_syst.h"
#include "mchf_ls.h"
#include "mdiis_engine.h"
#include "wflags.h"
#include "grid_r.h"
#include "qr_decomp.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

int main(int argc,char* argv[]) {

    if (argc == 2) {

        MCHF_Solver * const mchf_Solver = new MCHF_Solver(argv[1]);

        mchf_Solver->solve();
        mchf_Solver->mesureTime();

        delete mchf_Solver;

    } else {
    
        cout << "Error start mchf_Solver. Incorrect input." << endl;
        cout << "Correct example: file1 file2.cnf" << endl;
        cout << "file1 - main executable file," << endl;
        cout << "file2 - input configuration file." << endl;
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////

// end of file

/*
    ifstream fin(argv[1]);
    char ch;

    int i = 0;
    int star = -1;
    int j = 0;
    bool ins_flg = true;

    while (fin.get(ch)) {

        if (ch == '\n' && i == 0) continue;

        if (ins_flg) {
            cout << "    _A[pos + " << setw(3) << j++ << "] = ";
            ins_flg = false;
        } 

        ++i;

        if (ch == '\n') {

            cout << ";";// << i;
            i = 0;
            ins_flg = true;
        } 

        if (ch == '*') {        
            star = 4;
            cout << 'e';
        }

        if (star > 0) {        
            --star;
            continue;
        }

        cout << ch;
    }

    fin.close();
    exit(0);
*/
