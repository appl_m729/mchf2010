#include <iostream>
#include <cmath>
#include "energy_hash.h"
#include "coeffs.h"
#include "k_abcd_index.h"
#include "ab_index.h"
#include "one_rbtree.h"
#include "two_rbtree.h"
#include "csf_basis.h"
#include "nlmlms_state.h"
#include "csf_ls.h"
#include "det_state.h"
#include "shells_det.h"
#include "pnl_index_map.h"
#include "atom_conf.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Energy_Hash::Energy_Hash(const Atom_Conf * const aCnf,
                         const CSF_Basis * const CSFBas,
                         const Pnl_IndexMap * const pnlInd):

_csfBas(CSFBas), _pnlInd(pnlInd),
_shells(new Shells_Det(aCnf)),
_cft(new CompleteCoeffs), _indOne(new ab_Index(0,0)),
_indTwo(new k_abcd_Index(0,0,0,0,0)),
_oneTree(new One_RB_Tree), _twoTree(new Two_RB_Tree) {

    cout << "Energy_Hash: start collecting dets..";
    _setup();
    cout << ". done ok" << endl;
    cout << "I_ab size = " << _oneTree->size() << endl;
    cout << "Rk_abcd size = " << _twoTree->size() << endl;
}

Energy_Hash::~Energy_Hash() {

    delete _shells;
    delete _twoTree;
    delete _oneTree;
    delete _indTwo;
    delete _indOne;
    delete _cft;
}

void Energy_Hash::generate_One(One_List * const ext_oneLst) const {
    _oneTree->generate_OneList(ext_oneLst);
}

void Energy_Hash::generate_Two(Two_List * const ext_twoLst) const {
    _twoTree->generate_TwoList(ext_twoLst);
}

void Energy_Hash::_setup() {

    int i = 0;
    _csfBas->to_head_first();
    while (_csfBas->next_first()) {
        
        int j = i;
        _csfBas->to_first_item();
        while(_csfBas->next_secnd()) {

            for (int ki = 0; ki < _csfBas->csf_first().size(); ++ki) {
                for (int sj = 0; sj < _csfBas->csf_secnd().size(); ++sj) {

                    int pos1ki, pos1sj, pos2ki, pos2sj;
                    const int key = dfr_states(_csfBas->csf_first().det(ki),
                                               _csfBas->csf_secnd().det(sj),
                                                pos1ki, pos1sj, pos2ki, pos2sj);
                    if (0 == key) {

                        _collect_f_0(i,j,ki,sj);
                        _collect_g_0(i,j,ki,sj);

                    } else if (1 == key) {

                        _collect_f_1(i,j,ki,sj,pos1ki,pos1sj);
                        _collect_g_1(i,j,ki,sj,pos1ki,pos1sj);

                    } else if (2 == key) {

                        _collect_g_2(i,j,ki,sj,pos1ki,pos1sj,pos2ki,pos2sj);
                    } // if (key)
                } // for sj
            } // for ki
            ++j;  
        } // while j
        ++i;
    } // while i
}

void Energy_Hash::_collect_f_0(const int i, const int j, const int ki, const int sj) {

    _shells->reset(_csfBas->csf_first().det(ki));

    {for (int k = 0; k < _shells->length(); ++k) {

        const int a = _pnlInd->a(_shells->elst(k).n(),_shells->elst(k).l());
        _indOne->reset(a,a);

        const double Aki = _csfBas->csf_first().A(ki);
        const double Asj = _csfBas->csf_secnd().A(sj);

        double t_fact = 2.0;
        if (i == j) t_fact = 1.0;

        _oneTree->add(*_indOne, i,j, Aki*Asj*t_fact);
    }}
}

void Energy_Hash::_collect_f_1(const int i, const int j, const int ki, const int sj,
                               const int pos1ki, const int pos1sj) {
                      
    const nlmlms_State& el1 = _csfBas->csf_first().det(ki).elst(pos1ki);
    const nlmlms_State& el2 = _csfBas->csf_secnd().det(sj).elst(pos1sj);

    if (el1.l() != el2.l() || el1.ml() != el2.ml() || el1.ms() != el2.ms()) return;

    _indOne->reset(_pnlInd->a(el1.n(),el1.l()), _pnlInd->a(el2.n(),el2.l()));

    double t_fact = 2.0;
    if (i == j) t_fact = 1.0;

    const double AkiAsj = _csfBas->csf_first().A(ki)*_csfBas->csf_secnd().A(sj);

    _oneTree->add(*_indOne, i,j, AkiAsj*t_fact*_sign_one(pos1ki,pos1sj));
}

void Energy_Hash::_collect_g_0(const int i, const int j, const int ki, const int sj) {

    _shells->reset(_csfBas->csf_first().det(ki));

    for (int k1 = 0; k1 < _shells->length()-1; ++k1) {
    for (int k2 = k1+1; k2 < _shells->length(); ++k2) {

        _add_two_any( 1,i,j,ki,sj, 
                        _shells->elst(k1),_shells->elst(k2),
                        _shells->elst(k1),_shells->elst(k2));

        _add_two_any(-1,i,j,ki,sj, 
                        _shells->elst(k1),_shells->elst(k2),
                        _shells->elst(k2),_shells->elst(k1));
    }}
}

void Energy_Hash::_collect_g_1(const int i, const int j, const int ki, const int sj,
                               const int pos1ki, const int pos1sj) {

    _shells->reset(_csfBas->csf_first().det(ki));
    _shells->dislodge(pos1ki);

    const nlmlms_State el_a = _csfBas->csf_first().det(ki).elst(pos1ki);
    const nlmlms_State el_b = _csfBas->csf_secnd().det(sj).elst(pos1sj);

    const int t_ss = _sign_one(pos1ki,pos1sj);

    for (int k1 = 0; k1 < _shells->length(); ++k1) {
   
        _add_two_any( t_ss,i,j,ki,sj, _shells->elst(k1),el_a,
                                      _shells->elst(k1),el_b);

        _add_two_any(-t_ss,i,j,ki,sj, _shells->elst(k1),el_a,
                                      el_b,_shells->elst(k1));
    }
}

void Energy_Hash::_collect_g_2(const int i, const int j, const int ki, const int sj,
                               const int pos1ki, const int pos1sj, 
                               const int pos2ki, const int pos2sj) {                      

    const nlmlms_State e1 = _csfBas->csf_first().det(ki).elst(pos1ki);
    const nlmlms_State e2 = _csfBas->csf_first().det(ki).elst(pos2ki);
    const nlmlms_State e3 = _csfBas->csf_secnd().det(sj).elst(pos1sj);
    const nlmlms_State e4 = _csfBas->csf_secnd().det(sj).elst(pos2sj);

    const int t_ss = _sign_two(pos1ki,pos1sj,pos2ki,pos2sj);

    _add_two_any( t_ss,i,j,ki,sj, e1,e2,e3,e4);
    _add_two_any(-t_ss,i,j,ki,sj, e1,e2,e4,e3);
}

int Energy_Hash::_sign_one(const int pos1ki, const int pos1sj) const {

    const int sum = pos1ki + pos1sj;
    if ( 0 == (sum % 2)) return 1;
    return -1;
}

int Energy_Hash::_sign_two(const int pos1ki, const int pos1sj,
                           const int pos2ki, const int pos2sj) const {

    const int sum = pos1ki + pos1sj + pos2ki + pos2sj;
    if ( 0 == (sum % 2) ) return 1;
    return -1;                   
}

void Energy_Hash::_add_two_any(const double fctr_sign,
                               const int i, const int j, const int ki, const int sj,
                               const nlmlms_State& e1, const nlmlms_State& e2,
                               const nlmlms_State& e3, const nlmlms_State& e4) {

    if (e1.ms() != e3.ms()) return;
    if (e2.ms() != e4.ms()) return;
    if ((e1.ml() + e2.ml()) != (e3.ml() + e4.ml())) return;

    const int dif13 = _int_abs(e1.l() - e3.l());
    const int dif24 = _int_abs(e2.l() - e4.l());
    const int sum13 = e1.l() + e3.l();
    const int sum24 = e2.l() + e4.l();

    if ((sum13 % 2) != (sum24 % 2)) return;

    const int k_min = _max(dif13,dif24);
    const int k_max = _min(sum13,sum24);

    const int a = _pnlInd->a(e1.n(),e1.l());
    const int b = _pnlInd->a(e2.n(),e2.l());
    const int c = _pnlInd->a(e3.n(),e3.l());
    const int d = _pnlInd->a(e4.n(),e4.l());

    _indTwo->reset_abcd(a,b,c,d);

    double t_fact = 2.0;
    if (i == j) t_fact = 1.0;

    for (int k = k_min; k <= k_max; k += 2) {

        _indTwo->reset_k(k);

        const double gnt13 = _cft->gonth(k, e1.l(),e1.ml(), e3.l(),e3.ml());
        const double gnt42 = _cft->gonth(k, e4.l(),e4.ml(), e2.l(),e2.ml());

        const double rezlt = t_fact*fctr_sign*gnt13*gnt42;
        const double AkiAsj = _csfBas->csf_first().A(ki)*_csfBas->csf_secnd().A(sj);

        _twoTree->add(*_indTwo, i,j, AkiAsj*rezlt);
    }
}

int Energy_Hash::_max(const int a, const int b) const {

    if (a < b) return b;
    return a;
}

int Energy_Hash::_min(const int a, const int b) const {

    if (a > b) return b;
    return a;
}

int Energy_Hash::_int_abs(const int a) const {

    if (a < 0) return -a;
    return a;
}

////////////////////////////////////////////////////////////////////////////

// end of file

