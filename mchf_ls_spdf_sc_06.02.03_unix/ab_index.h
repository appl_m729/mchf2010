#ifndef _AB_INDEX_H_
#define _AB_INDEX_H_

////////////////////////////////////////////////////////////////////////////

class ab_Index {

public:

    ab_Index(const int ext_a, const int ext_b);
    ab_Index(const ab_Index& rhs);
    ~ab_Index();

    ab_Index& operator= (const ab_Index& rhs);

    void reset(const int ext_a, const int ext_b);
    void print() const;

    int a() const {return _a;}
    int b() const {return _b;}
    void ab(int& ext_a, int& ext_b) const;

    friend bool operator==(const ab_Index& lhs, const ab_Index& rhs);
    friend bool operator> (const ab_Index& lhs, const ab_Index& rhs);

private:

    int _a,_b;

    void _regulate();
};

////////////////////////////////////////////////////////////////////////////

#endif  // _AB_INDEX_H_

// end of file

