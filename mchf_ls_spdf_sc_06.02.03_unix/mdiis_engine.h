#ifndef _MDIIS_ENGINE_H_
#define _MDIIS_ENGINE_H_

////////////////////////////////////////////////////////////////////////////

class QR_Decompose;
class VecEq_Conf;


class mDIIS_Engine {

public:

    mDIIS_Engine(const int max_depth, const VecEq_Conf * const veqCnf);
    ~mDIIS_Engine();

    void add(const double * const x, const double * const dx);
    void produce(double * const x_new);

private:

    const VecEq_Conf   * const _veqCnf;
    QR_Decompose       * const _qrDecomp;

    const int _max_Depth;
    int _crnt_Depth;

    int    *_ind;
    double *_x, *_dx, *_dxdx;
    double *_c;

    void _shift_indexes();
    void _put_x_dq_in_array(const double * const x_ext, const double * const dq);
    void _calc_new_dxidxj_values();
    void _setup_qr();
    void _calc_ck();
    void _calc_new_x(double * const x) const;
};

////////////////////////////////////////////////////////////////////////////

#endif // _MDIIS_ENGINE_H_

// end of file
