The Multiconfiguration Hartree-Fock code (MCHF) consists of two parts.

The first one constructs many-electron basis
set by means of the direct diagonalization approach
(based on the technique of ladder angular and spin momentum operators).
It is implemented as a universal computational
scheme for arbitrary electron configurations.
M.S. Litsarev, O.V. Ivanov. Multiconfiguration Hartree-Fock
method: Direct diagonalization for the construction of a multielectron basis.
Journal of Experimental and Theoretical Physics 111(1), 22 (2010).

The second part solves multiconfiguration Hartree-Fock equations
reducing them to a system of matrix-vector equations
within analytical basis set expansion
of the radial parts of one-electron spin-orbitals.
The construction and solving of the matrix-vector equations
is done automatically as a stable numerical scheme.
[2] M.S.Litsarev, O.V.Ivanov. Systemofmatrix-vector
equations in the multiconfiguration Hartree-Fock method.
Bulletin of the Lebedev Physics Institute 37(9), 284 (2010).
